﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// CollisionからヒットしたRigidbodyを出力する。
	/// </summary>
#else
	/// <summary>
	/// Output Rigidbody hit from Collision.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Collision/Collision.Rigidbody")]
	[BuiltInCalculator]
	public class CollisionRigidbodyCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// Collision
		/// </summary>
		[SerializeField] private InputSlotCollision _Collision;

#if ARBOR_DOC_JA
		/// <summary>
		/// ヒットしたRigidbody。
		/// </summary>
#else
		/// <summary>
		/// Hit the Rigidbody
		/// </summary>
#endif
		[SerializeField] private OutputSlotRigidbody _Rigidbody;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			Collision collision = null;
			_Collision.GetValue(ref collision);
            if (collision != null )
			{
				_Rigidbody.SetValue(collision.rigidbody);
            }
		}
	}
}
