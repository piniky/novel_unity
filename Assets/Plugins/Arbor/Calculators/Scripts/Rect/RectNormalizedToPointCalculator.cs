﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 正規化座標を指定して長方形内部の位置を計算する。
	/// </summary>
#else
	/// <summary>
	/// Calculates a point inside a rectangle, given normalized coordinates.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Rect/Rect.NormalizedToPoint")]
	[BuiltInCalculator]
	public class RectNormalizedToPointCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// Rect
		/// </summary>
		[SerializeField] private FlexibleRect _Rect;

#if ARBOR_DOC_JA
		/// <summary>
		/// ポイントを取得するための正規化座標
		/// </summary>
#else
		/// <summary>
		/// Normalized coordinates to get a point for.
		/// </summary>
#endif
		[SerializeField] private FlexibleVector2 _NormalizedRectCoordinates;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Result output
		/// </summary>
#endif
		[SerializeField] private OutputSlotVector2 _Result;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			_Result.SetValue(Rect.NormalizedToPoint(_Rect.value, _NormalizedRectCoordinates.value));
        }
	}
}
