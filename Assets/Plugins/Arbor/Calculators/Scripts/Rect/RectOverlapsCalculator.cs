﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 矩形同士が重なっているかどうか。
	/// </summary>
#else
	/// <summary>
	/// Whether rectangles overlap each other or not.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Rect/Rect.Overlaps")]
	[BuiltInCalculator]
	public class RectOverlapsCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// Rect
		/// </summary>
		[SerializeField] private FlexibleRect _Rect;

#if ARBOR_DOC_JA
		/// <summary>
		/// 重なっているか確認するRect
		/// </summary>
#else
		/// <summary>
		/// Check whether overlap Rect
		/// </summary>
#endif
		[SerializeField] private FlexibleRect _Other;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Result output
		/// </summary>
#endif
		[SerializeField] private OutputSlotBool _Result;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			_Result.SetValue(_Rect.value.Overlaps(_Other.value));
        }
	}
}
