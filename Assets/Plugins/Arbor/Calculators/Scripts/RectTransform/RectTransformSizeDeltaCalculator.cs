﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// アンカー間の距離と比較した RectTransform のサイズ。
	/// </summary>
#else
	/// <summary>
	/// The size of this RectTransform relative to the distances between the anchors.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("RectTransform/RectTransform.SizeDelta")]
	[BuiltInCalculator]
	public class RectTransformSizeDeltaCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// RectTransform
		/// </summary>
		[SerializeField] private FlexibleRectTransform _RectTransform;

#if ARBOR_DOC_JA
		/// <summary>
		/// サイズ
		/// </summary>
#else
		/// <summary>
		/// Size
		/// </summary>
#endif
		[SerializeField] private OutputSlotVector2 _SizeDelta;

		#endregion // Serialize fields

		public override bool OnCheckDirty()
		{
			return true;
		}

		// Use this for calculate
		public override void OnCalculate()
		{
			RectTransform rectTransform = _RectTransform.value;
			if (rectTransform != null)
			{
				_SizeDelta.SetValue(rectTransform.sizeDelta);
			}
		}
	}
}
