﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Collision2DからヒットしたGameObjectを出力する。
	/// </summary>
#else
	/// <summary>
	/// Output GameObject hit from Collision2D.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Collision2D/Collision2D.GameObject")]
	[BuiltInCalculator]
	public class Collision2DGameObjectCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// Collision2D
		/// </summary>
		[SerializeField] private InputSlotCollision2D _Collision2D;

#if ARBOR_DOC_JA
		/// <summary>
		/// ヒットしたGameObject。
		/// </summary>
#else
		/// <summary>
		/// GameObject that hit.
		/// </summary>
#endif
		[SerializeField] private OutputSlotGameObject _GameObject;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			Collision2D collision2D = null;
			_Collision2D.GetValue(ref collision2D);
            if (collision2D != null )
			{
				_GameObject.SetValue(collision2D.gameObject);
            }
		}
	}
}
