﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// GameObjectにアタッチされているTransformを取得する。
	/// </summary>
#else
	/// <summary>
	/// Gets the Transform attached to GameObject.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Transform/Transform.Get")]
	[BuiltInCalculator]
	public class TransformGetCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// GameObject
		/// </summary>
		[SerializeField] private FlexibleGameObject _GameObject;

#if ARBOR_DOC_JA
		/// <summary>
		/// 取得したTransform
		/// </summary>
#else
		/// <summary>
		/// Get the Transform
		/// </summary>
#endif
		[SerializeField] private OutputSlotTransform _Transform;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			GameObject gameObject = _GameObject.value;
			if (gameObject != null)
			{
				_Transform.SetValue(gameObject.GetComponent<Transform>());
            }
		}
	}
}
