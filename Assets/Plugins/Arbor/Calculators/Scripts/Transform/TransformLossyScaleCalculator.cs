﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// オブジェクトのグローバルスケール
	/// </summary>
#else
	/// <summary>
	/// The global scale of the object
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Transform/Transform.LossyScale")]
	[BuiltInCalculator]
	public class TransformLossyScaleCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// Transform
		/// </summary>
		[SerializeField] private FlexibleTransform _Transform;

#if ARBOR_DOC_JA
		/// <summary>
		/// オブジェクトのグローバルスケール
		/// </summary>
#else
		/// <summary>
		/// The global scale of the object
		/// </summary>
#endif
		[SerializeField] private OutputSlotVector3 _LossyScale;

		#endregion // Serialize fields

		public override bool OnCheckDirty()
		{
			return true;
		}

		// Use this for calculate
		public override void OnCalculate()
		{
			Transform transform = _Transform.value;
			if (transform != null)
			{
				_LossyScale.SetValue(transform.lossyScale);
            }
		}
	}
}
