﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 値にもっとも近い2のべき乗値を計算する。
	/// </summary>
#else
	/// <summary>
	/// Outputs the closest power of two value.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Mathf/Mathf.ClosestPowerOfTwo")]
	[BuiltInCalculator]
	public class MathfClosestPowerOfTwoCalculator : Calculator
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 値
		/// </summary>
#else
		/// <summary>
		/// Value
		/// </summary>
#endif
		[SerializeField] private FlexibleInt _Value;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Result output
		/// </summary>
#endif
		[SerializeField] private OutputSlotInt _Result;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			_Result.SetValue(Mathf.ClosestPowerOfTwo(_Value.value));
		}
	}
}
