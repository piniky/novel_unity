﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// リニアからガンマ (sRGB)の色空間へ値を変換する。
	/// </summary>
#else
	/// <summary>
	/// Converts the given value from linear to gamma (sRGB) color space.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Mathf/Mathf.LinearToGammaSpace")]
	[BuiltInCalculator]
	public class MathfLinearToGammaSpaceCalculator : Calculator
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 値
		/// </summary>
#else
		/// <summary>
		/// Value
		/// </summary>
#endif
		[SerializeField] private FlexibleFloat _Value;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Result output
		/// </summary>
#endif
		[SerializeField] private OutputSlotFloat _Result;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			_Result.SetValue(Mathf.LinearToGammaSpace(_Value.value));
		}
	}
}
