﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 2Dのパーリンノイズを生成する。
	/// </summary>
#else
	/// <summary>
	/// Generate 2D Perlin noise.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Mathf/Mathf.PerlinNoise")]
	[BuiltInCalculator]
	public class MathfPerlinNoiseCalculator : Calculator
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// サンプル点のX座標。
		/// </summary>
#else
		/// <summary>
		/// X-coordinate of sample point.
		/// </summary>
#endif
		[SerializeField] private FlexibleFloat _X;

#if ARBOR_DOC_JA
		/// <summary>
		/// サンプル点のY座標。
		/// </summary>
#else
		/// <summary>
		/// Y-coordinate of sample point.
		/// </summary>
#endif
		[SerializeField] private FlexibleFloat _Y;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Result output
		/// </summary>
#endif
		[SerializeField] private OutputSlotFloat _Result;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			_Result.SetValue(Mathf.PerlinNoise(_X.value, _Y.value));
		}
	}
}
