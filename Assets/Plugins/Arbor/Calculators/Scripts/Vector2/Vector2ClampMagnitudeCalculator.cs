﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 大きさを MaxLength に制限したベクトルを計算する。
	/// </summary>
#else
	/// <summary>
	/// Calculates a vector with its magnitude clamped to MaxLength.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Vector2/Vector2.ClampMagnitude")]
	[BuiltInCalculator]
	public class Vector2ClampMagnitudeCalculator : Calculator
	{
        #region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// ベクトル
		/// </summary>
#else
        /// <summary>
        /// Vector
        /// </summary>
#endif
        [SerializeField] private FlexibleVector2 _Vector2;

#if ARBOR_DOC_JA
		/// <summary>
		/// 最大の長さ
		/// </summary>
#else
		/// <summary>
		/// Max length
		/// </summary>
#endif
		[SerializeField] private FlexibleFloat _MaxLength;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Output result
		/// </summary>
#endif
		[SerializeField] private OutputSlotVector2 _Result;

        #endregion // Serialize fields

        public override void OnCalculate()
		{
			_Result.SetValue( Vector2.ClampMagnitude(_Vector2.value, _MaxLength.value) );
		}
	}
}
