﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// intをfloatに変換する。
	/// </summary>
#else
	/// <summary>
	/// Convert int to float.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Int/Int.ToFloat")]
	[BuiltInCalculator]
	public class IntToFloatCalculator : Calculator
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 値
		/// </summary>
#else
		/// <summary>
		/// Value
		/// </summary>
#endif
		[SerializeField] FlexibleInt _Value;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Result output
		/// </summary>
#endif
		[SerializeField] OutputSlotFloat _Result;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			_Result.SetValue((float)_Value.value);
        }
	}
}
