﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// intを比較する。
	/// </summary>
#else
	/// <summary>
	/// Compare int.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Int/Int.Compare")]
	[BuiltInCalculator]
	public class IntCompareCalculator : Calculator
	{
		public enum CompareType
		{
			Equals,
			NotEquals,
            Greater,
			GreaterOrEquals,
			Less,
			LessOrEquals,
		}

		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 比較タイプ。
		/// <table class="docutils">
		/// <tr><td>Equals</td><td>値と等しい</td></tr>
		/// <tr><td>NotEquals</td><td>値と等しくない</td></tr>
		/// <tr><td>Greater</td><td>値よりも大きい</td></tr>
		/// <tr><td>GreaterOrEquals</td><td>値よりも大きいか等しい</td></tr>
		/// <tr><td>Less</td><td>値よりも小さい</td></tr>
		/// <tr><td>LessOrEquals</td><td>値よりも小さいか等しい</td></tr>
		/// </table>
		/// </summary>
#else
		/// <summary>
		/// Comparison type.
		/// <table class="docutils">
		/// <tr><td>Equals</td><td>Equal to value</td></tr>
		/// <tr><td>NotEquals</td><td>Not equal to value</td></tr>
		/// <tr><td>Greater</td><td>Greater than value</td></tr>
		/// <tr><td>GreaterOrEquals</td><td>Greater than or equal to value</td></tr>
		/// <tr><td>Less</td><td>Less than the value</td></tr>
		/// <tr><td>LessOrEquals</td><td>Less than or equal to value</td></tr>
		/// </table>
		/// </summary>
#endif
		[SerializeField] private CompareType _CompareType;

#if ARBOR_DOC_JA
		/// <summary>
		/// 値1
		/// </summary>
#else
		/// <summary>
		/// Value 1
		/// </summary>
#endif
		[SerializeField] private FlexibleInt _Value1;

#if ARBOR_DOC_JA
		/// <summary>
		/// 値2
		/// </summary>
#else
		/// <summary>
		/// Value 2
		/// </summary>
#endif
		[SerializeField] private FlexibleInt _Value2;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Result output
		/// </summary>
#endif
		[SerializeField] private OutputSlotBool _Result;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			bool result = false;
			switch (_CompareType)
			{
				case CompareType.Equals:
					result = _Value1.value == _Value2.value;
					break;
				case CompareType.NotEquals:
					result = _Value1.value != _Value2.value;
					break;
				case CompareType.Greater:
					result = _Value1.value > _Value2.value;
					break;
				case CompareType.GreaterOrEquals:
					result = _Value1.value >= _Value2.value;
					break;
				case CompareType.Less:
					result = _Value1.value < _Value2.value;
					break;
				case CompareType.LessOrEquals:
					result = _Value1.value <= _Value2.value;
					break;
			}
			_Result.SetValue(result);
		}
	}
}
