﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Rigdibody の回転
	/// </summary>
#else
	/// <summary>
	/// The rotation of the rigidbody.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Rigidbody2D/Rigidbody2D.Rotation")]
	[BuiltInCalculator]
	public class Rigidbody2DRotationCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// Rigidbody2D
		/// </summary>
		[SerializeField] private FlexibleRigidbody2D _Rigidbody2D;

#if ARBOR_DOC_JA
		/// <summary>
		/// Rigdibody の回転
		/// </summary>
#else
		/// <summary>
		/// The rotation of the rigidbody.
		/// </summary>
#endif
		[SerializeField] private OutputSlotFloat _Rotation;

		#endregion // Serialize fields

		public override bool OnCheckDirty()
		{
			return true;
		}

		// Use this for calculate
		public override void OnCalculate()
		{
			Rigidbody2D rigidbody2D = _Rigidbody2D.value;
			if (rigidbody2D != null)
			{
				_Rotation.SetValue(rigidbody2D.rotation);
            }
		}
	}
}
