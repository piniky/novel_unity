﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Boundsを作成する。
	/// </summary>
#else
	/// <summary>
	/// Compose Bounds.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Bounds/Bounds.Compose")]
	[BuiltInCalculator]
	public class BoundsComposeCalculator : Calculator
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 中心
		/// </summary>
#else
		/// <summary>
		/// Center
		/// </summary>
#endif
		[SerializeField] private FlexibleVector3 _Center;

#if ARBOR_DOC_JA
		/// <summary>
		/// サイズ
		/// </summary>
#else
		/// <summary>
		/// Size
		/// </summary>
#endif
		[SerializeField] private FlexibleVector3 _Size;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Result output
		/// </summary>
#endif
		[SerializeField] private OutputSlotBounds _Result;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			_Result.SetValue(new Bounds(_Center.value, _Size.value));
		}
	}
}
