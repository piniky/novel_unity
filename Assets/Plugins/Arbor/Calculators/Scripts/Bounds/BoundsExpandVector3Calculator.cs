﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 設定した amount によってサイズを大きくする。
	/// </summary>
#else
	/// <summary>
	/// Expand the bounds by increasing its size by amount along each side.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("Bounds/Bounds.ExpandVector3")]
	[BuiltInCalculator]
	public class BoundsExpandVector3Calculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// Bounds
		/// </summary>
		[SerializeField] private FlexibleBounds _Bounds;

#if ARBOR_DOC_JA
		/// <summary>
		/// 拡大する量
		/// </summary>
#else
		/// <summary>
		/// Amount to expand
		/// </summary>
#endif
		[SerializeField] private FlexibleVector3 _Amount;

#if ARBOR_DOC_JA
		/// <summary>
		/// 結果出力
		/// </summary>
#else
		/// <summary>
		/// Result output
		/// </summary>
#endif
		[SerializeField] private OutputSlotBounds _Result;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			Bounds bounds = _Bounds.value;
			bounds.Expand(_Amount.value);
			_Result.SetValue(bounds);
		}
	}
}
