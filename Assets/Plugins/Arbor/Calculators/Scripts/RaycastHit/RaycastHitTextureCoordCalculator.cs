﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 衝突した UV テクスチャの座標
	/// </summary>
#else
	/// <summary>
	/// The uv texture coordinate at the collision location.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("RaycastHit/RaycastHit.TextureCoord")]
	[BuiltInCalculator]
	public class RaycastHitTextureCoordCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// RaycastHit
		/// </summary>
		[SerializeField] private InputSlotRaycastHit _RaycastHit;

#if ARBOR_DOC_JA
		/// <summary>
		/// 当たったUV座標を出力
		/// </summary>
#else
		/// <summary>
		/// Output the hit UV coordinate
		/// </summary>
#endif
		[SerializeField] private OutputSlotVector2 _TextureCoord;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			RaycastHit raycastHit = new RaycastHit();
			if (_RaycastHit.GetValue(ref raycastHit) )
			{
				_TextureCoord.SetValue(raycastHit.textureCoord);
            }
        }
	}
}
