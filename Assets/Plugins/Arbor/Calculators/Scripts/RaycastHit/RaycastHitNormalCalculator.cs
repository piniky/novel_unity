﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 衝突した際の面の法線
	/// </summary>
#else
	/// <summary>
	/// The normal of the surface the ray hit.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddCalculatorMenu("RaycastHit/RaycastHit.Normal")]
	[BuiltInCalculator]
	public class RaycastHitNormalCalculator : Calculator
	{
		#region Serialize fields

		/// <summary>
		/// RaycastHit
		/// </summary>
		[SerializeField] private InputSlotRaycastHit _RaycastHit;

#if ARBOR_DOC_JA
		/// <summary>
		/// 法線を出力
		/// </summary>
#else
		/// <summary>
		/// Output the normal
		/// </summary>
#endif
		[SerializeField] private OutputSlotVector3 _Normal;

		#endregion // Serialize fields

		// Use this for calculate
		public override void OnCalculate()
		{
			RaycastHit raycastHit = new RaycastHit();
			if (_RaycastHit.GetValue(ref raycastHit) )
			{
				_Normal.SetValue(raycastHit.normal);
            }
        }
	}
}
