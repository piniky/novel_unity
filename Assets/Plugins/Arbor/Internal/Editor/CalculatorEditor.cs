﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

using Arbor;

namespace ArborEditor
{
	[CustomNodeEditor(typeof(CalculatorNode))]
	public sealed class CalculatorEditor : NodeEditor
	{
		static CalculatorEditor()
		{
			ArborSettings.onChangedLanguage += OnChanedLanguage;
		}

		public class CalculatorInfo
		{
			public string helpUrl;
			public string helpTooltip;
			public GUIContent titleContent;
		}

		static CalculatorInfo _InvalidCalculatorInfo = null;
		static Dictionary<System.Type, CalculatorInfo> _CalculatorInfos = new Dictionary<System.Type, CalculatorInfo>();

		static void OnChanedLanguage()
		{
			_CalculatorInfos.Clear();
		}

		public static CalculatorInfo GetCalculatorInfo(Object calculatorObj)
		{
			if (ComponentUtility.IsValidObject(calculatorObj))
			{
				System.Type classType = calculatorObj.GetType();
				return GetCalculatorInfo(classType);
			}

			if (_InvalidCalculatorInfo == null)
			{
				_InvalidCalculatorInfo = new CalculatorInfo();
				_InvalidCalculatorInfo.titleContent = EditorGUITools.GetTextContent("Missing");
			}
			return _InvalidCalculatorInfo;
		}

		public static CalculatorInfo GetCalculatorInfo(System.Type classType)
		{
			CalculatorInfo calculatorInfo;
			if (!_CalculatorInfos.TryGetValue(classType, out calculatorInfo))
			{
				string classTypeName = classType.Name;

				string docURL = ArborReferenceUtility.docUrl;

				CalculatorHelp help = AttributeHelper.GetAttribute<CalculatorHelp>(classType);

				calculatorInfo = new CalculatorInfo();
				if (help != null)
				{
					calculatorInfo.helpUrl = help.url;
					calculatorInfo.helpTooltip = string.Format("Open Reference for {0}.", classTypeName);
				}
				else if (AttributeHelper.HasAttribute<BuiltInCalculator>(classType))
				{
					calculatorInfo.helpUrl = docURL + "calculators/" + classTypeName.ToLower() + ".html";

					calculatorInfo.helpTooltip = string.Format("Open Reference for {0}.", classTypeName);
				}
				else
				{
					calculatorInfo.helpUrl = string.Empty;
					calculatorInfo.helpTooltip = "Open Arbor Document";
				}

				CalculatorTitle calculatorTitle = AttributeHelper.GetAttribute<CalculatorTitle>(classType);
				if (calculatorInfo.titleContent == null)
				{
					calculatorInfo.titleContent = new GUIContent();
				}
				if (calculatorTitle != null)
				{
					calculatorInfo.titleContent.text = EditorGUITools.NicifyVariableName(calculatorTitle.titleName);
				}
				else
				{
					calculatorInfo.titleContent.text = EditorGUITools.NicifyVariableName(classTypeName);
				}
				calculatorInfo.titleContent.image = Icons.calculatorNodeIcon;

				_CalculatorInfos.Add(classType, calculatorInfo);
			}

			return calculatorInfo;
		}

		public CalculatorNode calculatorNode
		{
			get
			{
				return node as CalculatorNode;
			}
		}

		private Editor _Editor;
		public Editor editor
		{
			get
			{
				return _Editor;
			}
		}

		public override void Validate(Node node)
		{
			base.Validate(node);

			if (_Editor != null && _Editor.target == null)
			{
				DestroyImmediate(_Editor);
				_Editor = null;
			}

			if (_Editor == null && calculatorNode != null)
			{
				_Editor = Editor.CreateEditor(calculatorNode.calculator);
			}
		}

		public CalculatorInfo GetCalculatorInfo()
		{
			return GetCalculatorInfo(calculatorNode.GetObject());
		}

		public override GUIContent GetTitleContent()
		{
			CalculatorInfo calculatorInfo = GetCalculatorInfo();
			return calculatorInfo.titleContent;
		}

		public override GUIStyle GetStyle()
		{
			return Styles.GetNodeStyle(Styles.Color.Blue, isSelection);
		}

		protected override void OnInitialize()
		{
			CalculatorNode calculatorNode = this.calculatorNode;

			_Editor = Editor.CreateEditor(calculatorNode.calculator);
		}

		private static readonly int s_CalculatorTitlebarHash = "s_CalculatorTitlebarHash".GetHashCode();

		void ChangeShowComment()
		{
			ArborFSMInternal stateMachine = node.stateMachine;

			Undo.RecordObject(stateMachine, "Change Show Comment");

			node.showComment = !node.showComment;

			window.UpdateNodeCommentControl(node);

			EditorUtility.SetDirty(stateMachine);
		}

		public void DoCalculatorContextMenu(Rect popupPosition, Rect headerPosition)
		{
			GenericMenu menu = new GenericMenu();

			menu.AddItem(Localization.GetTextContent("Show Comment"), calculatorNode.showComment, ChangeShowComment );

			menu.AddSeparator("");

			SetNodeContextMenu(menu);

			menu.DropDown(popupPosition);
		}

		public void CalculatorSettings(Rect popupPosition, Rect headerPosition)
		{
			int controlId = GUIUtility.GetControlID(s_CalculatorTitlebarHash, FocusType.Passive, popupPosition);

			Event current = Event.current;

			EventType typeForControl = current.GetTypeForControl(controlId);
			switch (typeForControl)
			{
				case EventType.MouseDown:
					if (popupPosition.Contains(current.mousePosition))
					{
						DoCalculatorContextMenu(popupPosition, headerPosition);

						current.Use();
					}
					break;
				case EventType.Repaint:
					Styles.titlebarText.Draw(popupPosition, EditorGUITools.popupContent, controlId, false);
					break;
			}
		}

		protected override void OnHeaderGUI()
		{
			Rect position = calculatorNode.position;
			position.x = position.y = 0;

			int controlId = GUIUtility.GetControlID(s_CalculatorTitlebarHash, FocusType.Passive, position);

			Event current = Event.current;

			EventType typeForControl = current.GetTypeForControl(controlId);

			Rect headerRect = GetHeaderRect(position);

			Rect namePosition = GetNameRect(position);
			namePosition.xMax -= 16f;

			Rect helpPosition = new Rect(namePosition.xMax + 8, namePosition.y, EditorGUIUtility.singleLineHeight, EditorGUIUtility.singleLineHeight);

			switch (typeForControl)
			{
				case EventType.ContextClick:
					if (headerRect.Contains(current.mousePosition))
					{
						DoCalculatorContextMenu(new Rect(current.mousePosition.x, current.mousePosition.y, 0, 0), headerRect);
						current.Use();
					}
					break;
			}

			CalculatorInfo calculatorInfo = GetCalculatorInfo();

			if (!string.IsNullOrEmpty(calculatorInfo.helpUrl))
			{
				EditorGUITools.HelpButton(helpPosition, calculatorInfo.helpUrl, calculatorInfo.helpTooltip);
			}

			Rect popupPosition = new Rect(helpPosition.xMax, helpPosition.y, EditorGUIUtility.singleLineHeight, namePosition.height);

			CalculatorSettings(popupPosition, headerRect);
		}

		public void OnEditorGUI()
		{
			Editor editor = this.editor;
			Object calculatorObj = calculatorNode.GetObject();

			using (new ProfilerScope("OnInspector"))
			{
				float labelWidth = EditorGUIUtility.labelWidth;
				EditorGUIUtility.labelWidth = 120.0f;

				if (editor != null)
				{
					try
					{
						editor.OnInspectorGUI();
					}
					catch (System.Exception ex)
					{
						if (EditorGUITools.ShouldRethrowException(ex))
						{
							throw;
						}
						else
						{
							Debug.LogException(ex);
						}
					}
				}
				else
				{
					if (calculatorObj != null)
					{
						MonoScript script = EditorGUITools.GetMonoScript(calculatorObj);
						if (script != null)
						{
							EditorGUI.BeginDisabledGroup(true);
							EditorGUILayout.ObjectField("Script", script, typeof(MonoScript), false);
							EditorGUI.EndDisabledGroup();
						}
					}
					EditorGUILayout.HelpBox(Localization.GetWord("MissingError"), MessageType.Error);
				}

				EditorGUIUtility.labelWidth = labelWidth;
			}
		}

		protected override void OnGUI()
		{
			using (new ProfilerScope("OnCalculatorGUI"))
			{
				CalculatorNode calculatorNode = this.calculatorNode;

				ArborEditorWindow window = this.window;

				using (new ProfilerScope("Contents"))
				{
					if (window != null)
					{
						window.SetBehaviourPosition(calculatorNode.GetObject(), calculatorNode.position);
					}

					EditorGUITools.DrawSeparator();

					OnEditorGUI();
				}
			}
		}

		[System.Reflection.Obfuscation(Exclude = true)]
		void OnDestroy()
		{
			if (_Editor != null)
			{
				Object.DestroyImmediate(_Editor);
				_Editor = null;
			}
		}
	}
}
