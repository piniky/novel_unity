﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

using Arbor;
namespace ArborEditor
{
	[CustomPropertyDrawer(typeof(CalculatorSlot), true)]
	public class CalculatorSlotPropertyDrawer : PropertyDrawer
	{
		static int s_CalculatorSlotHash = "s_CalculatorSlotHash".GetHashCode();

		static CalculatorSlot _DragSlot;
		static SlotTypeAttribute _DragSlotType;
		static Node _DragNode;
		static CalculatorSlot _HoverSlot;
		static Object _HoverObj;

		static System.Type GetCalculatorSlotType(CalculatorSlot slot, SlotTypeAttribute slotTypeAttribute)
		{
			if (slot == null)
			{
				return null;
			}

			System.Type type = slot.dataType;
			if (slotTypeAttribute != null && IsConnectableType(slot.dataType,slotTypeAttribute.connectableType) )
			{
				type = slotTypeAttribute.connectableType;
			}
			return type;
		}

		static SlotTypeAttribute GetSlotTypeAttribute(Object obj, CalculatorSlot slot, SlotTypeAttribute slotTypeAttribute)
		{
			if (slotTypeAttribute == null)
			{
				slotTypeAttribute = GetSlotType(obj, slot);
			}
			return slotTypeAttribute;
		}

		static SlotTypeAttribute GetSlotTypeAttribute(Object obj, CalculatorSlot slot, System.Reflection.FieldInfo fieldInfo)
		{
			SlotTypeAttribute slotTypeAttribute = AttributeHelper.GetAttribute<SlotTypeAttribute>(fieldInfo);
			return GetSlotTypeAttribute(obj, slot, slotTypeAttribute);
		}

		static System.Type GetCalculatorSlotType(Object obj,CalculatorSlot slot, System.Reflection.FieldInfo fieldInfo)
		{
			SlotTypeAttribute slotTypeAttribute = GetSlotTypeAttribute(obj,slot,fieldInfo);
			return GetCalculatorSlotType(slot, slotTypeAttribute);
		}

		static bool IsConnectableType(System.Type inputType, System.Type outputType)
		{
			return inputType == outputType || outputType.IsSubclassOf(inputType);
		}

		static InputSlot GetInputSlotFromPosition(Object obj, Vector2 position, System.Type outputType)
		{
			NodeBehaviour nodeBehaviour = obj as NodeBehaviour;
			if (nodeBehaviour != null)
			{
				int slotCount = nodeBehaviour.calculatorSlotFieldCount;
				for (int slotIndex = 0; slotIndex < slotCount; slotIndex++)
				{
					CalculatorSlotField slotInfo = nodeBehaviour.GetCalculatorSlotField(slotIndex);
					InputSlot inputSlot = slotInfo.slot as InputSlot;
					if (inputSlot != null)
					{
						System.Type inputType = GetCalculatorSlotType(obj, inputSlot, slotInfo.fieldInfo);
						if (IsVisible(obj, inputSlot) && inputSlot.position.Contains(position) && IsConnectableType(inputType, outputType))
						{
							return inputSlot;
						}
					}
				}
			}
			
			return null;
		}

		static OutputSlot GetOutputSlotFromPosition(Object obj,Vector2 position, System.Type inputType)
		{
			NodeBehaviour nodeBehaviour = obj as NodeBehaviour;
			if (nodeBehaviour != null)
			{
				int slotCount = nodeBehaviour.calculatorSlotFieldCount;
				for (int slotIndex = 0; slotIndex < slotCount; slotIndex++)
				{
					CalculatorSlotField slotInfo = nodeBehaviour.GetCalculatorSlotField(slotIndex);
					OutputSlot outputSlot = slotInfo.slot as OutputSlot;
					if (outputSlot != null)
					{
						System.Type outputType = GetCalculatorSlotType(obj, outputSlot, slotInfo.fieldInfo);
						if (IsVisible(obj, outputSlot) && outputSlot.position.Contains(position) && IsConnectableType(inputType, outputType))
						{
							return outputSlot;
						}
					}
				}
			}

			return null;
		}

		Object _TargetObject;
		Node _TargetNode;
		ArborFSMInternal _StateMachine;
		Vector2 _NodePosition;

		CalculatorSlot GetSlotFromPosition(CalculatorSlot sourceSlot,SlotTypeAttribute slotTypeAttribute,Vector2 position, out Object obj)
		{
			System.Type sourceType = sourceSlot.dataType;
			if (slotTypeAttribute != null)
			{
				sourceType = slotTypeAttribute.connectableType;
			}

			for (int i = 0, count = _StateMachine.stateCount; i < count; i++)
			{
				State state = _StateMachine.GetStateFromIndex(i);
				int behaviourCount = state.behaviourCount;
				for (int behaviourIndex = 0; behaviourIndex < behaviourCount; behaviourIndex++)
				{
					StateBehaviour behaviour = state.GetBehaviourFromIndex(behaviourIndex);
					if (_TargetNode == state || _TargetObject == behaviour)
					{
						continue;
					}
					CalculatorSlot findSlot = null;
					if (sourceSlot.slotType == SlotType.Input)
					{
						findSlot = GetOutputSlotFromPosition(behaviour, position, sourceType);
					}
					else
					{
						findSlot = GetInputSlotFromPosition(behaviour, position, sourceType);
					}

					if (findSlot != null)
					{
						obj = behaviour;
						return findSlot;
					}
				}
			}

			for (int i = 0, count = _StateMachine.calculatorCount; i < count; i++)
			{
				CalculatorNode calculatorNode = _StateMachine.GetCalculatorFromIndex(i);
				Calculator calculator = calculatorNode.calculator;

				if (_TargetNode == calculatorNode || _TargetObject == calculator)
				{
					continue;
				}

				CalculatorSlot findSlot = null;
				if (sourceSlot.slotType == SlotType.Input)
				{
					findSlot = GetOutputSlotFromPosition(calculator, position, sourceType);
				}
				else
				{
					findSlot = GetInputSlotFromPosition(calculator, position, sourceType);
				}

				if (findSlot != null)
				{
					obj = calculator;
					return findSlot;
				}
			}
			obj = null;
			return null;
		}
				
		void OnInputGUI(Rect position, SerializedProperty property, GUIContent label, InputSlot slot, SlotTypeAttribute slotTypeAttribute)
		{
			ArborEditorWindow window = ArborEditorWindow.GetCurrent();
			if (window == null)
			{
				return;
			}

			int controlID = EditorGUIUtility.GetControlID(s_CalculatorSlotHash, FocusType.Passive, position);
			CalculatorBranch branch = slot.branch;
			
			Event current = Event.current;

			Vector2 pinPos = new Vector2(position.x + 8, position.center.y);
			Vector2 pinControlPos = pinPos - EditorGUITools.kBezierTangentOffset;

			Vector2 targetPos = current.mousePosition;
			Vector2 targetControlPos = targetPos + EditorGUITools.kBezierTangentOffset;

			pinPos += _NodePosition;
			pinControlPos += _NodePosition;
			targetPos += _NodePosition;
			targetControlPos += _NodePosition;

			if (_HoverSlot != null)
			{
				targetPos = new Vector2(_HoverSlot.position.xMax - 8, _HoverSlot.position.center.y);
				targetControlPos = targetPos + EditorGUITools.kBezierTangentOffset;
			}

			switch (current.GetTypeForControl(controlID))
			{
				case EventType.MouseDown:
					if (position.Contains(current.mousePosition))
					{
						if (current.button == 0)
						{
							GUIUtility.hotControl = GUIUtility.keyboardControl = controlID;

							_DragNode = _TargetNode;
							_DragSlot = slot;
							_DragSlotType = slotTypeAttribute;

							if (branch != null)
							{
								branch.enabled = false;
							}

							window.BeginDragCalculatorBranch(_TargetNode.nodeID);
							window.DragCalculatorBranchBezier(pinPos, pinControlPos, targetPos, targetControlPos);

							current.Use();
						}
					}
					break;
				case EventType.MouseDrag:
					if (GUIUtility.hotControl == controlID && current.button == 0)
					{
						DragAndDrop.PrepareStartDrag();

						_HoverSlot = GetSlotFromPosition(slot,slotTypeAttribute,current.mousePosition + _NodePosition, out _HoverObj);

						current.Use();
					}
					break;
				case EventType.MouseUp:
					if (GUIUtility.hotControl == controlID && current.button == 0)
					{
						GUIUtility.hotControl = 0;

						_DragNode = null;
						_DragSlot = null;
						_DragSlotType = null;

						window.EndDragCalculatorBranch();

						if (_HoverSlot != null)
						{
							OutputSlot outputSlot = _HoverSlot as OutputSlot;
							if (branch != null)
							{
								_StateMachine.DeleteCalculatorBranch(branch);
								branch = null;
							}

							branch = EditorGUITools.Connect(_StateMachine,_TargetObject, slot, _HoverObj, outputSlot);
							if (branch != null)
							{
								branch.lineBezier = new Bezier2D(targetPos, targetControlPos, pinPos, pinControlPos);
							}
						}
						else if (branch != null)
						{
							_StateMachine.DeleteCalculatorBranch(branch);
							branch = null;
						}

						if (branch != null)
						{
							branch.enabled = true;
						}

						_HoverSlot = null;
						_HoverObj = null;

						current.Use();
					}
					break;
				case EventType.Repaint:
					{
						if (GUIUtility.hotControl == controlID && current.button == 0)
						{
							window.DragCalculatorBranchBezier(pinPos, pinControlPos, targetPos, targetControlPos);
						}
						else
						{
							if (branch != null)
							{
								branch.lineBezier.endPosition = pinPos;
								branch.lineBezier.endControl = pinControlPos;
							}
						}

						bool on = GUIUtility.hotControl == controlID && current.button == 0 || branch != null;
						bool hover = slot == _HoverSlot || GUIUtility.hotControl == controlID && current.button == 0;

						System.Type dragType = GetCalculatorSlotType(_DragSlot, _DragSlotType);
						System.Type slotType = GetCalculatorSlotType(slot, slotTypeAttribute);

						if (hover)
						{
							Styles.overlayBox.Draw(position, GUIContent.none, controlID, true);
						}
						else if (_DragSlot != null && _DragNode != _TargetNode && _DragSlot.slotType != slot.slotType && IsConnectableType(slotType,dragType) )
						{
							Styles.controlHighlight.Draw(position, GUIContent.none, controlID, true);
						}
						else if (on)
						{
							Styles.background.Draw(position, GUIContent.none, controlID, true);
						}
						else
						{
							EditorStyles.miniButton.Draw(position, GUIContent.none, controlID, false);
						}
						Styles.varPinIn.Draw(position, label, controlID, on);
					}
					break;
			}
		}

		void OnOutputGUI(Rect position, SerializedProperty property, GUIContent label, OutputSlot slot,SlotTypeAttribute slotTypeAttribute)
		{
			ArborEditorWindow window = ArborEditorWindow.GetCurrent();
			if (window == null)
			{
				return;
			}

			int controlID = EditorGUIUtility.GetControlID(s_CalculatorSlotHash, FocusType.Passive, position);

			Event current = Event.current;

			Vector2 pinPos = new Vector2(position.xMax - 8, position.center.y);
			Vector2 pinControlPos = pinPos + EditorGUITools.kBezierTangentOffset;
			Vector2 targetPos = current.mousePosition;
			Vector2 targetControlPos = targetPos - EditorGUITools.kBezierTangentOffset;

			pinPos += _NodePosition;
			pinControlPos += _NodePosition;
			targetPos += _NodePosition;
			targetControlPos += _NodePosition;

			if (_HoverSlot != null)
			{
				targetPos = new Vector2(_HoverSlot.position.x + 8, _HoverSlot.position.center.y);
				targetControlPos = targetPos - EditorGUITools.kBezierTangentOffset;
			}

			switch (current.GetTypeForControl(controlID))
			{
				case EventType.MouseDown:
					if (position.Contains(current.mousePosition))
					{
						if (current.button == 0)
						{
							GUIUtility.hotControl = GUIUtility.keyboardControl = controlID;

							_DragNode = _TargetNode;
							_DragSlot = slot;
							_DragSlotType = slotTypeAttribute;

							window.BeginDragCalculatorBranch(_TargetNode.nodeID);
							window.DragCalculatorBranchBezier(pinPos, pinControlPos, targetPos, targetControlPos);

							current.Use();
						}
					}
					break;
				case EventType.MouseDrag:
					if (GUIUtility.hotControl == controlID && current.button == 0)
					{
						DragAndDrop.PrepareStartDrag();

						_HoverSlot = GetSlotFromPosition(slot,slotTypeAttribute,current.mousePosition + _NodePosition, out _HoverObj);

						current.Use();
					}
					break;
				case EventType.MouseUp:
					if (GUIUtility.hotControl == controlID && current.button == 0)
					{
						GUIUtility.hotControl = 0;

						_DragNode = null;
						_DragSlot = null;
						_DragSlotType = null;

						window.EndDragCalculatorBranch();

						CalculatorBranch branch = null;

						if (_HoverSlot != null)
						{
							InputSlot inputSlot = _HoverSlot as InputSlot;

							branch = _StateMachine.GetCalculatorBranchFromID(inputSlot.branchID);
							if (branch != null)
							{
								_StateMachine.DeleteCalculatorBranch(branch);
								branch = null;
							}

							branch = EditorGUITools.Connect(_StateMachine,_HoverObj, inputSlot, _TargetObject, slot);
							if (branch != null)
							{
								branch.lineBezier = new Bezier2D(pinPos, pinControlPos, targetPos, targetControlPos);
							}
						}

						if (branch != null)
						{
							branch.enabled = true;
						}

						_HoverSlot = null;
						_HoverObj = null;

						current.Use();
					}
					break;
				case EventType.Repaint:
					{
						if (GUIUtility.hotControl == controlID && current.button == 0)
						{
							window.DragCalculatorBranchBezier(pinPos, pinControlPos, targetPos, targetControlPos);
						}

						bool on = false;
						int idCount = slot.branchIDs.Count;
						for (int idIndex = 0; idIndex < idCount; idIndex++)
						{
							int branchID = slot.branchIDs[idIndex];
							if (branchID != 0)
							{
								on = true;
								CalculatorBranch branch = _StateMachine.GetCalculatorBranchFromID(branchID);
								if (branch != null)
								{
									branch.lineBezier.startPosition = pinPos;
									branch.lineBezier.startControl = pinControlPos;
								}
							}
						}

						System.Type dragType = GetCalculatorSlotType(_DragSlot, _DragSlotType);
						System.Type slotType = GetCalculatorSlotType(slot, slotTypeAttribute);

						if (slot == _HoverSlot)
						{
							Styles.overlayBox.Draw(position, GUIContent.none, controlID, true);
						}
						else if (_DragSlot != null && _DragNode != _TargetNode && _DragSlot.slotType != slot.slotType && IsConnectableType(dragType, slotType) )
						{
							Styles.controlHighlight.Draw(position, GUIContent.none, controlID, true);

						}
						else if (on)
						{
							Styles.background.Draw(position, GUIContent.none, controlID, true);
						}
						else
						{
							EditorStyles.miniButton.Draw(position, GUIContent.none, controlID, false);
						}

						Styles.varPinOut.Draw(position, label, controlID, on);
					}
					break;
			}
		}

		static readonly Dictionary<System.Type, string> _TypeAliases = new Dictionary<System.Type, string>()
		{
			{ typeof(string), "string" },
			{ typeof(int), "int" },
			{ typeof(byte), "byte" },
			{ typeof(sbyte), "sbyte" },
			{ typeof(short), "short" },
			{ typeof(ushort), "ushort" },
			{ typeof(long), "long" },
			{ typeof(uint), "uint" },
			{ typeof(ulong), "ulong" },
			{ typeof(float), "float" },
			{ typeof(double), "double" },
			{ typeof(decimal), "decimal" },
			{ typeof(object), "object" },
			{ typeof(bool), "bool" },
			{ typeof(char), "char" }
		};

		protected virtual float GetFieldsHeight(SerializedProperty property)
		{
			return 0f;
		}

		protected virtual void OnFieldsGUI(Rect position, SerializedProperty property)
		{
		}

		void RebuildConnectInputGUI(InputSlot inputSlot,SlotTypeAttribute inputSlotTypeAttribute)
		{
			CalculatorBranch branch = inputSlot.branch;
			if ( (branch == null && inputSlot.branchID != 0) || (branch != null && branch.inputSlot != inputSlot) )
			{
				inputSlot.branchID = 0;
				EditorUtility.SetDirty(_TargetObject);
				branch = null;
			}

			if (branch != null)
			{
				branch.Refresh();
				System.Type inputType = GetCalculatorSlotType(inputSlot, inputSlotTypeAttribute);

				if (branch.outBehaviour != null && branch.outputSlot != null && branch.outputSlotFieldInfo != null)
				{
					System.Type outputType = GetCalculatorSlotType(branch.outBehaviour, branch.outputSlot, branch.outputSlotFieldInfo);

					bool isConnectable = IsConnectableType(inputType, outputType);
					if (!isConnectable)
					{
						inputSlot.stateMachine.DeleteCalculatorBranch(branch);
					}
				}
			}
		}

		void RebuildConnectOutputGUI(OutputSlot outputSlot, SlotTypeAttribute outputSlotTypeAttribute)
		{
			System.Type outputType = GetCalculatorSlotType(outputSlot, outputSlotTypeAttribute);

			for (int i = outputSlot.branchIDs.Count - 1; i >= 0; i-- )
			{
				int branchID = outputSlot.branchIDs[i];
				CalculatorBranch branch = outputSlot.stateMachine.GetCalculatorBranchFromID(branchID);
				if ( branch == null || (branch != null && branch.outputSlot != outputSlot) )
				{
					outputSlot.branchIDs.RemoveAt(i);
					EditorUtility.SetDirty(_TargetObject);
					branch = null;
				}
				if (branch != null)
				{
					branch.Refresh();

					if (branch.inBehaviour != null && branch.inputSlot != null && branch.inputSlotFieldInfo != null)
					{
						System.Type inputType = GetCalculatorSlotType(branch.inBehaviour, branch.inputSlot, branch.inputSlotFieldInfo);

						bool isConnectable = IsConnectableType(inputType, outputType);

						if (!isConnectable)
						{
							outputSlot.stateMachine.DeleteCalculatorBranch(branch);
						}
					}
				}
			}
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			using (new ProfilerScope("CalculatorSlotPropertyDrawer.OnGUI"))
			{
				_TargetObject = property.serializedObject.targetObject;
				if (_TargetObject is StateBehaviour)
				{
					StateBehaviour behaviour = _TargetObject as StateBehaviour;
					_StateMachine = behaviour.stateMachine;

					_TargetNode = behaviour.state;

					_NodePosition = new Vector2(_TargetNode.position.x, _TargetNode.position.y);
				}
				else if (_TargetObject is Calculator)
				{
					Calculator calculator = _TargetObject as Calculator;
					_StateMachine = calculator.stateMachine;

					_TargetNode = calculator.calculatorNode;

					_NodePosition = new Vector2(_TargetNode.position.x, _TargetNode.position.y);
				}

				Rect fieldPosition = new Rect(position);
				fieldPosition.height = GetFieldsHeight(property);
				
				Rect slotPosition = new Rect(position);
				slotPosition.yMin += fieldPosition.height;

				CalculatorSlot slot = EditorGUITools.GetPropertyObject<CalculatorSlot>(property);
				SlotTypeAttribute slotTypeAttribute = GetSlotTypeAttribute(_TargetObject, slot, fieldInfo);

				if (Event.current.type == EventType.Repaint)
				{
					slot.position = new Rect(slotPosition.x + _NodePosition.x, slotPosition.y + _NodePosition.y, slotPosition.width, slotPosition.height);
				}

				string typeName = string.Empty;
				System.Type slotType = GetCalculatorSlotType(_TargetObject, slot, fieldInfo);
				if (!_TypeAliases.TryGetValue(slotType, out typeName))
				{
					typeName = slotType.Name;
				}

				AddSlot(_TargetObject, slot);

				OnFieldsGUI(fieldPosition, property);

				if (slot != null)
				{
					switch (slot.slotType)
					{
						case SlotType.Input:
							RebuildConnectInputGUI(slot as InputSlot,slotTypeAttribute);
							break;
						case SlotType.Output:
							RebuildConnectOutputGUI(slot as OutputSlot, slotTypeAttribute);
							break;
					}
				}

				label = new GUIContent(label);
				label.tooltip = typeName;

				label = EditorGUI.BeginProperty(slotPosition, label, property);

				if (slot != null)
				{
					switch (slot.slotType)
					{
						case SlotType.Input:
							OnInputGUI(slotPosition, property, label, slot as InputSlot, slotTypeAttribute);
							break;
						case SlotType.Output:
							OnOutputGUI(slotPosition, property, label, slot as OutputSlot, slotTypeAttribute);
							break;
					}
				}

				EditorGUI.EndProperty();
			}
		}

		class SlotTypes
		{
			public Dictionary<CalculatorSlot, SlotTypeAttribute> slots = new Dictionary<CalculatorSlot, SlotTypeAttribute>();
			public Dictionary<CalculatorSlot, SlotTypeAttribute> slotsNext = new Dictionary<CalculatorSlot, SlotTypeAttribute>();

			public void Clear()
			{
				slots = new Dictionary<CalculatorSlot, SlotTypeAttribute>(slotsNext);
				slotsNext.Clear();
			}

			public SlotTypeAttribute GetSlotType(CalculatorSlot slot)
			{
				SlotTypeAttribute slotTypeAttribute = null;
				if (slots.TryGetValue(slot, out slotTypeAttribute))
				{
					return slotTypeAttribute;
				}
				if (slotsNext.TryGetValue(slot, out slotTypeAttribute))
				{
					return slotTypeAttribute;
				}
				return null;
			}
		}

		static Dictionary<Object, SlotTypes> _SlotTypes = new Dictionary<Object, SlotTypes>();

		public static void SetSlotType(Object obj, CalculatorSlot slot,SlotTypeAttribute slotTypeAttribute)
		{
			SlotTypes slotTypes = null;
			if (!_SlotTypes.TryGetValue(obj, out slotTypes))
			{
				slotTypes = new SlotTypes();
				_SlotTypes.Add(obj, slotTypes);
			}
			slotTypes.slotsNext[slot] = slotTypeAttribute;
		}

		public static void ClearSlotType(Object obj)
		{
			SlotTypes slotTypes = null;
			if (_SlotTypes.TryGetValue(obj, out slotTypes))
			{
				slotTypes.Clear();
			}
		}

		public static void ClearSlotType()
		{
			foreach (KeyValuePair<Object, SlotTypes> pair in _SlotTypes)
			{
				SlotTypes slotTypes = pair.Value;
				slotTypes.Clear();
			}
		}

		public static SlotTypeAttribute GetSlotType(Object obj, CalculatorSlot slot)
		{
			SlotTypes slotTypes = null;
			if (_SlotTypes.TryGetValue(obj, out slotTypes))
			{
				return slotTypes.GetSlotType(slot);
			}
			return null;
		}

		class HashSlots
		{
			public HashSet<CalculatorSlot> slots = new HashSet<CalculatorSlot>();
			public HashSet<CalculatorSlot> slotsNext = new HashSet<CalculatorSlot>();

			public void ClearVisible()
			{
				slots = new HashSet<CalculatorSlot>(slotsNext);
				slotsNext.Clear();
			}

			public bool IsVisible(CalculatorSlot slot)
			{
				return slots.Contains(slot) || slotsNext.Contains(slot);
			}
		}

		static Dictionary<Object, HashSlots> _VisibleSlots = new Dictionary<Object, HashSlots>();

		static void AddSlot(Object obj, CalculatorSlot slot)
		{
			if ((object)obj == null)
			{
				return;
			}

			HashSlots hashSlots = null;
			if (!_VisibleSlots.TryGetValue(obj, out hashSlots))
			{
				hashSlots = new HashSlots();
				_VisibleSlots.Add(obj, hashSlots);
			}
			hashSlots.slotsNext.Add(slot);
		}

		public static void ClearVisible(Object obj)
		{
			HashSlots hashSlots = null;
			if ( (object)obj != null && _VisibleSlots.TryGetValue(obj, out hashSlots) )
			{
				hashSlots.ClearVisible();
			}
		}

		public static void ClearVisible()
		{
			foreach (KeyValuePair<Object, HashSlots> pair in _VisibleSlots)
			{
				HashSlots hashSlots = pair.Value;
				hashSlots.ClearVisible();
			}
		}

		public static bool IsVisible(Object obj, CalculatorSlot slot)
		{
			HashSlots hashSlots = null;
			if ( (object)obj != null && _VisibleSlots.TryGetValue(obj, out hashSlots))
			{
				return hashSlots.IsVisible(slot);
			}
			return true;
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return GetFieldsHeight(property) + EditorGUIUtility.singleLineHeight;
		}
	}
}
