﻿using UnityEditor;
using System;
using System.Collections.Generic;

namespace ArborEditor
{
	public static class EnumUtility
	{
		static Dictionary<Type, Array> _Values = new Dictionary<Type, Array>();

		public static TEnum[] GetValues<TEnum>()
		{
			Type enumType = typeof( TEnum );
			if( !enumType.IsEnum )
			{
				throw new ArgumentException( "The type `" + enumType.Name + "' must be convertible to `enum' in order to use it as parameter `enumType'", "enumType" );
			}

			Array array = null;
			if( !_Values.TryGetValue( enumType, out array ) )
			{
				array = Enum.GetValues( enumType );
				_Values.Add( enumType, array );
			}
			return (TEnum[])array;
		}

		public static TEnum GetValueFromIndex<TEnum>(int index) where TEnum : struct
		{
			return GetValues<TEnum>()[index];
		}

		public static int GetIndexFromValue<TEnum>(TEnum value) where TEnum : struct
		{
			return ArrayUtility.IndexOf( GetValues<TEnum>(), value );
		}
	}
}
