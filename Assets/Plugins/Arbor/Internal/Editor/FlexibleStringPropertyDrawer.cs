﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

using Arbor;

namespace ArborEditor
{
	[CustomPropertyDrawer(typeof(FlexibleString))]
	public class FlexibleStringPropertyDrawer : PropertyDrawer
	{
		private static readonly System.Reflection.PropertyInfo s_contextWidthProperty;
		private static readonly GUIContent s_TempContent = null;
		const float kLineHeight = 13f;

		static FlexibleStringPropertyDrawer()
		{
			s_contextWidthProperty = typeof(EditorGUIUtility).GetProperty("contextWidth", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
			s_TempContent = new GUIContent();
		}

		static GUIContent GetTempContent(string text)
		{
			s_TempContent.text = text;
			s_TempContent.tooltip = string.Empty;
			return s_TempContent;
		}

		static int GetTextLine(string text)
		{
			return Mathf.Max(Mathf.CeilToInt(EditorStyles.textArea.CalcHeight(GetTempContent(text), (float)s_contextWidthProperty.GetValue(null, null)) / kLineHeight), 1) - 1;
		}

		static float GetTextHeight(string text)
		{
			return GetTextLine(text) * kLineHeight;
		}
		
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			label = EditorGUI.BeginProperty(position, label, property);

			SerializedProperty typeProperty = property.FindPropertyRelative("_Type");

			FlexibleString.Type type = EnumUtility.GetValueFromIndex<FlexibleString.Type>(typeProperty.enumValueIndex);

			Rect fieldPosition = EditorGUITools.SubtractPopupWidth(position);

			switch (type)
			{
				case FlexibleString.Type.Constant:
					if (AttributeHelper.HasAttribute<ConstantMultilineAttribute>(fieldInfo))
					{
						Rect labelPosition = EditorGUI.IndentedRect(fieldPosition);
						labelPosition.height = 16f;
						fieldPosition.yMin += labelPosition.height;

						EditorGUI.HandlePrefixLabel(fieldPosition, labelPosition, label);
						
						SerializedProperty valueProperty = property.FindPropertyRelative("_Value");
						EditorGUI.BeginChangeCheck();
						string value = GUI.TextArea(fieldPosition, valueProperty.stringValue);
						if (EditorGUI.EndChangeCheck())
						{
							valueProperty.stringValue = value;
						}
					}
					else
					{
						EditorGUI.PropertyField(fieldPosition, property.FindPropertyRelative("_Value"), label);
					}
					break;
				case FlexibleString.Type.Parameter:
					EditorGUI.PropertyField(fieldPosition, property.FindPropertyRelative("_Parameter"), label);
					break;
				case FlexibleString.Type.Calculator:
					EditorGUI.PropertyField(fieldPosition, property.FindPropertyRelative("_Slot"), label);
					break;
			}

			Rect popupRect = EditorGUITools.GetPopupRect(position);

			if (EditorGUITools.ButtonMouseDown(popupRect, GUIContent.none,FocusType.Passive,Styles.shurikenDropDown ))
			{
				GenericMenu menu = new GenericMenu();
				foreach (FlexibleString.Type t in System.Enum.GetValues(typeof(FlexibleString.Type)))
				{
					menu.AddItem( EditorGUITools.GetTextContent( t.ToString()), t == type, SelectType,new KeyValuePair<SerializedProperty, FlexibleString.Type>(property, t) );
				}
				menu.DropDown(popupRect);
            }

			EditorGUI.EndProperty();
		}

		private static void SelectType(object obj)
		{
			KeyValuePair<SerializedProperty, FlexibleString.Type> pair = (KeyValuePair<SerializedProperty, FlexibleString.Type>)obj;
			SerializedProperty property = pair.Key;
			FlexibleString.Type type = pair.Value;

			property.serializedObject.Update();

			SerializedProperty typeProperty = property.FindPropertyRelative("_Type");

			SerializedProperty slotProperty = property.FindPropertyRelative("_Slot");

			FlexibleString.Type oldType = EnumUtility.GetValueFromIndex<FlexibleString.Type>(typeProperty.enumValueIndex);
			if (oldType == FlexibleString.Type.Calculator)
			{
				Object targetObject = property.serializedObject.targetObject;
				ArborFSMInternal stateMachine = null;
				if (targetObject is StateBehaviour)
				{
					StateBehaviour behaviour = targetObject as StateBehaviour;
					stateMachine = behaviour.stateMachine;
				}
				else if (targetObject is Calculator)
				{
					Calculator calculator = targetObject as Calculator;
					stateMachine = calculator.stateMachine;
				}

				CalculatorBranch branch = stateMachine.GetCalculatorBranchFromID(slotProperty.FindPropertyRelative("branchID").intValue);
				if (branch != null)
				{
					stateMachine.DeleteCalculatorBranch(branch);
				}
			}

			typeProperty.enumValueIndex = EnumUtility.GetIndexFromValue(type);

			property.serializedObject.ApplyModifiedProperties();
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			float height = EditorGUIUtility.singleLineHeight;

			SerializedProperty typeProperty = property.FindPropertyRelative("_Type");
			FlexibleString.Type type = EnumUtility.GetValueFromIndex<FlexibleString.Type>(typeProperty.enumValueIndex);
			switch (type)
			{
				case FlexibleString.Type.Constant:
					if (AttributeHelper.HasAttribute<ConstantMultilineAttribute>(fieldInfo))
					{
						SerializedProperty valueProperty = property.FindPropertyRelative("_Value");
						height = 32.0f + GetTextHeight(valueProperty.stringValue);
					}
					else
					{
						height = EditorGUI.GetPropertyHeight(property.FindPropertyRelative("_Value"));
					}
					break;
				case FlexibleString.Type.Parameter:
					height = EditorGUI.GetPropertyHeight(property.FindPropertyRelative("_Parameter"));
					break;
				case FlexibleString.Type.Calculator:
					height = EditorGUI.GetPropertyHeight(property.FindPropertyRelative("_Slot"));
					break;
			}

			return height;
		}
	}
}
