﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace ArborEditor
{
	class ArborScriptCreator
	{
		static readonly string _StateBehaviourCSharpTemplatePath = @"C# Script-NewBehaviourScript";
		static readonly string _StateBehaviourJavaScriptTemplatePath = @"Javascript-NewBehaviourScript";
		static readonly string _StateBehaviourBooScriptTemplatePath = @"Boo Script-NewBehaviourScript";

		static readonly string _CalculatorCSharpTemplatePath = @"C# Script-NewCalculatorScript";
		static readonly string _CalculatorJavaScriptTemplatePath = @"Javascript-NewCalculatorScript";
		static readonly string _CalculatorBooScriptTemplatePath = @"Boo Script-NewCalculatorScript";

		static readonly Texture2D _CSharpIcon = EditorGUIUtility.FindTexture("cs Script Icon");
		static readonly Texture2D _JavaScriptIcon = EditorGUIUtility.FindTexture("js Script Icon");
		static readonly Texture2D _BooScriptIcon = EditorGUIUtility.FindTexture("boo Script Icon");

		[MenuItem("Assets/Create/Arbor/StateBehaviour/C# Script")]
		public static void CreateCSharpScriptStateBehaviour()
		{
			ProjectWindowUtil.StartNameEditingIfProjectWindowExists(0, ScriptableObject.CreateInstance<DoCreateScriptAsset>(), "NewStateBehaviourScript.cs", _CSharpIcon, _StateBehaviourCSharpTemplatePath);
		}

		[MenuItem("Assets/Create/Arbor/StateBehaviour/JavaScript")]
		public static void CreateJavaScriptStateBehaviour()
		{
			ProjectWindowUtil.StartNameEditingIfProjectWindowExists(0, ScriptableObject.CreateInstance<DoCreateScriptAsset>(), "NewStateBehaviourScript.js", _JavaScriptIcon, _StateBehaviourJavaScriptTemplatePath);
		}

		[MenuItem("Assets/Create/Arbor/StateBehaviour/Boo Script")]
		public static void CreateBooScriptStateBehaviour()
		{
			ProjectWindowUtil.StartNameEditingIfProjectWindowExists(0, ScriptableObject.CreateInstance<DoCreateScriptAsset>(), "NewStateBehaviourScript.boo", _BooScriptIcon, _StateBehaviourBooScriptTemplatePath);
		}

		[MenuItem("Assets/Create/Arbor/Calculator/C# Script")]
		public static void CreateCSharpScriptCalculator()
		{
			ProjectWindowUtil.StartNameEditingIfProjectWindowExists(0, ScriptableObject.CreateInstance<DoCreateScriptAsset>(), "NewCalculatorScript.cs", _CSharpIcon, _CalculatorCSharpTemplatePath);
		}

		[MenuItem("Assets/Create/Arbor/Calculator/JavaScript")]
		public static void CreateJavaScrippCalculatort()
		{
			ProjectWindowUtil.StartNameEditingIfProjectWindowExists(0, ScriptableObject.CreateInstance<DoCreateScriptAsset>(), "NewCalculatorScript.js", _JavaScriptIcon, _CalculatorJavaScriptTemplatePath);
		}

		[MenuItem("Assets/Create/Arbor/Calculator/Boo Script")]
		public static void CreateBooScriptpCalculator()
		{
			ProjectWindowUtil.StartNameEditingIfProjectWindowExists(0, ScriptableObject.CreateInstance<DoCreateScriptAsset>(), "NewCalculatorScript.boo", _BooScriptIcon, _CalculatorBooScriptTemplatePath);
		}
	}
}
