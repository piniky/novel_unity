﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace ArborEditor
{
	[System.Reflection.Obfuscation(Exclude = true)]
	public sealed class GridSettingsWindow : EditorWindow
	{
		private static GridSettingsWindow _Instance = null;

		public static GridSettingsWindow instance
		{
			get
			{
				if (_Instance == null)
				{
					GridSettingsWindow[] objects = Resources.FindObjectsOfTypeAll<GridSettingsWindow>();
					if (objects.Length > 0)
					{
						_Instance = objects[0];
					}
				}
				if (_Instance == null)
				{
					_Instance = ScriptableObject.CreateInstance<GridSettingsWindow>();
				}

				return _Instance;
			}
		}

		public void Init(Rect buttonRect)
		{
			buttonRect = EditorGUITools.GUIToScreenRect(buttonRect);

			Vector2 windowSize = new Vector2(300f, Mathf.Min(2f + 110.0f, 900f));
			ShowAsDropDown(buttonRect, windowSize);
		}

		[System.Reflection.Obfuscation(Exclude = true)]
		void OnGUI()
		{
			EditorGUI.BeginChangeCheck();

			ArborSettings.showGrid = EditorGUILayout.ToggleLeft(Localization.GetWord("Show Grid"), ArborSettings.showGrid);

			EditorGUI.BeginDisabledGroup(!ArborSettings.showGrid);

			ArborSettings.snapGrid = EditorGUILayout.ToggleLeft(Localization.GetWord("Snap Grid"), ArborSettings.snapGrid);

			EditorGUILayout.Space();

			ArborSettings.gridSize = EditorGUILayout.Slider(Localization.GetWord("Grid Size"), ArborSettings.gridSize, 1.0f, 1000.0f);
			ArborSettings.gridSplitNum = EditorGUILayout.IntSlider(Localization.GetWord("Grid Split Num"), ArborSettings.gridSplitNum, 1, 100);

			EditorGUI.EndDisabledGroup();

			if (GUILayout.Button(Localization.GetWord("Reset")))
			{
				ArborSettings.ResetGrid();
			}

			if (EditorGUI.EndChangeCheck())
			{
				ArborEditorWindow.GetCurrent().Repaint();
			}
		}
	}
}
