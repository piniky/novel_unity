﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;
using System.Collections;
using System.Collections.Generic;

using Arbor;

namespace ArborEditor
{
	[CustomPropertyDrawer(typeof(AnimatorParameterReference),true)]
	public class AnimatorParameterReferencePropertyDrawer : PropertyDrawer
	{
		protected virtual bool HasParameterType()
		{
			return false;
		}

		protected virtual AnimatorControllerParameterType GetParameterType()
		{
			return AnimatorControllerParameterType.Bool;
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.BeginProperty(position, label, property);
			int indentLevel = EditorGUI.indentLevel;
			EditorGUI.indentLevel = 0;

			SerializedProperty animatorProperty = property.FindPropertyRelative("animator");
			SerializedProperty nameProperty = property.FindPropertyRelative("name");
			SerializedProperty typeProperty = property.FindPropertyRelative("type");

			Rect labelRect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);
			EditorGUI.LabelField(labelRect, label);

			EditorGUI.indentLevel++;

			Rect containerRect = new Rect(position.x, labelRect.yMax, position.width, EditorGUIUtility.singleLineHeight);
			Rect parameterRect = new Rect(position.x, containerRect.yMax, position.width, EditorGUIUtility.singleLineHeight);

			EditorGUI.PropertyField(containerRect, animatorProperty);

			Animator animator = animatorProperty.objectReferenceValue as Animator;

			EditorGUITools.AnimatorParameterField(parameterRect, animator, nameProperty, typeProperty, EditorGUITools.GetTextContent("Parameter"), HasParameterType(), GetParameterType());

			EditorGUI.indentLevel = indentLevel;

			EditorGUI.EndProperty();
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
            return EditorGUIUtility.singleLineHeight * 3;
		}
	}
}
