﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

using Arbor;

namespace ArborEditor
{
	[CustomPropertyDrawer(typeof(FlexibleRectTransform))]
	public class FlexibleRectTransformPropertyDrawer : PropertyDrawer
	{
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.BeginProperty(position, label, property);

			SerializedProperty typeProperty = property.FindPropertyRelative("_Type");

			FlexibleRectTransform.Type type = EnumUtility.GetValueFromIndex<FlexibleRectTransform.Type>(typeProperty.enumValueIndex);

			switch (type)
			{
				case FlexibleRectTransform.Type.Constant:
					EditorGUI.PropertyField(EditorGUITools.SubtractPopupWidth(position), property.FindPropertyRelative("_Value"), label);
					break;
				case FlexibleRectTransform.Type.Parameter:
					EditorGUI.PropertyField(EditorGUITools.SubtractPopupWidth(position), property.FindPropertyRelative("_Parameter"), label);
					break;
				case FlexibleRectTransform.Type.Calculator:
					EditorGUI.PropertyField(EditorGUITools.SubtractPopupWidth(position), property.FindPropertyRelative("_Slot"), label);
					break;
			}

			Rect popupRect = EditorGUITools.GetPopupRect(position);

			if (EditorGUITools.ButtonMouseDown(popupRect, GUIContent.none,FocusType.Passive, Styles.shurikenDropDown))
			{
				GenericMenu menu = new GenericMenu();
				foreach (FlexibleRectTransform.Type t in System.Enum.GetValues(typeof(FlexibleRectTransform.Type)))
				{
					menu.AddItem( EditorGUITools.GetTextContent( t.ToString() ), t == type, SelectType,new KeyValuePair<SerializedProperty, FlexibleRectTransform.Type>(property, t) );
				}
				menu.DropDown(popupRect);
            }

			EditorGUI.EndProperty();
		}

		private static void SelectType(object obj)
		{
			KeyValuePair<SerializedProperty, FlexibleRectTransform.Type> pair = (KeyValuePair<SerializedProperty, FlexibleRectTransform.Type>)obj;
			SerializedProperty property = pair.Key;
			FlexibleRectTransform.Type type = pair.Value;

			property.serializedObject.Update();

			SerializedProperty typeProperty = property.FindPropertyRelative("_Type");

			SerializedProperty slotProperty = property.FindPropertyRelative("_Slot");

			FlexibleRectTransform.Type oldType = EnumUtility.GetValueFromIndex<FlexibleRectTransform.Type>(typeProperty.enumValueIndex);
			if (oldType == FlexibleRectTransform.Type.Calculator)
			{
				Object targetObject = property.serializedObject.targetObject;
				ArborFSMInternal stateMachine = null;
				if (targetObject is StateBehaviour)
				{
					StateBehaviour behaviour = targetObject as StateBehaviour;
					stateMachine = behaviour.stateMachine;
				}
				else if (targetObject is Calculator)
				{
					Calculator calculator = targetObject as Calculator;
					stateMachine = calculator.stateMachine;
				}

				CalculatorBranch branch = stateMachine.GetCalculatorBranchFromID(slotProperty.FindPropertyRelative("branchID").intValue);
				if (branch != null)
				{
					stateMachine.DeleteCalculatorBranch(branch);
				}
			}

			typeProperty.enumValueIndex = EnumUtility.GetIndexFromValue(type);

			property.serializedObject.ApplyModifiedProperties();
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			float height = EditorGUIUtility.singleLineHeight;

			SerializedProperty typeProperty = property.FindPropertyRelative("_Type");

			FlexibleRectTransform.Type type = EnumUtility.GetValueFromIndex<FlexibleRectTransform.Type>(typeProperty.enumValueIndex);

			switch (type)
			{
				case FlexibleRectTransform.Type.Constant:
					height = EditorGUI.GetPropertyHeight(property.FindPropertyRelative("_Value"));
					break;
				case FlexibleRectTransform.Type.Parameter:
					height = EditorGUI.GetPropertyHeight(property.FindPropertyRelative("_Parameter"));
					break;
				case FlexibleRectTransform.Type.Calculator:
					height = EditorGUI.GetPropertyHeight(property.FindPropertyRelative("_Slot"));
					break;
			}

			return height;
		}
	}
}
