﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;
using System.Linq;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

using Arbor;

namespace ArborEditor
{
	public static class EditorGUITools
	{
		private static GUIContent s_ContextPopupContent = new GUIContent(EditorGUIUtility.FindTexture("_Popup"));
		private static GUIContent s_HelpButtonContent = new GUIContent(EditorGUIUtility.FindTexture("_Help"));

		public static GUIContent popupContent
		{
			get
			{
				return s_ContextPopupContent;
			}
		}

		private static GUIContent[] s_WaitSpins;

		private static Material s_HandleWireMaterial2D;

		public static Material handleWireMaterial2D
		{
			get
			{
				if( !s_HandleWireMaterial2D )
				{
					s_HandleWireMaterial2D = (Material)EditorGUIUtility.LoadRequired("SceneView/2DHandleLines.mat");
				}
				return s_HandleWireMaterial2D;
			}
		}

		private delegate string DelegateSearchField(Rect position, string text);
		private delegate string DelegateToolbarSearhField(Rect position, string[] searchModes, ref int searchMode, string text);

		private static readonly DelegateSearchField _SearchField;
		private static readonly DelegateToolbarSearhField _ToolbarSearchField;

#if ARBOR_DLL || !UNITY_5_6_OR_NEWER
		private delegate bool DelegateButtonMouseDown(Rect position, GUIContent content, FocusType focusType, GUIStyle style);
		private static readonly DelegateButtonMouseDown _ButtonMouseDown;
#endif

#if ARBOR_DLL
		private static readonly System.Version s_UnityVersion;
#endif

		private static readonly MethodInfo _DoTextFieldMethod;
		private static readonly FieldInfo _RecycledEditorField;

		private delegate void DelegateClearGlobalCache();
		private static readonly DelegateClearGlobalCache _DelegateClearGlobalCache;

		private static Dictionary<System.Type, System.Type[]> s_DelegateArguments = new Dictionary<System.Type, System.Type[]>();

		static System.Type[] GetDelegateArguments(System.Type delegateType)
		{
			MethodInfo invokeMethod = delegateType.GetMethod("Invoke", BindingFlags.Instance | BindingFlags.Public);

			System.Type[] arg = null;
			if (!s_DelegateArguments.TryGetValue(delegateType, out arg))
			{
				List<System.Type> arguments = new List<System.Type>();
				foreach (ParameterInfo parameterInfo in invokeMethod.GetParameters())
				{
					arguments.Add(parameterInfo.ParameterType);
				}

				arg = arguments.ToArray();

				s_DelegateArguments.Add(delegateType, arg);
			}

			return arg;
		}

		public static TDelegate GetDelegate<TDelegate>(System.Type classType, string name, BindingFlags bindingAttr) where TDelegate : class
		{
			System.Type delegateType = typeof(TDelegate);

			MethodInfo method = classType.GetMethod(name, bindingAttr, null, GetDelegateArguments(delegateType), null);

			if (method == null)
			{
				return null;
			}

			return System.Delegate.CreateDelegate(delegateType, method) as TDelegate;
		}

		static EditorGUITools()
		{
			System.Type editorGUIType = typeof(EditorGUI);

			_SearchField = GetDelegate<DelegateSearchField>(editorGUIType, "SearchField", BindingFlags.Static | BindingFlags.NonPublic);
			_ToolbarSearchField = GetDelegate<DelegateToolbarSearhField>(editorGUIType, "ToolbarSearchField", BindingFlags.Static | BindingFlags.NonPublic);

#if ARBOR_DLL
			s_UnityVersion = UnityEditorInternal.InternalEditorUtility.GetUnityVersion();

			_ButtonMouseDown = GetDelegate<DelegateButtonMouseDown>( editorGUIType, "ButtonMouseDown", BindingFlags.Static | BindingFlags.NonPublic );
			if( _ButtonMouseDown == null )
			{
				_ButtonMouseDown = GetDelegate<DelegateButtonMouseDown>( editorGUIType,"DropdownButton", BindingFlags.Static | BindingFlags.Public );
			}
#elif !UNITY_5_6_OR_NEWER
			_ButtonMouseDown = GetDelegate<DelegateButtonMouseDown>(editorGUIType, "ButtonMouseDown", BindingFlags.Static | BindingFlags.NonPublic);
#endif

			_DoTextFieldMethod = editorGUIType.GetMethod("DoTextField", BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public);
			_RecycledEditorField = editorGUIType.GetField("s_RecycledEditor", BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public);

			Assembly unityEditorAssembly = Assembly.Load("UnityEditor.dll");
			System.Type scriptAttributeUtilityType = unityEditorAssembly.GetType("UnityEditor.ScriptAttributeUtility");
			_DelegateClearGlobalCache = GetDelegate<DelegateClearGlobalCache>(scriptAttributeUtilityType, "ClearGlobalCache", BindingFlags.Static | BindingFlags.NonPublic);

			s_WaitSpins = new GUIContent[12];
			for (int index = 0; index < 12; ++index)
			{
				s_WaitSpins[index] = new GUIContent(EditorGUIUtility.IconContent("WaitSpin" + index.ToString("00")));
			}
		}

		public static string SearchField(Rect position, string text)
		{
			if (_SearchField == null)
			{
				return text;
			}
			return _SearchField(position, text);
		}

		public static string ToolbarSearchField(Rect position, string[] searchModes, ref int searchMode, string text)
		{
			if (_ToolbarSearchField == null)
			{
				return text;
			}

			return _ToolbarSearchField(position, searchModes, ref searchMode, text);
		}

		public static bool ButtonMouseDown(Rect position, GUIContent content, FocusType focusType, GUIStyle style)
		{
#if !ARBOR_DLL && UNITY_5_6_OR_NEWER
			return EditorGUI.DropdownButton( position, content, focusType, style );
#else
			if (_ButtonMouseDown != null)
			{
				return _ButtonMouseDown(position, content, focusType, style);
			}
			return false;
#endif
		}

		private static System.Type GetScriptTypeFromProperty(SerializedProperty property)
		{
			SerializedProperty property1 = property.serializedObject.FindProperty("m_Script");
			if (property1 == null)
			{
				return null;
			}
			MonoScript monoScript = property1.objectReferenceValue as MonoScript;
			if (monoScript == null)
			{
				return null;
			}
			return monoScript.GetClass();
		}

		private static bool IsArrayOrList(System.Type listType)
		{
			return listType.IsArray || listType.IsGenericType && listType.GetGenericTypeDefinition() == typeof(List<>);
		}

		private static System.Type GetArrayOrListElementType(System.Type listType)
		{
			if (listType.IsArray)
			{
				return listType.GetElementType();
			}
			if (listType.IsGenericType && listType.GetGenericTypeDefinition() == typeof(List<>))
			{
				return listType.GetGenericArguments()[0];
			}
			return null;
		}

		class FieldList
		{
			public System.Type type
			{
				get;
				private set;
			}

			public class Field
			{
				public System.Type fieldType
				{
					get;
					private set;
				}
				public FieldInfo fieldInfo
				{
					get;
					private set;
				}

				public Field(System.Type type, FieldInfo fieldInfo)
				{
					this.fieldType = type;
					this.fieldInfo = fieldInfo;
				}
			}

			private Dictionary<string, Field> _Fields = new Dictionary<string, Field>();

			public FieldList(System.Type type)
			{
				this.type = type;
			}

			private static readonly System.Text.RegularExpressions.Regex s_ToFieldPathRegex = new System.Text.RegularExpressions.Regex(@"Array.data\[[0-9]*\]");
			private const string kArrayName = "[Array]";

			public static string ToFieldPath(string propertyPath)
			{
				return s_ToFieldPathRegex.Replace(propertyPath, kArrayName);
			}

			private static Field GetFieldInfoFromPropertyPath(System.Type typeFromProperty, string path)
			{
				FieldInfo fieldInfo1 = null;
				System.Type type = typeFromProperty;
				string[] strArray = path.Split('.');
				int length = strArray.Length;
				for (int index2 = 0; index2 < length; ++index2)
				{
					string name = strArray[index2];
					if (name == kArrayName)
					{
						if (IsArrayOrList(type))
						{
							type = GetArrayOrListElementType(type);
						}
					}
					else
					{
						FieldInfo fieldInfo2 = null;
						for (System.Type type1 = type; fieldInfo2 == null && type1 != null; type1 = type1.BaseType)
						{
							fieldInfo2 = type1.GetField(name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
						}
						if (fieldInfo2 == null)
						{
							type = null;
							return null;
						}
						fieldInfo1 = fieldInfo2;
						type = fieldInfo1.FieldType;
					}
				}

				if (fieldInfo1 == null)
				{
					return null;
				}

				return new Field(type, fieldInfo1);
			}

			public Field GetField(string path)
			{
				Field field = null;
				if (!_Fields.TryGetValue(path, out field))
				{
					field = GetFieldInfoFromPropertyPath(type, path);
					if (field != null)
					{
						_Fields.Add(path, field);
					}
				}

				return field;
			}

			public Field GetField(SerializedProperty property)
			{
				string path = ToFieldPath(property.propertyPath);
				return GetField(path);
			}
		}

		private static Dictionary<System.Type, FieldList> _FieldLists = new Dictionary<System.Type, FieldList>();

		private static FieldList GetFieldList(System.Type typeFromProperty)
		{
			FieldList fieldList = null;
			if (!_FieldLists.TryGetValue(typeFromProperty, out fieldList))
			{
				fieldList = new FieldList(typeFromProperty);
				_FieldLists.Add(typeFromProperty, fieldList);
			}
			return fieldList;
		}

		static FieldInfo GetFieldInfo(System.Type type, string path, out System.Type fieldType)
		{
			FieldList fieldList = GetFieldList(type);

			FieldList.Field field = fieldList.GetField(path);
			if (field != null)
			{
				fieldType = field.fieldType;
				return field.fieldInfo;
			}

			fieldType = null;
			return null;
		}

		public static FieldInfo GetFieldInfoFromProperty(SerializedProperty property, out System.Type fieldType)
		{
			System.Type typeFromProperty = GetScriptTypeFromProperty(property);
			if (typeFromProperty != null)
			{
				FieldList fieldList = GetFieldList(typeFromProperty);

				FieldList.Field field = fieldList.GetField(property);
				if (field != null)
				{
					fieldType = field.fieldType;
					return field.fieldInfo;
				}
			}
			fieldType = null;
			return null;
		}

		public static string DelayedTextField(Rect position, string value, GUIStyle style)
		{
			return EditorGUI.DelayedTextField(position, value, style);
		}

		private static Dictionary<string, GUIContent> _TextContents = new Dictionary<string, GUIContent>();
		public static GUIContent GetTextContent(string key)
		{
			if (string.IsNullOrEmpty(key))
			{
				return GUIContent.none;
			}

			GUIContent content = null;
			if (!_TextContents.TryGetValue(key, out content))
			{
				content = new GUIContent(key);
				_TextContents.Add(key, content);
			}

			return content;
		}

		private static Dictionary<Object, GUIContent> _ThumbnailContents = new Dictionary<Object, GUIContent>();
		public static GUIContent GetThumbnailContent(Object obj)
		{
			GUIContent content = null;
			if (!_ThumbnailContents.TryGetValue(obj, out content))
			{
				content = new GUIContent(AssetPreview.GetMiniThumbnail(obj));
				_ThumbnailContents.Add(obj, content);
			}

			return content;
		}

		private static Dictionary<string, string> _NicifyVariableNames = new Dictionary<string, string>();
		public static string NicifyVariableName(string name)
		{
			string value;
			if (!_NicifyVariableNames.TryGetValue(name, out value))
			{
				value = ObjectNames.NicifyVariableName(name);
				_NicifyVariableNames.Add(name, value);
			}

			return value;
		}

		public static void DrawArraw(Vector2 position, Vector2 direction, Color color, float width)
		{
			Vector2 cross = Vector3.Cross(direction, Vector3.forward).normalized;

			Vector3[] vector3Array = new Vector3[4];
			vector3Array[0] = position;
			vector3Array[1] = position - direction * width + cross * width * 0.5f;
			vector3Array[2] = position - direction * width - cross * width * 0.5f;
			vector3Array[3] = vector3Array[0];

			Shader.SetGlobalColor("_HandleColor", color);
			handleWireMaterial2D.SetPass(0);
			GL.Begin(4);
			GL.Color(color);
			GL.Vertex(vector3Array[0]);
			GL.Vertex(vector3Array[1]);
			GL.Vertex(vector3Array[2]);
			GL.End();
		}

		public static void BezierArrow(Bezier2D bezier, Vector2 offset, Texture2D tex, Color color, float width, float arrowWidth)
		{
			Vector2 startPosition = bezier.startPosition + offset;
			Vector2 startControl = bezier.startControl + offset;
			Vector2 endPosition = bezier.endPosition + offset;
			Vector2 endControl = bezier.endControl + offset;

			Vector2 v = (endPosition - endControl).normalized * arrowWidth;

			Handles.DrawBezier(startPosition, endPosition - v, startControl, endControl - v, color, tex, width);

			DrawArraw(endPosition, v.normalized, color, arrowWidth);
		}

		public static void DrawBezier(Bezier2D bezier, Vector2 offset, Texture2D tex, Color color, float width)
		{
			Vector2 startPosition = bezier.startPosition + offset;
			Vector2 startControl = bezier.startControl + offset;
			Vector2 endPosition = bezier.endPosition + offset;
			Vector2 endControl = bezier.endControl + offset;

			Handles.DrawBezier(startPosition, endPosition, startControl, endControl, color, tex, width);
		}

		static void SetDiscSectionPoints(Vector3[] dest, int count, Vector3 normal, Vector3 from, float angle)
		{
			from.Normalize();
			Quaternion quaternion = Quaternion.AngleAxis(angle / (float)count, normal);
			Vector3 vector3 = from;
			for (int index = 0; index < count; ++index)
			{
				dest[index] = vector3;
				vector3 = quaternion * vector3;
			}
		}

		private static Vector3[] s_DiscDest = null;
		private const int s_DiscVertexNum = 4;

		static void DrawSolidDisc(Vector3 center, float radius)
		{
			using (new ProfilerScope("DrawSolidDisc"))
			{
				if (s_DiscDest == null)
				{
					s_DiscDest = new Vector3[s_DiscVertexNum];

					Vector3 normal = Vector3.forward;
					Vector3 from = Vector3.Cross(normal, Vector3.up);
					if ((double)from.sqrMagnitude < 1.0 / 1000.0)
						from = Vector3.Cross(normal, Vector3.right);
					SetDiscSectionPoints(s_DiscDest, s_DiscDest.Length, normal, from, 360.0f);
				}

				Shader.SetGlobalColor("_HandleColor", Handles.color * new Color(1f, 1f, 1f, 0.5f));
				Shader.SetGlobalFloat("_HandleSize", 1f);

				handleWireMaterial2D.SetPass(0);

				GL.PushMatrix();
				GL.MultMatrix(Handles.matrix);
				GL.Begin(GL.TRIANGLES);
				GL.Color(Handles.color);

				Vector3 preDest = center + s_DiscDest[0] * radius;
				int destCount = s_DiscDest.Length;
				for (int index = 1; index < destCount; ++index)
				{
					Vector3 currentDest = center + s_DiscDest[index] * radius;

					GL.Vertex(center);
					GL.Vertex(preDest);
					GL.Vertex(currentDest);

					preDest = currentDest;
				}

				GL.Vertex(center);
				GL.Vertex(preDest);
				GL.Vertex(center + s_DiscDest[0] * radius);

				GL.End();
				GL.PopMatrix();
			}
		}

		static void GenerateSolidDisc(Vector3 center, float radius, Color color, List<Vector3> vertices, List<Color> colors, List<int> triangles)
		{
			using (new ProfilerScope("GenerateSolidDisc"))
			{
				if (s_DiscDest == null)
				{
					s_DiscDest = new Vector3[s_DiscVertexNum];

					Vector3 normal = Vector3.forward;
					Vector3 from = Vector3.Cross(normal, Vector3.up);
					if ((double)from.sqrMagnitude < 1.0 / 1000.0)
						from = Vector3.Cross(normal, Vector3.right);
					SetDiscSectionPoints(s_DiscDest, s_DiscDest.Length, normal, from, 360.0f);
				}

				int centerVertexIndex = vertices.Count;
				vertices.Add(center);
				colors.Add(color);

				Vector3 preDest = center + s_DiscDest[0] * radius;
				int destCount = s_DiscDest.Length;
				for (int index = 1; index < destCount; ++index)
				{
					Vector3 currentDest = center + s_DiscDest[index] * radius;

					int vertCount = vertices.Count;

					vertices.Add(preDest);
					vertices.Add(currentDest);

					colors.Add(color);
					colors.Add(color);

					triangles.Add(centerVertexIndex);
					triangles.Add(vertCount + 0);
					triangles.Add(vertCount + 1);

					preDest = currentDest;
				}

				triangles.Add(centerVertexIndex);
				triangles.Add(vertices.Count - 1);
				triangles.Add(centerVertexIndex + 1);
			}
		}

		static readonly List<Vector3> m_GenerateMeshVertices = new List<Vector3>();
		static readonly List<Color> m_GenerateMeshColors = new List<Color>();
		static readonly List<int> m_GenerateMeshTriangles = new List<int>();
		
		public static void GenerateBezierDottedMesh(Bezier2D bezier, Color color, float radius, float space, Vector2 shadowOffset, Color shadowColor, Mesh mesh)
		{
			using (new ProfilerScope("GenerateBezierDottedMesh"))
			{
				mesh.Clear();

				int vertexPerDot = s_DiscVertexNum * 3 * 2;
				int maxVertex = 65000;
				int maxDotCount = maxVertex / vertexPerDot;

				int dotCount = (int)(bezier.length / space);
				if (dotCount >= maxDotCount)
				{
					space = bezier.length / maxDotCount;
				}

				for (float l = 0.0f; l <= bezier.length; l += space)
				{
					float tl = l / bezier.length;
					Vector2 point = bezier.GetLinearPoint(tl);

					GenerateSolidDisc(point + shadowOffset, radius, shadowColor, m_GenerateMeshVertices, m_GenerateMeshColors, m_GenerateMeshTriangles);
					GenerateSolidDisc(point, radius, color, m_GenerateMeshVertices, m_GenerateMeshColors, m_GenerateMeshTriangles);
				}

				mesh.SetVertices(m_GenerateMeshVertices);
				mesh.SetColors(m_GenerateMeshColors);
				mesh.SetTriangles(m_GenerateMeshTriangles,0);

				m_GenerateMeshVertices.Clear();
				m_GenerateMeshColors.Clear();
				m_GenerateMeshTriangles.Clear();
			}
		}
		
		static readonly Color s_IntColor = Color.cyan;
		static readonly Color s_BoolColor = Color.red;
		static readonly Color s_StringColor = Color.magenta;
		static readonly Color s_FloatColor = new Color(0.5f, 1, 0.5f);
		static readonly Color s_Vector2Color = new Color(1, 0.5f, 0);
		static readonly Color s_Vector3Color = Color.yellow;
		static readonly Color s_QuaternionColor = new Color(0.5f, 1.0f, 0);
		static readonly Color s_UnityObjectColor = new Color(0, 0.5f, 1);
		static readonly Color s_ObjectColor = Color.white;
		static readonly Color s_OtherColor = new Color(0.5f, 0, 1);

		public static Color GetTypeColor(System.Type type)
		{
			if (type == typeof(int) || type == typeof(long) )
			{
				return s_IntColor;
			}
			else if (type == typeof(bool))
			{
				return s_BoolColor;
			}
			else if (type == typeof(string))
			{
				return s_StringColor;
			}
			else if (type == typeof(float))
			{
				return s_FloatColor;
			}
			else if (type == typeof(Vector2))
			{
				return s_Vector2Color;
			}
			else if (type == typeof(Vector3))
			{
				return s_Vector3Color;
			}
			else if (type == typeof(Quaternion))
			{
				return s_QuaternionColor;
			}
			else if (typeof(Object).IsAssignableFrom(type))
			{
				return s_UnityObjectColor;
			}
			else if (type == typeof(object))
			{
				return s_ObjectColor;
			}

			return s_OtherColor;
		}
		
		public static void BezierDotted(Bezier2D bezier, Vector2 offset, Color color, float radius, float space)
		{
			Color cachedColor = Handles.color;
			Handles.color = color;

			for (float l = 0.0f; l <= bezier.length; l += space)
			{
				float tl = l / bezier.length;
				DrawSolidDisc(bezier.GetLinearPoint(tl) + offset, radius);
			}

			Handles.color = cachedColor;
		}

		public static void DrawMesh(Mesh mesh)
		{
			if (Event.current.type != EventType.Repaint)
				return;

			using (new ProfilerScope("EditorGUITools.DrawMesh"))
			{
				Shader.SetGlobalColor("_HandleColor", Handles.color * new Color(1f, 1f, 1f, 0.5f));
				Shader.SetGlobalFloat("_HandleSize", 1f);

				handleWireMaterial2D.SetPass(0);

				Graphics.DrawMeshNow(mesh, Handles.matrix);
			}
		}

		public static void DrawGridBackground(Rect position)
		{
			if (Event.current.type == EventType.Repaint)
			{
				Styles.graphBackground.Draw(position, false, false, false, false);
			}
		}

		public static void DrawSeparator()
		{
			Rect rect = GUILayoutUtility.GetRect(0.0f, 1.0f);

			if (Event.current.type == EventType.Repaint)
			{
				Texture tex = Styles.titlebar.normal.background;

				GUI.DrawTextureWithTexCoords(rect, tex, new Rect(0, 1.0f, 1.0f, 1.0f - 1.0f / (float)tex.height));
			}
		}
		
		private static readonly Color _GridMinorColorDark = new Color(0.5f, 0.5f, 0.5f, 0.18f);
		private static readonly Color _GridMajorColorDark = new Color(0.5f, 0.5f, 0.5f, 0.28f);

		public static Color gridMinorColor
		{
			get
			{
				return _GridMinorColorDark;
			}
		}

		public static Color gridMajorColor
		{
			get
			{
				return _GridMajorColorDark;
			}
		}

		public static Node[] DuplicateNodes(ArborFSMInternal stateMachine, Vector2 position, Node[] sourceNodes)
		{
			List<Node> duplicateNodes = new List<Node>();

			Vector2 minPosition = new Vector2(float.MaxValue, float.MaxValue);

			foreach (Node sourceNode in sourceNodes)
			{
				minPosition.x = Mathf.Min(sourceNode.position.x, minPosition.x);
				minPosition.y = Mathf.Min(sourceNode.position.y, minPosition.y);
			}

			position -= minPosition;

			foreach (Node sourceNode in sourceNodes)
			{
				if (sourceNode is State)
				{
					State sourceState = sourceNode as State;

					bool duplicatable = true;

					int behaviourCount = sourceState.behaviourCount;
					for (int behaviourIndex = 0; behaviourIndex < behaviourCount; behaviourIndex++)
					{
						StateBehaviour sourceBehaviour = sourceState.GetBehaviourFromIndex(behaviourIndex);
						if (sourceBehaviour == null)
						{
							duplicatable = false;
							break;
						}
					}

					if (duplicatable)
					{
						State state = stateMachine.CreateState(sourceState.resident);

						if (state != null)
						{
							state.name = sourceState.name;
							state.position = sourceState.position;
							state.position.x += position.x;
							state.position.y += position.y;
							state.showComment = sourceState.showComment;
							state.nodeComment = sourceState.nodeComment;

							for (int behaviourIndex = 0; behaviourIndex < behaviourCount; behaviourIndex++)
							{
								StateBehaviour sourceBehaviour = sourceState.GetBehaviourFromIndex(behaviourIndex);
								if (sourceBehaviour != null)
								{
									StateBehaviour behaviour = state.AddBehaviour(sourceBehaviour.GetType());

									if (behaviour != null)
									{
										CopyBehaviour(sourceBehaviour, behaviour, true);
									}
								}
							}

							duplicateNodes.Add(state);
						}
					}
					else
					{
						Debug.LogError(Localization.GetWord("MissingDuplicateError"));
					}
				}
				else if (sourceNode is CommentNode)
				{
					CommentNode sourceComment = sourceNode as CommentNode;
					CommentNode comment = stateMachine.CreateComment();

					if (comment != null)
					{
						comment.comment = sourceComment.comment;
						comment.position = sourceComment.position;
						comment.position.x += position.x;
						comment.position.y += position.y;
						comment.showComment = sourceComment.showComment;
						comment.nodeComment = sourceComment.nodeComment;

						duplicateNodes.Add(comment);
					}
				}
				else if (sourceNode is CalculatorNode)
				{
					CalculatorNode sourceCalculator = sourceNode as CalculatorNode;
					if (sourceCalculator.calculator != null)
					{
						CalculatorNode calculator = stateMachine.CreateCalculator(sourceCalculator.calculator.GetType());

						if (calculator != null)
						{
							calculator.position = sourceCalculator.position;
							calculator.position.x += position.x;
							calculator.position.y += position.y;
							calculator.showComment = sourceCalculator.showComment;
							calculator.nodeComment = sourceCalculator.nodeComment;

							CopyCalculator(sourceCalculator.calculator, calculator.calculator, true);

							duplicateNodes.Add(calculator);
						}
					}
					else
					{
						Debug.LogError(Localization.GetWord("MissingDuplicateError"));
					}
				}
				else if (sourceNode is GroupNode)
				{
					GroupNode sourceGroup = sourceNode as GroupNode;
					GroupNode group = stateMachine.CreateGroup();

					if (group != null)
					{
						group.name = sourceGroup.name;
						group.position = sourceGroup.position;
						group.position.x += position.x;
						group.position.y += position.y;
						group.showComment = sourceGroup.showComment;
						group.nodeComment = sourceGroup.nodeComment;

						duplicateNodes.Add(group);
					}
				}
			}

			return duplicateNodes.ToArray();
		}

		public static void MoveCalculator(CalculatorNode calculatorNode, Calculator sourceCalculator)
		{
			bool cachedEnabled = ComponentUtility.enabled;
			ComponentUtility.enabled = false;

			Calculator destCalculator = calculatorNode.CreateCalculator(sourceCalculator.GetType());

			if (destCalculator != null)
			{
				CopyCalculator(sourceCalculator, destCalculator, false);
			}

			ComponentUtility.enabled = cachedEnabled;
		}

		public static Rect GUIToScreenRect(Rect guiRect)
		{
			Vector2 vector2 = GUIUtility.GUIToScreenPoint(new Vector2(guiRect.x, guiRect.y));
			guiRect.x = vector2.x;
			guiRect.y = vector2.y;
			return guiRect;
		}

		public static void MoveBehaviour(State state, StateBehaviour sourceBehaviour)
		{
			bool cachedEnabled = ComponentUtility.enabled;
			ComponentUtility.enabled = false;

			StateBehaviour destBehaviour = state.AddBehaviour(sourceBehaviour.GetType());

			if (destBehaviour != null)
			{
				EditorGUITools.CopyBehaviour(sourceBehaviour, destBehaviour, false);
			}

			ComponentUtility.enabled = cachedEnabled;
		}

		public static bool CheckLoop(Object inputObj, Object outputObj)
		{
			if (outputObj is StateBehaviour)
			{
				return false;
			}

			if (inputObj == outputObj)
			{
				return true;
			}

			bool check = false;

			NodeBehaviour nodeBehaviour = outputObj as NodeBehaviour;
			if (nodeBehaviour != null)
			{
				int slotCount = nodeBehaviour.calculatorSlotFieldCount;
				for (int slotIndex = 0; slotIndex < slotCount; slotIndex++)
				{
					CalculatorSlotField slotInfo = nodeBehaviour.GetCalculatorSlotField(slotIndex);
					InputSlot s = slotInfo.slot as InputSlot;
					if (s != null)
					{
						CalculatorBranch branch = s.branch;
						if (branch != null)
						{
							if (CheckLoop(inputObj, branch.outBehaviour))
							{
								check = true;
								break;
							}
						}
					}
				}
			}

			return check;
		}

		public static CalculatorBranch Connect(ArborFSMInternal stateMachine, Object inputObj, InputSlot inputSlot, Object outputObj, OutputSlot outputSlot)
		{
			if (CheckLoop(inputObj, outputObj))
			{
				Debug.LogError("Calculator node has become an infinite loop.");
				return null;
			}

			CalculatorBranch branch = stateMachine.CreateCalculatorBranch();

			branch.SetBehaviour(inputObj, outputObj);

			Undo.RecordObjects(new Object[] { inputObj, outputObj }, "Connect Calculator");

			outputSlot.stateMachine = stateMachine;
			outputSlot.branchIDs.Add(branch.branchID);

			inputSlot.stateMachine = stateMachine;
			inputSlot.branchID = branch.branchID;

			EditorUtility.SetDirty(inputObj);
			EditorUtility.SetDirty(outputObj);

			return branch;
		}

		static void DisconnectCalculatorBranch(ArborFSMInternal stateMachine, Object obj)
		{
			Undo.RecordObject(obj, "Copy Calculator");

			stateMachine.DisconnectCalculatorBranch(obj);

			EditorUtility.SetDirty(obj);
		}

		static void ReconnectCalculatorBranch(ArborFSMInternal stateMachine, Object obj)
		{
			NodeBehaviour nodeBehaviour = obj as NodeBehaviour;
			if (nodeBehaviour == null)
			{
				return;
			}

			int slotCount = nodeBehaviour.calculatorSlotFieldCount;
			for (int slotIndex = 0; slotIndex < slotCount; slotIndex++)
			{
				CalculatorSlotField slotInfo = nodeBehaviour.GetCalculatorSlotField(slotIndex);
				CalculatorSlot s = slotInfo.slot;

				if (s is InputSlot)
				{
					InputSlot inputSlot = s as InputSlot;

					if (inputSlot.stateMachine != null && stateMachine != inputSlot.stateMachine)
					{
						inputSlot.stateMachine = null;
						inputSlot.branchID = 0;
					}
					else
					{
						CalculatorBranch branch = inputSlot.branch;
						if (branch != null)
						{
							Object inBehaviour = branch.inBehaviour;
							Object outBehaviour = branch.outBehaviour;
							Bezier2D bezier = branch.lineBezier;
							OutputSlot outputSlot = branch.outputSlot;

							if (inBehaviour == obj)
							{
								stateMachine.DeleteCalculatorBranch(branch);
							}

							branch = Connect(stateMachine, obj, inputSlot, outBehaviour, outputSlot);
							if (branch != null)
							{
								branch.lineBezier = new Bezier2D(bezier);
								branch.enabled = true;
							}
						}
					}
				}
				else if (s is OutputSlot)
				{
					OutputSlot outputSlot = s as OutputSlot;
					outputSlot.branchIDs.Clear();
				}
			}
		}

		public static void CopyBehaviour(StateBehaviour source, StateBehaviour dest, bool checkStateLink)
		{
			if (dest == null)
			{
				return;
			}

			ArborFSMInternal stateMachine = dest.stateMachine;
			int stateID = dest.stateID;
			bool expanded = dest.expanded;

			DisconnectCalculatorBranch(stateMachine, dest);

			EditorUtility.CopySerialized(source, dest);

			dest.expanded = expanded;

			SerializedObject serializedObject = new SerializedObject(dest);

			serializedObject.Update();

			SerializedProperty stateMachineProperty = serializedObject.FindProperty("_StateMachine");
			stateMachineProperty.objectReferenceValue = stateMachine;

			SerializedProperty stateIDProperty = serializedObject.FindProperty("_NodeID");
			stateIDProperty.intValue = stateID;

			serializedObject.ApplyModifiedProperties();
			serializedObject.Dispose();

			if (checkStateLink)
			{
				Undo.RecordObject(dest, "Copy Behaviour");

				EachField<StateLink>.Find(dest, dest.GetType(), (s) =>
				{
					if (stateMachine != source.stateMachine || stateMachine.GetStateFromID(s.stateID) == null)
					{
						s.stateID = 0;
					}
				});

				ReconnectCalculatorBranch(stateMachine, dest);

				EditorUtility.SetDirty(dest);
			}
		}

		static void CopyCalculator(Calculator source, Calculator dest, bool checkBranch)
		{
			if (dest == null)
			{
				return;
			}

			ArborFSMInternal stateMachine = dest.stateMachine;
			int calculatorID = dest.calculatorID;

			DisconnectCalculatorBranch(stateMachine, dest);

			EditorUtility.CopySerialized(source, dest);

			SerializedObject serializedObject = new SerializedObject(dest);

			serializedObject.Update();

			SerializedProperty stateMachineProperty = serializedObject.FindProperty("_StateMachine");
			stateMachineProperty.objectReferenceValue = stateMachine;

			SerializedProperty stateIDProperty = serializedObject.FindProperty("_NodeID");
			stateIDProperty.intValue = calculatorID;

			serializedObject.ApplyModifiedProperties();
			serializedObject.Dispose();

			if (checkBranch)
			{
				Undo.RecordObject(dest, "Copy Calculator");

				ReconnectCalculatorBranch(stateMachine, dest);

				EditorUtility.SetDirty(dest);
			}
		}

		struct ContextMenuElement
		{
			public string menuItem;
			public MethodInfo method;
			public MethodInfo validateMethod;
			public int index;
			public int priority;
		}

		class CompareMenuIndex : IComparer
		{
			int IComparer.Compare(object xo, object yo)
			{
				ContextMenuElement element1 = (ContextMenuElement)xo;
				ContextMenuElement element2 = (ContextMenuElement)yo;
				if (element1.priority != element2.priority)
					return element1.priority.CompareTo(element2.priority);
				return element1.index.CompareTo(element2.index);
			}
		}

		static ContextMenuElement[] ExtractEditorMenuItem(System.Type behaviourType)
		{
			Dictionary<string, ContextMenuElement> dic = new Dictionary<string, ContextMenuElement>();

			foreach (BehaviourMenuItemUtilitty.Element element in BehaviourMenuItemUtilitty.elements)
			{
				if (element.menuItem.type == behaviourType || behaviourType.IsSubclassOf(element.menuItem.type))
				{
					ContextMenuElement menuEelement = dic.ContainsKey(element.menuItem.menuItem) ? dic[element.menuItem.menuItem] : new ContextMenuElement();
					menuEelement.menuItem = element.menuItem.menuItem;
					if (element.menuItem.validate)
					{
						menuEelement.validateMethod = element.method;
					}
					else
					{
						menuEelement.method = element.method;
						menuEelement.index = element.index;
						menuEelement.priority = element.menuItem.priority;
					}
					dic[element.menuItem.menuItem] = menuEelement;
				}
			}

			ContextMenuElement[] elements = dic.Values.ToArray();
			System.Array.Sort(elements, new CompareMenuIndex());

			return elements;
		}

		static ContextMenuElement[] ExtractContextMenu(System.Type type)
		{
			Dictionary<string, ContextMenuElement> dic = new Dictionary<string, ContextMenuElement>();

			MethodInfo[] methods = type.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
			for (int index = 0; index < methods.Length; ++index)
			{
				MethodInfo method = methods[index];
				foreach (System.Attribute attr in AttributeHelper.GetAttributes(method))
				{
					ContextMenu contextMenu = attr as ContextMenu;
					if (contextMenu != null)
					{
						ContextMenuElement element = dic.ContainsKey(contextMenu.menuItem) ? dic[contextMenu.menuItem] : new ContextMenuElement();
						element.menuItem = contextMenu.menuItem;
						element.method = method;
						dic[contextMenu.menuItem] = element;
					}
				}
			}

			return dic.Values.ToArray();
		}

		static void ExecuteContextMenu(object obj)
		{
			KeyValuePair<object, ContextMenuElement> pair = (KeyValuePair<object, ContextMenuElement>)obj;
			object target = pair.Key;
			ContextMenuElement contextMenu = pair.Value;

			contextMenu.method.Invoke(target, null);
		}

		static void ExecuteEditorContextMenu(object obj)
		{
			KeyValuePair<MenuCommand, ContextMenuElement> pair = (KeyValuePair<MenuCommand, ContextMenuElement>)obj;
			MenuCommand command = pair.Key;
			ContextMenuElement contextMenu = pair.Value;

			contextMenu.method.Invoke(null, new object[] { command });
		}

		public static void AddContextMenu(GenericMenu menu, Object obj)
		{
			ContextMenuElement[] editorContextMenus = ExtractEditorMenuItem(obj.GetType());
			ContextMenuElement[] contextMenus = ExtractContextMenu(obj.GetType());

			if (editorContextMenus.Length > 0 || contextMenus.Length > 0)
			{
				menu.AddSeparator("");
				if (editorContextMenus.Length > 0)
				{
					MenuCommand command = new MenuCommand(obj);
					foreach (ContextMenuElement element in editorContextMenus)
					{
						bool enable = true;
						if (element.validateMethod != null)
						{
							enable = (bool)element.validateMethod.Invoke(null, new object[] { command });
						}
						if (enable)
						{
							menu.AddItem(EditorGUITools.GetTextContent(element.menuItem), false, ExecuteEditorContextMenu, new KeyValuePair<MenuCommand, ContextMenuElement>(command, element));
						}
						else
						{
							menu.AddDisabledItem(EditorGUITools.GetTextContent(element.menuItem));
						}
					}
				}
				if (contextMenus.Length > 0)
				{
					foreach (ContextMenuElement element in contextMenus)
					{
						menu.AddItem(EditorGUITools.GetTextContent(element.menuItem), false, ExecuteContextMenu, new KeyValuePair<object, ContextMenuElement>(obj, element));
					}
				}
			}
		}

		public class Pivot
		{
			public Vector2 position;
			public Vector2 normal;

			public Pivot(Vector2 position, Vector2 normal)
			{
				this.position = position;
				this.normal = normal;
			}
		}

		public const float kStateBezierTargetOffsetY = 16.0f;
		public const float kBezierTangent = 50f;
		public static readonly Vector2 kBezierTangentOffset = new Vector2(kBezierTangent, 0.0f);

		public static void HelpButton(Rect position, string url, string tooltip)
		{
			GUIContent content = new GUIContent(s_HelpButtonContent);
			content.tooltip = tooltip;

			if (GUI.Button(position, content, Styles.titlebarText))
			{
				Help.BrowseURL(url);
			}
		}

		public static void HelpButton(string url, string tooltip)
		{
			Rect position = GUILayoutUtility.GetRect(0.0f, 20.0f);

			position.x += position.width - 18.0f;
			position.width = 16.0f;

			HelpButton(position, url, tooltip);
		}

		public static Rect GetPopupRect(Rect position)
		{
			position.xMin = position.xMax - 13f;
			return position;
		}

		public static Rect SubtractPopupWidth(Rect position)
		{
			position.width -= 14f;
			return position;
		}

		public static Rect PrefixLabel(Rect totalPosition, GUIContent label)
		{
			Rect labelPosition = new Rect(totalPosition.x + EditorGUI.indentLevel * 15f, totalPosition.y, EditorGUIUtility.labelWidth - EditorGUI.indentLevel * 15f, EditorGUIUtility.singleLineHeight);
			Rect rect = new Rect(totalPosition.x + EditorGUIUtility.labelWidth, totalPosition.y, totalPosition.width - EditorGUIUtility.labelWidth, totalPosition.height);
			EditorGUI.HandlePrefixLabel(totalPosition, labelPosition, label, 0, EditorStyles.label);
			return rect;
		}

		public static Quaternion RotationField(Rect position, GUIContent label, Quaternion quaternion)
		{
			EditorGUI.BeginChangeCheck();
			Vector3 eulerValue = EditorGUI.Vector3Field(position, label, quaternion.eulerAngles);
			if (EditorGUI.EndChangeCheck())
			{
				quaternion = Quaternion.Euler(eulerValue);
			}

			return quaternion;
		}

		public static Quaternion RotationField(GUIContent label, Quaternion quaternion, params GUILayoutOption[] options)
		{
			Rect rect = GUILayoutUtility.GetRect(0.0f, 0.0f, options);
			return RotationField(rect, label, quaternion);
		}

		static System.Text.StringBuilder s_ObjectPathBuilder = new System.Text.StringBuilder(1000);
		const string kPropertyArrayPrefix = ".Array.data[";

		static string GetPropertyPath(SerializedProperty property)
		{
			string path = property.propertyPath;
			if (path.Contains(kPropertyArrayPrefix))
			{
				s_ObjectPathBuilder.Length = 0;
				s_ObjectPathBuilder.Append(path);

				s_ObjectPathBuilder.Replace(kPropertyArrayPrefix, "[");
				path = s_ObjectPathBuilder.ToString();
			}
			return path;
		}

		private static object GetValue_Imp(object source, string name)
		{
			if (source == null)
			{
				return null;
			}

			System.Type type = source.GetType();

			System.Type fieldType = null;
			FieldInfo fieldInfo = GetFieldInfo(type, name, out fieldType);

			if (fieldInfo != null)
			{
				return fieldInfo.GetValue(source);
			}
			
			return null;
		}

		private static object GetValue_Imp(object source, string name, int index)
		{
			IList list = GetValue_Imp(source, name) as IList;
			if (list == null)
			{
				return null;
			}

			return list[index];
		}

		public static object GetPropertyObject(SerializedProperty property)
		{
			object obj = property.serializedObject.targetObject;

			string path = GetPropertyPath(property);

			string[] elements = path.Split('.');

			int length = elements.Length;
			for (int i = 0; i < length; i++)
			{
				string element = elements[i];

				if (element.Contains('['))
				{
					var array = element.Split('[', ']');
					var elementName = array[0];
					var index = System.Convert.ToInt32(array[1]);
					obj = GetValue_Imp(obj, elementName, index);
				}
				else
				{
					obj = GetValue_Imp(obj, element);
				}
			}
			return obj;
		}

		public static T GetPropertyObject<T>(SerializedProperty property)
		{
			return (T)GetPropertyObject(property);
		}

		public static Rect FromToRect(Vector2 start, Vector2 end)
		{
			Rect rect = new Rect(start.x, start.y, end.x - start.x, end.y - start.y);
			if (rect.width < 0.0f)
			{
				rect.x += rect.width;
				rect.width = -rect.width;
			}
			if (rect.height < 0.0f)
			{
				rect.y += rect.height;
				rect.height = -rect.height;
			}
			return rect;
		}

		public static MonoScript GetMonoScript(Object obj)
		{
			MonoBehaviour monoBehaviour = obj as MonoBehaviour;
			if (monoBehaviour != null)
			{
				return MonoScript.FromMonoBehaviour(monoBehaviour);
			}
			ScriptableObject scriptableObject = obj as ScriptableObject;
			if (scriptableObject != null)
			{
				return MonoScript.FromScriptableObject(scriptableObject);
			}

			return null;
		}

		public static float indent
		{
			get
			{
				return EditorGUI.indentLevel * 15f;
			}
		}

		public static bool LabelHasContent(GUIContent label)
		{
			if (label == null || label.text != string.Empty)
				return true;
			return (UnityEngine.Object)label.image != (UnityEngine.Object)null;
		}

		static float kLabelFloatMinW
		{
			get
			{
				return EditorGUIUtility.labelWidth + EditorGUIUtility.fieldWidth;
			}
		}

		static float kLabelFloatMaxW
		{
			get
			{
				return EditorGUIUtility.labelWidth + EditorGUIUtility.fieldWidth;
			}
		}

		public static Rect GetControlRect(bool hasLabel, float height, GUIStyle style, params GUILayoutOption[] options)
		{
			return GUILayoutUtility.GetRect(!hasLabel ? EditorGUIUtility.fieldWidth : kLabelFloatMinW, kLabelFloatMaxW, height, height, style, options);
		}

		public static Rect MultiFieldPrefixLabel(Rect totalPosition, int id, GUIContent label, int columns, GUIStyle style, out Rect labelPosition)
		{
			if (columns == 0)
			{
				labelPosition = totalPosition;
				labelPosition.height = 16f;
				Rect rect = totalPosition;
				rect.xMin = totalPosition.xMax;
				Vector2 size = style.CalcSize(label);
				labelPosition.width = size.x;
				return rect;
			}
			if (columns == 1 || EditorGUIUtility.wideMode)
			{
				labelPosition = new Rect(totalPosition.x, totalPosition.y, EditorGUIUtility.labelWidth, 16f);
				Rect rect = totalPosition;
				rect.xMin += EditorGUIUtility.labelWidth;
				if (columns > 1)
				{
					--labelPosition.width;
					--rect.xMin;
				}
				if (columns == 2)
				{
					float num = (float)(((double)rect.width - 4.0) / 3.0);
					rect.xMax -= num + 2f;
				}
				return rect;
			}
			labelPosition = new Rect(totalPosition.x, totalPosition.y, totalPosition.width, 16f);
			Rect rect1 = totalPosition;
			rect1.xMin += indent + 15f;
			rect1.yMin += 16f;
			return rect1;
		}

		public static void DrawIndicator(Rect position, string text)
		{
			int index = (int)Mathf.Repeat(Time.realtimeSinceStartup * 10f, 11.99f);
			GUIStyle style = EditorStyles.largeLabel;
			GUIContent content = s_WaitSpins[index];
			content.text = text;

			Vector2 size = style.CalcSize(content);

			Vector2 min = position.center - size * 0.5f;
			Vector2 max = position.center + size * 0.5f;
			position.min = min;
			position.max = max;

			GUI.Label(position, content, style);
		}

		public static string TextArea(Rect position, string text, int controlId, GUIStyle style)
		{
			bool changed = false;
			text = (string)_DoTextFieldMethod.Invoke(null, new object[] { _RecycledEditorField.GetValue(null), controlId, EditorGUI.IndentedRect(position), text, style, (string)null, changed, false, true, false });
			return text;
		}

		public static Vector2 SnapToGrid(Vector2 position)
		{
			if (ArborSettings.showGrid && ArborSettings.snapGrid)
			{
				float gridSizeMinor = ArborSettings.gridSize / (float)ArborSettings.gridSplitNum;
				int num1 = Mathf.RoundToInt(position.x / gridSizeMinor);
				int num2 = Mathf.RoundToInt(position.y / gridSizeMinor);
				position.x = (float)num1 * gridSizeMinor;
				position.y = (float)num2 * gridSizeMinor;
			}
			return position;
		}

		public static Rect SnapPositionToGrid(Rect position)
		{
			position.position = SnapToGrid(position.position);
			return position;
		}

		public static void ClearPropertyDrawerCache()
		{
			_DelegateClearGlobalCache();
		}

		public static bool ShouldRethrowException(System.Exception exception)
		{
			while (exception is TargetInvocationException && exception.InnerException != null)
				exception = exception.InnerException;
			return exception is ExitGUIException;
		}

		private static string StringPopupInternal(Rect position, GUIContent label, string selectedValue, GUIContent[] displayedOptions, string[] optionValues)
		{
			int selectedIndex = -1;
			if (optionValues != null)
			{
				selectedIndex = 0;
				while (selectedIndex < optionValues.Length && selectedValue != optionValues[selectedIndex])
					++selectedIndex;
			}
			int index = EditorGUI.Popup(position, label, selectedIndex, displayedOptions);
			if (optionValues == null)
				return string.Empty;
			if (index < 0 || index >= optionValues.Length)
				return selectedValue;
			return optionValues[index];
		}

		static void StringPopup(Rect position, SerializedProperty property, GUIContent[] displayedOptions, string[] optionValues, GUIContent label)
		{
			label = EditorGUI.BeginProperty(position, label, property);
			EditorGUI.BeginChangeCheck();
			string value = StringPopupInternal(position, label, property.stringValue, displayedOptions, optionValues);
			if (EditorGUI.EndChangeCheck())
			{
				property.stringValue = value;
			}
			EditorGUI.EndProperty();
		}

		static void StringPopup(SerializedProperty property, GUIContent[] displayedOptions, string[] optionValues, GUIContent label)
		{
			StringPopup(EditorGUILayout.GetControlRect(true, 16f, EditorStyles.popup), property, displayedOptions, optionValues, label);
		}

		static int GetLayerIndex(Animator animator, string layerName)
		{
			AnimatorController animatorController = animator.runtimeAnimatorController as AnimatorController;
			AnimatorControllerLayer[] layers = animatorController.layers;

			int layerCount = layers.Length;

			for (int i = 0; i < layerCount; i++)
			{
				AnimatorControllerLayer layer = layers[i];

				if (layer.name == layerName)
				{
					return i;
				}
			}

			return -1;
		}

		static AnimatorControllerLayer AnimatorLayerFieldInternal(Animator animator, SerializedProperty layerNameProperty)
		{
			if (animator == null || animator.runtimeAnimatorController == null)
			{
				layerNameProperty.stringValue = string.Empty;
				return null;
			}

			AnimatorController animatorController = animator.runtimeAnimatorController as AnimatorController;
			AnimatorControllerLayer[] layers = animatorController.layers;

			int layerCount = layers.Length;

			string[] layerNames = new string[layerCount];
			GUIContent[] layerDisplayed = new GUIContent[layerCount];

			for (int i = 0; i < layerCount; i++)
			{
				AnimatorControllerLayer layer = layers[i];

				string name = layer.name;

				layerNames[i] = name;
				layerDisplayed[i] = new GUIContent(name);
			}

			StringPopup(layerNameProperty, layerDisplayed, layerNames, null);

			int layerIndex = GetLayerIndex(animator, layerNameProperty.stringValue);

			AnimatorControllerLayer selectedLayer = (layerIndex >= 0) ? layers[layerIndex] : null;

			if (selectedLayer != null)
			{
				layerNameProperty.stringValue = selectedLayer.name;
			}
			else
			{
				layerNameProperty.stringValue = string.Empty;
			}

			return selectedLayer;
		}

		public static void AnimatorLayerField(Animator animator, SerializedProperty layerNameProperty)
		{
			AnimatorLayerFieldInternal(animator, layerNameProperty);
		}

		public static void AnimatorStateField(Animator animator, SerializedProperty layerNameProperty, SerializedProperty stateNameProperty)
		{
			AnimatorControllerLayer layer = AnimatorLayerFieldInternal(animator, layerNameProperty);
			if (layer == null)
			{
				return;
			}

			ChildAnimatorState[] states = layer.stateMachine.states;

			int stateCount = states.Length;

			string[] stateNames = new string[stateCount];
			GUIContent[] stateDisplayed = new GUIContent[stateCount];

			for (int i = 0; i < stateCount; i++)
			{
				AnimatorState state = states[i].state;

				string stateName = state.name;

				stateNames[i] = stateName;
				stateDisplayed[i] = new GUIContent(stateName);
			}

			StringPopup(stateNameProperty, stateDisplayed,stateNames,null );
		}

		public class AnimatorParameters
		{
			public GUIContent[] displayNames;
			public string[] names;
			public int[] types;
			public int selected;

			public Dictionary<AnimatorControllerParameterType, AnimatorParameters> parametersByType = new Dictionary<AnimatorControllerParameterType, AnimatorParameters>();

			public AnimatorParameters GetTypeParameter(AnimatorControllerParameterType type)
			{
				AnimatorParameters results = null;
				if (parametersByType.TryGetValue(type, out results))
				{
					return results;
				}
				return null;
			}

			public bool Update(AnimatorController animatorController, string name)
			{
				AnimatorControllerParameter[] animParames = animatorController.parameters;

				int parameterCount = (animParames != null) ? animParames.Length + 1: 0;
				if (parameterCount > 0)
				{
					selected = -1;

					if (names == null || names.Length != parameterCount)
					{
						names = new string[parameterCount];
					}
					if (displayNames == null || displayNames.Length != parameterCount)
					{
						displayNames = new GUIContent[parameterCount];
					}
					if (this.types == null || this.types.Length != parameterCount)
					{
						this.types = new int[parameterCount];
					}

					names[0] = string.Empty;
					displayNames[0] = EditorGUITools.GetTextContent("[None]");
					this.types[0] = 0;

					Dictionary<AnimatorControllerParameterType, List<int>> typeIndexes = new Dictionary<AnimatorControllerParameterType, List<int>>();

					for (int paramIndex = 1; paramIndex < parameterCount; paramIndex++)
					{
						AnimatorControllerParameter parameter = animParames[paramIndex-1];

						string parameterName = parameter.name;

						displayNames[paramIndex] = new GUIContent(parameterName);
						names[paramIndex] = parameterName;
						types[paramIndex] = (int)parameter.type;

						List<int> indexes = null;
						if (!typeIndexes.TryGetValue(parameter.type, out indexes))
						{
							indexes = new List<int>();
							typeIndexes.Add(parameter.type, indexes);
						}
						indexes.Add(paramIndex);

						if (parameterName == name)
						{
							selected = paramIndex;
						}
					}

					parametersByType.Clear();

					foreach (KeyValuePair<AnimatorControllerParameterType, List<int>> pair in typeIndexes)
					{
						AnimatorParameters p = new AnimatorParameters();
						List<int> indexes = pair.Value;

						p.selected = 0;

						p.displayNames = new GUIContent[indexes.Count+1];
						p.names = new string[indexes.Count+1];
						p.types = new int[indexes.Count+1];

						p.displayNames[0] = EditorGUITools.GetTextContent("[None]");
						p.names[0] = string.Empty;
						p.types[0] = 0;

						int count = 1;
						foreach (int index in indexes)
						{
							p.displayNames[count] = displayNames[index];
							p.names[count] = names[index];
							p.types[count] = types[index];
							if (index == selected)
							{
								p.selected = count;
							}
							count++;
						}

						parametersByType.Add(pair.Key, p);
					}

					return true;
				}

				return false;
			}

			public void Popup(Rect position, SerializedProperty nameProperty, SerializedProperty typeProperty,GUIContent label)
			{
				label = EditorGUI.BeginProperty(position, label, nameProperty);
				EditorGUI.BeginChangeCheck();
				selected = EditorGUI.Popup(position,label, selected, displayNames);
				if (EditorGUI.EndChangeCheck())
				{
					if (selected >= 0)
					{
						nameProperty.stringValue = names[selected];
						if (typeProperty != null)
						{
							typeProperty.intValue = types[selected];
						}
					}
				}
				EditorGUI.EndProperty();
			}
		}
		private static readonly Dictionary<AnimatorController, AnimatorParameters> _Parameters = new Dictionary<AnimatorController, AnimatorParameters>();

		private static AnimatorParameters GetAnimatorParameters(AnimatorController animatorController)
		{
			AnimatorParameters parameters;
			if (!_Parameters.TryGetValue(animatorController, out parameters))
			{
				parameters = new AnimatorParameters();
				_Parameters.Add(animatorController, parameters);
			}
			
			return parameters;
		}

		public static void AnimatorParameterField(Rect position, Animator animator, SerializedProperty nameProperty, SerializedProperty typeProperty, GUIContent label,bool hasType = false,AnimatorControllerParameterType parameterType = AnimatorControllerParameterType.Bool)
		{
			AnimatorParameters parameters = null;

			if (animator != null && animator.runtimeAnimatorController != null)
			{
				AnimatorController animatorController = animator.runtimeAnimatorController as AnimatorController;

				string name = nameProperty.stringValue;

				parameters = GetAnimatorParameters(animatorController);

				if (parameters.Update(animatorController, name))
				{
					if (hasType)
					{
						parameters = parameters.GetTypeParameter(parameterType);
					}
				}
				else
				{
					parameters = null;
				}
			}
			
			if (parameters != null)
			{
				parameters.Popup(position, nameProperty, typeProperty, label);
			}
			else
			{
				EditorGUI.BeginDisabledGroup(true);

				label = EditorGUI.BeginProperty(position, label, nameProperty);
				
				EditorGUI.Popup(position, label, -1, new GUIContent[] { GUIContent.none });

				EditorGUI.EndProperty();

				EditorGUI.EndDisabledGroup();
			}
		}
		
		public static void AnimatorParameterField(Animator animator, SerializedProperty nameProperty, SerializedProperty typeProperty, GUIContent label)
		{
			AnimatorParameterField(EditorGUILayout.GetControlRect(true, 16f, EditorStyles.popup), animator, nameProperty, typeProperty,label);
		}

		public static void AnimatorParameterField(Animator animator, SerializedProperty nameProperty, SerializedProperty typeProperty, GUIContent label, AnimatorControllerParameterType parameterType)
		{
			AnimatorParameterField(EditorGUILayout.GetControlRect(true, 16f, EditorStyles.popup), animator, nameProperty, typeProperty, label,true,parameterType);
		}
	}
}
