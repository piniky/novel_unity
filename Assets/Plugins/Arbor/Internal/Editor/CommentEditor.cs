﻿using UnityEngine;
using UnityEditor;
using System.Collections;

using Arbor;
using System;

namespace ArborEditor
{
	[CustomNodeEditor(typeof(CommentNode))]
	public sealed class CommentEditor : NodeEditor
	{
		public CommentNode comment
		{
			get
			{
				return node as CommentNode;
			}
		}

		private static GUIContent s_Content = null;
		private static SystemLanguage s_ContentLanguage;

		public override GUIContent GetTitleContent()
		{
			if (s_Content == null || s_ContentLanguage != ArborSettings.currnentLanguage)
			{
				s_Content = new GUIContent(Localization.GetWord("Comment"), Icons.commentNodeIcon);
				s_ContentLanguage = ArborSettings.currnentLanguage;
			}
			return s_Content;
		}

		public override GUIStyle GetStyle()
		{
			return Styles.GetNodeStyle(Styles.Color.Yellow, isSelection);
		}

		void CommentField()
		{
			GUIStyle style = EditorStyles.textArea;

			EditorGUI.BeginChangeCheck();
			string commentText = EditorGUILayout.TextArea(comment.comment, style);
			if (EditorGUI.EndChangeCheck())
			{
				Undo.RecordObject(comment.stateMachine, "Change Comment");

				comment.comment = commentText;

				EditorUtility.SetDirty(comment.stateMachine);
			}
		}

		protected override void OnGUI()
		{
			using (new ProfilerScope("OnCommentGUI"))
			{
				EditorGUITools.DrawSeparator();

				CommentField();
			}
		}
	}
}
