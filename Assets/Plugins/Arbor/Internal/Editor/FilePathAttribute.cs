﻿using UnityEngine;
using UnityEditorInternal;
using System;

namespace ArborEditor
{
	[AttributeUsage(AttributeTargets.Class)]
	internal class FilePathAttribute : Attribute
	{
		public string filepath;

		public FilePathAttribute(string relativePath, Location location)
		{
			if (string.IsNullOrEmpty(relativePath))
			{
				Debug.LogError((object)"Invalid relative path! (its null or empty)");
			}
			else
			{
				if (relativePath[0] == '/')
				{
					relativePath = relativePath.Substring(1);
				}
				if (location == Location.PreferencesFolder)
				{
					filepath = InternalEditorUtility.unityPreferencesFolder + "/" + relativePath;
				}
				else
				{
					filepath = relativePath;
				}
			}
		}

		public enum Location
		{
			PreferencesFolder,
			ProjectFolder,
		}
	}
}
