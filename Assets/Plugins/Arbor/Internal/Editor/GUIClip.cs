﻿using UnityEngine;
using System.Reflection;

namespace ArborEditor
{
	internal class GUIClip
	{
#if ARBOR_DLL || !UNITY_5_5_OR_NEWER
		private delegate void DelegatePush( Rect screenRect, Vector2 scrollOffset, Vector2 renderOffset, bool resetOffset );
		private delegate void DelegatePop();

		private static readonly DelegatePush _Push;
		private static readonly DelegatePop _Pop;
#endif

		private delegate Rect DelegateGetTopmostRect();
		static DelegateGetTopmostRect s_GetTopmostRect;

		private delegate Rect DelegateGetVisibleRect();
		static DelegateGetVisibleRect s_GetVisibleRect;
		
		public static Rect topmostRect
		{
			get
			{
				return s_GetTopmostRect();
			}
		}

		public static Rect visibleRect
		{
			get
			{
				return s_GetVisibleRect();
			}
		}

		static GUIClip()
		{
			Assembly assembly = Assembly.Load("UnityEngine.dll");
			System.Type guiClipType = assembly.GetType("UnityEngine.GUIClip");

#if ARBOR_DLL
			_Push = EditorGUITools.GetDelegate<DelegatePush>( guiClipType,"Push", BindingFlags.Static | BindingFlags.NonPublic );
			if( _Push == null )
			{
				_Push = EditorGUITools.GetDelegate<DelegatePush>( guiClipType, "BeginClip", BindingFlags.Static | BindingFlags.Public );
			}
			_Pop = EditorGUITools.GetDelegate<DelegatePop>( guiClipType,"Pop", BindingFlags.Static | BindingFlags.NonPublic );
			if( _Pop == null )
			{
				_Pop = EditorGUITools.GetDelegate<DelegatePop>( guiClipType, "EndClip", BindingFlags.Static | BindingFlags.Public );
			}
#elif !UNITY_5_5_OR_NEWER
			_Push = EditorGUITools.GetDelegate<DelegatePush>( guiClipType,"Push", BindingFlags.Static | BindingFlags.NonPublic );
			_Pop = EditorGUITools.GetDelegate<DelegatePop>( guiClipType,"Pop", BindingFlags.Static | BindingFlags.NonPublic );
#endif

			PropertyInfo topmostRectProperty = guiClipType.GetProperty("topmostRect", BindingFlags.Static | BindingFlags.Public);
			s_GetTopmostRect = (DelegateGetTopmostRect)System.Delegate.CreateDelegate(typeof(DelegateGetTopmostRect), topmostRectProperty.GetGetMethod());

			PropertyInfo visibleRectProperty = guiClipType.GetProperty("visibleRect", BindingFlags.Static | BindingFlags.Public);
			s_GetVisibleRect = (DelegateGetVisibleRect)System.Delegate.CreateDelegate(typeof(DelegateGetVisibleRect), visibleRectProperty.GetGetMethod());
		}

		public static void Push(Rect screenRect, Vector2 scrollOffset, Vector2 renderOffset, bool resetOffset)
		{
#if !ARBOR_DLL && UNITY_5_5_OR_NEWER
			GUI.BeginClip(screenRect, scrollOffset, renderOffset, resetOffset);
#else
			if( _Push != null )
			{
				_Push( screenRect, scrollOffset, renderOffset, resetOffset );
			}
#endif
		}

		public static void Pop()
		{
#if !ARBOR_DLL && UNITY_5_5_OR_NEWER
			GUI.EndClip();
#else
			if( _Pop != null )
			{
				_Pop();
			}
#endif
		}
	}
}
