﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using Arbor;

namespace ArborEditor
{
	public class Clipboard : Arbor.ScriptableSingleton<Clipboard>
	{
		delegate NodeBehaviour CreateNodeBehaviour(ArborFSMInternal stateMachine, int nodeID, System.Type type);

		private static readonly System.Reflection.FieldInfo s_IsEditorField;
		private static readonly System.Reflection.FieldInfo s_NodeBehaviour_StateMachineField;
		
		static Clipboard()
		{
			s_IsEditorField = typeof(ArborFSMInternal).GetField("_IsEditor", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);

			System.Type nodeBehaviourType = typeof(NodeBehaviour);
			s_NodeBehaviour_StateMachineField = nodeBehaviourType.GetField("_StateMachine", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
		}

		public static void CopyStateMachine(ArborFSMInternal source)
		{
			instance.CopyStateMachineInternal(source);
		}

		public static void CopyBehaviour(StateBehaviour source)
		{
			instance.CopyBehaviourInternal(source);
		}

		public static void PasteBehaviourValues(StateBehaviour behaviour)
		{
			instance.PasteBehaviourValuesInternal(behaviour);
		}

		public static void PasteBehaviourAsNew(State state)
		{
			instance.PasteBehaviourAsNewInternal(state);
		}

		public static bool hasBehaviour
		{
			get
			{
				return instance.hasBehaviourInternal;
			}
		}

		public static bool CompareBehaviourType(System.Type behaviourType)
		{
			return instance.CompareBehaviourTypeInternal(behaviourType);
		}

		public static bool hasCopyedNodes
		{
			get
			{
				return instance.hasCopyedNodesInternal;
			}
		}

		public static void CopyNodes(Node[] nodes)
		{
			instance.CopyNodesInternal(nodes);
		}

		public static Node[] PasteNodes(ArborFSMInternal stateMachine, Vector2 position)
		{
			return instance.PasteNodesInternal(stateMachine, position);
		}

		[System.Reflection.Obfuscation(Exclude = true)]
		[SerializeField]
		private GameObject _GameObject = null;

		[System.Reflection.Obfuscation(Exclude = true)]
		[SerializeField]
		private StateBehaviour _CopyBehaviour = null;

		[System.Reflection.Obfuscation(Exclude = true)]
		[SerializeField]
		private ArborFSMInternal _NodeClipboard = null;

		[System.Reflection.Obfuscation(Exclude = true)]
		[SerializeField]
		private List<State> _CopyStates = new List<State>();

		[System.Reflection.Obfuscation(Exclude = true)]
		[SerializeField]
		private List<CommentNode> _CopyComments = new List<CommentNode>();

		[System.Reflection.Obfuscation(Exclude = true)]
		[SerializeField]
		private List<CalculatorNode> _CopyCalculators = new List<CalculatorNode>();

		[System.Reflection.Obfuscation(Exclude = true)]
		[SerializeField]
		private List<GroupNode> _CopyGroups = new List<GroupNode>();

		private GameObject gameObject
		{
			get
			{
				if (instance._GameObject == null)
				{
					instance._GameObject = EditorUtility.CreateGameObjectWithHideFlags("Clipboard", HideFlags.HideAndDontSave);
					instance._GameObject.tag = "EditorOnly";
				}

				return instance._GameObject;
			}
		}

		private void ClearInternal()
		{
			if (_CopyBehaviour != null)
			{
				DestroyImmediate(_CopyBehaviour);
				_CopyBehaviour = null;
			}
			if (_NodeClipboard != null)
			{
				Object.DestroyImmediate(_NodeClipboard);
				_NodeClipboard = null;

				foreach (State state in _CopyStates)
				{
					int behaviourCount = state.behaviourCount;
					for (int behaviourIndex = 0; behaviourIndex < behaviourCount; behaviourIndex++)
					{
						StateBehaviour behaviour = state.GetBehaviourFromIndex(behaviourIndex);
						Object.DestroyImmediate(behaviour);
					}
				}
				_CopyStates.Clear();

				_CopyComments.Clear();

				foreach (CalculatorNode calculatorNode in _CopyCalculators)
				{
					Object.DestroyImmediate(calculatorNode.calculator);
				}
				_CopyCalculators.Clear();

				_CopyGroups.Clear();
			}
		}

		private static void CopyNodeBehaviour(NodeBehaviour source, NodeBehaviour dest)
		{
			ArborFSMInternal stateMachine = dest.stateMachine;

			EditorUtility.CopySerialized(source, dest);

			dest.hideFlags |= HideFlags.HideAndDontSave;

			s_NodeBehaviour_StateMachineField.SetValue(dest, stateMachine);
		}
		
		private void CopyStateMachineInternal(ArborFSMInternal source)
		{
			ClearInternal();

			ArborFSMInternal dest = gameObject.AddComponent(source.GetType()) as ArborFSMInternal;
			s_IsEditorField.SetValue(dest, true);

			EditorUtility.CopySerialized(source, dest);

			bool cachedEnabled = ComponentUtility.enabled;
			ComponentUtility.enabled = false;

			for (int count = dest.nodeCount, i = 0; i < count; i++)
			{
				Node node = dest.GetNodeFromIndex(i);
				if (node is State)
				{
					State state = node as State;
					
					int behaviourCount = state.behaviourCount;
					for (int behaviourIndex = 0; behaviourIndex < behaviourCount; behaviourIndex++)
					{
						StateBehaviour behaviour = state.GetBehaviourFromIndex(behaviourIndex);
						if (behaviour != null)
						{
							StateBehaviour copyBehaviour = NodeBehaviour.CreateNodeBehaviour(dest, node.nodeID,behaviour.GetType()) as StateBehaviour;

							CopyNodeBehaviour(behaviour, copyBehaviour);

							state.SetBehaviour(behaviourIndex, copyBehaviour);
						}
					}
				}
				else if (node is CalculatorNode)
				{
					CalculatorNode calculatorNode = node as CalculatorNode;

					Calculator calculator = calculatorNode.calculator;

					Calculator copyCalculator = NodeBehaviour.CreateNodeBehaviour(dest, node.nodeID, calculator.GetType()) as Calculator;

					CopyNodeBehaviour(calculator, copyCalculator);

					calculatorNode.SetCalculator(copyCalculator);
				}
			}

			_NodeClipboard = dest;

			ComponentUtility.enabled = cachedEnabled;

			UnityEditorInternal.ComponentUtility.CopyComponent(dest);
		}

		private void CopyBehaviourInternal(StateBehaviour source)
		{
			ClearInternal();

			_CopyBehaviour = gameObject.AddComponent(source.GetType()) as StateBehaviour;

			EditorUtility.CopySerialized(source, _CopyBehaviour);
		}

		private void PasteBehaviourValuesInternal(StateBehaviour behaviour)
		{
			EditorGUITools.CopyBehaviour(_CopyBehaviour, behaviour, true);
		}

		private void PasteBehaviourAsNewInternal(State state)
		{
			StateBehaviour behaviour = state.AddBehaviour(_CopyBehaviour.GetType());

			EditorGUITools.CopyBehaviour(_CopyBehaviour, behaviour, true);
		}

		private bool hasBehaviourInternal
		{
			get
			{
				return _CopyBehaviour != null;
			}
		}

		private bool CompareBehaviourTypeInternal(System.Type behaviourType)
		{
			return _CopyBehaviour != null && _CopyBehaviour.GetType() == behaviourType;
		}

		private bool hasCopyedNodesInternal
		{
			get
			{
				return _CopyStates.Count != 0 || _CopyComments.Count != 0 || _CopyCalculators.Count != 0 || _CopyGroups.Count != 0;
			}
		}

		private void CopyNodesInternal(Node[] nodes)
		{
			ClearInternal();

			_NodeClipboard = gameObject.AddComponent<ArborFSMInternal>();
			s_IsEditorField.SetValue(_NodeClipboard, true);

			bool cachedEnabled = ComponentUtility.enabled;
			ComponentUtility.enabled = false;

			foreach (Node node in nodes)
			{
				if (node is State)
				{
					State state = node as State;
					State copyState = _NodeClipboard.CreateState(state.nodeID, state.resident);

					copyState.name = state.name;
					copyState.position = state.position;
					copyState.showComment = state.showComment;
					copyState.nodeComment = state.nodeComment;

					int behaviourCount = state.behaviourCount;
					for (int behaviourIndex = 0; behaviourIndex < behaviourCount; behaviourIndex++)
					{
						StateBehaviour behaviour = state.GetBehaviourFromIndex(behaviourIndex);
						if (behaviour != null)
						{
							StateBehaviour copyBehaviour = copyState.AddBehaviour(behaviour.GetType());

							CopyNodeBehaviour(behaviour, copyBehaviour);
						}
					}

					_CopyStates.Add(copyState);
				}
				else if (node is CommentNode)
				{
					CommentNode comment = node as CommentNode;
					CommentNode copyComment = _NodeClipboard.CreateComment(comment.nodeID);

					copyComment.position = comment.position;
					copyComment.showComment = comment.showComment;
					copyComment.nodeComment = comment.nodeComment;
					copyComment.comment = comment.comment;

					_CopyComments.Add(copyComment);
				}
				else if (node is CalculatorNode)
				{
					CalculatorNode calculatorNode = node as CalculatorNode;
					CalculatorNode copyCalculatorNode = _NodeClipboard.CreateCalculator(calculatorNode.calculator.GetType());

					copyCalculatorNode.position = calculatorNode.position;
					copyCalculatorNode.showComment = calculatorNode.showComment;
					copyCalculatorNode.nodeComment = calculatorNode.nodeComment;

					CopyNodeBehaviour(calculatorNode.calculator, copyCalculatorNode.calculator);

					_CopyCalculators.Add(copyCalculatorNode);
				}
				else if (node is GroupNode)
				{
					GroupNode groupNode = node as GroupNode;
					GroupNode copyGroupNode = _NodeClipboard.CreateGroup(groupNode.nodeID);

					copyGroupNode.name = groupNode.name;
					copyGroupNode.position = groupNode.position;
					copyGroupNode.showComment = groupNode.showComment;
					copyGroupNode.nodeComment = groupNode.nodeComment;

					_CopyGroups.Add(copyGroupNode);
				}
			}

			ComponentUtility.enabled = cachedEnabled;
		}

		private Node[] PasteNodesInternal(ArborFSMInternal stateMachine, Vector2 position)
		{
			List<Node> nodes = new List<Node>();
			foreach (State state in _CopyStates)
			{
				nodes.Add(state);
			}
			foreach (CommentNode commentNode in _CopyComments)
			{
				nodes.Add(commentNode);
			}
			foreach (CalculatorNode calculatorNode in _CopyCalculators)
			{
				nodes.Add(calculatorNode);
			}
			foreach (GroupNode groupNode in _CopyGroups)
			{
				nodes.Add(groupNode);
			}
			return EditorGUITools.DuplicateNodes(stateMachine, position, nodes.ToArray());
		}
	}
}
