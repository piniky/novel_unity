﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System.Collections;

using Arbor;

namespace ArborEditor
{
	public class ParameterContainerInternalInspector : Editor
	{
		ParameterContainerInternal _ParameterContainer;
		ReorderableList _ParametersList;

		static string GetTypeValueName(Parameter.Type type)
		{
			switch (type)
			{
				case Parameter.Type.Int:
					return "intValue";
				case Parameter.Type.Float:
					return "floatValue";
				case Parameter.Type.Bool:
					return "boolValue";
				case Parameter.Type.String:
					return "stringValue";
				case Parameter.Type.GameObject:
					return "gameObjectValue";
				case Parameter.Type.Vector2:
					return "vector2Value";
				case Parameter.Type.Vector3:
					return "vector3Value";
				case Parameter.Type.Quaternion:
					return "quaternionValue";
				case Parameter.Type.Rect:
					return "rectValue";
				case Parameter.Type.Bounds:
					return "boundsValue";
				case Parameter.Type.Transform:
				case Parameter.Type.RectTransform:
				case Parameter.Type.Rigidbody:
				case Parameter.Type.Rigidbody2D:
				case Parameter.Type.Component:
					return "objectReferenceValue";
				case Parameter.Type.Long:
					return "longValue";
				default:
					throw new System.NotImplementedException("It is an unimplemented Parameter type(" + type + ")");
			}
		}

		static bool IsTypeObject(Parameter.Type type)
		{
			switch (type)
			{
				case Parameter.Type.GameObject:
				case Parameter.Type.Transform:
				case Parameter.Type.RectTransform:
				case Parameter.Type.Rigidbody:
				case Parameter.Type.Rigidbody2D:
				case Parameter.Type.Component:
					return true;
			}

			return false;
		}

		static System.Type GetObjectType(Parameter.Type type)
		{
			switch (type)
			{
				case Parameter.Type.GameObject:
					return typeof(GameObject);
				case Parameter.Type.Transform:
					return typeof(Transform);
				case Parameter.Type.RectTransform:
					return typeof(RectTransform);
				case Parameter.Type.Rigidbody:
					return typeof(Rigidbody);
				case Parameter.Type.Rigidbody2D:
					return typeof(Rigidbody2D);
				case Parameter.Type.Component:
					return typeof(Component);
				default:
					throw new System.ArgumentException("Parameter type not an Object type(" + type + ")");
			}
		}

		[System.Reflection.Obfuscation( Exclude = true )]
		void OnEnable()
		{
			_ParameterContainer = target as ParameterContainerInternal;

			_ParametersList = new ReorderableList(serializedObject, serializedObject.FindProperty("_Parameters"));

			_ParametersList.drawHeaderCallback = DrawHeader;
			_ParametersList.onAddDropdownCallback = OnAddDropdown;
			_ParametersList.elementHeightCallback = GetElementHeight;
			_ParametersList.drawElementCallback = DrawElement;
		}

		void DrawHeader(Rect rect)
		{
			EditorGUI.LabelField(rect, _ParametersList.serializedProperty.displayName);
		}

		void OnAddDropdown(Rect buttonRect, ReorderableList list)
		{
			GenericMenu genericMenu = new GenericMenu();
			foreach (object userData in System.Enum.GetValues(typeof(Parameter.Type)))
			{
				genericMenu.AddItem(EditorGUITools.GetTextContent(userData.ToString()), false, AddParameterMenu, userData);
			}
			genericMenu.DropDown(buttonRect);
		}

		float GetElementHeight(int index)
		{
			SerializedProperty property = _ParametersList.serializedProperty.GetArrayElementAtIndex(index);

			Parameter.Type type = EnumUtility.GetValueFromIndex<Parameter.Type>(property.FindPropertyRelative("type").enumValueIndex);
			string valueName = GetTypeValueName(type);
			SerializedProperty valueProperty = property.FindPropertyRelative(valueName);
			float height = EditorGUI.GetPropertyHeight(valueProperty, GUIContent.none, true);

			return _ParametersList.elementHeight + height + EditorGUIUtility.standardVerticalSpacing;
		}

		void DrawElement(Rect rect, int index, bool isActive, bool isFocused)
		{
			SerializedProperty property = _ParametersList.serializedProperty.GetArrayElementAtIndex(index);

			SerializedProperty nameProperty = property.FindPropertyRelative("name");

			rect.yMin += EditorGUIUtility.standardVerticalSpacing;

			Rect nameRect = new Rect(rect);
			nameRect.height = EditorGUIUtility.singleLineHeight;

			Parameter.Type type = EnumUtility.GetValueFromIndex<Parameter.Type>(property.FindPropertyRelative("type").enumValueIndex);

			string label = System.Enum.GetName(typeof(Parameter.Type), type);

			EditorGUI.BeginChangeCheck();
			string name = EditorGUI.TextField(nameRect, label,nameProperty.stringValue);
			if (EditorGUI.EndChangeCheck() && name != nameProperty.stringValue)
			{
				nameProperty.stringValue = _ParameterContainer.MakeUniqueName(name);
			}

			
			string valueName = GetTypeValueName(type);
			SerializedProperty valueProperty = property.FindPropertyRelative(valueName);

			Rect valueRect = new Rect(rect);
			valueRect.yMin += _ParametersList.elementHeight;

			if (IsTypeObject(type))
			{
				System.Type objectType = GetObjectType(type);

				valueRect.height = EditorGUIUtility.singleLineHeight;

				EditorGUI.BeginChangeCheck();
				Object obj = EditorGUI.ObjectField(valueRect, GUIContent.none,valueProperty.objectReferenceValue, objectType, true);
				if (EditorGUI.EndChangeCheck())
				{
					valueProperty.objectReferenceValue = obj;
				}
			}
			else
			{
				EditorGUI.PropertyField(valueRect, valueProperty, GUIContent.none, true);
			}
		}

		void AddParameterMenu(object value)
		{
			Undo.RecordObject(_ParameterContainer, "Parameter Added");
			Parameter.Type type = (Parameter.Type)value;
			_ParameterContainer.AddParam("New " + type.ToString(), type);
		}

		public override void OnInspectorGUI()
		{
			serializedObject.Update();

			_ParametersList.DoLayoutList();
			
			serializedObject.ApplyModifiedProperties();
		}
	}
}
