﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

using Arbor;

namespace ArborEditor
{
	[CustomPropertyDrawer(typeof(Rigidbody2DParameterReference))]
	public class Rigidbody2DParameterReferencePropertyDrawer : ParameterReferencePropertyDrawer
	{
		protected override bool CheckType(Parameter.Type type)
		{
			return type == Parameter.Type.Rigidbody2D;
		}
	}
}
