﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace ArborEditor
{
	public sealed class Styles
	{
		public static GUIContent addIconContent;
		public static GUIContent removeIconContent;
		public static Texture2D connectionTexture;
		public static GUIStyle invisibleButton;
		public static GUIStyle RLHeader;
		public static GUIStyle RLBackground;
		public static GUIStyle separator;
		public static GUIStyle background;
		public static GUIStyle header;
		public static GUIStyle componentButton;
		public static GUIStyle groupButton;
		public static GUIStyle previewBackground;
		public static GUIStyle previewHeader;
		public static GUIStyle previewText;
		public static GUIStyle rightArrow;
		public static GUIStyle leftArrow;
		public static GUIStyle shurikenDropDown;
		public static GUIStyle titlebar;
		public static GUIStyle titlebarText;
		public static GUIStyle foldout;
		public static GUIStyle hostview;
		public static GUIStyle graphBackground;
		public static GUIStyle selectionRect;
		public static GUIStyle varPinIn;
		public static GUIStyle varPinOut;
		public static GUIStyle overlayBox;
		public static GUIStyle controlHighlight;
		public static GUIStyle preSlider;
		public static GUIStyle preSliderThumb;
		public static GUIStyle addBehaviourButton;
		public static GUIStyle treeStyle;
		public static GUIStyle label;
		public static GUIStyle graphLabel;
		public static GUIStyle largeButton;
		public static GUIStyle insertion;
		public static GUIStyle insertionAbove;
		public static GUIStyle lockButton;
		public static GUIStyle countBadge;
		public static GUIStyle breakpoint;
		public static GUIStyle breakpointOn;
		public static GUIStyle nodeCommentField;

		private static GUISkin _Skin = null;
		private static readonly Dictionary<string, GUIStyle> _StyleCache;

		private static GUISkin skin
		{
			get
			{
				if (_Skin == null)
				{
					_Skin = EditorResources.Load<GUISkin>("EditorSkin", ".guiskin");
				}
				return _Skin;
			}
		}

		static Styles()
		{
			_StyleCache = new Dictionary<string, GUIStyle>();

			addIconContent = new GUIContent(EditorGUIUtility.FindTexture("Toolbar Plus"));
			removeIconContent = new GUIContent(EditorGUIUtility.FindTexture("Toolbar Minus"));
			connectionTexture = UnityEditor.Graphs.Styles.connectionTexture.image as Texture2D;
			invisibleButton = (GUIStyle)"InvisibleButton";
			RLHeader = (GUIStyle)"RL Header";
			RLBackground = new GUIStyle((GUIStyle)"RL Background");
			separator = (GUIStyle)"sv_iconselector_sep";
			background = (GUIStyle)"grey_border";
			header = new GUIStyle((GUIStyle)"IN BigTitle");
			componentButton = new GUIStyle((GUIStyle)"PR Label");
			previewBackground = (GUIStyle)"PopupCurveSwatchBackground";
			previewHeader = new GUIStyle(EditorStyles.label);
			previewText = new GUIStyle(EditorStyles.wordWrappedLabel);
			rightArrow = (GUIStyle)"AC RightArrow";
			leftArrow = (GUIStyle)"AC LeftArrow";
			shurikenDropDown = (GUIStyle)"ShurikenDropDown";
			titlebar = (GUIStyle)"IN Title";
			titlebarText = (GUIStyle)"IN TitleText";
			foldout = (GUIStyle)"Foldout";
			hostview = new GUIStyle((GUIStyle)"hostview");
			hostview.stretchHeight = false;
			hostview.padding = new RectOffset();
			graphBackground = GetStyle("graph background");
			selectionRect = (GUIStyle)"SelectionRect";
			varPinIn = (GUIStyle)"flow varPin in";
			varPinOut = (GUIStyle)"flow varPin out";
			overlayBox = (GUIStyle)"TL SelectionButton PreDropGlow";
			controlHighlight = (GUIStyle)"flow overlay box";
			preSlider = (GUIStyle)"preSlider";
			preSliderThumb = (GUIStyle)"preSliderThumb";
			treeStyle = new GUIStyle((GUIStyle)"PR Label");
			label = new GUIStyle(EditorStyles.label);
			treeStyle.padding.left = 2;
			addBehaviourButton = EditorStyles.miniButton;
			largeButton = (GUIStyle)"LargeButton";
			insertion = (GUIStyle)"PR Insertion";
			insertionAbove = (GUIStyle)"PR Insertion Above";
			lockButton = (GUIStyle)"IN LockButton";
			countBadge = (GUIStyle)"CN CountBadge";
			breakpoint = GetStyle("breakpoint");
			breakpointOn = GetStyle("breakpoint on");
			nodeCommentField = GetStyle("node comment");
			header.font = EditorStyles.boldLabel.font;
			RLBackground.stretchHeight = false;
			componentButton.alignment = TextAnchor.MiddleLeft;
			componentButton.padding.left -= 15;
			componentButton.fixedHeight = 20f;
			groupButton = new GUIStyle(componentButton);
			groupButton.padding.left += 17;
			previewText.padding.left += 3;
			previewText.padding.right += 3;
			++previewHeader.padding.left;
			previewHeader.padding.right += 3;
			previewHeader.padding.top += 3;
			previewHeader.padding.bottom += 2;
			
			graphLabel = GetStyle("graph label");
		}

		public static GUIStyle GetStyle(string name)
		{
			GUIStyle style = null;
			if (!_StyleCache.TryGetValue(name, out style))
			{
				style = skin.FindStyle(name);
				if (style != null)
				{
					_StyleCache.Add(name, style);
				}
			}
			return style;
		}

		public static GUIStyle GetNodeStyle(Styles.Color color, bool on)
		{
			string name = string.Format("node {0}{1}", (int)color, (!on) ? string.Empty : " on");
			return GetStyle(name);
		}
		
		public enum Color
		{
			Gray = 0,
			Grey = 0,
			Blue = 1,
			Aqua = 2,
			Green = 3,
			Yellow = 4,
			Orange = 5,
			Red = 6,
			White = 7,
		}
	}
}
