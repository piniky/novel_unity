﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Arbor;

namespace ArborEditor
{
#if !ARBOR_DLL
	internal sealed class LanguageImporter : AssetPostprocessor
	{
		static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
		{
			foreach (string importAssetPath in importedAssets)
			{
				string directory = Path.GetDirectoryName(importAssetPath);
				string languageName = Path.GetFileNameWithoutExtension(importAssetPath);
				string ext = Path.GetExtension(importAssetPath);

				if (ext == ".txt" && directory == Localization.languageDirectory)
				{
					try
					{
						SystemLanguage language = (SystemLanguage)System.Enum.Parse(typeof(SystemLanguage), languageName);

						TextAsset languageAsset = AssetDatabase.LoadAssetAtPath(importAssetPath, typeof(TextAsset)) as TextAsset;
						if (languageAsset != null)
						{
							Localization.instance.Load(language, languageAsset.text);
						}
					}
					catch
					{
					}
				}
			}
		}
	}
#endif

	[Obfuscation(Exclude = true)]
	public sealed class Localization : Arbor.ScriptableSingleton<Localization>
	{
		[InitializeOnLoadMethod]
		static void InitializeOnLoad()
		{
			Localization localization = instance;
			localization.Initialize();
		}

		public class WordDictionary : Dictionary<string, GUIContent>
		{
		}
		
		private SortedDictionary<SystemLanguage, WordDictionary> _LanguageDics = new SortedDictionary<SystemLanguage, WordDictionary>();
		private List<SystemLanguage> _Languages = new List<SystemLanguage>();

		const string k_DirectoryName = "Languages";

#if !ARBOR_DLL
		public static string languageDirectory
		{
			get
			{
				return PathUtility.Combine(EditorResources.directory,k_DirectoryName);
			}
		}
#endif
		
		private void Initialize()
		{
			foreach (SystemLanguage language in System.Enum.GetValues(typeof(SystemLanguage)))
			{
				string languagePath = PathUtility.Combine(k_DirectoryName, language.ToString());

				TextAsset languageAsset = EditorResources.Load<TextAsset>(languagePath, ".txt");
				if (languageAsset != null)
				{
					Load(language, languageAsset.text);
				}
			}
		}

		public void Load(SystemLanguage language, string text)
		{
			WordDictionary wordDic = null;
			if (_LanguageDics.TryGetValue(language, out wordDic))
			{
				wordDic.Clear();
			}
			else
			{
				wordDic = new WordDictionary();

				_LanguageDics.Add(language, wordDic);

				_Languages.Add(language);
			}

			foreach (string line in text.Split('\n'))
			{
				if (line.StartsWith("//"))
				{
					continue;
				}

				int firstColonIndex = line.IndexOf(':');
				if (firstColonIndex < 0)
				{
					continue;
				}

				string key = line.Substring(0, firstColonIndex);
				string word = line.Substring(firstColonIndex + 1).Trim().Replace("\\n", "\n");

				wordDic.Add(key, new GUIContent(word));
			}

			_LanguageLabels = null;
		}

		public static GUIContent GetTextContent(SystemLanguage language, string key)
		{
			if (string.IsNullOrEmpty(key))
			{
				return GUIContent.none;
			}

			WordDictionary wordDic = null;
			if (instance._LanguageDics.TryGetValue(language, out wordDic))
			{
				GUIContent content;
				if (wordDic.TryGetValue(key, out content))
				{
					return content;
				}
			}

			return new GUIContent(key);
		}

		public static GUIContent GetTextContent(string key)
		{
			return GetTextContent(ArborSettings.currnentLanguage, key);
		}

		public static string GetWord(SystemLanguage language, string key)
		{
			return GetTextContent(language, key).text;
		}

		public static string GetWord(string key)
		{
			return GetWord(ArborSettings.currnentLanguage, key);
		}

		private static GUIContent[] _LanguageLabels;
		private static SystemLanguage _LastLanguage;

		public static void LanguagePopup()
		{
			using (new ProfilerScope("LanguagePopup"))
			{
				int languageCount = instance._Languages.Count + 1;

				bool changed = _LastLanguage != ArborSettings.currnentLanguage;
				_LastLanguage = ArborSettings.currnentLanguage;

				if (_LanguageLabels == null || _LanguageLabels.Length != languageCount)
				{
					_LanguageLabels = new GUIContent[languageCount];

					changed = true;
				}

				if (changed)
				{
					_LanguageLabels[0] = EditorGUITools.GetTextContent(Localization.GetWord("Auto") + "(" + Localization.GetWord(ArborSettings.GetAutoLanguage().ToString()) + ")");

					if (instance._Languages != null)
					{
						int count = instance._Languages.Count;
						for (int i = 0; i < count; i++)
						{
							SystemLanguage language = instance._Languages[i];
							_LanguageLabels[i + 1] = Localization.GetTextContent(language.ToString());
						}
					}
				}

				int selectIndex = 0;
				if (instance._Languages != null)
				{
					int count = instance._Languages.Count;
					for (int i = 0; i < count; i++)
					{
						SystemLanguage language = instance._Languages[i];
						if (!ArborSettings.autoLanguage && language == ArborSettings.language)
						{
							selectIndex = i + 1;
						}
					}
				}

				EditorGUI.BeginChangeCheck();
				selectIndex = EditorGUILayout.Popup(selectIndex, _LanguageLabels, EditorStyles.toolbarPopup, GUILayout.Width(100.0f));
				if (EditorGUI.EndChangeCheck())
				{
					if (selectIndex == 0)
					{
						ArborSettings.autoLanguage = true;
					}
					else
					{
						ArborSettings.autoLanguage = false;
						ArborSettings.language = instance._Languages[selectIndex - 1];
					}
				}
			}
		}

		public static bool ContainsLanguage(SystemLanguage language)
		{
			return instance._LanguageDics.ContainsKey(language);
		}
	}
}
