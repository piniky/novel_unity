﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Arbor;

namespace ArborEditor
{
	[System.Reflection.Obfuscation(Exclude = true)]
	[InitializeOnLoad]
	internal sealed class CalculateMenuWindow : EditorWindow
	{
		private static CalculateMenuWindow _Instance;

		public static CalculateMenuWindow instance
		{
			get
			{
				if (_Instance == null)
				{
					CalculateMenuWindow[] objects = Resources.FindObjectsOfTypeAll<CalculateMenuWindow>();
					if (objects.Length > 0)
					{
						_Instance = objects[0];
					}
				}
				if (_Instance == null)
				{
					_Instance = ScriptableObject.CreateInstance<CalculateMenuWindow>();
				}
				return _Instance;
			}
		}

		private class Element : System.IComparable
		{
			public int level;
			public GUIContent content;

			public string name
			{
				get
				{
					return content.text;
				}
			}

			public int CompareTo(object obj)
			{
				return name.CompareTo((obj as Element).name);
			}
		}

		private class CalculatorElement : Element
		{
			public System.Type classType;

			public CalculatorElement(int level, string name, System.Type classType, Texture icon)
			{
				this.level = level;
				this.classType = classType;

				this.content = new GUIContent(name, icon);
			}
		}

		[System.Reflection.Obfuscation(Exclude = true)]
		[System.Serializable]
		private class GroupElement : Element
		{
			public Vector2 scroll;
			public int selectedIndex;

			public GroupElement(int level, string name)
			{
				this.level = level;
				this.content = EditorGUITools.GetTextContent(name);
			}
		}

		private static bool s_DirtyList;

		private Element[] _Tree;
		private Element[] _SearchResultTree;

		private List<GroupElement> _Stack = new List<GroupElement>();
		private float _Anim = 1f;
		private int _AnimTarget = 1;
		private long _LastTime;
		private bool _ScrollToSelected;
		private Vector2 _Position;

		private bool hasSearch
		{
			get
			{
				return !string.IsNullOrEmpty(ArborSettings.calculatorSearch);
			}
		}

		private GroupElement activeParent
		{
			get
			{
				return _Stack[_Stack.Count - 2 + _AnimTarget];
			}
		}

		private Element[] activeTree
		{
			get
			{
				if (hasSearch)
				{
					return _SearchResultTree;
				}
				return _Tree;
			}
		}

		private Element activeElement
		{
			get
			{
				if (activeTree == null)
				{
					return null;
				}
				List<Element> children = GetChildren(activeTree, activeParent);
				if (children.Count == 0)
				{
					return null;
				}
				return children[activeParent.selectedIndex];
			}
		}

		static CalculateMenuWindow()
		{
			s_DirtyList = true;
		}

		private List<Element> GetChildren(Element[] tree, Element parent)
		{
			List<Element> list = new List<Element>();
			int num = -1;
			int index;
			for (index = 0; index < tree.Length; ++index)
			{
				if (tree[index] == parent)
				{
					num = parent.level + 1;
					++index;
					break;
				}
			}
			if (num == -1)
				return list;
			for (; index < tree.Length; ++index)
			{
				Element element = tree[index];
				if (element.level >= num)
				{
					if (element.level <= num || hasSearch)
						list.Add(element);
				}
				else
					break;
			}
			return list;
		}

		public class CalculateMenuItem
		{
			public string menuName;
			public Texture icon;
			public System.Type classType;
		};

		private void CreateCalculatorTree()
		{
			List<string> list1 = new List<string>();
			List<Element> list2 = new List<Element>();

			list2.Add(new GroupElement(0, "Calculators"));

			List<CalculateMenuItem> items = new List<CalculateMenuItem>();

			foreach (MonoScript script in MonoImporter.GetAllRuntimeMonoScripts())
			{
				if (script != null && script.hideFlags == 0)
				{
					System.Type classType = script.GetClass();
					if (classType != null)
					{
						if (classType.IsSubclassOf(typeof(Calculator)))
						{
							if (AttributeHelper.HasAttribute<HideBehaviour>(classType))
							{
								continue;
							}

							CalculateMenuItem menuItem = new CalculateMenuItem();
							menuItem.classType = classType;
							menuItem.icon = AssetDatabase.GetCachedIcon(AssetDatabase.GetAssetPath(script));

							menuItem.menuName = "Scripts/" + classType.Name;

							AddCalculatorMenu calculateorMenu = AttributeHelper.GetAttribute<AddCalculatorMenu>(classType);
							if (calculateorMenu != null)
							{
								menuItem.menuName = calculateorMenu.menuName;
							}

							if (string.IsNullOrEmpty(menuItem.menuName))
							{
								continue;
							}

							items.Add(menuItem);
						}
					}
				}
			}

			items.Sort((a, b) =>
			{
				string[] aNames = a.menuName.Split('/');
				string[] bNames = b.menuName.Split('/');
				int i = 0;

				while (i < aNames.Length && i < bNames.Length)
				{
					int compare = 0;
					if (i + 1 >= aNames.Length || i + 1 >= bNames.Length)
					{
						compare = bNames.Length - aNames.Length;
					}
					if (compare == 0)
					{
						compare = aNames[i].CompareTo(bNames[i]);
					}
					if (compare != 0)
					{
						return compare;
					}
					i++;
				}
				return bNames.Length - aNames.Length;
			});

			int itemCount = items.Count;
			for (int itemIndex = 0; itemIndex < itemCount; itemIndex++)
			{
				CalculateMenuItem item = items[itemIndex];

				string str = item.menuName;
				string[] strArray = str.Split(new char[] { '/' });

				while (strArray.Length - 1 < list1.Count)
					list1.RemoveAt(list1.Count - 1);
				while (list1.Count > 0 && strArray[list1.Count - 1] != list1[list1.Count - 1])
					list1.RemoveAt(list1.Count - 1);
				while (strArray.Length - 1 > list1.Count)
				{
					list2.Add(new GroupElement(list1.Count + 1, strArray[list1.Count]));
					list1.Add(strArray[list1.Count]);
				}
				list2.Add(new CalculatorElement(list1.Count + 1, strArray[strArray.Length - 1], item.classType, item.icon));
			}

			_Tree = list2.ToArray();
			if (_Stack.Count == 0)
			{
				_Stack.Add(_Tree[0] as GroupElement);
			}
			else
			{
				GroupElement groupElement = _Tree[0] as GroupElement;
				int level = 0;
				while (true)
				{
					GroupElement groupElement2 = _Stack[level];
					_Stack[level] = groupElement;
					_Stack[level].selectedIndex = groupElement2.selectedIndex;
					_Stack[level].scroll = groupElement2.scroll;
					level++;
					if (level == _Stack.Count)
					{
						break;
					}
					List<Element> children = GetChildren(activeTree, groupElement);
					Element element = children.FirstOrDefault((c) => c.name == _Stack[level].name);
					if (element != null && element is GroupElement)
					{
						groupElement = (element as GroupElement);
					}
					else
					{
						while (_Stack.Count > level)
						{
							_Stack.RemoveAt(level);
						}
					}
				}
			}
			s_DirtyList = false;
			RebuildSearch();
		}

		private void RebuildSearch()
		{
			if (!hasSearch)
			{
				_SearchResultTree = null;
				if (_Stack[_Stack.Count - 1].name == "Search")
				{
					_Stack.Clear();
					_Stack.Add(_Tree[0] as GroupElement);
				}
				_AnimTarget = 1;
				_LastTime = System.DateTime.Now.Ticks;
			}
			else
			{
				string str1 = ArborSettings.calculatorSearch.ToLower();
				string[] strArray = str1.Split(new char[] { ' ' });

				List<Element> list1 = new List<Element>();
				List<Element> list2 = new List<Element>();
				foreach (Element element in _Tree)
				{
					if (element is CalculatorElement)
					{
						string str2 = element.name.ToLower().Replace(" ", string.Empty);
						bool flag1 = true;
						bool flag2 = false;
						for (int index2 = 0; index2 < strArray.Length; ++index2)
						{
							string str3 = strArray[index2];
							if (str2.Contains(str3))
							{
								if (index2 == 0 && str2.StartsWith(str3))
								{
									flag2 = true;
								}
							}
							else
							{
								flag1 = false;
								break;
							}
						}
						if (flag1)
						{
							if (flag2)
								list1.Add(element);
							else
								list2.Add(element);
						}
					}
				}
				list1.Sort();
				list2.Sort();
				List<Element> list3 = new List<Element>();
				list3.Add((Element)new GroupElement(0, "Search"));
				list3.AddRange(list1);
				list3.AddRange(list2);
				_SearchResultTree = list3.ToArray();
				_Stack.Clear();
				_Stack.Add(_SearchResultTree[0] as GroupElement);
				if (GetChildren(activeTree, activeParent).Count >= 1)
				{
					activeParent.selectedIndex = 0;
				}
				else
				{
					activeParent.selectedIndex = -1;
				}
			}
		}

		private GroupElement GetElementRelative(int rel)
		{
			int index = _Stack.Count + rel - 1;
			if (index < 0)
			{
				return null;
			}
			return _Stack[index];
		}

		private void GoToParent()
		{
			if (_Stack.Count <= 1)
			{
				return;
			}
			_AnimTarget = 0;
			_LastTime = System.DateTime.Now.Ticks;
		}

		private void ListGUI(Element[] tree, float anim, GroupElement parent, GroupElement grandParent)
		{
			anim = Mathf.Floor(anim) + Mathf.SmoothStep(0.0f, 1f, Mathf.Repeat(anim, 1f));
			Rect position1 = this.position;
			position1.x = (float)((double)this.position.width * (1.0 - (double)anim) + 1.0);
			position1.y = 30f;
			position1.height -= 30f;
			position1.width -= 2f;
			GUILayout.BeginArea(position1);
			Rect rect = GUILayoutUtility.GetRect(10f, 25f);
			string name = parent.name;
			GUI.Label(rect, name, Styles.header);
			if (grandParent != null)
			{
				Rect position2 = new Rect(rect.x + 4f, rect.y + 7f, 13f, 13f);
				if (Event.current.type == EventType.Repaint)
				{
					Styles.leftArrow.Draw(position2, false, false, false, false);
				}
				if (Event.current.type == EventType.MouseDown && rect.Contains(Event.current.mousePosition))
				{
					this.GoToParent();
					Event.current.Use();
				}
			}
			ListGUI(tree, parent);
			GUILayout.EndArea();
		}

		private void GoToChild(Element e, bool addIfComponent)
		{
			if (e is CalculatorElement)
			{
				if (!addIfComponent)
					return;

				CalculatorElement calculatorElement = e as CalculatorElement;

				ArborEditorWindow.GetCurrent().CreateCalculator(_Position, calculatorElement.classType);

				Close();
			}
			else
			{
				if (hasSearch)
					return;
				_LastTime = System.DateTime.Now.Ticks;
				if (_AnimTarget == 0)
				{
					_AnimTarget = 1;
				}
				else
				{
					if (_Anim != 1.0f)
						return;
					_Anim = 0.0f;
					_Stack.Add(e as GroupElement);
				}
			}
		}

		private void ListGUI(Element[] tree, GroupElement parent)
		{
			parent.scroll = GUILayout.BeginScrollView(parent.scroll);
			EditorGUIUtility.SetIconSize(new Vector2(16f, 16f));
			List<Element> children = GetChildren(tree, parent);
			Rect rect1 = new Rect();
			for (int index1 = 0; index1 < children.Count; ++index1)
			{
				Element e = children[index1];
				Rect rect2 = GUILayoutUtility.GetRect(16.0f, 20.0f, GUILayout.ExpandWidth(true));
				if ((Event.current.type == EventType.MouseMove || Event.current.type == EventType.MouseDown) && (parent.selectedIndex != index1 && rect2.Contains(Event.current.mousePosition)))
				{
					parent.selectedIndex = index1;
					this.Repaint();
				}
				bool flag = false;
				if (index1 == parent.selectedIndex)
				{
					flag = true;
					rect1 = rect2;
				}
				if (Event.current.type == EventType.Repaint)
				{
					(!(e is CalculatorElement) ? Styles.groupButton : Styles.componentButton).Draw(rect2, e.content, false, false, flag, flag);
					if (!(e is CalculatorElement))
					{
						Rect position = new Rect((float)((double)rect2.x + (double)rect2.width - 13.0), rect2.y + 4f, 13f, 13f);
						Styles.rightArrow.Draw(position, false, false, false, false);
					}
				}
				if (Event.current.type == EventType.MouseDown && rect2.Contains(Event.current.mousePosition))
				{
					Event.current.Use();
					parent.selectedIndex = index1;
					GoToChild(e, true);
				}
			}
			EditorGUIUtility.SetIconSize(Vector2.zero);
			GUILayout.EndScrollView();
			if (!_ScrollToSelected || Event.current.type != EventType.Repaint)
				return;
			_ScrollToSelected = false;
			Rect lastRect = GUILayoutUtility.GetLastRect();
			if (rect1.yMax - lastRect.height > parent.scroll.y)
			{
				parent.scroll.y = rect1.yMax - lastRect.height;
				Repaint();
			}
			if (rect1.y >= parent.scroll.y)
			{
				return;
			}
			parent.scroll.y = rect1.y;
			Repaint();
		}

		public void Init(Vector2 position, Rect buttonRect)
		{
			CreateCalculatorTree();
			ShowAsDropDown(buttonRect, new Vector2(300f, 320f));
			Focus();

			_Position = position;
		}

		void DrawBackground()
		{
			if (Event.current.type != EventType.Repaint)
			{
				return;
			}

			Rect rect = position;
			rect.position = Vector2.zero;

			Styles.background.Draw(rect, false, false, false, false);
		}

		void SearchGUI()
		{
			Rect rect = GUILayoutUtility.GetRect(10f, 20f);
			rect.x += 8f;
			rect.width -= 16f;
			GUI.SetNextControlName("ArborCalculatorSearch");
			string str = EditorGUITools.SearchField(rect, ArborSettings.calculatorSearch);
			if (str != ArborSettings.calculatorSearch)
			{
				ArborSettings.calculatorSearch = str;
				RebuildSearch();
			}
		}

		private void HandleKeyboard()
		{
			Event current = Event.current;
			if (current.type != EventType.KeyDown)
			{
				return;
			}

			if (current.keyCode == KeyCode.DownArrow)
			{
				++activeParent.selectedIndex;
				activeParent.selectedIndex = Mathf.Min(activeParent.selectedIndex, GetChildren(activeTree, activeParent).Count - 1);
				_ScrollToSelected = true;
				current.Use();
			}
			if (current.keyCode == KeyCode.UpArrow)
			{
				--activeParent.selectedIndex;
				activeParent.selectedIndex = Mathf.Max(activeParent.selectedIndex, 0);
				_ScrollToSelected = true;
				current.Use();
			}
			if (current.keyCode == KeyCode.Return || current.keyCode == KeyCode.KeypadEnter)
			{
				GoToChild(activeElement, true);
				current.Use();
			}
			if (hasSearch)
			{
				return;
			}
			if (current.keyCode == KeyCode.LeftArrow || current.keyCode == KeyCode.Backspace)
			{
				GoToParent();
				current.Use();
			}
			if (current.keyCode == KeyCode.RightArrow)
			{
				GoToChild(activeElement, false);
				current.Use();
			}
			if (current.keyCode != KeyCode.Escape)
			{
				return;
			}
			Close();
			current.Use();
		}

		[System.Reflection.Obfuscation(Exclude = true)]
		void OnEnable()
		{
			wantsMouseMove = true;
		}

		[System.Reflection.Obfuscation(Exclude = true)]
		void OnGUI()
		{
			DrawBackground();

			if (s_DirtyList)
			{
				CreateCalculatorTree();
			}

			HandleKeyboard();

			GUILayout.Space(7f);
			EditorGUI.FocusTextInControl("ArborCalculatorSearch");

			SearchGUI();

			ListGUI(activeTree, _Anim, GetElementRelative(0), GetElementRelative(-1));
			if (_Anim < 1.0)
			{
				ListGUI(activeTree, _Anim + 1f, GetElementRelative(-1), GetElementRelative(-2));
			}

			if (_Anim == _AnimTarget || Event.current.type != EventType.Repaint)
				return;
			long ticks = System.DateTime.Now.Ticks;
			float num = (float)(ticks - _LastTime) / 1E+07f;
			_LastTime = ticks;
			_Anim = Mathf.MoveTowards(_Anim, _AnimTarget, num * 4f);
			if (_AnimTarget == 0 && _Anim == 0.0f)
			{
				_Anim = 1f;
				_AnimTarget = 1;
				_Stack.RemoveAt(_Stack.Count - 1);
			}
			Repaint();
		}
	}
}
