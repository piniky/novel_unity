﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

using Arbor;

namespace ArborEditor
{
	[CustomPropertyDrawer(typeof(FlexibleTransform))]
	public class FlexibleTransformPropertyDrawer : PropertyDrawer
	{
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.BeginProperty(position, label, property);

			SerializedProperty typeProperty = property.FindPropertyRelative("_Type");

			FlexibleTransform.Type type = EnumUtility.GetValueFromIndex<FlexibleTransform.Type>(typeProperty.enumValueIndex);

			switch (type)
			{
				case FlexibleTransform.Type.Constant:
					EditorGUI.PropertyField(EditorGUITools.SubtractPopupWidth(position), property.FindPropertyRelative("_Value"), label);
					break;
				case FlexibleTransform.Type.Parameter:
					EditorGUI.PropertyField(EditorGUITools.SubtractPopupWidth(position), property.FindPropertyRelative("_Parameter"), label);
					break;
				case FlexibleTransform.Type.Calculator:
					EditorGUI.PropertyField(EditorGUITools.SubtractPopupWidth(position), property.FindPropertyRelative("_Slot"), label);
					break;
			}

			Rect popupRect = EditorGUITools.GetPopupRect(position);

			if (EditorGUITools.ButtonMouseDown(popupRect, GUIContent.none,FocusType.Passive, Styles.shurikenDropDown))
			{
				GenericMenu menu = new GenericMenu();
				foreach (FlexibleTransform.Type t in System.Enum.GetValues(typeof(FlexibleTransform.Type)))
				{
					menu.AddItem( EditorGUITools.GetTextContent( t.ToString()), t == type, SelectType,new KeyValuePair<SerializedProperty, FlexibleTransform.Type>(property, t) );
				}
				menu.DropDown(popupRect);
            }

			EditorGUI.EndProperty();
		}

		private static void SelectType(object obj)
		{
			KeyValuePair<SerializedProperty, FlexibleTransform.Type> pair = (KeyValuePair<SerializedProperty, FlexibleTransform.Type>)obj;
			SerializedProperty property = pair.Key;
			FlexibleTransform.Type type = pair.Value;

			property.serializedObject.Update();

			SerializedProperty typeProperty = property.FindPropertyRelative("_Type");

			SerializedProperty slotProperty = property.FindPropertyRelative("_Slot");

			FlexibleTransform.Type oldType = EnumUtility.GetValueFromIndex<FlexibleTransform.Type>(typeProperty.enumValueIndex);
			if (oldType == FlexibleTransform.Type.Calculator)
			{
				Object targetObject = property.serializedObject.targetObject;
				ArborFSMInternal stateMachine = null;
				if (targetObject is StateBehaviour)
				{
					StateBehaviour behaviour = targetObject as StateBehaviour;
					stateMachine = behaviour.stateMachine;
				}
				else if (targetObject is Calculator)
				{
					Calculator calculator = targetObject as Calculator;
					stateMachine = calculator.stateMachine;
				}

				CalculatorBranch branch = stateMachine.GetCalculatorBranchFromID(slotProperty.FindPropertyRelative("branchID").intValue);
				if (branch != null)
				{
					stateMachine.DeleteCalculatorBranch(branch);
				}
			}

			typeProperty.enumValueIndex = EnumUtility.GetIndexFromValue(type);

			property.serializedObject.ApplyModifiedProperties();
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			float height = EditorGUIUtility.singleLineHeight;

			SerializedProperty typeProperty = property.FindPropertyRelative("_Type");

			FlexibleTransform.Type type = EnumUtility.GetValueFromIndex<FlexibleTransform.Type>(typeProperty.enumValueIndex);

			switch (type)
			{
				case FlexibleTransform.Type.Constant:
					height = EditorGUI.GetPropertyHeight(property.FindPropertyRelative("_Value"));
					break;
				case FlexibleTransform.Type.Parameter:
					height = EditorGUI.GetPropertyHeight(property.FindPropertyRelative("_Parameter"));
					break;
				case FlexibleTransform.Type.Calculator:
					height = EditorGUI.GetPropertyHeight(property.FindPropertyRelative("_Slot"));
					break;
			}

			return height;
		}
	}
}
