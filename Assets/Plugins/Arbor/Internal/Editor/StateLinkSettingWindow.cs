﻿using UnityEngine;
using UnityEditor;
using System.Collections;

using Arbor;

namespace ArborEditor
{
	public class StateLinkSettingWindow : EditorWindow
	{
		private class Styles
		{
			public static GUIStyle background;

			static Styles()
			{
				background = (GUIStyle)"grey_border";
			}
		}

		private static StateLinkSettingWindow _Instance;

		public static StateLinkSettingWindow instance
		{
			get
			{
				if (_Instance == null)
				{
					StateLinkSettingWindow[] objects = Resources.FindObjectsOfTypeAll<StateLinkSettingWindow>();
					if (objects.Length > 0)
					{
						_Instance = objects[0];
					}
				}
				if (_Instance == null)
				{
					_Instance = ScriptableObject.CreateInstance<StateLinkSettingWindow>();
				}
				return _Instance;
			}
		}

		private SerializedObject _SerializedObject;
		private string _StateLinkPath;

		public void Init(SerializedProperty stateLinkProperty, Rect buttonRect)
		{
			buttonRect = EditorGUITools.GUIToScreenRect(buttonRect);
			ShowAsDropDown(buttonRect, new Vector2(300f, 60f));
			Focus();

			_SerializedObject = stateLinkProperty.serializedObject;
			_StateLinkPath = stateLinkProperty.propertyPath;
		}

		[System.Reflection.Obfuscation(Exclude = true)]
		private void OnEnable()
		{
			minSize = new Vector2(minSize.x, 60f);
			maxSize = new Vector2(maxSize.x, 60f);
		}

		void DrawBackground()
		{
			if (Event.current.type != EventType.Repaint)
			{
				return;
			}

			Rect rect = position;
			rect.position = Vector2.zero;

			Styles.background.Draw(rect, false, false, false, false);
		}

		[System.Reflection.Obfuscation(Exclude = true)]
		void OnGUI()
		{
			_SerializedObject.Update();

			DrawBackground();

			SerializedProperty stateLinkProperty = _SerializedObject.FindProperty(_StateLinkPath);

			SerializedProperty nameProperty = stateLinkProperty.FindPropertyRelative("name");
			SerializedProperty transitionTimingProperty = stateLinkProperty.FindPropertyRelative("transitionTiming");
			SerializedProperty lineColorProperty = stateLinkProperty.FindPropertyRelative("lineColor");
			SerializedProperty lineColorChangedProperty = stateLinkProperty.FindPropertyRelative("lineColorChanged");

			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(nameProperty);
			if (EditorGUI.EndChangeCheck())
			{
				ArborEditorWindow.GetCurrent().Repaint();
			}

			System.Type type = null;
			System.Reflection.FieldInfo stateLinkFieldInfo = EditorGUITools.GetFieldInfoFromProperty(stateLinkProperty, out type);

			FixedTransitionTiming fixedTransitionTiming = AttributeHelper.GetAttribute<FixedTransitionTiming>(stateLinkFieldInfo);
			FixedImmediateTransition fixedImmediateTransition = AttributeHelper.GetAttribute<FixedImmediateTransition>(stateLinkFieldInfo);

			int tempTransitionTiming = transitionTimingProperty.enumValueIndex;
			if (fixedTransitionTiming != null)
			{
				transitionTimingProperty.enumValueIndex = EnumUtility.GetIndexFromValue(fixedTransitionTiming.transitionTiming);
				GUI.enabled = false;
			}
			else if (fixedImmediateTransition != null)
			{
				transitionTimingProperty.enumValueIndex = EnumUtility.GetIndexFromValue(fixedImmediateTransition.immediate? TransitionTiming.Immediate : TransitionTiming.LateUpdateOverwrite);
				GUI.enabled = false;
			}

			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(transitionTimingProperty);
			if (EditorGUI.EndChangeCheck())
			{
				ArborEditorWindow.GetCurrent().Repaint();
			}

			if (fixedTransitionTiming != null || fixedImmediateTransition != null)
			{
				transitionTimingProperty.enumValueIndex = tempTransitionTiming;
				GUI.enabled = true;
			}

			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(lineColorProperty);
			if (EditorGUI.EndChangeCheck())
			{
				lineColorChangedProperty.boolValue = true;
				ArborEditorWindow.GetCurrent().Repaint();
			}

			_SerializedObject.ApplyModifiedProperties();
		}
	}
}
