﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Arbor;

namespace ArborEditor
{
	[System.Reflection.Obfuscation(Exclude = true)]
	public sealed class ArborEditorWindow : EditorWindow, IHasCustomMenu
	{
		[MenuItem("Window/Arbor Editor")]
		public static void OpenFromMenu()
		{
			EditorWindow.GetWindow<ArborEditorWindow>("Arbor Editor");
		}

		public static void Open(ArborFSMInternal stateMachine)
		{
			ArborEditorWindow window = EditorWindow.GetWindow<ArborEditorWindow>("Arbor Editor");
			window.Initialize(stateMachine);
		}

		[System.Reflection.Obfuscation(Exclude = true)]
		[SerializeField]
		private ArborFSMInternal _StateMachine;

		[System.Reflection.Obfuscation(Exclude = true)]
		[SerializeField]
		private int _StateMachineInstanceID = 0;

		[System.Reflection.Obfuscation(Exclude = true)]
		[SerializeField]
		private List<int> _Selection = new List<int>();

		[SerializeField] private GraphGUI _GraphGUI;

		[System.Reflection.Obfuscation(Exclude = true)]
		[SerializeField]
		private bool _IsLocked = false;

		static ArborEditorWindow _CurrentWindow;

		[System.Reflection.Obfuscation(Exclude = true)]
		[SerializeField]
		private List<NodeEditor> _NodeEditors = new List<NodeEditor>();

		private Dictionary<int, NodeEditor> _DicNodeEditors = new Dictionary<int, NodeEditor>();

		private ControlLayer _NodeCommentLayer = new ControlLayer();
		private Dictionary<Node, NodeCommentControl> _DicNodeCommentControls = new Dictionary<Node, NodeCommentControl>();

		private ControlLayer _GroupLayer = new ControlLayer();
		private Dictionary<GroupNode, GroupControl> _DicGroupControls = new Dictionary<GroupNode, GroupControl>();

		private bool _DragStateBranchEnable = false;
		private Bezier2D _DragStateBranchBezier;
		private int _DragStateBranchNodeID = 0;
		private int _DragStateBranchHoverStateID = 0;

		private bool _DragCalculatorBranchEnable = false;
		private int _DragCalculatorBranchNodeID = 0;
		private Bezier2D _DragCalculatorBranchBezier;

		private static readonly int s_DragSelectionControlID = "DragSelection".GetHashCode();
		private static readonly int s_DragNodesControlID = "DragNodes".GetHashCode();

		private Vector2 _DragBeginPos;
		private List<int> _OldSelection;
		public enum SelectionMode
		{
			None,
			Pick,
			Rect,
		};
		private SelectionMode _SelectionMode = SelectionMode.None;
		private bool _IsDragSelection = false;

		private Dictionary<Node, Rect> _DragNodePositions = new Dictionary<Node, Rect>();
		private Rect _ResizeNodePosition;
		private Vector2 _LastMousePosition;
		private Vector2 _DragStateDistance;

		Rect _ToolBarRect;
		Rect _StateListRect;

		Vector2 _StateListScrollPos = Vector2.zero;

		enum SearchMode
		{
			All,
			Name,
			Type
		};

		SearchMode _SearchMode = SearchMode.All;
		string _SearchText;

		bool _FrameSelected = false;

		bool _IsPlaying = false;

		private Node[] selection
		{
			get
			{
				List<Node> nodes = new List<Node>();

				int selectionCount = _Selection.Count;
				for (int selectionIndex = 0; selectionIndex < selectionCount; selectionIndex++)
				{
					int nodeID = _Selection[selectionIndex];
					Node node = _StateMachine.GetNodeFromID(nodeID);

					if (node != null)
					{
						nodes.Add(node);
					}
				}

				return nodes.ToArray();
			}
		}

		void FinalizeCalculatorBranch()
		{
			foreach (KeyValuePair<int, Mesh> pair in _CalculatorBranchMeshes)
			{
				DestroyImmediate(pair.Value);
			}
			_CalculatorBranchMeshes.Clear();
		}

		void CreateNodeEditor(Node node)
		{
			if (!NodeEditor.HasEditor(node))
			{
				return;
			}

			NodeEditor editor = NodeEditor.CreateEditor(this, node);

			if (editor == null)
			{
				return;
			}

			_NodeEditors.Add(editor);
			_DicNodeEditors.Add(node.nodeID, editor);
		}

		NodeEditor GetNodeEditor(Node node)
		{
			NodeEditor result;
			if (_DicNodeEditors.TryGetValue(node.nodeID, out result))
			{
				return result;
			}

			for (int i = 0, count = _NodeEditors.Count; i < count; i++)
			{
				NodeEditor nodeEditor = _NodeEditors[i];
				if (nodeEditor.nodeID == node.nodeID)
				{
					_DicNodeEditors.Add(node.nodeID, nodeEditor);
					return nodeEditor;
				}
			}

			return null;
		}

		void DeleteNodeEditor(Node node)
		{
			NodeEditor nodeEditor = GetNodeEditor(node);

			if (nodeEditor != null)
			{
				_NodeEditors.Remove(nodeEditor);
				_DicNodeEditors.Remove(node.nodeID);

				Object.DestroyImmediate(nodeEditor);
			}
		}

		void InitNodeEditor()
		{
			FinalizeNodeEditor();

			if (_StateMachine == null)
			{
				return;
			}

			int nodeCount = _StateMachine.nodeCount;
			for (int nodeIndex = 0; nodeIndex < nodeCount; nodeIndex++)
			{
				Node node = _StateMachine.GetNodeFromIndex(nodeIndex);

				CreateNodeEditor(node);
			}
		}

		private bool _ClearPropertyDrawerCache;

		void RebuildNodeEditor()
		{
			bool removed = false;

			for (int count = _NodeEditors.Count, i = count - 1; i >= 0; i--)
			{
				NodeEditor nodeEditor = _NodeEditors[i];
				if (nodeEditor == null)
				{
					_NodeEditors.RemoveAt(i);

					_ClearPropertyDrawerCache = true;
					removed = true;
				}
				else
				{
					Node node = _StateMachine != null ? _StateMachine.GetNodeFromID(nodeEditor.nodeID) : null;

					if (node == null)
					{
						_NodeEditors.RemoveAt(i);
						Object.DestroyImmediate(nodeEditor);

						_ClearPropertyDrawerCache = true;
						removed = true;
					}
					else
					{
						nodeEditor.Validate(node);
					}
				}
			}

			if (removed)
			{
				_DicNodeEditors.Clear();
			}

			if (_StateMachine != null)
			{
				int nodeCount = _StateMachine.nodeCount;
				for (int nodeIndex = 0; nodeIndex < nodeCount; nodeIndex++)
				{
					Node node = _StateMachine.GetNodeFromIndex(nodeIndex);

					NodeEditor nodeEditor = GetNodeEditor(node);
					if (nodeEditor == null)
					{
						CreateNodeEditor(node);

						_ClearPropertyDrawerCache = true;
					}
				}
			}
		}

		void FinalizeNodeEditor()
		{
			for (int i = 0, count = _NodeEditors.Count; i < count; i++)
			{
				NodeEditor nodeEditor = _NodeEditors[i];
				if (nodeEditor != null)
				{
					Object.DestroyImmediate(nodeEditor);
				}
			}

			_NodeEditors.Clear();
			_DicNodeEditors.Clear();
		}

		public void UpdateNodeCommentControl(Node node)
		{
			if (node.showComment)
			{
				CreateNodeCommentControl(node);
			}
			else
			{
				DeleteNodeCommentControl(node);
			}
		}

		void CreateNodeCommentControl(Node node)
		{
			if (GetNodeCommentControl(node) != null)
			{
				return;
			}

			NodeCommentControl nodeCommentControl = CreateInstance<NodeCommentControl>();
			nodeCommentControl.hideFlags = HideFlags.HideAndDontSave;
			nodeCommentControl.node = node;
			nodeCommentControl.focusType = FocusType.Keyboard;

			_NodeCommentLayer.Add(nodeCommentControl);
			_DicNodeCommentControls.Add(node, nodeCommentControl);
		}

		NodeCommentControl GetNodeCommentControl(Node node)
		{
			NodeCommentControl result;
			if (_DicNodeCommentControls.TryGetValue(node, out result))
			{
				return result;
			}

			for (int i = 0, count = _NodeCommentLayer.controlCount; i < count; i++)
			{
				NodeCommentControl nodeCommentControl = _NodeCommentLayer.GetControlFromIndex(i) as NodeCommentControl;
				if (nodeCommentControl.node == node)
				{
					_DicNodeCommentControls.Add(node, nodeCommentControl);
					return nodeCommentControl;
				}
			}
			return null;
		}

		void DeleteNodeCommentControl(Node node)
		{
			NodeCommentControl nodeCommentControl = GetNodeCommentControl(node);
			if (nodeCommentControl != null)
			{
				_NodeCommentLayer.Remove(nodeCommentControl);
				_DicNodeCommentControls.Remove(node);
			}
		}

		void InitNodeCommentLayer()
		{
			FinalizeNodeCommentLayer();

			if (_StateMachine == null)
			{
				return;
			}

			int nodeCount = _StateMachine.nodeCount;
			for (int nodeIndex = 0; nodeIndex < nodeCount; nodeIndex++)
			{
				Node node = _StateMachine.GetNodeFromIndex(nodeIndex);

				UpdateNodeCommentControl(node);
			}
		}

		void RebuildNodeCommentLayer()
		{
			bool removed = false;

			for (int count = _NodeCommentLayer.controlCount, i = count - 1; i >= 0; i--)
			{
				NodeCommentControl nodeCommentControl = _NodeCommentLayer.GetControlFromIndex(i) as NodeCommentControl;
				Node node = _StateMachine != null ? _StateMachine.GetNodeFromID(nodeCommentControl.nodeID) : null;

				if (node == null || !node.showComment)
				{
					_NodeCommentLayer.Remove(nodeCommentControl);
					removed = true;
				}
				else
				{
					nodeCommentControl.node = node;
				}
			}

			if (removed)
			{
				_DicNodeCommentControls.Clear();
			}

			if (_StateMachine != null)
			{
				int nodeCount = _StateMachine.nodeCount;
				for (int nodeIndex = 0; nodeIndex < nodeCount; nodeIndex++)
				{
					Node node = _StateMachine.GetNodeFromIndex(nodeIndex);

					NodeCommentControl nodeCommentControl = GetNodeCommentControl(node);
					if (nodeCommentControl == null)
					{
						UpdateNodeCommentControl(node);
					}
				}
			}
		}

		void FinalizeNodeCommentLayer()
		{
			_NodeCommentLayer.Clear();
			_DicNodeCommentControls.Clear();
		}

		void CreateGroupControl(GroupNode groupNode)
		{
			GroupControl groupControl = CreateInstance<GroupControl>();
			groupControl.hideFlags = HideFlags.HideAndDontSave;
			groupControl.Initialize(this, groupNode);

			_GroupLayer.Add(groupControl);
			_DicGroupControls.Add(groupNode, groupControl);
		}

		GroupControl GetGroupControl(GroupNode groupNode)
		{
			GroupControl result;
			if (_DicGroupControls.TryGetValue(groupNode, out result))
			{
				return result;
			}

			for (int i = 0, count = _GroupLayer.controlCount; i < count; i++)
			{
				GroupControl groupControl = _GroupLayer.GetControlFromIndex(i) as GroupControl;
				if (groupControl.groupNode == groupNode)
				{
					_DicGroupControls.Add(groupNode, groupControl);
					return groupControl;
				}
			}

			return null;
		}

		void DeleteGroupControl(GroupNode groupNode)
		{
			GroupControl groupControl = GetGroupControl(groupNode);
			if (groupControl != null)
			{
				_GroupLayer.Remove(groupControl);
				_DicGroupControls.Remove(groupNode);
			}
		}

		void InitGroupLayer()
		{
			FinalizeGroupLayer();

			if (_StateMachine == null)
			{
				return;
			}

			_GroupLayer.order = -1000;

			int groupCount = _StateMachine.groupCount;
			for (int groupIndex = 0; groupIndex < groupCount; groupIndex++)
			{
				GroupNode groupNode = _StateMachine.GetGroupFromIndex(groupIndex);

				CreateGroupControl(groupNode);
			}
		}

		void RebuildGroupLayer()
		{
			bool removed = false;

			for (int count = _GroupLayer.controlCount, i = count - 1; i >= 0; i--)
			{
				GroupControl groupControl = _GroupLayer.GetControlFromIndex(i) as GroupControl;
				GroupNode groupNode = _StateMachine != null ? _StateMachine.GetGroupFromID(groupControl.groupNodeID) : null;

				if (groupNode == null)
				{
					_GroupLayer.Remove(groupControl);

					removed = true;
				}
				else
				{
					groupControl.groupNode = groupNode;
				}
			}

			if (removed)
			{
				_DicGroupControls.Clear();
			}

			if (_StateMachine != null)
			{
				int groupCount = _StateMachine.groupCount;
				for (int groupIndex = 0; groupIndex < groupCount; groupIndex++)
				{
					GroupNode groupNode = _StateMachine.GetGroupFromIndex(groupIndex);

					GroupControl groupControl = GetGroupControl(groupNode);
					if (groupControl == null)
					{
						CreateGroupControl(groupNode);
					}
				}
			}
		}

		void FinalizeGroupLayer()
		{
			_GroupLayer.Clear();
			_DicGroupControls.Clear();
		}

		private bool _Initialized = false;

		[System.Reflection.Obfuscation(Exclude = true)]
		void OnEnable()
		{
			_CurrentWindow = this;

			if (_GraphGUI == null)
			{
				_GraphGUI = new GraphGUI();
			}

			_GraphGUI.hostWindow = this;
			
			wantsMouseMove = true;

			titleContent = new GUIContent("Arbor Editor", EditorResources.LoadTexture("ArborLogoIcon"));

			_Initialized = false;

			Repaint();
		}

		[System.Reflection.Obfuscation(Exclude = true)]
		void OnDisable()
		{
			FinalizeCalculatorBranch();
			FinalizeNodeEditor();
			FinalizeGroupLayer();
			FinalizeNodeCommentLayer();
		}

		[System.Reflection.Obfuscation(Exclude = true)]
		void OnValidate()
		{
			_DicNodeEditors.Clear();
			_DicGroupControls.Clear();
			_DicNodeCommentControls.Clear();

			RebuildNodeEditor();
			RebuildNodeCommentLayer();
			RebuildGroupLayer();
		}

		void Initialize()
		{
			FinalizeCalculatorBranch();
			FinalizeNodeCommentLayer();
			FinalizeGroupLayer();
			FinalizeNodeEditor();

			_StateMachine = null;
			_StateMachineInstanceID = 0;

			_Initialized = false;

			_Selection.Clear();

			EditorUtility.SetDirty(this);

			Repaint();
		}

		void Initialize(ArborFSMInternal stateMachine)
		{
			if (_StateMachine == stateMachine)
			{
				return;
			}

			Undo.RecordObject(this, "Select StateMachine");

			_CurrentWindow = this;

			FinalizeCalculatorBranch();
			FinalizeNodeCommentLayer();
			FinalizeGroupLayer();
			FinalizeNodeEditor();

			_StateMachine = stateMachine;

			if (_StateMachine != null)
			{
				_StateMachineInstanceID = _StateMachine.GetInstanceID();
			}
			else
			{
				_StateMachineInstanceID = 0;
			}

			_Initialized = false;

			_Selection.Clear();

			EditorUtility.SetDirty(this);

			Repaint();
		}

		public void BeginDragStateBranch(int nodeID)
		{
			_DragStateBranchEnable = true;
			_DragStateBranchNodeID = nodeID;
			_DragStateBranchHoverStateID = 0;
		}

		public void EndDragStateBranch()
		{
			_DragStateBranchEnable = false;
			_DragStateBranchNodeID = 0;
			_DragStateBranchHoverStateID = 0;

			Repaint();
		}

		public void DragStateBranchBezie(Bezier2D bezier)
		{
			_DragStateBranchBezier = bezier;
		}

		public void DragStateBranchHoverStateID(int stateID)
		{
			if (_DragStateBranchHoverStateID != stateID)
			{
				_DragStateBranchHoverStateID = stateID;
			}
		}

		public void BeginDragCalculatorBranch(int nodeID)
		{
			_DragCalculatorBranchEnable = true;
			_DragCalculatorBranchNodeID = nodeID;
		}

		public void EndDragCalculatorBranch()
		{
			_DragCalculatorBranchEnable = false;
			_DragCalculatorBranchNodeID = 0;

			Repaint();
		}

		public void DragCalculatorBranchBezier(Vector2 start, Vector2 startTangent, Vector2 end, Vector2 endTangent)
		{
			if (_DragCalculatorBranchBezier == null)
			{
				_DragCalculatorBranchBezier = new Bezier2D(start, startTangent, end, endTangent);
			}
			else
			{
				_DragCalculatorBranchBezier.startPosition = start;
				_DragCalculatorBranchBezier.startControl = startTangent;
				_DragCalculatorBranchBezier.endPosition = end;
				_DragCalculatorBranchBezier.endControl = endTangent;
			}
		}

		public static ArborEditorWindow GetCurrent()
		{
			return _CurrentWindow;
		}

		void SelectAll()
		{
			Undo.IncrementCurrentGroup();

			Undo.RecordObject(this, "Selection State");

			_Selection.Clear();

			for (int i = 0, count = _StateMachine.nodeCount; i < count; i++)
			{
				Node node = _StateMachine.GetNodeFromIndex(i);
				_Selection.Add(node.nodeID);
			}

			Undo.CollapseUndoOperations(Undo.GetCurrentGroup());

			EditorUtility.SetDirty(this);
		}

		public bool IsSelection(Node node)
		{
			return _Selection.Contains(node.nodeID);
		}

		public void ChangeSelectNode(Node node, bool add)
		{
			int nodeID = node.nodeID;

			if (add)
			{
				Undo.IncrementCurrentGroup();

				Undo.RecordObject(this, "Selection Node");

				if (_Selection.Contains(nodeID))
				{
					_Selection.Remove(nodeID);
				}
				else
				{
					_Selection.Add(nodeID);
				}

				Undo.CollapseUndoOperations(Undo.GetCurrentGroup());

				EditorUtility.SetDirty(this);
			}
			else
			{
				if (!_Selection.Contains(nodeID))
				{
					Undo.IncrementCurrentGroup();

					Undo.RecordObject(this, "Selection Node");

					_Selection.Clear();
					_Selection.Add(nodeID);

					Undo.CollapseUndoOperations(Undo.GetCurrentGroup());

					EditorUtility.SetDirty(this);
				}
				HandleUtility.Repaint();
			}

			GroupNode group = node as GroupNode;
			if (group != null)
			{
				GroupControl groupControl = GetGroupControl(group);
				if (groupControl != null)
				{
					_GroupLayer.Focus(groupControl);
				}
			}
		}

		bool IsNodeInRect(Node node, Rect rect)
		{
			Rect position = node.position;

			if (node is GroupNode)
			{
				GUIStyle nodeStyle = GetNodeStyle(node);
				position = NodeUtility.GetHeaderRect(nodeStyle, node.position);
			}

			return (position.xMax >= rect.x && position.x <= rect.xMax &&
				position.yMax >= rect.y && position.y <= rect.yMax);
		}

		List<Node> GetNodesInRect(Rect rect)
		{
			List<Node> nodes = new List<Node>();

			for (int i = 0, count = _StateMachine.nodeCount; i < count; i++)
			{
				Node node = _StateMachine.GetNodeFromIndex(i);

				if (IsNodeInRect(node, rect))
				{
					nodes.Add(node);
				}
			}

			return nodes;
		}

		void SelectNodesInRect(Rect rect)
		{
			Undo.RecordObject(this, "Selection State");

			_Selection.Clear();

			for (int i = 0, count = _StateMachine.nodeCount; i < count; i++)
			{
				Node node = _StateMachine.GetNodeFromIndex(i);

				if (IsNodeInRect(node, rect))
				{
					_Selection.Add(node.nodeID);
				}
			}

			Undo.CollapseUndoOperations(Undo.GetCurrentGroup());

			EditorUtility.SetDirty(this);
		}

		public bool OnBeginDragNodes(Event current, Node selectTargetNode, Rect nodePosition)
		{
			int nodeID = selectTargetNode.nodeID;
			if (IsRenaming(nodeID))
			{
				GUIStyle style = GetNodeStyle(selectTargetNode);
				GUIContent content = GetNodeContent(selectTargetNode);
				if (NodeUtility.GetHeaderContentRect(style, content,nodePosition).Contains(current.mousePosition))
				{
					return false;
				}
			}

			ChangeSelectNode(selectTargetNode, EditorGUI.actionKey || current.shift);

			Undo.IncrementCurrentGroup();

			_LastMousePosition = selectTargetNode.position.position + current.mousePosition;

			_DragStateDistance = Vector2.zero;
			foreach (Node node in selection)
			{
				_DragNodePositions[node] = node.position;

				if (node is GroupNode)
				{
					foreach (Node targetNode in GetNodesInRect(node.position))
					{
						if (!(targetNode is GroupNode))
						{
							_DragNodePositions[targetNode] = targetNode.position;
						}
					}
				}
			}

			return true;
		}

		public void OnEndDragNodes(Event current)
		{
			_DragNodePositions.Clear();
		}

		public void OnDragNodes(Event current, Node selectTargetNode)
		{
			Vector2 currentPosition = selectTargetNode.position.position + current.mousePosition;
			_DragStateDistance += currentPosition - _LastMousePosition;
			_LastMousePosition = currentPosition;
			foreach (KeyValuePair<Node, Rect> pair in _DragNodePositions)
			{
				Node node = pair.Key;
				Rect rect = pair.Value;

				Rect position = node.position;
				position.x = rect.x + _DragStateDistance.x;
				position.y = rect.y + _DragStateDistance.y;

				position = EditorGUITools.SnapPositionToGrid(position);

				if ((position.x != node.position.x || position.y != node.position.y))
				{
					Undo.RecordObject(_StateMachine, "Move Node");

					node.position = position;

					Undo.CollapseUndoOperations(Undo.GetCurrentGroup());

					EditorUtility.SetDirty(_StateMachine);
				}
			}
		}

		public void OnEscapeDragNodes(Event current)
		{
			foreach (KeyValuePair<Node, Rect> pair in _DragNodePositions)
			{
				Node node = pair.Key;
				Rect position = pair.Value;

				position = EditorGUITools.SnapPositionToGrid(position);

				if ((position.x != node.position.x || position.y != node.position.y))
				{
					Undo.RecordObject(_StateMachine, "Move Node");

					node.position = position;

					Undo.CollapseUndoOperations(Undo.GetCurrentGroup());

					EditorUtility.SetDirty(_StateMachine);
				}
			}

			OnEndDragNodes(current);
		}

		public void OnBeginResizeNode(Event current, Node node)
		{
			_LastMousePosition = node.position.position + current.mousePosition;

			ChangeSelectNode(node, false);

			_ResizeNodePosition = node.position;
		}

		public void OnResizeNode(Event current, Node node, Vector2 minSize)
		{
			Vector2 mousePosition = node.position.position + current.mousePosition;
			mousePosition = EditorGUITools.SnapToGrid(mousePosition);

			_DragStateDistance = mousePosition - _LastMousePosition;

			Undo.RecordObject(_StateMachine, "Move Node");

			Vector2 nodeSize = _ResizeNodePosition.size + _DragStateDistance;
			node.position.size = Vector2.Max(nodeSize, minSize);

			Undo.CollapseUndoOperations(Undo.GetCurrentGroup());

			EditorUtility.SetDirty(_StateMachine);
		}

		public bool IsDragScroll()
		{
			return _GraphGUI.isDragScroll;
		}

		public void DragNodes(Node selectTargetNode)
		{
			Rect nodePosition = selectTargetNode.position;
			nodePosition.x = nodePosition.y = 0;

			Event current = Event.current;
			int controlId = GUIUtility.GetControlID(s_DragNodesControlID, FocusType.Passive);

			switch (current.GetTypeForControl(controlId))
			{
				case EventType.MouseDown:
					if (current.button == 0 && nodePosition.Contains(current.mousePosition))
					{
						if (OnBeginDragNodes(current, selectTargetNode, nodePosition))
						{
							GUIUtility.hotControl = controlId;
							GUIUtility.keyboardControl = 0;

							current.Use();
						}
					}
					break;
				case EventType.MouseUp:
					if (GUIUtility.hotControl == controlId)
					{
						OnEndDragNodes(current);
						GUIUtility.hotControl = 0;
						current.Use();
					}
					break;
				case EventType.MouseDrag:
					if (GUIUtility.hotControl == controlId)
					{
						OnDragNodes(current,selectTargetNode);

						current.Use();
					}
					break;
				case EventType.KeyDown:
					if (GUIUtility.hotControl == controlId && current.keyCode == KeyCode.Escape)
					{
						OnEscapeDragNodes(current);

						GUIUtility.hotControl = 0;
						current.Use();

						Undo.CollapseUndoOperations(Undo.GetCurrentGroup());
					}
					break;
				case EventType.Repaint:
					if (_GraphGUI.isDragScroll && GUIUtility.hotControl == controlId)
					{
						OnDragNodes(current, selectTargetNode);
					}
					break;
			}
		}

		bool IsContainsState(Vector2 position)
		{
			for (int i = 0, count = _StateMachine.stateCount; i < count; i++)
			{
				State state = _StateMachine.GetStateFromIndex(i);
				if (state.position.Contains(position))
				{
					return true;
				}
			}
			return false;
		}

		void DragSelection()
		{
			int controlId = GUIUtility.GetControlID(s_DragSelectionControlID, FocusType.Passive);
			Event current = Event.current;

			switch (current.GetTypeForControl(controlId))
			{
				case EventType.MouseDown:
					if (!IsContainsState(current.mousePosition) && current.button == 0 && !(current.clickCount == 2 || current.alt))
					{
						Undo.IncrementCurrentGroup();

						GUIUtility.hotControl = GUIUtility.keyboardControl = controlId;
						_DragBeginPos = current.mousePosition;
						_OldSelection = new List<int>(_Selection);
						if (!EditorGUI.actionKey && !current.shift)
						{
							_Selection.Clear();
						}
						_SelectionMode = SelectionMode.Pick;
						current.Use();
					}
					break;
				case EventType.MouseUp:
					if (GUIUtility.hotControl == controlId)
					{
						GUIUtility.hotControl = GUIUtility.keyboardControl = 0;
						_OldSelection.Clear();
						_SelectionMode = SelectionMode.None;
						current.Use();
					}
					break;
				case EventType.MouseDrag:
					if (GUIUtility.hotControl == controlId)
					{
						_SelectionMode = SelectionMode.Rect;
						SelectNodesInRect(EditorGUITools.FromToRect(_DragBeginPos, current.mousePosition));
						current.Use();
					}
					break;
				case EventType.KeyDown:
					if (_SelectionMode != SelectionMode.None && current.keyCode == KeyCode.Escape)
					{
						Undo.RecordObject(this, "Selection State");

						_Selection = _OldSelection;
						GUIUtility.hotControl = GUIUtility.keyboardControl = 0;

						Undo.CollapseUndoOperations(Undo.GetCurrentGroup());

						EditorUtility.SetDirty(this);

						current.Use();
					}
					break;
				case EventType.Repaint:
					if (_SelectionMode == SelectionMode.Rect)
					{
						Styles.selectionRect.Draw(EditorGUITools.FromToRect(_DragBeginPos, current.mousePosition), false, false, false, false);
					}
					break;
			}

			_IsDragSelection = GUIUtility.hotControl == controlId;
		}

		private RenameOverlay _RenameOverlay;
		private RenameOverlay GetRenameOverlay()
		{
			if (_RenameOverlay == null)
			{
				_RenameOverlay = new RenameOverlay();
			}
			return _RenameOverlay;
		}

		public void BeginRename(State state)
		{
			RenameOverlay renameOverlay = GetRenameOverlay();
			renameOverlay.BeginRename(state.name, state.nodeID, 0.0f);
		}

		public void BeginRename(GroupNode group)
		{
			RenameOverlay renameOverlay = GetRenameOverlay();
			renameOverlay.BeginRename(group.name, group.nodeID, 0.0f);
		}

		private void RenameEnded()
		{
			RenameOverlay renameOverlay = GetRenameOverlay();
			if (renameOverlay.userAcceptedRename)
			{
				string name = !string.IsNullOrEmpty(renameOverlay.name) ? renameOverlay.name : renameOverlay.originalName;
				int nodeID = renameOverlay.userData;
				Node node = _StateMachine.GetNodeFromID(nodeID);
				if (node != null)
				{
					State state = node as State;
					if (state != null && name != state.name)
					{
						ArborFSMInternal stateMachine = state.stateMachine;

						Undo.RecordObject(stateMachine, "Rename State");

						state.name = name;

						EditorUtility.SetDirty(stateMachine);
					}

					GroupNode group = node as GroupNode;
					if (group != null && name != group.name)
					{
						ArborFSMInternal stateMachine = group.stateMachine;

						Undo.RecordObject(stateMachine, "Rename Group");

						group.name = name;

						EditorUtility.SetDirty(stateMachine);
					}
				}
			}
			renameOverlay.Clear();
		}

		void HandleRenameOverlay()
		{
			RenameOverlay renameOverlay = GetRenameOverlay();
			if (renameOverlay.OnGUI((GUIStyle)"PR TextField"))
			{
				return;
			}
			RenameEnded();
			GUIUtility.ExitGUI();
		}

		public bool IsRenaming(int instanceID)
		{
			RenameOverlay renameOverlay = GetRenameOverlay();
			if (renameOverlay.IsRenaming() && renameOverlay.userData == instanceID)
				return !renameOverlay.isWaitingForDelay;
			return false;
		}

		void CreateState(Vector2 position, bool resident)
		{
			Undo.IncrementCurrentGroup();

			State state = _StateMachine.CreateState(resident);

			if (state != null)
			{
				Undo.RecordObject(_StateMachine, "Created State");

				state.position = new Rect(position.x, position.y, 300, 100);

				EditorUtility.SetDirty(_StateMachine);

				CreateNodeEditor(state);
				UpdateNodeCommentControl(state);

				Undo.RecordObject(this, "Created State");

				_Selection.Clear();
				_Selection.Add(state.nodeID);

				BeginRename(state);
			}

			Undo.CollapseUndoOperations(Undo.GetCurrentGroup());

			Repaint();
		}

		void CreateComment(Vector2 position)
		{
			Undo.IncrementCurrentGroup();

			CommentNode comment = _StateMachine.CreateComment();

			if (comment != null)
			{
				Undo.RecordObject(_StateMachine, "Created Comment");

				comment.position = new Rect(position.x, position.y, 300, 100);

				EditorUtility.SetDirty(_StateMachine);

				CreateNodeEditor(comment);

				Undo.RecordObject(this, "Created Comment");

				_Selection.Clear();
				_Selection.Add(comment.nodeID);
			}

			Undo.CollapseUndoOperations(Undo.GetCurrentGroup());

			Repaint();
		}

		public void CreateCalculator(Vector2 position, System.Type calculatorType)
		{
			Undo.IncrementCurrentGroup();

			CalculatorNode calculator = _StateMachine.CreateCalculator(calculatorType);

			if (calculator != null)
			{
				Undo.RecordObject(_StateMachine, "Created Calculator");

				calculator.position = new Rect(position.x, position.y, 300, 100);

				EditorUtility.SetDirty(_StateMachine);

				CreateNodeEditor(calculator);
				UpdateNodeCommentControl(calculator);

				Undo.RecordObject(this, "Created Calculator");

				_Selection.Clear();
				_Selection.Add(calculator.nodeID);
			}

			Undo.CollapseUndoOperations(Undo.GetCurrentGroup());

			Repaint();
		}

		public void CreateGroup(Vector2 position)
		{
			Undo.IncrementCurrentGroup();

			GroupNode group = _StateMachine.CreateGroup();

			if (group != null)
			{
				Undo.RecordObject(_StateMachine, "Created Group");

				group.position = new Rect(position.x, position.y, 300, 100);

				EditorUtility.SetDirty(_StateMachine);

				CreateGroupControl(group);
				UpdateNodeCommentControl(group);

				Undo.RecordObject(this, "Created Group");

				_Selection.Clear();
				_Selection.Add(group.nodeID);

				BeginRename(group);
			}

			Undo.CollapseUndoOperations(Undo.GetCurrentGroup());

			Repaint();
		}

		void CreateState(object obj)
		{
			Vector2 position = (Vector2)obj;

			CreateState(position, false);
		}

		void CreateResidentState(object obj)
		{
			Vector2 position = (Vector2)obj;

			CreateState(position, true);
		}

		void CreateComment(object obj)
		{
			Vector2 position = (Vector2)obj;

			CreateComment(position);
		}

		void CreateCalculator(object obj)
		{
			Vector2 position = (Vector2)obj;

			Rect buttonRect = EditorGUITools.GUIToScreenRect(this.position);
			Vector2 vector2 = GUIUtility.GUIToScreenPoint(new Vector2(position.x - _GraphGUI.viewArea.xMin, position.y - _GraphGUI.viewArea.yMin));
			buttonRect.x += vector2.x;
			buttonRect.y += vector2.y;
			buttonRect.width = 0;
			buttonRect.height = 0;

			CalculateMenuWindow.instance.Init(position, buttonRect);
		}

		void CreateGroup(object obj)
		{
			Vector2 position = (Vector2)obj;

			CreateGroup(position);
		}

		void CopyNodes()
		{
			Clipboard.CopyNodes(selection);
		}

		void CutNodes()
		{
			Clipboard.CopyNodes(selection);
			DeleteNodes();
		}

		void SelectNodes(Node[] nodes)
		{
			_Selection.Clear();

			foreach (Node node in nodes)
			{
				_Selection.Add(node.nodeID);

				if (node is State)
				{
					State state = node as State;

					int behaviourCount = state.behaviourCount;
					for (int behaviourIndex = 0; behaviourIndex < behaviourCount; behaviourIndex++)
					{
						StateBehaviour behaviour = state.GetBehaviourFromIndex(behaviourIndex);
						EditorUtility.SetDirty(behaviour);
					}
				}
				else if (node is CalculatorNode)
				{
					CalculatorNode calculatorNode = node as CalculatorNode;
					EditorUtility.SetDirty(calculatorNode.calculator);
				}
			}
		}

		public void DuplicateNodes(Vector2 position, Node[] sourceNodes)
		{
			Undo.IncrementCurrentGroup();

			Undo.RegisterCompleteObjectUndo(_StateMachine, "Duplicate Nodes");

			Node[] nodes = EditorGUITools.DuplicateNodes(_StateMachine, position, sourceNodes );

			if (nodes != null)
			{
				EditorUtility.SetDirty(_StateMachine);

				Undo.RecordObject(this, "Duplicate Nodes");

				SelectNodes(nodes);

				EditorUtility.SetDirty(this);
			}

			Undo.CollapseUndoOperations(Undo.GetCurrentGroup());

			Repaint();
		}

		void DuplicateNodes(object obj)
		{
			Vector2 position = (Vector2)obj;

			DuplicateNodes(position, selection);
		}

		void PasteNodes(object obj)
		{
			Vector2 position = (Vector2)obj;

			Undo.IncrementCurrentGroup();

			Undo.RegisterCompleteObjectUndo(_StateMachine, "Paste Nodes");

			Node[] nodes = Clipboard.PasteNodes(_StateMachine, position);

			if (nodes != null)
			{
				EditorUtility.SetDirty(_StateMachine);

				Undo.RecordObject(this, "Paste Nodes");

				SelectNodes(nodes);

				EditorUtility.SetDirty(this);
			}

			Undo.CollapseUndoOperations(Undo.GetCurrentGroup());

			Repaint();
		}

		string GetNodeName(Node node)
		{
			State state = node as State;
			if (state != null)
			{
				return state.name;
			}

			CommentNode commentNode = node as CommentNode;
			if (commentNode != null)
			{
				return Localization.GetWord("Comment");
			}

			CalculatorNode calculatorNode = node as CalculatorNode;
			if (calculatorNode != null)
			{
				Object calculatorObj = calculatorNode.GetObject();
				CalculatorEditor.CalculatorInfo calculatorInfo = CalculatorEditor.GetCalculatorInfo(calculatorObj);
				return calculatorInfo.titleContent.text;
			}

			GroupNode groupNode = node as GroupNode;
			if (groupNode != null)
			{
				return groupNode.name;
			}

			return "Node";
		}

		public void DeleteNodes(Node[] deleteNodes)
		{
			Undo.IncrementCurrentGroup();
			int undoGroup = Undo.GetCurrentGroup();

			List<int> deleteNodeIDs = new List<int>();

			foreach (Node deleteNode in deleteNodes)
			{
				if (_StateMachine.DeleteNode(deleteNode))
				{
					deleteNodeIDs.Add(deleteNode.nodeID);
					DeleteNodeCommentControl(deleteNode);
					DeleteNodeEditor(deleteNode);
				}
				else
				{
					string name = GetNodeName(deleteNode);
					Debug.LogErrorFormat(Localization.GetWord("DeleteError"), name);
				}
			}

			Undo.RecordObject(this, "Delete Nodes");

			foreach (int deleteNodeID in deleteNodeIDs)
			{
				_Selection.Remove(deleteNodeID);
			}

			Undo.CollapseUndoOperations(undoGroup);

			EditorUtility.SetDirty(this);

			Repaint();
		}

		void DeleteNodes()
		{
			DeleteNodes(selection);
		}

		public void DeleteBehaviour(State state, Object behaviourObj)
		{
			StateEditor stateEditor = GetNodeEditor(state) as StateEditor;
			stateEditor.RemoveBehaviourEditor(behaviourObj);

			Undo.IncrementCurrentGroup();
			int undoGruop = Undo.GetCurrentGroup();

			state.DestroyBehaviour(behaviourObj);
			behaviourObj = null;

			Undo.CollapseUndoOperations(undoGruop);
		}

		[System.Reflection.Obfuscation(Exclude = true)]
		private void OnSelectionChange()
		{
			if (_IsLocked)
			{
				return;
			}

			GameObject gameObject = Selection.activeGameObject;
			if (gameObject == null)
			{
				return;
			}

			ArborFSMInternal stateMachine = gameObject.GetComponent<ArborFSMInternal>();
			if (stateMachine != null)
			{
				Initialize(stateMachine);
			}
		}

		void HandleContextMenu()
		{
			Event current = Event.current;

			if (!(_DragStateBranchEnable || _DragCalculatorBranchEnable) && current.type == EventType.ContextClick)
			{
				GenericMenu menu = new GenericMenu();

				menu.AddItem(Localization.GetTextContent("Create State"), false, CreateState, current.mousePosition);
				menu.AddItem(Localization.GetTextContent("Create Resident State"), false, CreateResidentState, current.mousePosition);

				menu.AddSeparator("");

				menu.AddItem(Localization.GetTextContent("Create Calculator"), false, CreateCalculator, current.mousePosition);

				menu.AddSeparator("");

				menu.AddItem(Localization.GetTextContent("Create Group"), false, CreateGroup, current.mousePosition);
				menu.AddItem(Localization.GetTextContent("Create Comment"), false, CreateComment, current.mousePosition);

				menu.AddSeparator("");

				if (_Selection.Count > 0)
				{
					menu.AddItem(Localization.GetTextContent("Cut"), false, CutNodes);
					menu.AddItem(Localization.GetTextContent("Copy"), false, CopyNodes);
				}
				else
				{
					menu.AddDisabledItem(Localization.GetTextContent("Cut"));
					menu.AddDisabledItem(Localization.GetTextContent("Copy"));
				}

				if (Clipboard.hasCopyedNodes)
				{
					menu.AddItem(Localization.GetTextContent("Paste"), false, PasteNodes, current.mousePosition);
				}
				else
				{
					menu.AddDisabledItem(Localization.GetTextContent("Paste"));
				}

				menu.AddSeparator("");

				if (_Selection.Count > 0)
				{
					menu.AddItem(Localization.GetTextContent("Duplicate"), false, DuplicateNodes, current.mousePosition);
					menu.AddItem(Localization.GetTextContent("Delete"), false, DeleteNodes);
				}
				else
				{
					menu.AddDisabledItem(Localization.GetTextContent("Duplicate"));
					menu.AddDisabledItem(Localization.GetTextContent("Delete"));
				}

				menu.ShowAsContext();

				current.Use();
			}
		}

		void FrameSelected()
		{
			Vector2 frameSelectTarget = Vector2.zero;
			foreach (Node node in selection)
			{
				frameSelectTarget += node.position.center;
			}
			frameSelectTarget /= (float)selection.Length;

			_GraphGUI.FrameSelected(frameSelectTarget);
		}

		Vector2 _MousePosition;

		void HandleCommand()
		{
			Event current = Event.current;

			EventType eventType = current.type;

			switch (eventType)
			{
				case EventType.MouseMove:
					_MousePosition = current.mousePosition;
					break;
				case EventType.ValidateCommand:
					switch (current.commandName)
					{
						case "Copy":
						case "Cut":
						case "Duplicate":
						case "Delete":
						case "SoftDelete":
						case "FrameSelected":
							if (_Selection.Count > 0)
							{
								current.Use();
							}
							break;
						case "Paste":
							if (Clipboard.hasCopyedNodes)
							{
								current.Use();
							}
							break;
						case "SelectAll":
							if (_StateMachine.nodeCount > 0)
							{
								current.Use();
							}
							break;
					}
					break;
				case EventType.ExecuteCommand:
					switch (current.commandName)
					{
						case "Copy":
							CopyNodes();
							break;
						case "Cut":
							CutNodes();
							break;
						case "Paste":
							PasteNodes(_MousePosition);
							break;
						case "Duplicate":
							DuplicateNodes(_MousePosition);
							break;
						case "FrameSelected":
							FrameSelected();
							break;
						case "Delete":
						case "SoftDelete":
							DeleteNodes();
							break;
						case "SelectAll":
							SelectAll();
							break;
					}
					break;
			}
		}

		void CalculateRect()
		{
			_ToolBarRect = new Rect(0.0f, 0.0f, this.position.width, EditorStyles.toolbar.fixedHeight);

			if (ArborSettings.openStateList)
			{
				_StateListRect = new Rect(0.0f, _ToolBarRect.yMax, ArborSettings.stateListWidth, this.position.height - _ToolBarRect.height);

				float graphWidth = this.position.width - _StateListRect.width;
				if (graphWidth < k_MinRightSideWidth)
				{
					graphWidth = k_MinRightSideWidth;
				}

				float graphHeight = this.position.height - _ToolBarRect.height;
				_GraphGUI.position = new Rect(_StateListRect.xMax, _ToolBarRect.yMax, graphWidth, graphHeight);
			}
			else
			{
				float graphWidth = this.position.width;
				float graphHeight = this.position.height - _ToolBarRect.height;
				_GraphGUI.position = new Rect(0.0f, _ToolBarRect.yMax, graphWidth, graphHeight);
			}
		}

		static int s_MouseDeltaReaderHash = "s_MouseDeltaReaderHash".GetHashCode();
		static Vector2 s_MouseDeltaReaderLastPos;

		static Vector2 MouseDeltaReader(Rect position, bool activated)
		{
			int controlId = GUIUtility.GetControlID(s_MouseDeltaReaderHash, FocusType.Passive, position);
			Event current = Event.current;
			switch (current.GetTypeForControl(controlId))
			{
				case EventType.MouseDown:
					if (activated && GUIUtility.hotControl == 0 && (position.Contains(current.mousePosition) && current.button == 0))
					{
						GUIUtility.hotControl = controlId;
						GUIUtility.keyboardControl = 0;

						s_MouseDeltaReaderLastPos = GUIUtility.GUIToScreenPoint(current.mousePosition);

						current.Use();
						break;
					}
					break;
				case EventType.MouseUp:
					if (GUIUtility.hotControl == controlId && current.button == 0)
					{
						GUIUtility.hotControl = 0;
						current.Use();
						break;
					}
					break;
				case EventType.MouseDrag:
					if (GUIUtility.hotControl == controlId)
					{
						Vector2 vector2_1 = GUIUtility.GUIToScreenPoint(current.mousePosition);
						Vector2 vector2_2 = vector2_1 - s_MouseDeltaReaderLastPos;
						s_MouseDeltaReaderLastPos = vector2_1;
						current.Use();
						return vector2_2;
					}
					break;
			}
			return Vector2.zero;
		}

		static readonly float k_MinLeftSideWidth = 150f;
		static readonly float k_MinRightSideWidth = 110f;

		void ResizeHandling(float width, float height)
		{
			if (!ArborSettings.openStateList)
			{
				return;
			}

			Rect dragRect = new Rect(ArborSettings.stateListWidth, _ToolBarRect.height, 5.0f, height);
			float minLeftSide = k_MinLeftSideWidth;
			float minRightSide = k_MinRightSideWidth;

			if (Event.current.type == EventType.Repaint)
			{
				EditorGUIUtility.AddCursorRect(dragRect, MouseCursor.SplitResizeLeftRight);
			}
			float num = 0.0f;
			float x = MouseDeltaReader(dragRect, true).x;
			if (x != 0.0f)
			{
				dragRect.x += x;
			}

			if (dragRect.x < minLeftSide || minLeftSide > width - minRightSide)
			{
				num = minLeftSide;
			}
			else if (dragRect.x > width - minRightSide)
			{
				num = width - minRightSide;
			}
			else
			{
				num = dragRect.x;
			}

			if (num > 0.0)
			{
				ArborSettings.stateListWidth = num;
			}
		}

		void ClearCount()
		{
			for (int stateIndex = 0, stateCount = _StateMachine.stateCount; stateIndex < stateCount; stateIndex++)
			{
				State state = _StateMachine.GetStateFromIndex(stateIndex);
				StateEditor stateEditor = GetNodeEditor(state) as StateEditor;

				state.transitionCount = 0;

				for (int behaviourIndex = 0, behaviourCount = state.behaviourCount; behaviourIndex < behaviourCount; behaviourIndex++)
				{
					StateBehaviour behaviour = state.GetBehaviourFromIndex(behaviourIndex);

					StateEditor.BehaviourEditor behaviourEditor = stateEditor.GetBehaviourEditor(behaviour);

					foreach (StateEditor.StateLinkProperty stateLinkProperty in behaviourEditor.stateLinkProperties)
					{
						stateLinkProperty.stateLink.transitionCount = 0;
					}
				}
			}
		}

		void SetBreakPoints()
		{
			Undo.RecordObject(_StateMachine, "BreakPoint On");

			foreach (Node node in selection)
			{
				State state = node as State;
				if (state != null)
				{
					state.breakPoint = true;
				}
			}

			EditorUtility.SetDirty(_StateMachine);
		}

		void ReleaseBreakPoints()
		{
			Undo.RecordObject(_StateMachine, "BreakPoint Off");

			foreach (Node node in selection)
			{
				State state = node as State;
				if (state != null)
				{
					state.breakPoint = false;
				}
			}

			EditorUtility.SetDirty(_StateMachine);
		}

		void ReleaseAllBreakPoints()
		{
			Undo.RecordObject(_StateMachine, "Delete All BreakPoint");

			for (int stateIndex = 0, stateCount = _StateMachine.stateCount; stateIndex < stateCount; stateIndex++)
			{
				State state = _StateMachine.GetStateFromIndex(stateIndex);

				state.breakPoint = false;
			}

			EditorUtility.SetDirty(_StateMachine);
		}

		void DrawToolbar()
		{
			using (new ProfilerScope("DrawToolbar"))
			{
				GUILayout.BeginArea(_ToolBarRect);

				EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);

				if (_StateMachine == null && _StateMachineInstanceID != 0)
				{
					_StateMachine = EditorUtility.InstanceIDToObject(_StateMachineInstanceID) as ArborFSMInternal;
					_Selection.Clear();
				}

				using (new ProfilerScope("StateMachine Field"))
				{
					EditorGUI.BeginChangeCheck();

					ArborFSMInternal stateMachine = EditorGUILayout.ObjectField(_StateMachine, typeof(ArborFSMInternal), true, GUILayout.Width(200)) as ArborFSMInternal;

					if (EditorGUI.EndChangeCheck())
					{
						Initialize(stateMachine);
					}
				}

				using (new ProfilerScope("StateList Toggle"))
				{
					Rect stateListButtonPosition = GUILayoutUtility.GetRect(100.0f, 20.0f);

					ArborSettings.openStateList = GUI.Toggle(stateListButtonPosition, ArborSettings.openStateList, Localization.GetWord("State List"), EditorStyles.toolbarButton);
				}

				GUILayout.FlexibleSpace();

				using (new ProfilerScope("Debug Button"))
				{
					Rect buttonPosition = GUILayoutUtility.GetRect(80.0f, 20.0f);
					if (GUI.Button(buttonPosition, Localization.GetTextContent("Debug"), EditorStyles.toolbarDropDown))
					{
						GenericMenu menu = new GenericMenu();

						bool isSelectionState = false;
						foreach (Node node in selection)
						{
							if (node is State)
							{
								isSelectionState = true;
								break;
							}
						}

						if (isSelectionState)
						{
							menu.AddItem(Localization.GetTextContent("Set BreakPoints"), false, SetBreakPoints);
							menu.AddItem(Localization.GetTextContent("Release BreakPoints"), false, ReleaseBreakPoints);
						}
						else
						{
							menu.AddDisabledItem(Localization.GetTextContent("Set BreakPoints"));
							menu.AddDisabledItem(Localization.GetTextContent("Release BreakPoints"));
						}

						menu.AddItem(Localization.GetTextContent("Release all BreakPoints"), false, ReleaseAllBreakPoints);

						if (Application.isPlaying)
						{
							menu.AddItem(Localization.GetTextContent("Clear Count"), false, ClearCount);
						}
						else
						{
							menu.AddDisabledItem(Localization.GetTextContent("Clear Count"));
						}

						menu.DropDown(buttonPosition);
					}
				}

				Localization.LanguagePopup();

				using (new ProfilerScope("Grid Button"))
				{
					Rect gridButtonPosition = GUILayoutUtility.GetRect(80.0f, 20.0f);
					if (GUI.Button(gridButtonPosition, Localization.GetTextContent("Grid"), EditorStyles.toolbarDropDown))
					{
						GridSettingsWindow.instance.Init(gridButtonPosition);
					}
				}

				using (new ProfilerScope("Help Button"))
				{
					Rect helpButtonPosition = GUILayoutUtility.GetRect(22.0f, 20.0f);
					helpButtonPosition.x += 5.0f;
					helpButtonPosition.width -= 10.0f;

					string siteURL = Localization.GetWord("SiteURL");
					EditorGUITools.HelpButton(helpButtonPosition, siteURL, "Open Reference");
				}

				EditorGUILayout.EndHorizontal();

				GUILayout.EndArea();
			}
		}

		private static int s_DrawBranchHash = "s_DrawBranchHash".GetHashCode();

		const float kBezierArrowWidth = 16.0f;

		void DrawBranchStateLink(StateBehaviour behaviour, StateLink stateLink)
		{
			using (new ProfilerScope("DrawBranchStateLink"))
			{
				if (stateLink.lineEnable && stateLink.stateID != 0)
				{
					Color lineColor = Color.white;
					if (stateLink.lineColorChanged)
					{
						lineColor = stateLink.lineColor;
					}

					Color shadowColor = new Color(0, 0, 0, 1.0f);
					float width = 5;
					if (Application.isPlaying)
					{
						int index = _StateMachine.IndexOfStateLinkHistory(stateLink);
						if (index != -1)
						{
							float t = (float)index / 4.0f;

							shadowColor = Color.Lerp(new Color(0.0f, 0.5f, 0.5f, 1.0f), Color.black, t);
							lineColor *= Color.Lerp(Color.white, Color.gray, t);
							width = Mathf.Lerp(15, 5, t);
						}
						else
						{
							lineColor *= Color.gray;
						}
					}

					Vector2 shadowPos = Vector2.one * 3;

					Bezier2D bezier = new Bezier2D(
						stateLink.lineStart,
						stateLink.lineStartTangent,
						stateLink.lineEnd,
						stateLink.lineEndTangent);

					using (new ProfilerScope("DrawBezierArrow"))
					{
						EditorGUITools.BezierArrow(bezier, shadowPos, Styles.connectionTexture, shadowColor, width, kBezierArrowWidth);
						EditorGUITools.BezierArrow(bezier, Vector2.zero, Styles.connectionTexture, lineColor, width, kBezierArrowWidth);
					}

					int controlID = EditorGUIUtility.GetControlID(s_DrawBranchHash, FocusType.Passive);

					Event currentEvent = Event.current;

					EventType eventType = currentEvent.GetTypeForControl(controlID);

					float distance = HandleUtility.DistancePointBezier(currentEvent.mousePosition, bezier.startPosition, bezier.endPosition, bezier.startControl, bezier.endControl);

					switch (eventType)
					{
						case EventType.MouseDown:
							if (distance <= 15.0f)
							{
								if (currentEvent.button == 1 || Application.platform == RuntimePlatform.OSXEditor && currentEvent.control)
								{
									State prevState = behaviour.state;
									State nextState = behaviour.stateMachine.GetStateFromID(stateLink.stateID);

									GenericMenu menu = new GenericMenu();
									menu.AddItem(EditorGUITools.GetTextContent(Localization.GetWord("Go to Previous State") + " : " + prevState.name), false, () =>
									{
										_Selection.Clear();
										_Selection.Add(prevState.nodeID);
										_FrameSelected = true;
									});
									menu.AddItem(EditorGUITools.GetTextContent(Localization.GetWord("Go to Next State") + " : " + nextState.name), false, () =>
									{
										_Selection.Clear();
										_Selection.Add(nextState.nodeID);
										_FrameSelected = true;
									});

									menu.ShowAsContext();
									currentEvent.Use();
								}
							}
							break;
					}
				}
			}
		}

		public void DrawBehaviourBranches(StateEditor stateEditor)
		{
			using (new ProfilerScope("DrawBehaviourBranches"))
			{
				State state = stateEditor.state;
				int behaviourCount = state.behaviourCount;

				for (int behaviourIndex = 0; behaviourIndex < behaviourCount; behaviourIndex++)
				{
					StateBehaviour behaviour = state.GetBehaviourFromIndex(behaviourIndex);
					if (behaviour != null)
					{
						StateEditor.BehaviourEditor behaviourEditor = stateEditor.GetBehaviourEditor(behaviour);

						foreach (StateEditor.StateLinkProperty stateLinkProperty in behaviourEditor.stateLinkProperties)
						{
							DrawBranchStateLink(behaviour, stateLinkProperty.stateLink);
						}
					}
				}
			}
		}

		void DrawStateLinkTransitionCount(StateBehaviour behaviour, StateLink stateLink)
		{
			if (stateLink.lineEnable && stateLink.stateID != 0)
			{
				Vector2 startPosition = stateLink.lineStart;
				Vector2 startControl = stateLink.lineStartTangent;
				Vector2 endPosition = stateLink.lineEnd;
				Vector2 endControl = stateLink.lineEndTangent;

				Vector2 v = (endPosition - endControl).normalized * kBezierArrowWidth;

				Vector2 pos = Bezier2D.GetPoint(startPosition, startControl, endPosition - v, endControl - v, 0.5f);

				GUIStyle style = Styles.countBadge;
				GUIContent content = new GUIContent(stateLink.transitionCount.ToString());
				Vector2 contentSize = style.CalcSize(content);

				Rect rect = new Rect(pos.x - contentSize.x / 2.0f, pos.y - contentSize.y / 2.0f, contentSize.x, contentSize.y);

				Color lineColor = Color.white;

				int index = _StateMachine.IndexOfStateLinkHistory(stateLink);
				if (index != -1)
				{
					float t = (float)index / 4.0f;

					lineColor *= Color.Lerp(Color.white, Color.gray, t);
				}
				else
				{
					lineColor *= Color.gray;
				}

				Color savedColor = GUI.color;
				GUI.color = lineColor;

				EditorGUI.LabelField(rect, content, style);

				GUI.color = savedColor;
			}
		}

		public void DrawBehaviourBranchesTransitionCount(StateEditor stateEditor)
		{
			using (new ProfilerScope("DrawBehaviourBranches"))
			{
				State state = stateEditor.state;
				int behaviourCount = state.behaviourCount;

				for (int behaviourIndex = 0; behaviourIndex < behaviourCount; behaviourIndex++)
				{
					StateBehaviour behaviour = state.GetBehaviourFromIndex(behaviourIndex);
					if (behaviour != null)
					{
						StateEditor.BehaviourEditor behaviourEditor = stateEditor.GetBehaviourEditor(behaviour);

						foreach (StateEditor.StateLinkProperty stateLinkProperty in behaviourEditor.stateLinkProperties)
						{
							DrawStateLinkTransitionCount(behaviour, stateLinkProperty.stateLink);
						}
					}
				}
			}
		}

		void DrawStateList()
		{
			using (new ProfilerScope("DrawStateList"))
			{
				GUILayout.BeginArea(_StateListRect);

				EditorGUILayout.BeginVertical(Styles.background);

				int stateCount = 0;

				if (_StateMachine != null)
				{
					stateCount = _StateMachine.stateCount;
				}
				else
				{
					EditorGUI.BeginDisabledGroup(true);
				}

				Rect searchRect = GUILayoutUtility.GetRect(0.0f, 20.0f);
				searchRect.y += 4f;
				searchRect.x += 8f;
				searchRect.width -= 16f;

				string[] names = System.Enum.GetNames(typeof(SearchMode));
				int searchMode = (int)_SearchMode;
				_SearchText = EditorGUITools.ToolbarSearchField(searchRect, names, ref searchMode, _SearchText);
				_SearchMode = (SearchMode)searchMode;

				if (stateCount > 0)
				{
					_StateListScrollPos = EditorGUILayout.BeginScrollView(_StateListScrollPos);

					GUILayout.Space(3.0f);

					List<State> viewStates = new List<State>();
					if (!string.IsNullOrEmpty(_SearchText))
					{
						for (int i = 0; i < stateCount; i++)
						{
							State state = _StateMachine.GetStateFromIndex(i);
							switch (_SearchMode)
							{
								case SearchMode.All:
									if (state.name.IndexOf(_SearchText, System.StringComparison.OrdinalIgnoreCase) >= 0)
									{
										viewStates.Add(state);
									}
									else
									{
										int behaviourCount = state.behaviourCount;
										for (int behaviourIndex = 0; behaviourIndex < behaviourCount; behaviourIndex++)
										{
											StateBehaviour behaviour = state.GetBehaviourFromIndex(behaviourIndex);

											if (behaviour.GetType().Name.Equals(_SearchText, System.StringComparison.OrdinalIgnoreCase))
											{
												viewStates.Add(state);
												break;
											}
										}
									}
									break;
								case SearchMode.Name:
									if (state.name.IndexOf(_SearchText, System.StringComparison.OrdinalIgnoreCase) >= 0)
									{
										viewStates.Add(state);
									}
									break;
								case SearchMode.Type:
									{
										int behaviourCount = state.behaviourCount;
										for (int behaviourIndex = 0; behaviourIndex < behaviourCount; behaviourIndex++)
										{
											StateBehaviour behaviour = state.GetBehaviourFromIndex(behaviourIndex);

											if (behaviour.GetType().Name.Equals(_SearchText, System.StringComparison.OrdinalIgnoreCase))
											{
												viewStates.Add(state);
												break;
											}
										}
									}
									break;
							}
						}
					}
					else
					{
						for (int i = 0; i < stateCount; i++)
						{
							State state = _StateMachine.GetStateFromIndex(i);
							viewStates.Add(state);
						}
					}

					viewStates.Sort((a, b) =>
					{
						if (_StateMachine.startStateID == a.nodeID)
						{
							return -1;
						}
						else if (_StateMachine.startStateID == b.nodeID)
						{
							return 1;
						}
						if (!a.resident && b.resident)
						{
							return -1;
						}
						else if (a.resident && !b.resident)
						{
							return 1;
						}
						return a.name.CompareTo(b.name);
					});

					EditorGUIUtility.SetIconSize(new Vector2(16f, 16f));

					int viewStateCount = viewStates.Count;
					for (int stateIndex = 0; stateIndex < viewStateCount; stateIndex++)
					{
						State state = viewStates[stateIndex];

						bool on = _Selection.Contains(state.nodeID);

						string key = string.Format("node element{0}", (!on) ? string.Empty : " on");
						GUIStyle nodeStyle = Styles.GetStyle(key);

						Color color = StateColor.GetListColor(state);

						Color tempBackgroundColor = GUI.backgroundColor;
						GUI.backgroundColor = color;

						Rect rect = GUILayoutUtility.GetRect(0.0f, 25.0f);

						Texture2D icon = Icons.GetStateIcon(state);
						GUIContent stateContent = new GUIContent(state.name, icon);

						if (EditorGUITools.ButtonMouseDown(rect, stateContent, FocusType.Passive, nodeStyle))
						{
							_Selection.Clear();
							_Selection.Add(state.nodeID);
							_FrameSelected = true;
						}

						GUI.backgroundColor = tempBackgroundColor;

						if (state.breakPoint)
						{
							GUIStyle style = (Application.isPlaying && EditorApplication.isPaused && state.stateMachine.currentState == state) ? Styles.breakpointOn : Styles.breakpoint;
							GUIContent content = new GUIContent(string.Empty, Localization.GetWord("BreakPoint"));
							Vector2 size = style.CalcSize(content);
							Rect breakRect = new Rect(rect.x, rect.center.y - size.y * 0.5f, size.x, size.y);
							EditorGUI.LabelField(breakRect, content, style);
						}

						if (Application.isPlaying)
						{
							GUIStyle style = Styles.countBadge;
							GUIContent content = new GUIContent(state.transitionCount.ToString());
							Vector2 size = style.CalcSize(content);
							Rect countRect = new Rect(rect.x + rect.width - size.x - 2, rect.center.y - size.y * 0.5f, size.x, size.y);

							EditorGUI.LabelField(countRect, content, style);
						}

						GUILayout.Space(1);
					}

					EditorGUIUtility.SetIconSize(Vector2.zero);

					EditorGUILayout.EndScrollView();
				}

				if (_StateMachine == null)
				{
					EditorGUI.EndDisabledGroup();
				}

				EditorGUILayout.EndVertical();

				GUILayout.EndArea();
			}
		}

		void UpdateGraphExtents()
		{
			if (Event.current.type != EventType.Repaint)
			{
				return;
			}

			Rect oldExtents = _GraphGUI.extents;

			if (_StateMachine.nodeCount > 0)
			{
				Vector2 min = new Vector2(float.MaxValue, float.MaxValue);
				Vector2 max = new Vector2(float.MinValue, float.MinValue);

				for (int i = 0, count = _StateMachine.nodeCount; i < count; i++)
				{
					Node node = _StateMachine.GetNodeFromIndex(i);
					min = Vector2.Min(min, node.position.min);
					max = Vector2.Max(max, node.position.max);
				}

				_GraphGUI.extents = Rect.MinMaxRect(min.x, min.y, max.x, max.y);
			}
			else
			{
				_GraphGUI.extents = new Rect();
			}

			_GraphGUI.extents.xMin -= _GraphGUI.position.width * 0.6f;
			_GraphGUI.extents.xMax += _GraphGUI.position.width * 0.6f;
			_GraphGUI.extents.yMin -= _GraphGUI.position.height * 0.6f;
			_GraphGUI.extents.yMax += _GraphGUI.position.height * 0.6f;

			if (_GraphGUI.extents != oldExtents)
			{
				Repaint();
			}
		}

		public bool IsDragBranchHover(State state)
		{
			return _DragStateBranchEnable && _DragStateBranchHoverStateID == state.nodeID;
		}

		GUIStyle GetNodeStyle(Node node)
		{
			if (node is GroupNode)
			{
				GroupControl groupControl = GetGroupControl(node as GroupNode);
				return groupControl.GetStyle();
			}
			else
			{
				NodeEditor nodeEditor = GetNodeEditor(node);
				if (nodeEditor != null)
				{
					return nodeEditor.GetStyle();
				}
			}

			return null;
		}

		GUIContent GetNodeContent(Node node)
		{
			if (node is GroupNode)
			{
				GroupControl groupControl = GetGroupControl(node as GroupNode);
				return groupControl.GetContent();
			}
			else
			{
				NodeEditor nodeEditor = GetNodeEditor(node);
				if (nodeEditor != null)
				{
					return nodeEditor.GetTitleContent();
				}
			}

			return null;
		}

		static Dictionary<Object, Rect> _BehaviourPosition = new Dictionary<Object, Rect>();
		private Dictionary<int, Mesh> _CalculatorBranchMeshes = new Dictionary<int, Mesh>();

		public void SetBehaviourPosition(Object obj, Rect position)
		{
			_BehaviourPosition[obj] = position;
		}

		void DrawCalculatorBranchies()
		{
			using (new ProfilerScope("DrawCalculatorBranchies"))
			{
				for (int i = 0, count = _StateMachine.calculatorBranchCount; i < count; i++)
				{
					CalculatorBranch branch = _StateMachine.GetCalculatorBranchFromIndex(i);
					using (new ProfilerScope("DrawCalculatorBranch"))
					{
						if (Event.current.type == EventType.Repaint)
						{
							InputSlot inputSlot = branch.inputSlot;
							OutputSlot outputSlot = branch.outputSlot;

							if (inputSlot != null && outputSlot != null)
							{
								Color color = Color.white;

								float alpha = 1.0f;
								if (!branch.enabled)
								{
									alpha = 0.1f;
								}

								bool changed = false;

								if (!CalculatorSlotPropertyDrawer.IsVisible(branch.inBehaviour, inputSlot))
								{
									Rect inRect = new Rect();
									if (_BehaviourPosition.TryGetValue(branch.inBehaviour, out inRect))
									{
										Vector2 endPosition = new Vector2(inRect.x - 8.0f, inRect.y + 8.0f);
										branch.lineBezier.endPosition = endPosition;
										branch.lineBezier.endControl = endPosition - EditorGUITools.kBezierTangentOffset;
									}
								}

								color = EditorGUITools.GetTypeColor(outputSlot.dataType);

								if (!CalculatorSlotPropertyDrawer.IsVisible(branch.outBehaviour, outputSlot))
								{
									Rect outRect = new Rect();
									if (_BehaviourPosition.TryGetValue(branch.outBehaviour, out outRect))
									{
										Vector2 startPosition = new Vector2(outRect.xMax - 8.0f, outRect.y + 8.0f);
										branch.lineBezier.startPosition = startPosition;
										branch.lineBezier.startControl = startPosition + EditorGUITools.kBezierTangentOffset;
									}
								}

								color.a = alpha;

								Mesh mesh = null;
								if (!_CalculatorBranchMeshes.TryGetValue(branch.branchID, out mesh))
								{
									mesh = new Mesh();
									mesh.name = "CalculatorBranch";
									mesh.hideFlags |= HideFlags.HideAndDontSave;
									mesh.MarkDynamic();

									_CalculatorBranchMeshes.Add(branch.branchID, mesh);

									changed = true;
								}

								if (changed || branch.lineBezier.isChanged)
								{
									Vector2 shadowPos = Vector2.one * 3;
									EditorGUITools.GenerateBezierDottedMesh(branch.lineBezier, color, 2.0f, 10.0f, shadowPos, new Color(0, 0, 0, alpha), mesh);
								}

								EditorGUITools.DrawMesh(mesh);
							}
						}

						if (Application.isPlaying && branch.isUsed)
						{
							Bezier2D bezier = branch.lineBezier;
							Vector2 pos = bezier.GetPoint(0.5f);

							object value = branch.currentValue;
							string valueString = value != null ? value.ToString() : "null";

							GUIStyle style = Styles.countBadge;
							GUIContent content = new GUIContent(valueString);
							Vector2 size = style.CalcSize(content);

							Rect rect = new Rect(pos.x - size.x / 2, pos.y - size.y / 2, size.x, size.y);

							if (EditorGUITools.ButtonMouseDown(rect, content, FocusType.Passive, style) && value is Object)
							{
								EditorGUIUtility.PingObject(value as Object);
							}
						}
					}
				}
			}
		}

		private HashSet<int> _InVisibleNodes = new HashSet<int>();

		void InitializeVisibleNodes()
		{
			if (_StateMachine == null)
			{
				return;
			}

			_InVisibleNodes.Clear();
		}

		bool IsDraggingTargetState(State state)
		{
			StateEditor stateEditor = GetNodeEditor(state) as StateEditor;

			int behaviourCount = state.behaviourCount;

			for (int behaviourIndex = 0; behaviourIndex < behaviourCount; behaviourIndex++)
			{
				StateBehaviour behaviour = state.GetBehaviourFromIndex(behaviourIndex);
				if (behaviour != null)
				{
					StateEditor.BehaviourEditor behaviourEditor = stateEditor.GetBehaviourEditor(behaviour);

					foreach (StateEditor.StateLinkProperty stateLinkProperty in behaviourEditor.stateLinkProperties)
					{
						State targetState = _StateMachine.GetStateFromID(stateLinkProperty.stateLink.stateID);
						if (targetState != null && _DragNodePositions.ContainsKey(targetState))
						{
							return true;
						}
					}
				}
			}

			return false;
		}

		bool IsDraggingBranch(Node node)
		{
			return _DragStateBranchEnable && _DragStateBranchNodeID == node.nodeID ||
				_DragCalculatorBranchEnable && _DragCalculatorBranchNodeID == node.nodeID;
		}

		void UpdateVisibleNodes()
		{
			if (Event.current.type != EventType.Repaint)
			{
				return;
			}

			HashSet<int> oldInVisibleNodes = new HashSet<int>(_InVisibleNodes);

			_InVisibleNodes.Clear();

			int stateCount = _StateMachine.stateCount;
			for (int i = 0; i < stateCount; i++)
			{
				State state = _StateMachine.GetStateFromIndex(i);

				if (!_DragNodePositions.ContainsKey(state) && !IsDraggingTargetState(state) && !_GraphGUI.viewArea.Overlaps(state.position) && !IsDraggingBranch(state) )
				{
					_InVisibleNodes.Add(state.nodeID);
				}
			}

			int commentCount = _StateMachine.commentCount;
			for (int i = 0; i < commentCount; i++)
			{
				CommentNode comment = _StateMachine.GetCommentFromIndex(i);
				if (!_DragNodePositions.ContainsKey(comment) && !_GraphGUI.viewArea.Overlaps(comment.position))
				{
					_InVisibleNodes.Add(comment.nodeID);
				}
			}

			int calculatorCount = _StateMachine.calculatorCount;
			for (int i = 0; i < calculatorCount; i++)
			{
				CalculatorNode calculatorNode = _StateMachine.GetCalculatorFromIndex(i);
				if (!_DragNodePositions.ContainsKey(calculatorNode) && !_GraphGUI.viewArea.Overlaps(calculatorNode.position) && !IsDraggingBranch(calculatorNode) )
				{
					_InVisibleNodes.Add(calculatorNode.nodeID);
				}
			}

			if (!oldInVisibleNodes.SetEquals(_InVisibleNodes))
			{
				Repaint();
			}
		}

		bool IsVisibleNode(int nodeID)
		{
			return !_InVisibleNodes.Contains(nodeID);
		}

		void ClearVisibleCalculatorSlot()
		{
			int stateCount = _StateMachine.stateCount;
			for (int i = 0; i < stateCount; i++)
			{
				State state = _StateMachine.GetStateFromIndex(i);

				if (!IsVisibleNode(state.nodeID))
				{
					continue;
				}

				for (int behaviourCount = state.behaviourCount, behaviourIndex = 0; behaviourIndex < behaviourCount; behaviourIndex++)
				{
					Object behaviourObj = state.GetBehaviourObjectFromIndex(behaviourIndex);
					CalculatorSlotPropertyDrawer.ClearVisible(behaviourObj);
				}
			}

			int calculatorCount = _StateMachine.calculatorCount;
			for (int i = 0; i < calculatorCount; i++)
			{
				CalculatorNode calculatorNode = _StateMachine.GetCalculatorFromIndex(i);

				if (!IsVisibleNode(calculatorNode.nodeID))
				{
					continue;
				}

				CalculatorSlotPropertyDrawer.ClearVisible(calculatorNode.GetObject());
			}
		}

		void OnGraphGUI()
		{
			using (new ProfilerScope("OnGraphGUI"))
			{
				if ((_DragStateBranchEnable || _DragCalculatorBranchEnable) && Event.current.type == EventType.MouseDown && (Event.current.button == 1 || Application.platform == RuntimePlatform.OSXEditor && Event.current.control))
				{
					Event.current.Use();
				}

				_GroupLayer.Update();
				_NodeCommentLayer.Update();

				_GroupLayer.BeginLayer();
				_NodeCommentLayer.BeginLayer();

				CalculatorSlotPropertyDrawer.ClearSlotType();
				ClearVisibleCalculatorSlot();

				using (new ProfilerScope("Windows"))
				{
					using (new ProfilerScope("BeginWindows"))
					{
						EditorGUIUtility.SetIconSize(new Vector2(16f,16f));
						BeginWindows();
						EditorGUIUtility.SetIconSize(Vector2.zero);
					}

					DrawCalculatorBranchies();

					using (new ProfilerScope("States"))
					{
						int stateCount = _StateMachine.stateCount;
						for (int i = 0; i < stateCount; i++)
						{
							State state = _StateMachine.GetStateFromIndex(i);
							StateEditor stateEditor = GetNodeEditor(state) as StateEditor;

							if (stateEditor != null)
							{
								stateEditor.UpdateBehaviour();

								DrawBehaviourBranches(stateEditor);
							}
						}

						if (Application.isPlaying)
						{
							for (int i = 0; i < stateCount; i++)
							{
								State state = _StateMachine.GetStateFromIndex(i);
								StateEditor stateEditor = GetNodeEditor(state) as StateEditor;

								if (stateEditor != null)
								{
									DrawBehaviourBranchesTransitionCount(stateEditor);
								}
							}
						}
					}

					using (new ProfilerScope("Nodes"))
					{
						for (int i = 0, count = _NodeEditors.Count; i < count; i++)
						{
							NodeEditor nodeEditor = _NodeEditors[i];
							if (nodeEditor != null && IsVisibleNode(nodeEditor.node.nodeID))
							{
								nodeEditor.DoWindow();
							}
						}
					}

					using (new ProfilerScope("EndWindows"))
					{
						EditorGUIUtility.SetIconSize(new Vector2(16f, 16f));
						EndWindows();
						EditorGUIUtility.SetIconSize(Vector2.zero);
					}
				}

				_NodeCommentLayer.EndLayer();

				_GroupLayer.EndLayer();

				if (_DragStateBranchEnable)
				{
					Vector2 shadowPos = Vector2.one * 3;

					EditorGUITools.BezierArrow(_DragStateBranchBezier, shadowPos, Styles.connectionTexture, new Color(0, 0, 0, 1.0f), 5.0f, kBezierArrowWidth);
					EditorGUITools.BezierArrow(_DragStateBranchBezier, Vector2.zero, Styles.connectionTexture, new Color(1.0f, 0.8f, 0.8f, 1.0f), 5.0f, kBezierArrowWidth);
				}
				if (_DragCalculatorBranchEnable)
				{
					Vector2 shadowPos = Vector2.one * 3;

					EditorGUITools.BezierDotted(_DragCalculatorBranchBezier, shadowPos, new Color(0, 0, 0, 1.0f), 2.0f, 10.0f);
					EditorGUITools.BezierDotted(_DragCalculatorBranchBezier, Vector2.zero, new Color(1.0f, 0.8f, 0.8f, 1.0f), 2.0f, 10.0f);
				}

				HandleContextMenu();

				HandleCommand();

				if (_FrameSelected)
				{
					FrameSelected();
					_FrameSelected = false;
				}

				DragSelection();

				RenameOverlay renameOverlay = GetRenameOverlay();
				if (renameOverlay.IsRenaming())
				{
					int nodeID = renameOverlay.userData;
					Node node = _StateMachine.GetNodeFromID(nodeID);
					if (node != null)
					{
						renameOverlay.editFieldRect = NodeUtility.GetHeaderContentRect(GetNodeStyle(node),GetNodeContent(node),node.position);
					}

					HandleRenameOverlay();
				}
			}
		}

		void OnEvent()
		{
			RenameOverlay renameOverlay = GetRenameOverlay();
			renameOverlay.OnEvent();
		}

		[System.Reflection.Obfuscation(Exclude = true)]
		void Update()
		{

		}

		void RebuildIfNecessary()
		{
			RebuildNodeEditor();
			RebuildNodeCommentLayer();
			RebuildGroupLayer();
		}

		[System.Reflection.Obfuscation(Exclude = true)]
		void OnGUI()
		{
			using (new ProfilerScope("ArborEditorWindow.OnGUI"))
			{
				if (_StateMachine == null && _StateMachineInstanceID != 0)
				{
					_StateMachine = EditorUtility.InstanceIDToObject(_StateMachineInstanceID) as ArborFSMInternal;
					if (_StateMachine == null)
					{
						Initialize();
					}
					else
					{
						_Selection.Clear();
					}
				}

				bool isPlaying = EditorApplication.isPlaying;
				if (_IsPlaying != isPlaying)
				{
					_Initialized = false;
					_IsPlaying = isPlaying;
				}

				if (!_Initialized)
				{
					RenameOverlay renameOverlay = GetRenameOverlay();
					if (renameOverlay.IsRenaming())
					{
						renameOverlay.EndRename(false);
					}
					_ClearPropertyDrawerCache = true;

					InitNodeEditor();
					InitializeVisibleNodes();
					InitNodeCommentLayer();
					InitGroupLayer();

					_Initialized = true;
				}

				RebuildIfNecessary();

				if (_ClearPropertyDrawerCache)
				{
					EditorGUITools.ClearPropertyDrawerCache();
					_ClearPropertyDrawerCache = false;
				}

				OnEvent();

				ResizeHandling(this.position.width, this.position.height - EditorStyles.toolbar.fixedHeight);
				CalculateRect();

				DrawToolbar();

				EditorGUILayout.BeginHorizontal();

				if (ArborSettings.openStateList)
				{
					DrawStateList();
				}

				if (_StateMachine == null)
				{
					EditorGUILayout.EndHorizontal();
					return;
				}

				Event current = Event.current;
				if (current.type == EventType.ValidateCommand && current.commandName == "UndoRedoPerformed")
				{
					Repaint();
					HandleUtility.Repaint();
				}

#if ARBOR_TRIAL
				GUIContent openContent = EditorGUITools.GetTextContent( "Open Asset Store" );
				Vector2 openButtonSize = Styles.largeButton.CalcSize( openContent );
				Rect openRect = new Rect( _GraphGUI.viewPosition.xMin + 16.0f, _GraphGUI.viewPosition.yMax - openButtonSize.y - 16.0f, openButtonSize.x, openButtonSize.y );

				if( Event.current.type != EventType.Repaint )
				{
					if( GUI.Button( openRect, openContent, Styles.largeButton ) )
					{
						UnityEditorInternal.AssetStore.Open( "/content/47081" );
					}
				}
#endif

				using (new ProfilerScope("GraphGUI"))
				{
					_GraphGUI.label = Localization.GetTextContent("StateMachine");

					_GraphGUI.BeginGraphGUI();

					OnGraphGUI();

					_GraphGUI.isDragScroll = _DragStateBranchEnable || _DragCalculatorBranchEnable || _IsDragSelection || _DragNodePositions.Count>0;

					_GraphGUI.EndGraphGUI();

					UpdateGraphExtents();
					UpdateVisibleNodes();
				}

#if ARBOR_TRIAL
				if( Event.current.type == EventType.Repaint )
				{
					if( GUI.Button( openRect, openContent, Styles.largeButton ) )
					{
						UnityEditorInternal.AssetStore.Open( "/content/47081" );
					}
				}
#endif

				EditorGUILayout.EndHorizontal();
			}

			if (Event.current.type == EventType.DragExited)
			{
				StateEditor.BehaviourDragInfo behaviourDragInfo = StateEditor.GetBehaviourDragInfo();
				if (behaviourDragInfo != null)
				{
					behaviourDragInfo.dragging = false;
				}
			}
		}

		[System.Reflection.Obfuscation(Exclude = true)]
		private void ShowButton(Rect r)
		{
			bool flag = GUI.Toggle(r, _IsLocked, GUIContent.none, Styles.lockButton);
			if (flag == _IsLocked)
			{
				return;
			}
			_IsLocked = flag;
		}

		[System.Reflection.Obfuscation(Exclude = true)]
		private void OnInspectorUpdate()
		{
			if (_GraphGUI.isDragScroll || EditorApplication.isPlaying)
			{
				Repaint();
			}
		}

		private void FlipLocked()
		{
			_IsLocked = !_IsLocked;
		}

		public void AddItemsToMenu(GenericMenu menu)
		{
			menu.AddItem(EditorGUITools.GetTextContent("Lock"), _IsLocked, FlipLocked);
		}
	}
}