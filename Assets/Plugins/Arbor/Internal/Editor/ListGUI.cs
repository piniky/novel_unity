﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace ArborEditor
{
	public sealed class ListGUI
	{
		private SerializedProperty _Property;
		private GUIContent _Label;

		public ListGUI(SerializedProperty property)
		{
			_Property = property;
			_Label = new GUIContent( EditorGUITools.NicifyVariableName( property.name ) );
		}

		public delegate void DelegateAddButton();

		public DelegateAddButton addButton;

		public delegate void DelegateDrawChild(SerializedProperty property);

		public DelegateDrawChild drawChild;

		public delegate void DelegateRemove(SerializedProperty property);

		public DelegateRemove remove;

		public void OnGUI()
		{
			using( new Arbor.ProfilerScope( "ListGUI.OnGUI" ) )
			{
				GUILayout.BeginHorizontal( Styles.RLHeader );

				GUILayout.Label( _Label );

				GUILayout.FlexibleSpace();
				if( GUILayout.Button( Styles.addIconContent, Styles.invisibleButton, GUILayout.Width( 20f ), GUILayout.Height( 20 ) ) )
				{
					if( addButton != null )
					{
						addButton();
					}
					else
					{
						_Property.arraySize++;
					}

					_Property.serializedObject.ApplyModifiedProperties();
					_Property.serializedObject.Update();
				}
				GUILayout.EndHorizontal();

				if( _Property.arraySize > 0 )
				{
					EditorGUILayout.BeginVertical( Styles.RLBackground );

					for( int i = 0 ; i < _Property.arraySize ; i++ )
					{
						EditorGUILayout.BeginHorizontal();

						EditorGUILayout.BeginVertical();

						if( drawChild != null )
						{
							using( new Arbor.ProfilerScope( "DrawChild" ) )
							{
								drawChild( _Property.GetArrayElementAtIndex( i ) );
							}
						}

						EditorGUILayout.EndVertical();

						if( GUILayout.Button( Styles.removeIconContent, Styles.invisibleButton, GUILayout.Width( 20f ), GUILayout.Height( 20 ) ) )
						{
							if( remove != null )
							{
								remove( _Property.GetArrayElementAtIndex( i ) );
							}
							_Property.DeleteArrayElementAtIndex( i );
							break;
						}

						EditorGUILayout.EndHorizontal();

						GUILayout.Space( EditorGUIUtility.standardVerticalSpacing );

						if( i < _Property.arraySize - 1 )
						{
							EditorGUITools.DrawSeparator();
						}
					}

					GUILayout.Space( 10 );

					EditorGUILayout.EndVertical();
				}
			}
		}
	}
}
