﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

using Arbor;

namespace ArborEditor
{
	[CustomPropertyDrawer(typeof(FlexibleQuaternion))]
	public class FlexibleQuaternionPropertyDrawer : PropertyDrawer
	{
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.BeginProperty(position, label, property);

			SerializedProperty typeProperty = property.FindPropertyRelative("_Type");

			FlexibleQuaternion.Type type = EnumUtility.GetValueFromIndex<FlexibleQuaternion.Type>(typeProperty.enumValueIndex);

			switch (type)
			{
				case FlexibleQuaternion.Type.Constant:
					{
						SerializedProperty valueProperty = property.FindPropertyRelative("_Value");
						EditorGUI.BeginChangeCheck();
						Quaternion quaternionValue = EditorGUITools.RotationField(EditorGUITools.SubtractPopupWidth(position), label, valueProperty.quaternionValue);
						if( EditorGUI.EndChangeCheck() )
						{
							valueProperty.quaternionValue = quaternionValue;
                        }
					}
					break;
				case FlexibleQuaternion.Type.Parameter:
					EditorGUI.PropertyField(EditorGUITools.SubtractPopupWidth(position), property.FindPropertyRelative("_Parameter"), label);
					break;
				case FlexibleQuaternion.Type.Calculator:
					EditorGUI.PropertyField(EditorGUITools.SubtractPopupWidth(position), property.FindPropertyRelative("_Slot"), label);
					break;
			}

			Rect popupRect = EditorGUITools.GetPopupRect(position);

			if (EditorGUITools.ButtonMouseDown(popupRect, GUIContent.none,FocusType.Passive, Styles.shurikenDropDown))
			{
				GenericMenu menu = new GenericMenu();
				foreach (FlexibleQuaternion.Type t in System.Enum.GetValues(typeof(FlexibleQuaternion.Type)))
				{
					menu.AddItem( EditorGUITools.GetTextContent( t.ToString()), t == type, SelectType,new KeyValuePair<SerializedProperty, FlexibleQuaternion.Type>(property, t) );
				}
				menu.DropDown(popupRect);
            }

			EditorGUI.EndProperty();
		}

		private static void SelectType(object obj)
		{
			KeyValuePair<SerializedProperty, FlexibleQuaternion.Type> pair = (KeyValuePair<SerializedProperty, FlexibleQuaternion.Type>)obj;
			SerializedProperty property = pair.Key;
			FlexibleQuaternion.Type type = pair.Value;

			property.serializedObject.Update();

			SerializedProperty typeProperty = property.FindPropertyRelative("_Type");

			SerializedProperty slotProperty = property.FindPropertyRelative("_Slot");

			FlexibleQuaternion.Type oldType = EnumUtility.GetValueFromIndex<FlexibleQuaternion.Type>(typeProperty.enumValueIndex);
			if (oldType == FlexibleQuaternion.Type.Calculator)
			{
				Object targetObject = property.serializedObject.targetObject;
				ArborFSMInternal stateMachine = null;
				if (targetObject is StateBehaviour)
				{
					StateBehaviour behaviour = targetObject as StateBehaviour;
					stateMachine = behaviour.stateMachine;
				}
				else if (targetObject is Calculator)
				{
					Calculator calculator = targetObject as Calculator;
					stateMachine = calculator.stateMachine;
				}

				CalculatorBranch branch = stateMachine.GetCalculatorBranchFromID(slotProperty.FindPropertyRelative("branchID").intValue);
				if (branch != null)
				{
					stateMachine.DeleteCalculatorBranch(branch);
				}
			}

			typeProperty.enumValueIndex = EnumUtility.GetIndexFromValue(type);

			property.serializedObject.ApplyModifiedProperties();
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			float height = EditorGUIUtility.singleLineHeight;

			SerializedProperty typeProperty = property.FindPropertyRelative("_Type");

			FlexibleQuaternion.Type type = EnumUtility.GetValueFromIndex<FlexibleQuaternion.Type>(typeProperty.enumValueIndex);

			switch (type)
			{
				case FlexibleQuaternion.Type.Constant:
					height = (!EditorGUIUtility.wideMode ? 16.0f : 0.0f) + 16.0f;
					break;
				case FlexibleQuaternion.Type.Parameter:
					height = EditorGUI.GetPropertyHeight(property.FindPropertyRelative("_Parameter"));
					break;
				case FlexibleQuaternion.Type.Calculator:
					height = EditorGUI.GetPropertyHeight(property.FindPropertyRelative("_Slot"));
					break;
			}

			return height;
		}
	}
}
