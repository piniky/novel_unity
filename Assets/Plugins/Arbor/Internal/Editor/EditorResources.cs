﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;
using System.Reflection;

namespace ArborEditor
{
	public class EditorResources : Arbor.ScriptableSingleton<EditorResources>
	{
		const string k_DirectoryName = "EditorResources";

		private IAssetLoader m_AssetLoader;
		private IAssetLoader loader
		{
			get
			{
				if (m_AssetLoader == null)
				{
#if ARBOR_DLL
					Assembly assembly = Assembly.GetExecutingAssembly();
					string directory = Path.GetDirectoryName(assembly.Location);
					m_AssetLoader = new AssetBundleLoader( PathUtility.Combine(directory, k_DirectoryName) );
#else
					MonoScript script = MonoScript.FromScriptableObject(this);

					if (script != null)
					{
						string directory = Path.GetDirectoryName(AssetDatabase.GetAssetPath(script));
						_Directory = PathUtility.Combine(directory, k_DirectoryName);
						m_AssetLoader = new AssetDatabaseLoader(_Directory);
					}
#endif
				}

				return m_AssetLoader;
			}
		}

#if !ARBOR_DLL
		private string _Directory;

		public static string directory
		{
			get
			{
				return instance._Directory;
			}
		}
#endif

		public static Object Load(string name, System.Type type)
		{
			IAssetLoader loader = instance.loader;
			if (loader == null)
			{
				return null;
			}

			return loader.Load(name, type);
		}

		public static T Load<T>(string name) where T : Object
		{
			IAssetLoader loader = instance.loader;
			if (loader == null)
			{
				return null;
			}

			return loader.Load<T>(name);
		}

		public static Object Load(string name, string ext,System.Type type)
		{
			IAssetLoader loader = instance.loader;
			if (loader == null)
			{
				return null;
			}

			Object obj = loader.Load(name, type);
			if (obj != null)
			{
				return obj;
			}
			return loader.Load(Path.ChangeExtension(name, ext), type);
		}

		public static T Load<T>(string name,string ext) where T : Object
		{
			IAssetLoader loader = instance.loader;
			if (loader == null)
			{
				return null;
			}

			T obj = loader.Load<T>(name);
			if (obj != null)
			{
				return obj;
			}
			return loader.Load<T>(Path.ChangeExtension(name,ext));			
		}

		public static Texture2D LoadTexture(string name)
		{
			Texture2D tex = Load<Texture2D>(name);
			if (tex != null)
			{
				return tex;
			}
			tex = Load<Texture2D>(name + ".png");
			return tex;
		}
	}
}
