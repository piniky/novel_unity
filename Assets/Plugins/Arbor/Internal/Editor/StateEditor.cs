﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

using Arbor;

namespace ArborEditor
{
	[CustomNodeEditor(typeof(State))]
	public sealed class StateEditor : NodeEditor
	{
		public State state
		{
			get
			{
				return node as State;
			}
		}

		public class StateLinkProperty
		{
			public GUIContent label;
			public SerializedProperty property;
			public StateLink stateLink;
		}

		public class BehaviourEditor
		{
			static BehaviourEditor()
			{
				ArborSettings.onChangedLanguage += OnChanedLanguage;
			}

			public class BehaviourInfo
			{
				public string helpUrl;
				public string helpTooltip;
				public GUIContent titleContent;
			}

			static BehaviourInfo _InvalidBehaviourInfo = null;
			static Dictionary<System.Type, BehaviourInfo> _BehaviourInfos = new Dictionary<System.Type, BehaviourInfo>();

			static void OnChanedLanguage()
			{
				_BehaviourInfos.Clear();
			}

			public static BehaviourInfo GetBehaviourInfo(System.Type classType)
			{
				BehaviourInfo behaviourInfo;
				if (!_BehaviourInfos.TryGetValue(classType, out behaviourInfo))
				{
					string classTypeName = classType.Name;

					string docURL = ArborReferenceUtility.docUrl;

					BehaviourHelp help = AttributeHelper.GetAttribute<BehaviourHelp>(classType);

					behaviourInfo = new BehaviourInfo();
					if (help != null)
					{
						behaviourInfo.helpUrl = help.url;

						behaviourInfo.helpTooltip = string.Format("Open Reference for {0}.", classTypeName);
					}
					else if (AttributeHelper.HasAttribute<BuiltInBehaviour>(classType))
					{
						behaviourInfo.helpUrl = docURL + "behaviours/" + classTypeName.ToLower() + ".html";

						behaviourInfo.helpTooltip = string.Format("Open Reference for {0}.", classTypeName);
					}
					else
					{
						behaviourInfo.helpUrl = string.Empty;
						behaviourInfo.helpTooltip = "Open Arbor Document";
					}

					BehaviourTitle behaviourTitle = AttributeHelper.GetAttribute<BehaviourTitle>(classType);
					if (behaviourTitle != null)
					{
						behaviourInfo.titleContent = EditorGUITools.GetTextContent(EditorGUITools.NicifyVariableName(behaviourTitle.titleName));
					}
					else
					{
						behaviourInfo.titleContent = EditorGUITools.GetTextContent(EditorGUITools.NicifyVariableName(classTypeName));
					}

					_BehaviourInfos.Add(classType, behaviourInfo);
				}

				return behaviourInfo;
			}

			public static BehaviourInfo GetBehaviourInfo(Object behaviourObj)
			{
				if (ComponentUtility.IsValidObject(behaviourObj))
				{
					System.Type classType = behaviourObj.GetType();
					return GetBehaviourInfo(classType);
				}

				if (_InvalidBehaviourInfo == null)
				{
					_InvalidBehaviourInfo = new BehaviourInfo();
					_InvalidBehaviourInfo.titleContent = EditorGUITools.GetTextContent("Missing");
				}
				return _InvalidBehaviourInfo;
			}

			private StateEditor m_StateEditor;

			private Object m_BehaviourObj;

			public StateEditor stateEditor
			{
				get
				{
					return m_StateEditor;
				}
			}

			public State state
			{
				get
				{
					return (m_StateEditor != null) ? m_StateEditor.state : null;
				}
			}

			public Object behaviourObj
			{
				get
				{
					return m_BehaviourObj;
				}
			}

			private Editor m_Editor;

			public Editor editor
			{
				get
				{
					return m_Editor;
				}
			}
			public List<StateLinkProperty> stateLinkProperties = new List<StateLinkProperty>();

			public bool expanded = true;

			public BehaviourEditor(StateEditor stateEditor, Object behaviourObj)
			{
				m_StateEditor = stateEditor;
				m_BehaviourObj = behaviourObj;

				if (m_BehaviourObj is StateBehaviour)
				{
					m_Editor = Editor.CreateEditor(m_BehaviourObj);
				}
			}

			public void Validate()
			{
				if (m_Editor != null && m_Editor.target == null)
				{
					DestroyImmediate(m_Editor);
					m_Editor = null;
				}

				if (m_Editor == null && m_BehaviourObj is StateBehaviour)
				{
					m_Editor = Editor.CreateEditor(m_BehaviourObj);
				}
			}

			struct Path
			{
				public string name;
				public int depth;
				public bool isArray;
				public int index;
				public bool isStateLink;

				public Path(string name, int depth, bool isArray, string type)
				{
					this.name = name;
					this.depth = depth;
					this.isArray = isArray;
					this.index = 0;
					this.isStateLink = type == "StateLink";
				}
			}

			static Stack<Path> s_StateLinkPaths = new Stack<Path>();
			static System.Text.StringBuilder s_StateLinkNameBuilder = new System.Text.StringBuilder();

			static bool HasVisibleChildFields(SerializedProperty property)
			{
				if (!property.hasVisibleChildren)
				{
					return false;
				}

				switch (property.propertyType)
				{
					case SerializedPropertyType.Vector2:
					case SerializedPropertyType.Vector3:
					case SerializedPropertyType.Rect:
					case SerializedPropertyType.Bounds:
						return false;
				}

				return true;
			}

			bool IsStateLinkArray()
			{
				if (s_StateLinkPaths.Count > 0)
				{
					Path path = s_StateLinkPaths.Peek();
					return path.isArray && path.isStateLink;
				}
				return false;
			}

			string GetName(SerializedProperty iterator)
			{
				s_StateLinkNameBuilder.Length = 0;

				if (s_StateLinkPaths.Count > 0)
				{
					Path path = s_StateLinkPaths.Peek();
					if (path.isArray)
					{
						int index = path.index;

						s_StateLinkNameBuilder.Length = 0;
						s_StateLinkNameBuilder.AppendFormat("[{0}]", index);

						path.index++;

						s_StateLinkPaths.Pop();
						s_StateLinkPaths.Push(path);

						return s_StateLinkNameBuilder.ToString();
					}
				}

				return iterator.displayName;
			}

			public void UpdateStateLink()
			{
				using (new ProfilerScope("UpdateStateLink"))
				{
					if (editor == null)
					{
						return;
					}

					SerializedObject serializedObject = editor.serializedObject;

					serializedObject.Update();

					SerializedProperty iterator = serializedObject.GetIterator();

					bool enterChildren = true;

					s_StateLinkPaths.Clear();

					stateLinkProperties.Clear();

					while (iterator.NextVisible(enterChildren))
					{
						using (new ProfilerScope("Iterator"))
						{
							int depth = iterator.depth;

							while (s_StateLinkPaths.Count > 0 && s_StateLinkPaths.Peek().depth >= depth)
							{
								s_StateLinkPaths.Pop();
							}

							if (HasVisibleChildFields(iterator))
							{
								string iteratorType = iterator.type;

								bool isIteratorArray = iterator.isArray;

								if (isIteratorArray)
								{
									s_StateLinkPaths.Push(new Path(GetName(iterator), depth, true, iteratorType));

									enterChildren = true;
								}
								else if (iteratorType == "StateLink" || IsStateLinkArray())
								{
									s_StateLinkNameBuilder.Length = 0;

									s_StateLinkNameBuilder.Insert(0, GetName(iterator));

									foreach (Path path in s_StateLinkPaths)
									{
										if (!path.isArray)
										{
											s_StateLinkNameBuilder.Insert(0, "/");
										}

										s_StateLinkNameBuilder.Insert(0, path.name);
									}

									StateLinkProperty stateLinkProperty = new StateLinkProperty();
									stateLinkProperty.label = EditorGUITools.GetTextContent(s_StateLinkNameBuilder.ToString());
									stateLinkProperty.property = iterator.Copy();
									stateLinkProperty.stateLink = EditorGUITools.GetPropertyObject<StateLink>(stateLinkProperty.property);
										
									stateLinkProperties.Add(stateLinkProperty);

									enterChildren = false;
								}
								else
								{
									s_StateLinkPaths.Push(new Path(GetName(iterator), depth, false, iteratorType));

									enterChildren = true;
								}
							}
							else
							{
								enterChildren = false;
							}
						}
					}

					serializedObject.ApplyModifiedProperties();
				}
			}

			private static int s_BehaviourTitlebarHash = "s_BehaviourTitlebarHash".GetHashCode();

			void MoveUpBehaviourContextMenu(object obj)
			{
				int index = (int)obj;

				ArborFSMInternal stateMachine = state.stateMachine;

				Undo.IncrementCurrentGroup();

				Undo.RecordObject(stateMachine, "MoveUp Behaviour");

				state.SwapBehaviour(index, index - 1);

				Undo.CollapseUndoOperations(Undo.GetCurrentGroup());

				EditorUtility.SetDirty(stateMachine);
			}

			void MoveDownBehaviourContextMenu(object obj)
			{
				int index = (int)obj;

				ArborFSMInternal stateMachine = state.stateMachine;

				Undo.IncrementCurrentGroup();

				Undo.RecordObject(stateMachine, "MoveDown Behaviour");

				state.SwapBehaviour(index, index + 1);

				Undo.CollapseUndoOperations(Undo.GetCurrentGroup());

				EditorUtility.SetDirty(stateMachine);
			}

			void CopyBehaviourContextMenu()
			{
				StateBehaviour behaviour = m_BehaviourObj as StateBehaviour;

				Clipboard.CopyBehaviour(behaviour);
			}

			void PasteBehaviourContextMenu()
			{
				StateBehaviour behaviour = m_BehaviourObj as StateBehaviour;

				Undo.IncrementCurrentGroup();

				Undo.RecordObject(behaviour, "Paste Behaviour");

				Clipboard.PasteBehaviourValues(behaviour);

				Undo.CollapseUndoOperations(Undo.GetCurrentGroup());

				EditorUtility.SetDirty(behaviour);
			}

			void DeleteBehaviourContextMenu()
			{
				if (m_StateEditor != null && m_StateEditor.window != null)
				{
					m_StateEditor.window.DeleteBehaviour(state, behaviourObj);
				}
			}

			void EditScriptBehaviourContextMenu(object obj)
			{
				MonoScript script = obj as MonoScript;

				AssetDatabase.OpenAsset(script);
			}

			public BehaviourInfo GetBehaviourInfo()
			{
				return GetBehaviourInfo(m_BehaviourObj);
			}

			bool BehaviourTitlebar(Rect position, bool foldout)
			{
				using (new ProfilerScope("EditorGUITools.BehvaiourTitlebar"))
				{
					int controlId = GUIUtility.GetControlID(s_BehaviourTitlebarHash, FocusType.Passive, position);

					Event current = Event.current;

					//foldout = EditorGUI.Foldout( position,foldout,GUIContent.none,s_BehaviourTitlebar );

					StateBehaviour behaviour = behaviourObj as StateBehaviour;

					BehaviourInfo behaviourInfo = GetBehaviourInfo();

					Rect iconPosition = new Rect(position.x + (float)Styles.titlebar.padding.left, position.y + (float)Styles.titlebar.padding.top, 16f, 16f);

					Rect checkPosition = new Rect(iconPosition.xMax, iconPosition.y, 16f, 16f);

					Rect popupPosition = new Rect(position.xMax - (float)Styles.titlebar.padding.right - 2.0f - 16.0f, iconPosition.y, 16f, 16f);

					Rect helpPosition = new Rect();
					Rect textPosition = new Rect();

					if (string.IsNullOrEmpty(behaviourInfo.helpUrl))
					{
						textPosition = new Rect(checkPosition.xMax + 4.0f, iconPosition.y, popupPosition.x - iconPosition.xMax - 4.0f, iconPosition.height);
					}
					else
					{
						helpPosition = new Rect(popupPosition.x - 18.0f, iconPosition.y, 16f, 16f);
						textPosition = new Rect(checkPosition.xMax + 4.0f, iconPosition.y, helpPosition.x - iconPosition.xMax - 4.0f - 4.0f, iconPosition.height);
					}

					if (behaviour != null)
					{
						EditorGUI.BeginChangeCheck();
						bool behaviourEnabled = EditorGUI.Toggle(checkPosition, behaviour.behaviourEnabled);
						if (EditorGUI.EndChangeCheck())
						{
							Undo.RecordObject(behaviourObj, (!behaviourEnabled ? "Disable" : "Enable") + " Behaviour");
							behaviour.behaviourEnabled = behaviourEnabled;
							EditorUtility.SetDirty(behaviourObj);
						}
					}

					if (!string.IsNullOrEmpty(behaviourInfo.helpUrl))
					{
						EditorGUITools.HelpButton(helpPosition, behaviourInfo.helpUrl, behaviourInfo.helpTooltip);
					}

					EventType typeForControl = current.GetTypeForControl(controlId);
					switch (typeForControl)
					{
						case EventType.MouseDown:
							if (popupPosition.Contains(current.mousePosition))
							{
								GenericMenu menu = new GenericMenu();

								int index = -1;
								int behaviourCount = state.behaviourCount;
								for (int behaviourIndex = 0; behaviourIndex < behaviourCount; behaviourIndex++)
								{
									if (state.GetBehaviourObjectFromIndex(behaviourIndex) == behaviourObj)
									{
										index = behaviourIndex;
										break;
									}
								}

								if (index >= 1)
								{
									menu.AddItem(Localization.GetTextContent("Move Up"), false, MoveUpBehaviourContextMenu, index);
								}
								else
								{
									menu.AddDisabledItem(Localization.GetTextContent("Move Up"));
								}

								if (index < behaviourCount - 1)
								{
									menu.AddItem(Localization.GetTextContent("Move Down"), false, MoveDownBehaviourContextMenu, index);
								}
								else
								{
									menu.AddDisabledItem(Localization.GetTextContent("Move Down"));
								}

								if (behaviour != null)
								{
									menu.AddItem(Localization.GetTextContent("Copy"), false, CopyBehaviourContextMenu);
									if (Clipboard.CompareBehaviourType(behaviourObj.GetType()))
									{
										menu.AddItem(Localization.GetTextContent("Paste"), false, PasteBehaviourContextMenu);
									}
									else
									{
										menu.AddDisabledItem(Localization.GetTextContent("Paste"));
									}
								}

								menu.AddItem(Localization.GetTextContent("Delete"), false, DeleteBehaviourContextMenu);
								
								menu.AddSeparator("");

								MonoScript script = EditorGUITools.GetMonoScript(behaviourObj);

								if (script != null)
								{
									menu.AddItem(Localization.GetTextContent("Edit Script"), false, EditScriptBehaviourContextMenu, script);
								}
								else
								{
									menu.AddDisabledItem(Localization.GetTextContent("Edit Script"));
								}

								EditorGUITools.AddContextMenu(menu, behaviourObj);

								menu.DropDown(popupPosition);

								current.Use();
							}
							else if (checkPosition.Contains(current.mousePosition))
							{
								if (current.button == 0 && (Application.platform != RuntimePlatform.OSXEditor || !current.control))
								{

								}
							}
							else if (position.Contains(current.mousePosition))
							{
								if (current.button == 0 && (Application.platform != RuntimePlatform.OSXEditor || !current.control))
								{
									GUIUtility.hotControl = GUIUtility.keyboardControl = controlId;
									current.Use();
								}
							}
							break;
						case EventType.MouseDrag:
							if (GUIUtility.hotControl == controlId && current.button == 0)
							{
								GUIUtility.hotControl = 0;

								if ((object)behaviourObj != null)
								{
									BeginDragBehaviour(behaviourObj, controlId);
								}

								current.Use();
							}
							break;
						case EventType.MouseUp:
							if (GUIUtility.hotControl == controlId)
							{
								GUIUtility.hotControl = 0;

								if (position.Contains(current.mousePosition))
								{
									foldout = !foldout;
								}
								current.Use();
							}
							break;
						case EventType.KeyDown:
							if (GUIUtility.keyboardControl == controlId)
							{
								if (current.keyCode == KeyCode.LeftArrow)
								{
									foldout = false;
									current.Use();
								}
								if (current.keyCode == KeyCode.RightArrow)
								{
									foldout = true;
									current.Use();
									break;
								}
							}
							break;
						case EventType.Repaint:
							bool isDragging = GetDragControlID() == controlId;
							bool isHover = position.Contains(current.mousePosition) || isDragging;
							bool isActive = GUIUtility.hotControl == controlId || isDragging;
							bool on = foldout;
							bool hasKeyboardFocus = GUIUtility.keyboardControl == controlId;
							Styles.titlebar.Draw(position, GUIContent.none, isHover, isActive, on, hasKeyboardFocus);
							GUIStyle.none.Draw(iconPosition, EditorGUITools.GetThumbnailContent(behaviourObj), isHover, isActive, on, hasKeyboardFocus);
							Styles.titlebarText.Draw(textPosition, behaviourInfo.titleContent, isHover, isActive, on, hasKeyboardFocus);
							Styles.titlebarText.Draw(popupPosition, EditorGUITools.popupContent, isHover, isActive, on, hasKeyboardFocus);
							Styles.foldout.Draw(new Rect(position.x + 3f, position.y + 3f, 16f, 16f), isHover, isActive, on, hasKeyboardFocus);
							break;
					}

					return foldout;
				}
			}

			bool BehaviourTitlebar(bool foldout)
			{
				Rect position = GUILayoutUtility.GetRect(GUIContent.none, Styles.titlebar);

				return BehaviourTitlebar(position, foldout);
			}

			private static State _DragTargetState = null;

			static Bezier2D GetTargetBezier(State state, Vector2 targetPos, Vector2 leftPos, Vector2 rightPos)
			{
				bool right = (targetPos - leftPos).magnitude > (targetPos - rightPos).magnitude;

				Vector2 startPos;
				Vector2 startTangent;
				
				if (right)
				{
					startPos = rightPos;
					startTangent = rightPos + EditorGUITools.kBezierTangentOffset;
				}
				else
				{
					startPos = leftPos;
					startTangent = leftPos - EditorGUITools.kBezierTangentOffset;
				}

				return new Bezier2D(startPos,startTangent, targetPos, startTangent);
			}

			static Bezier2D GetTargetBezier(State state, State target, Vector2 leftPos, Vector2 rightPos)
			{
				Vector2 startPos = Vector2.zero;
				Vector2 startTangent = Vector2.zero;
				Vector2 endPos = Vector2.zero;
				Vector2 endTangent = Vector2.zero;

				bool right = true;

				if (target != null)
				{
					Rect targetRect = target.position;
					targetRect.x -= state.position.x;
					targetRect.y -= state.position.y;

					EditorGUITools.Pivot findPivot = null;

					List<EditorGUITools.Pivot> pivots = new List<EditorGUITools.Pivot>();

					pivots.Add(new EditorGUITools.Pivot(new Vector2(targetRect.xMin, targetRect.yMin + EditorGUITools.kStateBezierTargetOffsetY), -Vector2.right));
					pivots.Add(new EditorGUITools.Pivot(new Vector2(targetRect.xMax, targetRect.yMin + EditorGUITools.kStateBezierTargetOffsetY), Vector2.right));

					if (targetRect.x == 0.0f)
					{
						if (targetRect.y > 0.0f)
						{
							findPivot = pivots[0];
							right = false;
						}
						else
						{
							findPivot = pivots[1];
							right = true;
						}
					}
					else
					{
						float findDistance = 0.0f;

						int pivotCount = pivots.Count;
						for (int pivotIndex = 0; pivotIndex < pivotCount; pivotIndex++)
						{
							EditorGUITools.Pivot pivot = pivots[pivotIndex];

							Vector2 vl = leftPos - pivot.position;
							Vector2 vr = rightPos - pivot.position;

							float leftDistance = vl.magnitude;
							float rightDistance = vr.magnitude;

							float distance = 0.0f;
							bool checkRight = false;

							if (leftDistance > rightDistance)
							{
								distance = rightDistance;
								checkRight = true;
							}
							else
							{
								distance = leftDistance;
								checkRight = false;
							}

							if (findPivot == null || distance < findDistance)
							{
								findPivot = pivot;
								findDistance = distance;
								right = checkRight;
							}
						}
					}

					if (right)
					{
						startPos = rightPos;
						startTangent = rightPos + EditorGUITools.kBezierTangentOffset;
					}
					else
					{
						startPos = leftPos;
						startTangent = leftPos - EditorGUITools.kBezierTangentOffset;
					}

					endPos = findPivot.position;
					endTangent = endPos + findPivot.normal * EditorGUITools.kBezierTangent;
				}

				return new Bezier2D(startPos, startTangent, endPos, endTangent);
			}

			private static State GetStateFromPosition(ArborFSMInternal stateMachine, Vector2 position)
			{
				for (int i = 0, count = stateMachine.stateCount; i < count; i++)
				{
					State state = stateMachine.GetStateFromIndex(i);
					if (!state.resident && state.position.Contains(position))
					{
						return state;
					}
				}

				return null;
			}

			private static int s_StateLinkHash = "s_StateLinkHash".GetHashCode();

			static TransitionTiming GetTransitionTiming(SerializedProperty property)
			{
				System.Type type = null;
				System.Reflection.FieldInfo stateLinkFieldInfo = EditorGUITools.GetFieldInfoFromProperty(property, out type);

				FixedTransitionTiming fixedTransitionTiming = AttributeHelper.GetAttribute<FixedTransitionTiming>(stateLinkFieldInfo);
				FixedImmediateTransition fixedImmediateTransition = AttributeHelper.GetAttribute<FixedImmediateTransition>(stateLinkFieldInfo);

				TransitionTiming transitionTiming = TransitionTiming.LateUpdateDontOverwrite;

				if (fixedTransitionTiming != null)
				{
					transitionTiming = fixedTransitionTiming.transitionTiming;
				}
				else if (fixedImmediateTransition != null)
				{
					transitionTiming = fixedImmediateTransition.immediate ? TransitionTiming.Immediate : TransitionTiming.LateUpdateOverwrite;
				}
				else
				{
					SerializedProperty transitionTimingProperty = property.FindPropertyRelative("transitionTiming");
					transitionTiming = EnumUtility.GetValueFromIndex<TransitionTiming>(transitionTimingProperty.enumValueIndex);
				}

				return transitionTiming;
			}

			public static void SingleStateLinkField(Rect position, GUIContent label, SerializedProperty property)
			{
				using (new ProfilerScope("SingleStateLinkField"))
				{
					StateBehaviour behaviour = property.serializedObject.targetObject as StateBehaviour;

					if (behaviour == null || behaviour.stateID == 0 || behaviour.stateMachine == null || property.isArray)
					{
						EditorGUI.HelpBox(position, "This is Arbor Editor only.", MessageType.Error);

						return;
					}

					ArborFSMInternal stateMachine = behaviour.stateMachine;
					State state = stateMachine.GetStateFromID(behaviour.stateID);

					SerializedProperty stateIDProperty = property.FindPropertyRelative("stateID");
					SerializedProperty nameProperty = property.FindPropertyRelative("name");
					SerializedProperty lineEnableProperty = property.FindPropertyRelative("lineEnable");
					SerializedProperty lineStartProperty = property.FindPropertyRelative("lineStart");
					SerializedProperty lineStartTangentProperty = property.FindPropertyRelative("lineStartTangent");
					SerializedProperty lineEndProperty = property.FindPropertyRelative("lineEnd");
					SerializedProperty lineEndTangentProperty = property.FindPropertyRelative("lineEndTangent");
					SerializedProperty lineColorProperty = property.FindPropertyRelative("lineColor");
					SerializedProperty lineColorChangedProperty = property.FindPropertyRelative("lineColorChanged");

					if (!string.IsNullOrEmpty(nameProperty.stringValue))
					{
						label = EditorGUITools.GetTextContent(nameProperty.stringValue);
					}

					TransitionTiming transitionTiming = GetTransitionTiming(property);

					label.image = Icons.GetTransitionTimingIcon(transitionTiming);

					int controlID = EditorGUIUtility.GetControlID(s_StateLinkHash, FocusType.Passive, position);

					Event currentEvent = Event.current;

					State linkState = stateMachine.GetStateFromID(stateIDProperty.intValue);

					bool dragging = (GUIUtility.hotControl == controlID && currentEvent.button == 0);

					State targetState = dragging ? _DragTargetState : linkState;

					Vector2 nowPos = currentEvent.mousePosition;

					Vector2 leftPos = new Vector2(position.x + 8, position.center.y);
					Vector2 rightPos = new Vector2(position.x + position.width - 8, position.center.y);

					Bezier2D bezier = new Bezier2D();
					if (targetState != null)
					{
						bezier = GetTargetBezier(state, targetState, leftPos, rightPos);
					}
					else if (dragging)
					{
						bezier = GetTargetBezier(state, nowPos, leftPos, rightPos);
					}
					else
					{
						bezier.startPosition = rightPos;
					}

					Rect boxRect = new Rect(bezier.startPosition.x - 8, position.y, 16, position.height);

					Vector2 statePosition = new Vector2(state.position.x, state.position.y);

					bezier.startPosition += statePosition;
					bezier.startControl += statePosition;
					bezier.endPosition += statePosition;
					bezier.endControl += statePosition;

					Rect settingRect = position;

					settingRect.x += position.width - 16 - 16;
					settingRect.y += (position.height - 16) / 2;
					settingRect.height = 16;
					settingRect.width = 16;

					ArborEditorWindow window = ArborEditorWindow.GetCurrent();

					EventType eventType = currentEvent.GetTypeForControl(controlID);

					switch (eventType)
					{
						case EventType.MouseDown:
							if (position.Contains(nowPos) && !settingRect.Contains(nowPos))
							{
								if (currentEvent.button == 0)
								{
									GUIUtility.hotControl = GUIUtility.keyboardControl = controlID;

									_DragTargetState = null;

									if (window != null)
									{
										window.BeginDragStateBranch(state.nodeID);
										window.DragStateBranchBezie(bezier);
									}

									lineEnableProperty.boolValue = false;

									currentEvent.Use();
								}
							}
							break;
						case EventType.MouseDrag:
							if (GUIUtility.hotControl == controlID && currentEvent.button == 0)
							{
								DragAndDrop.PrepareStartDrag();

								State nextState = GetStateFromPosition(stateMachine, nowPos + statePosition);

								if (nextState != null)
								{
									if (window)
									{
										window.DragStateBranchHoverStateID(nextState.nodeID);
									}

									_DragTargetState = nextState;
								}
								else
								{
									if (window)
									{
										window.DragStateBranchHoverStateID(0);
									}
									_DragTargetState = null;
								}

								currentEvent.Use();
							}
							break;
						case EventType.MouseUp:
							if (GUIUtility.hotControl == controlID)
							{
								if (currentEvent.button == 0)
								{
									GUIUtility.hotControl = 0;

									if (_DragTargetState != linkState)
									{
										Undo.RecordObject(behaviour, "Link State");

										if (_DragTargetState != null)
										{
											stateIDProperty.intValue = _DragTargetState.nodeID;
										}
										else
										{
											stateIDProperty.intValue = 0;
										}

										lineEnableProperty.boolValue = stateIDProperty.intValue != 0;
										lineStartProperty.vector2Value = bezier.startPosition;
										lineStartTangentProperty.vector2Value = bezier.startControl;
										lineEndProperty.vector2Value = bezier.endPosition;
										lineEndTangentProperty.vector2Value = bezier.endControl;

										EditorUtility.SetDirty(behaviour);
									}
									else
									{
										lineEnableProperty.boolValue = stateIDProperty.intValue != 0;
									}

									if (window != null)
									{
										window.EndDragStateBranch();
									}

									_DragTargetState = null;

									currentEvent.Use();
								}
							}
							break;
						case EventType.Repaint:
							Vector2 iconSize = EditorGUIUtility.GetIconSize();
							EditorGUIUtility.SetIconSize(new Vector2(16f, 16f));

							if (GUIUtility.hotControl == controlID && currentEvent.button == 0)
							{
								if (window)
								{
									window.DragStateBranchBezie(bezier);
								}
							}
							else if (linkState != null)
							{
								lineEnableProperty.boolValue = true;
								lineStartProperty.vector2Value = bezier.startPosition;
								lineStartTangentProperty.vector2Value = bezier.startControl;
								lineEndProperty.vector2Value = bezier.endPosition;
								lineEndTangentProperty.vector2Value = bezier.endControl;
							}

							bool on = GUIUtility.hotControl == controlID && currentEvent.button == 0 || linkState != null;

							Color savedColor = GUI.backgroundColor;

							Color lineColor = Color.white;

							if (lineColorChangedProperty.boolValue)
							{
								lineColor = lineColorProperty.colorValue;
							}

							GUI.backgroundColor = new Color(lineColor.r, lineColor.g, lineColor.b);
							EditorStyles.miniButton.Draw(position, label, controlID, on);

							GUI.backgroundColor = savedColor;

							EditorStyles.radioButton.Draw(boxRect, GUIContent.none, controlID, on);

							EditorGUIUtility.SetIconSize(iconSize);
							break;
					}

					if (EditorGUITools.ButtonMouseDown(settingRect, EditorGUITools.popupContent, FocusType.Passive, GUIStyle.none))
					{
						StateLinkSettingWindow.instance.Init(property, settingRect);
					}
				}
			}

			public static void SingleStateLinkField(GUIContent label, SerializedProperty property)
			{
				Rect position = GUILayoutUtility.GetRect(0.0f, 20.0f);

				SingleStateLinkField(position, label, property);
			}

			public void OnGUI()
			{
				Editor editor = m_Editor;

				StateBehaviour behaviour = m_BehaviourObj as StateBehaviour;

				float labelWidth = EditorGUIUtility.labelWidth;
				EditorGUIUtility.labelWidth = 120.0f;

				bool expanded = behaviour != null ? behaviour.expanded : this.expanded;
				bool resultExpanded = BehaviourTitlebar(expanded);
				if (expanded != resultExpanded)
				{
					expanded = resultExpanded;

					if (behaviour != null)
					{
						behaviour.expanded = expanded;
						EditorUtility.SetDirty(behaviour);
					}
					else
					{
						this.expanded = expanded;
					}
				}

				if (stateEditor != null && stateEditor.window != null && (object)behaviourObj != null && Event.current.type == EventType.Repaint)
				{
					Rect lastRect = GUILayoutUtility.GetLastRect();
					Rect statePosition = state.position;
					lastRect.x += statePosition.x;
					lastRect.y += statePosition.y;

					stateEditor.window.SetBehaviourPosition(m_BehaviourObj, lastRect);
				}

				if (expanded)
				{
					if (editor != null)
					{
						GUIStyle marginStyle = (editor.UseDefaultMargins()) ? EditorStyles.inspectorDefaultMargins : EditorStyles.inspectorFullWidthMargins;
						EditorGUILayout.BeginVertical(marginStyle);
						using (new ProfilerScope("OnInspector"))
						{
							try
							{
								editor.OnInspectorGUI();
							}
							catch (System.Exception ex)
							{
								if (EditorGUITools.ShouldRethrowException(ex))
								{
									throw;
								}
								else
								{
									Debug.LogException(ex);
								}
							}
						}
						EditorGUILayout.EndVertical();
					}
					else
					{
						EditorGUILayout.BeginVertical(EditorStyles.inspectorDefaultMargins);

						if (behaviourObj != null)
						{
							MonoScript script = EditorGUITools.GetMonoScript(behaviourObj);
							if (script != null)
							{
								EditorGUI.BeginDisabledGroup(true);
								EditorGUILayout.ObjectField("Script", script, typeof(MonoScript), false);
								EditorGUI.EndDisabledGroup();
							}
						}

						EditorGUILayout.HelpBox(Localization.GetWord("MissingError"), MessageType.Error);

						EditorGUILayout.EndVertical();
					}
				}

				if (editor != null)
				{
					editor.serializedObject.Update();

					UpdateStateLink();

					foreach (StateEditor.StateLinkProperty stateLinkProperty in stateLinkProperties)
					{
						SingleStateLinkField(stateLinkProperty.label, stateLinkProperty.property);
					}

					editor.serializedObject.ApplyModifiedProperties();
				}

				EditorGUIUtility.labelWidth = labelWidth;
			}
		}

		private Dictionary<Object, BehaviourEditor> _BehaviourEditors = new Dictionary<Object, BehaviourEditor>();

		public BehaviourEditor GetBehaviourEditor(Object behaviourObj)
		{
			using (new ProfilerScope("GetBehaviourEditor"))
			{
				BehaviourEditor behaviourEditor = null;

				if ((object)behaviourObj != null && _BehaviourEditors.TryGetValue(behaviourObj, out behaviourEditor))
				{
					Editor editor = behaviourEditor.editor;
					if (editor == null)
					{
						_BehaviourEditors.Remove(behaviourObj);
						behaviourEditor = null;
					}
					else if (behaviourObj != editor.target)
					{
						Object.DestroyImmediate(editor);
						_BehaviourEditors.Remove(behaviourObj);

						behaviourEditor = null;
					}
				}

				if (behaviourEditor == null)
				{
					behaviourEditor = new BehaviourEditor(this, behaviourObj);

					if ((object)behaviourObj != null)
					{
						_BehaviourEditors.Add(behaviourObj, behaviourEditor);
					}
				}

				return behaviourEditor;
			}
		}

		public void UpdateBehaviour()
		{
			using (new ProfilerScope("UpdateBehaviour"))
			{
				int behaviourCount = state.behaviourCount;
				for (int i = 0; i < behaviourCount; i++)
				{
					Object behaviourObj = state.GetBehaviourObjectFromIndex(i);

					BehaviourEditor behaviourEditor = GetBehaviourEditor(behaviourObj);

					behaviourEditor.UpdateStateLink();
				}
			}
		}

		public void RemoveBehaviourEditor(Object behaviourObj)
		{
			BehaviourEditor behaviourEditor = null;
			if (_BehaviourEditors.TryGetValue(behaviourObj, out behaviourEditor))
			{
				Object.DestroyImmediate(behaviourEditor.editor);
				_BehaviourEditors.Remove(behaviourObj);
			}
		}

		[System.Reflection.Obfuscation(Exclude = true)]
		void OnDestroy()
		{
			if (_BehaviourEditors == null)
			{
				return;
			}
			foreach (KeyValuePair<Object, BehaviourEditor> pair in _BehaviourEditors)
			{
				Object.DestroyImmediate(pair.Value.editor);
			}
			_BehaviourEditors.Clear();
		}

		public override void Validate(Node node)
		{
			base.Validate(node);

			foreach (KeyValuePair<Object, BehaviourEditor> pair in _BehaviourEditors)
			{
				BehaviourEditor behaviourEditor = pair.Value;

				behaviourEditor.Validate();
			}
		}

		private GUIContent _TitleContent = new GUIContent();

		public override GUIContent GetTitleContent()
		{
			string name = (window != null && window.IsRenaming(state.nodeID))? string.Empty : state.name;

			Texture2D icon = Icons.GetStateIcon(state);

			_TitleContent.text = name;
			_TitleContent.image = icon;

			return _TitleContent;
		}

		public override GUIStyle GetStyle()
		{
			Styles.Color color = StateColor.GetStyleColor(state);
			return Styles.GetNodeStyle(color, isSelection);
		}

		private static readonly int s_StateTitlebarHash = "s_StateTitlebarHash".GetHashCode();

		protected override void OnHeaderGUI()
		{
			State state = this.state;

			Rect position = state.position;
			position.x = position.y = 0;

			int controlId = GUIUtility.GetControlID(s_StateTitlebarHash, FocusType.Passive, position);

			Event current = Event.current;

			EventType typeForControl = current.GetTypeForControl(controlId);

			Rect headerRect = GetHeaderRect(position);

			Rect namePosition = GetNameRect(position);

			switch (typeForControl)
			{
				case EventType.ContextClick:
					if (headerRect.Contains(current.mousePosition))
					{
						DoStateContextMenu(new Rect(current.mousePosition.x, current.mousePosition.y, 0, 0), headerRect, state);
						current.Use();
					}
					break;
				case EventType.MouseDown:
					if (current.button == 0 && current.clickCount == 2 && namePosition.Contains(current.mousePosition))
					{
						GUIUtility.hotControl = controlId;
						current.Use();
					}
					break;
				case EventType.MouseUp:
					if (GUIUtility.hotControl == controlId)
					{
						GUIUtility.hotControl = 0;

						window.BeginRename(state);
						current.Use();
					}
					break;
			}

			Rect popupPosition = new Rect(namePosition.xMax + 8, namePosition.y, EditorGUIUtility.singleLineHeight, namePosition.height);

			StateSettings(popupPosition, GetHeaderRect(position), state);
		}

		void ChangeShowComment()
		{
			ArborFSMInternal stateMachine = node.stateMachine;

			Undo.RecordObject(stateMachine, "Change Show Comment");

			node.showComment = !node.showComment;

			window.UpdateNodeCommentControl(node);

			EditorUtility.SetDirty(stateMachine);
		}

		void SetStartStateContextMenu()
		{
			ArborFSMInternal stateMachine = state.stateMachine;

			SerializedObject serializedObject = new SerializedObject(stateMachine);

			serializedObject.Update();

			SerializedProperty startStateIDPropery = serializedObject.FindProperty("_StartStateID");

			startStateIDPropery.intValue = state.nodeID;

			serializedObject.ApplyModifiedProperties();

			serializedObject.Dispose();
		}

		void RenameStateContextMenu()
		{
			if (window != null)
			{
				window.BeginRename(state);
			}
		}

		void FlipStateBreakPoint()
		{
			ArborFSMInternal stateMachine = state.stateMachine;

			if (state.breakPoint)
			{
				Undo.RecordObject(stateMachine, "State BreakPoint Off");
			}
			else
			{
				Undo.RecordObject(stateMachine, "State BreakPoint On");
			}

			state.breakPoint = !state.breakPoint;

			EditorUtility.SetDirty(stateMachine);
		}

		void TransitionState()
		{
			ArborFSMInternal stateMachine = state.stateMachine;

			stateMachine.Transition(state);
		}

		void PasteBehaviourToStateContextMenu()
		{
			Undo.IncrementCurrentGroup();

			ArborFSMInternal stateMachine = state.stateMachine;

			Undo.RecordObject(stateMachine, "Paste Behaviour");

			Clipboard.PasteBehaviourAsNew(state);

			Undo.CollapseUndoOperations(Undo.GetCurrentGroup());

			EditorUtility.SetDirty(stateMachine);
		}

		void AddBehaviourToStateContextMenu(object obj)
		{
			Rect position = (Rect)obj;

			BehaviourMenuWindow.instance.Init(state, position);
		}

		public void DoStateContextMenu(Rect popupPosition, Rect headerPosition, State state)
		{
			GenericMenu menu = new GenericMenu();

			SerializedObject serializedObject = new SerializedObject(state.stateMachine);

			SerializedProperty startStateIDPropery = serializedObject.FindProperty("_StartStateID");

			menu.AddItem(Localization.GetTextContent("Rename"), false, RenameStateContextMenu);

			menu.AddItem(Localization.GetTextContent("Show Comment"), state.showComment, ChangeShowComment);

			if (!state.resident)
			{
				if (startStateIDPropery.intValue == state.nodeID)
				{
					menu.AddDisabledItem(Localization.GetTextContent("Set Start State"));
				}
				else
				{
					menu.AddItem(Localization.GetTextContent("Set Start State"), false, SetStartStateContextMenu);
				}

				menu.AddItem(Localization.GetTextContent("BreakPoint"), state.breakPoint, FlipStateBreakPoint);
			}

			menu.AddSeparator("");

			menu.AddItem(Localization.GetTextContent("Add Behaviour"), false, AddBehaviourToStateContextMenu, EditorGUITools.GUIToScreenRect(headerPosition));

			if (Clipboard.hasBehaviour)
			{
				menu.AddItem(Localization.GetTextContent("Paste Behaviour"), false, PasteBehaviourToStateContextMenu);
			}
			else
			{
				menu.AddDisabledItem(Localization.GetTextContent("Paste Behaviour"));
			}

			menu.AddSeparator("");

			SetNodeContextMenu(menu);
			
			if (!state.resident)
			{
				menu.AddSeparator("");
				if (Application.isPlaying)
				{
					menu.AddItem(Localization.GetTextContent("Transition"), state.breakPoint, TransitionState);
				}
				else
				{
					menu.AddDisabledItem(Localization.GetTextContent("Transition"));
				}
			}

			menu.DropDown(popupPosition);
		}

		public void StateSettings(Rect popupPosition, Rect headerPosition, State state)
		{
			int controlId = GUIUtility.GetControlID(s_StateTitlebarHash, FocusType.Passive, popupPosition);

			Event current = Event.current;

			EventType typeForControl = current.GetTypeForControl(controlId);
			switch (typeForControl)
			{
				case EventType.MouseDown:
					if (popupPosition.Contains(current.mousePosition))
					{
						DoStateContextMenu(popupPosition, headerPosition, state);

						current.Use();
					}
					break;
				case EventType.Repaint:
					Styles.titlebarText.Draw(popupPosition, EditorGUITools.popupContent, controlId, false);
					break;
			}
		}

		protected override bool HasOutside()
		{
			return true;
		}

		protected override void OnDrawOutside()
		{
			State state = this.state;

			if (state.breakPoint)
			{
				GUIStyle style = (Application.isPlaying && EditorApplication.isPaused && state.stateMachine.currentState == state) ? Styles.breakpointOn : Styles.breakpoint;
				GUIContent content = new GUIContent(string.Empty, Localization.GetWord("BreakPoint"));
				Vector2 size = style.CalcSize(content);
				Rect breakRect = new Rect(-size.x * 0.5f, -size.y * 0.5f, size.x, size.y);
				breakRect.position += state.position.position;

				EditorGUI.LabelField(breakRect, content, style);
			}

			if (Application.isPlaying)
			{
				GUIStyle style = Styles.countBadge;
				GUIContent content = new GUIContent(state.transitionCount.ToString());
				Vector2 size = style.CalcSize(content);
				Rect countRect = new Rect(state.position.width - size.x * 0.5f, -size.y * 0.5f, size.x, size.y);
				countRect.position += state.position.position;

				EditorGUI.LabelField(countRect, content, style);
			}
		}

		public class BehaviourDragInfo
		{
			public bool dragging = false;
			public int controlID = 0;
			public Object behaviourObj = null;
		}

		static BehaviourDragInfo s_BehaviourDragInfo = null;
		const string k_DragBehaviourGenericKey = "Arbor.BehaviourDragInfo";

		static void BeginDragBehaviour(Object behaviourObj, int controlId)
		{
			DragAndDrop.PrepareStartDrag();

			if (s_BehaviourDragInfo == null)
			{
				s_BehaviourDragInfo = new BehaviourDragInfo();
			}

			s_BehaviourDragInfo.dragging = true;
			s_BehaviourDragInfo.controlID = controlId;
			s_BehaviourDragInfo.behaviourObj = behaviourObj;

			DragAndDrop.SetGenericData(k_DragBehaviourGenericKey, s_BehaviourDragInfo);

			DragAndDrop.objectReferences = new Object[0];
			DragAndDrop.paths = null;
			DragAndDrop.activeControlID = controlId;
			DragAndDrop.StartDrag(ObjectNames.GetDragAndDropTitle(behaviourObj));
		}

		public static BehaviourDragInfo GetBehaviourDragInfo()
		{
			return DragAndDrop.GetGenericData(k_DragBehaviourGenericKey) as BehaviourDragInfo;
		}

		public static int GetDragControlID()
		{
			BehaviourDragInfo behaviourDragInfo = GetBehaviourDragInfo();
			if (behaviourDragInfo == null || !behaviourDragInfo.dragging)
			{
				return 0;
			}

			return behaviourDragInfo.controlID;
		}

		static void MoveBehaviour(State state, int fromIndex, int toIndex)
		{
			ArborFSMInternal stateMachine = state.stateMachine;

			Undo.IncrementCurrentGroup();

			Undo.RecordObject(stateMachine, "MoveDown Behaviour");

			state.MoveBehaviour(fromIndex, toIndex);

			Undo.CollapseUndoOperations(Undo.GetCurrentGroup());

			EditorUtility.SetDirty(stateMachine);
		}

		static int s_DoDragBehaviourHash = "DoDragBehaviour".GetHashCode();

		public static void DropBehaviour(State state, int index, bool top, bool last)
		{
			Rect position = GUILayoutUtility.GetRect(0, last ? 0 : -EditorGUIUtility.standardVerticalSpacing, GUIStyle.none);
			if (last)
			{
				position.yMin = position.yMin - EditorGUIUtility.singleLineHeight;
			}
			else
			{
				position.height = EditorGUIUtility.singleLineHeight;
			}

			int controlId = GUIUtility.GetControlID(s_DoDragBehaviourHash, FocusType.Passive, position);

			bool isBehaviour = false;
			bool draggable = false;
			BehaviourDragInfo behaviourDragInfo = GetBehaviourDragInfo();
			int fromIndex = -1;

			if (behaviourDragInfo == null || behaviourDragInfo.behaviourObj == null)
			{
				foreach (Object draggedObject in DragAndDrop.objectReferences)
				{
					MonoScript script = draggedObject as MonoScript;
					if (script != null)
					{
						System.Type classType = script.GetClass();

						if (classType.IsSubclassOf(typeof(StateBehaviour)))
						{
							isBehaviour = true;
							break;
						}
					}
				}

				draggable = isBehaviour;
			}
			else
			{
				Object draggingStateBehaviour = behaviourDragInfo.behaviourObj;

				int behaviourCount = state.behaviourCount;
				bool contains = false;
				for (int behaviourIndex = 0; behaviourIndex < behaviourCount; behaviourIndex++)
				{
					Object b = state.GetBehaviourObjectFromIndex(behaviourIndex);
					if (b == draggingStateBehaviour)
					{
						contains = true;
						fromIndex = behaviourIndex;
						break;
					}
				}

				if (fromIndex < index)
				{
					index--;
				}

				draggable = fromIndex != index && contains;
			}

			Event current = Event.current;

			Rect dropRect = new Rect(position);
			if (last)
			{
				dropRect.yMin = dropRect.yMax - dropRect.height * 0.5f;
			}
			else if (top)
			{
				dropRect.yMax = dropRect.yMin + dropRect.height * 0.5f;
			}
			else
			{
				dropRect.y -= dropRect.height * 0.5f;
			}

			EventType typeForControl = current.GetTypeForControl(controlId);
			switch (typeForControl)
			{
				case EventType.DragUpdated:
				case EventType.DragPerform:
					bool perform = typeForControl == EventType.DragPerform;
					if (behaviourDragInfo != null)
					{
						behaviourDragInfo.dragging = true;
					}

					if (draggable && dropRect.Contains(current.mousePosition))
					{
						DragAndDrop.visualMode = DragAndDropVisualMode.Move;

						if (perform)
						{
							if (isBehaviour)
							{
								bool findBehaviour = false;

								foreach (Object draggedObject in DragAndDrop.objectReferences)
								{
									MonoScript script = draggedObject as MonoScript;
									if (script != null)
									{
										System.Type classType = script.GetClass();

										if (classType.IsSubclassOf(typeof(StateBehaviour)))
										{
											if (!findBehaviour)
											{
												state.InsertBehaviour(index, classType);
												findBehaviour = true;

												DragAndDrop.AcceptDrag();
												DragAndDrop.activeControlID = 0;

												EditorGUIUtility.ExitGUI();
											}
										}
										else
										{
											Debug.LogError(classType.Name + " is not support State Script.");
										}
									}
								}
							}
							else
							{
								MoveBehaviour(state, fromIndex, index);

								DragAndDrop.AcceptDrag();
								DragAndDrop.activeControlID = 0;

								EditorGUIUtility.ExitGUI();
							}
						}

						current.Use();
					}
					break;
				case EventType.Repaint:
					if (draggable && (isBehaviour || behaviourDragInfo.dragging))
					{
						if (dropRect.Contains(current.mousePosition))
						{
							if (last)
							{
								Styles.insertion.Draw(position, true, true, true, false);
							}
							else
							{
								Styles.insertionAbove.Draw(position, true, true, true, false);
							}
						}
					}
					break;
			}
		}

		protected override void OnGUI()
		{
			using (new ProfilerScope("OnStateGUI"))
			{
				State state = this.state;
				
				using (new ProfilerScope("Behaviours"))
				{
					int behaviourCount = state.behaviourCount;
					if (behaviourCount > 0)
					{
						for (int behaviourIndex = 0; behaviourIndex < behaviourCount; behaviourIndex++)
						{
							DropBehaviour(state, behaviourIndex, behaviourIndex == 0, false);

							Object behaviourObj = state.GetBehaviourObjectFromIndex(behaviourIndex);

							//if( behaviourObj != null )
							{
								StateEditor.BehaviourEditor behaviourEditor = GetBehaviourEditor(behaviourObj);
								if (behaviourEditor != null)
								{
									BehaviourEditor.BehaviourInfo behaviourInfo = behaviourEditor.GetBehaviourInfo();
									GUIContent titleContent = behaviourInfo.titleContent;

									using (new ProfilerScope(titleContent.text))
									{
										behaviourEditor.OnGUI();
									}
								}
							}
						}

						DropBehaviour(state, behaviourCount, false, true);
					}
				}

				Event current = Event.current;
				switch (current.type)
				{
					case EventType.DragUpdated:
					case EventType.DragPerform:
						bool isBehaviour = false;
						foreach (Object draggedObject in DragAndDrop.objectReferences)
						{
							MonoScript script = draggedObject as MonoScript;
							if (script != null)
							{
								System.Type classType = script.GetClass();

								if (classType != null && classType.IsSubclassOf(typeof(StateBehaviour)))
								{
									isBehaviour = true;
									break;
								}
							}
						}

						if (isBehaviour)
						{
							DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

							if (current.type == EventType.DragPerform)
							{
								bool findBehaviour = false;

								foreach (Object draggedObject in DragAndDrop.objectReferences)
								{
									MonoScript script = draggedObject as MonoScript;
									if (script != null)
									{
										System.Type classType = script.GetClass();

										if (classType.IsSubclassOf(typeof(StateBehaviour)))
										{
											if (!findBehaviour)
											{
												state.AddBehaviour(classType);
												findBehaviour = true;

												DragAndDrop.AcceptDrag();
												DragAndDrop.activeControlID = 0;

												current.Use();
											}
										}
										else
										{
											Debug.LogError(classType.Name + " is not support State Script.");
										}
									}
								}
							}
						}
						break;
				}
			}
		}
	}
}
