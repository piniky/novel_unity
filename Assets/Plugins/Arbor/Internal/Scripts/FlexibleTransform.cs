﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 参照方法が複数ある柔軟なTransform型を扱うクラス。
	/// </summary>
#else
	/// <summary>
	/// Class to handle a flexible Transform type reference method there is more than one.
	/// </summary>
#endif
	[System.Serializable]
	public class FlexibleTransform
	{
#if ARBOR_DOC_JA
		/// <summary>
		/// 参照タイプ
		/// </summary>
#else
		/// <summary>
		/// Reference type
		/// </summary>
#endif
		public enum Type
		{
#if ARBOR_DOC_JA
			/// <summary>
			/// 定数
			/// </summary>
#else
			/// <summary>
			/// Constant
			/// </summary>
#endif
			Constant,

#if ARBOR_DOC_JA
			/// <summary>
			/// パラメータ
			/// </summary>
#else
			/// <summary>
			/// Parameter
			/// </summary>
#endif
			Parameter,

#if ARBOR_DOC_JA
			/// <summary>
			/// 演算スロット
			/// </summary>
#else
			/// <summary>
			/// Calculator slot
			/// </summary>
#endif
			Calculator,
		};

		[System.Reflection.Obfuscation( Exclude = true )]
		[SerializeField] private Type _Type = Type.Constant;

		[System.Reflection.Obfuscation( Exclude = true )]
		[SerializeField] private Transform _Value;

		[System.Reflection.Obfuscation( Exclude = true )]
		[SerializeField] private TransformParameterReference _Parameter;

		[System.Reflection.Obfuscation( Exclude = true )]
		[SerializeField] private InputSlotTransform _Slot;

#if ARBOR_DOC_JA
		/// <summary>
		/// Typeを返す
		/// </summary>
#else
		/// <summary>
		/// It returns a type
		/// </summary>
#endif
		public Type type
		{
			get
			{
				return _Type;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Parameterを返す。TypeがParameter以外の場合はnull。
		/// </summary>
#else
		/// <summary>
		/// It return a Paramter. It is null if Type is other than Parameter.
		/// </summary>
#endif
		public Parameter parameter
		{
			get
			{
				if (_Type == Type.Parameter)
				{
					return _Parameter.parameter;
				}
				return null;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 値を返す
		/// </summary>
#else
		/// <summary>
		/// It returns a value
		/// </summary>
#endif
		public Transform value
		{
			get
			{
				Transform value = null;
				switch (_Type)
				{
					case Type.Constant:
						value = _Value;
						break;
					case Type.Parameter:
						if (_Parameter.parameter != null)
						{
							value = _Parameter.parameter.objectReferenceValue as Transform;
						}
						break;
					case Type.Calculator:
						_Slot.GetValue(ref value);
						break;
				}

				return value;
			}
		}

		public FlexibleTransform(Transform value)
		{
			_Type = Type.Constant;
			_Value = value;
		}

		public FlexibleTransform(TransformParameterReference parameter)
		{
			_Type = Type.Parameter;
			_Parameter = parameter;
		}

		public FlexibleTransform(InputSlotTransform slot)
		{
			_Type = Type.Calculator;
			_Slot = slot;
		}

		public static explicit operator Transform(FlexibleTransform af)
		{
			return af.value;
		}

		public static explicit operator FlexibleTransform(Transform value)
		{
			return new FlexibleTransform(value);
		}
	}
}
