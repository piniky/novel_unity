﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Boolパラメータの参照。
	/// </summary>
#else
	/// <summary>
	/// Reference Bool parameters.
	/// </summary>
#endif
	[System.Serializable]
	public class BoolParameterReference : ParameterReference
	{
	}
}
