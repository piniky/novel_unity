﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Arbor Editorにあるノードの基底クラス
	/// </summary>
#else
	/// <summary>
	/// Base class of a node in Arbor Editor
	/// </summary>
#endif
	[System.Serializable]
	public class Node
	{
		[System.Reflection.Obfuscation(Exclude = true)]
		[SerializeField]
		protected ArborFSMInternal _StateMachine;

		[System.Reflection.Obfuscation(Exclude = true)]
		[SerializeField]
		[FormerlySerializedAs("_StateID")]
		[FormerlySerializedAs("_CalculatorID")]
		[FormerlySerializedAs("_CommentID")]
		protected int _NodeID;

#if ARBOR_DOC_JA
		/// <summary>
		/// Arbor Editor上での位置。
		/// </summary>
#else
		/// <summary>
		/// Position on the Arbor Editor.
		/// </summary>
#endif
		public Rect position;

#if ARBOR_DOC_JA
		/// <summary>
		/// コメントを表示するかどうか
		/// </summary>
#else
		/// <summary>
		/// Whether to display comments
		/// </summary>
#endif
		public bool showComment;

#if ARBOR_DOC_JA
		/// <summary>
		/// コメント
		/// </summary>
#else
		/// <summary>
		/// Comment
		/// </summary>
#endif
		public string nodeComment;

#if ARBOR_DOC_JA
		/// <summary>
		/// FSMを取得。
		/// </summary>
#else
		/// <summary>
		/// Gets the state machine.
		/// </summary>
#endif
		public ArborFSMInternal stateMachine
		{
			get
			{
				return _StateMachine;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// ノードIDを取得。
		/// </summary>
#else
		/// <summary>
		/// Gets the node identifier.
		/// </summary>
#endif
		public int nodeID
		{
			get
			{
				return _NodeID;
			}
		}

		public Node(ArborFSMInternal stateMachine, int nodeID)
		{
			_StateMachine = stateMachine;
			_NodeID = nodeID;
		}
	}
}
