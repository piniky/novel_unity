﻿using UnityEngine;
using System.Collections.Generic;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// パラメータコンテナ。
	/// GameObjectにアタッチして使用する。
	/// </summary>
#else
	/// <summary>
	/// ParameterContainer.
	/// Is used by attaching to GameObject.
	/// </summary>
#endif
	[AddComponentMenu("")]
	public class ParameterContainerInternal : ParameterContainerBase
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>Paramerterの一覧。<br/>
		/// <ul>
		/// <li>+ボタンから、パラメータの型を選択して作成。</li>
		/// <li>パラメータを選択し、-ボタンをクリックで削除。</li>
		/// </ul>
		/// </summary>
#else
		/// <summary>List of parameters.<br/>
		/// <ul>
		/// <li>From the + button, select the type of parameter to create.</li>
		/// <li>Select the parameter and delete it by clicking the - button.</li>
		/// </ul>
		/// </summary>
#endif
		[System.Reflection.Obfuscation(Exclude = true)]
		[SerializeField]
		private List<Parameter> _Parameters = new List<Parameter>();

		#endregion // Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// パラメータの配列を取得。
		/// </summary>
#else
		/// <summary>
		/// Get an array of parameters.
		/// </summary>
#endif
		public Parameter[] parameters
		{
			get
			{
				return _Parameters.ToArray();
			}
		}

		private Dictionary<int, Parameter> _DicParameters;

		private Dictionary<int, Parameter> dicParameters
		{
			get
			{
				if (_DicParameters == null)
				{
					_DicParameters = new Dictionary<int, Parameter>();

					int parameterCount = _Parameters.Count;
					for (int parameterIndex = 0; parameterIndex < parameterCount; parameterIndex++)
					{
						Parameter param = _Parameters[parameterIndex];
						_DicParameters.Add(param.id, param);
					}
				}

				return _DicParameters;
			}
		}

		[System.Reflection.Obfuscation(Exclude = true)]
		void OnValidate()
		{
			if (Application.isPlaying)
			{
				int parameterCount = _Parameters.Count;
				for (int parameterIndex = 0; parameterIndex < parameterCount; parameterIndex++)
				{
					Parameter parameter = _Parameters[parameterIndex];
					parameter.OnChanged();
				}
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// パラメータを追加する。
		/// </summary>
		/// <param name="name">名前。nameが重複していた場合はユニークな名前に変更される。</param>
		/// <param name="type">型。</param>
		/// <returns>追加されたパラメータ。</returns>
#else
		/// <summary>
		/// Add a parameter.
		/// </summary>
		/// <param name="name">Name. It is changed to a unique name if the name had been duplicated.</param>
		/// <param name="type">Type.</param>
		/// <returns>It added parameters.</returns>
#endif
		public Parameter AddParam(string name, Parameter.Type type)
		{
			Parameter parameter = new Parameter();
			parameter.container = this;
			parameter.id = MakeUniqueParamID();
			parameter.name = MakeUniqueName(name);
			parameter.type = type;

			_Parameters.Add(parameter);
			dicParameters.Add(parameter.id, parameter);

			return parameter;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 名前からパラメータを取得する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <returns>パラメータ。存在しなかった場合はnullを返す。</returns>
#else
		/// <summary>
		/// Get the parameters from the name.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <returns>Parameters. Return null if you did not exist.</returns>
#endif
		public Parameter GetParam(string name)
		{
			return _Parameters.Find(parameter => parameter.name == name);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 名前からパラメータのIDを取得する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <returns>パラメータのID。存在しなかった場合は0を返す。</returns>
#else
		/// <summary>
		/// Get the parameters ID from the name.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <returns>Parameters ID. Return 0 if you did not exist.</returns>
#endif
		public int GetParamID(string name)
		{
			Parameter parameter = GetParam(name);
			if (parameter != null)
			{
				if (!dicParameters.ContainsKey(parameter.id))
				{
					dicParameters.Add(parameter.id, parameter);
				}
				return parameter.id;
			}

			return 0;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// IDからパラメータを取得する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <returns>パラメータ。存在しなかった場合はnullを返す。</returns>
#else
		/// <summary>
		/// Get the parameters from the ID.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <returns>Parameters. Return null if you did not exist.</returns>
#endif
		public Parameter GetParam(int id)
		{
			Parameter result = null;
			if (dicParameters.TryGetValue(id, out result))
			{
				if (result.id == id)
				{
					return result;
				}
			}

			result = _Parameters.Find(parameter => parameter.id == id);
			if (result != null)
			{
				dicParameters.Add(result.id, result);
				return result;
			}

			return null;
		}

		int MakeUniqueParamID()
		{
			int count = _Parameters.Count;

			System.Random random = new System.Random(count);

			while (true)
			{
				int id = random.Next();

				if (id != 0 && GetParam(id) == null)
				{
					return id;
				}
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// パラメータを削除する。
		/// </summary>
		/// <param name="parameter">パラメータ。</param>
#else
		/// <summary>
		/// Delete a parameter.
		/// </summary>
		/// <param name="parameter">Parameter.</param>
#endif
		public void DeleteParam(Parameter parameter)
		{
			if (parameter != null)
			{
				_Parameters.Remove(parameter);
				dicParameters.Remove(parameter.id);
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 名前を指定してパラメータを削除する。
		/// </summary>
		/// <param name="name">名前。</param>
#else
		/// <summary>
		/// Delete the parameters by name.
		/// </summary>
		/// <param name="name">Name.</param>
#endif
		public void DeleteParam(string name)
		{
			Parameter parameter = GetParam(name);
			DeleteParam(parameter);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// IDを指定してパラメータを削除する。
		/// </summary>
		/// <param name="id">ID。</param>
#else
		/// <summary>
		/// Delete the parameters by ID.
		/// </summary>
		/// <param name="id">ID.</param>
#endif
		public void DeleteParam(int id)
		{
			Parameter parameter = GetParam(id);
			DeleteParam(parameter);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 重複しない名前を生成する。
		/// </summary>
		/// <param name="name">元の名前。</param>
		/// <returns>結果の名前。</returns>
#else
		/// <summary>
		/// It generates a name that does not overlap.
		/// </summary>
		/// <param name="name">The original name.</param>
		/// <returns>Result.</returns>
#endif
		public string MakeUniqueName(string name)
		{
			string searchName = name;
			int count = 0;
			while (true)
			{
				if (GetParam(searchName) == null)
				{
					break;
				}

				searchName = name + " " + count;
				count++;
			}

			return searchName;
		}

		private bool SetFloat(Parameter parameter, float value)
		{
			if (parameter != null && parameter.type == Parameter.Type.Float)
			{
				if (parameter.floatValue != value)
				{
					parameter.floatValue = value;
					parameter.OnChanged();
				}
				return true;
			}

			return false;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Float型の値を設定する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <param name="value">値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// It wants to set the value of the Float type.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool SetFloat(string name, float value)
		{
			Parameter parameter = GetParam(name);
			return SetFloat(parameter, value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Float型の値を設定する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <param name="value">値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// It wants to set the value of the Float type.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool SetFloat(int id, float value)
		{
			Parameter parameter = GetParam(id);
			return SetFloat(parameter, value);
		}

		private bool GetFloat(Parameter parameter, out float value)
		{
			if (parameter == null || parameter.type != Parameter.Type.Float)
			{
				value = 0.0f;
				return false;
			}

			value = parameter.floatValue;

			return true;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Float型の値を取得する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <param name="value">取得する値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// Get the value of the Float type.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="value">The value you get.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool GetFloat(string name, out float value)
		{
			Parameter parameter = GetParam(name);
			return GetFloat(parameter, out value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Float型の値を取得する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <param name="value">取得する値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// Get the value of the Float type.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="value">The value you get.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool GetFloat(int id, out float value)
		{
			Parameter parameter = GetParam(id);
			return GetFloat(parameter, out value);
		}

		private bool SetInt(Parameter parameter, int value)
		{
			if (parameter != null && parameter.type == Parameter.Type.Int)
			{
				if (parameter.intValue != value)
				{
					parameter.intValue = value;
					parameter.OnChanged();
				}
				return true;
			}

			return false;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Int型の値を設定する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <param name="value">値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// It wants to set the value of the Int type.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool SetInt(string name, int value)
		{
			Parameter parameter = GetParam(name);
			return SetInt(parameter, value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Int型の値を設定する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <param name="value">値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// It wants to set the value of the Int type.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool SetInt(int id, int value)
		{
			Parameter parameter = GetParam(id);
			return SetInt(parameter, value);
		}

		private bool GetInt(Parameter parameter, out int value)
		{
			if (parameter == null || parameter.type != Parameter.Type.Int)
			{
				value = 0;
				return false;
			}

			value = parameter.intValue;

			return true;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Int型の値を取得する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <param name="value">取得する値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// Get the value of the Int type.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool GetInt(string name, out int value)
		{
			Parameter parameter = GetParam(name);
			return GetInt(parameter, out value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Int型の値を取得する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <param name="value">取得する値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// Get the value of the Int type.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool GetInt(int id, out int value)
		{
			Parameter parameter = GetParam(id);
			return GetInt(parameter, out value);
		}

		private bool SetLong(Parameter parameter, long value)
		{
			if (parameter != null && parameter.type == Parameter.Type.Long)
			{
				if (parameter.longValue != value)
				{
					parameter.longValue = value;
					parameter.OnChanged();
				}
				return true;
			}

			return false;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Long型の値を設定する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <param name="value">値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// It wants to set the value of the Long type.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool SetLong(string name, long value)
		{
			Parameter parameter = GetParam(name);
			return SetLong(parameter, value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Long型の値を設定する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <param name="value">値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// It wants to set the value of the Long type.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool SetLong(int id, long value)
		{
			Parameter parameter = GetParam(id);
			return SetLong(parameter, value);
		}

		private bool GetLong(Parameter parameter, out long value)
		{
			if (parameter == null || parameter.type != Parameter.Type.Long)
			{
				value = 0;
				return false;
			}

			value = parameter.longValue;

			return true;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Long型の値を取得する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <param name="value">取得する値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// Get the value of the Long type.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool GetLong(string name, out long value)
		{
			Parameter parameter = GetParam(name);
			return GetLong(parameter, out value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Long型の値を取得する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <param name="value">取得する値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// Get the value of the Long type.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool GetLong(int id, out long value)
		{
			Parameter parameter = GetParam(id);
			return GetLong(parameter, out value);
		}

		private bool SetBool(Parameter parameter, bool value)
		{
			if (parameter != null && parameter.type == Parameter.Type.Bool)
			{
				if (parameter.boolValue != value)
				{
					parameter.boolValue = value;
					parameter.OnChanged();
				}
				return true;
			}

			return false;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Bool型の値を設定する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <param name="value">値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// It wants to set the value of the Bool type.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool SetBool(string name, bool value)
		{
			Parameter parameter = GetParam(name);
			return SetBool(parameter, value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Bool型の値を設定する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <param name="value">値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// It wants to set the value of the Bool type.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool SetBool(int id, bool value)
		{
			Parameter parameter = GetParam(id);
			return SetBool(parameter, value);
		}

		private bool GetBool(Parameter parameter, out bool value)
		{
			if (parameter == null || parameter.type != Parameter.Type.Bool)
			{
				value = false;
				return false;
			}

			value = parameter.boolValue;

			return true;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Bool型の値を取得する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <param name="value">取得する値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// Get the value of the Bool type.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool GetBool(string name, out bool value)
		{
			Parameter parameter = GetParam(name);
			return GetBool(parameter, out value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Bool型の値を取得する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <param name="value">取得する値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// Get the value of the Bool type.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool GetBool(int id, out bool value)
		{
			Parameter parameter = GetParam(id);
			return GetBool(parameter, out value);
		}

		private bool SetString(Parameter parameter, string value)
		{
			if (parameter != null && parameter.type == Parameter.Type.String)
			{
				if (parameter.stringValue != value)
				{
					parameter.stringValue = value;
					parameter.OnChanged();
				}
				return true;
			}

			return false;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// String型の値を設定する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <param name="value">値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// It wants to set the value of the String type.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool SetString(string name, string value)
		{
			Parameter parameter = GetParam(name);
			return SetString(parameter, value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// String型の値を設定する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <param name="value">値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// It wants to set the value of the String type.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool SetString(int id, string value)
		{
			Parameter parameter = GetParam(id);
			return SetString(parameter, value);
		}

		private bool GetString(Parameter parameter, out string value)
		{
			if (parameter == null || parameter.type != Parameter.Type.String)
			{
				value = string.Empty;
				return false;
			}

			value = parameter.stringValue;

			return true;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// String型の値を取得する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <param name="value">取得する値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// Get the value of the String type.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool GetString(string name, out string value)
		{
			Parameter parameter = GetParam(name);
			return GetString(parameter, out value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// String型の値を取得する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <param name="value">取得する値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// Get the value of the String type.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool GetString(int id, out string value)
		{
			Parameter parameter = GetParam(id);
			return GetString(parameter, out value);
		}

		private bool SetGameObject(Parameter parameter, GameObject value)
		{
			if (parameter != null && parameter.type == Parameter.Type.GameObject)
			{
				if (parameter.gameObjectValue != value)
				{
					parameter.gameObjectValue = value;
					parameter.OnChanged();
				}
				return true;
			}

			return false;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// GameObject型の値を設定する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <param name="value">値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// It wants to set the value of the GameObject type.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool SetGameObject(string name, GameObject value)
		{
			Parameter parameter = GetParam(name);
			return SetGameObject(parameter, value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// GameObject型の値を設定する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <param name="value">値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// It wants to set the value of the GameObject type.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool SetGameObject(int id, GameObject value)
		{
			Parameter parameter = GetParam(id);
			return SetGameObject(parameter, value);
		}

		private bool GetGameObject(Parameter parameter, out GameObject value)
		{
			if (parameter == null || parameter.type != Parameter.Type.GameObject)
			{
				value = null;
				return false;
			}

			value = parameter.gameObjectValue;

			return true;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// GameObject型の値を取得する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <param name="value">取得する値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// Get the value of the GameObject type.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="value">The value you get.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool GetGameObject(string name, out GameObject value)
		{
			Parameter parameter = GetParam(name);
			return GetGameObject(parameter, out value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// GameObject型の値を取得する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <param name="value">取得する値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// Get the value of the GameObject type.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="value">The value you get.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool GetGameObject(int id, out GameObject value)
		{
			Parameter parameter = GetParam(id);
			return GetGameObject(parameter, out value);
		}

		private bool SetVector2(Parameter parameter, Vector2 value)
		{
			if (parameter != null && parameter.type == Parameter.Type.Vector2)
			{
				if (parameter.vector2Value != value)
				{
					parameter.vector2Value = value;
					parameter.OnChanged();
				}
				return true;
			}

			return false;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Vector2型の値を設定する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <param name="value">値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// It wants to set the value of the Vector2 type.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool SetVector2(string name, Vector2 value)
		{
			Parameter parameter = GetParam(name);
			return SetVector2(parameter, value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Vector2型の値を設定する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <param name="value">値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// It wants to set the value of the Vector2 type.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool SetVector2(int id, Vector2 value)
		{
			Parameter parameter = GetParam(id);
			return SetVector2(parameter, value);
		}

		private bool GetVector2(Parameter parameter, out Vector2 value)
		{
			if (parameter == null || parameter.type != Parameter.Type.Vector2)
			{
				value = Vector2.zero;
				return false;
			}

			value = parameter.vector2Value;

			return true;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Vector2型の値を取得する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <param name="value">取得する値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// Get the value of the Vector2 type.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="value">The value you get.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool GetVector2(string name, out Vector2 value)
		{
			Parameter parameter = GetParam(name);
			return GetVector2(parameter, out value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Vector2型の値を取得する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <param name="value">取得する値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// Get the value of the Vector2 type.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="value">The value you get.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool GetVector2(int id, out Vector2 value)
		{
			Parameter parameter = GetParam(id);
			return GetVector2(parameter, out value);
		}

		private bool SetVector3(Parameter parameter, Vector3 value)
		{
			if (parameter != null && parameter.type == Parameter.Type.Vector3)
			{
				if (parameter.vector3Value != value)
				{
					parameter.vector3Value = value;
					parameter.OnChanged();
				}
				return true;
			}

			return false;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Vector3型の値を設定する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <param name="value">値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// It wants to set the value of the Vector3 type.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool SetVector3(string name, Vector3 value)
		{
			Parameter parameter = GetParam(name);
			return SetVector3(parameter, value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Vector3型の値を設定する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <param name="value">値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// It wants to set the value of the Vector3 type.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool SetVector3(int id, Vector3 value)
		{
			Parameter parameter = GetParam(id);
			return SetVector3(parameter, value);
		}

		private bool GetVector3(Parameter parameter, out Vector3 value)
		{
			if (parameter == null || parameter.type != Parameter.Type.Vector3)
			{
				value = Vector3.zero;
				return false;
			}

			value = parameter.vector3Value;

			return true;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Vector3型の値を取得する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <param name="value">取得する値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// Get the value of the Vector3 type.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="value">The value you get.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool GetVector3(string name, out Vector3 value)
		{
			Parameter parameter = GetParam(name);
			return GetVector3(parameter, out value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Vector3型の値を取得する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <param name="value">取得する値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// Get the value of the Vector3 type.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="value">The value you get.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool GetVector3(int id, out Vector3 value)
		{
			Parameter parameter = GetParam(id);
			return GetVector3(parameter, out value);
		}

		private bool SetQuaternion(Parameter parameter, Quaternion value)
		{
			if (parameter != null && parameter.type == Parameter.Type.Quaternion)
			{
				if (parameter.quaternionValue != value)
				{
					parameter.quaternionValue = value;
					parameter.OnChanged();
				}
				return true;
			}

			return false;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Quaternion型の値を設定する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <param name="value">値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// It wants to set the value of the Quaternion type.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool SetQuaternion(string name, Quaternion value)
		{
			Parameter parameter = GetParam(name);
			return SetQuaternion(parameter, value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Quaternion型の値を設定する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <param name="value">値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// It wants to set the value of the Quaternion type.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool SetQuaternion(int id, Quaternion value)
		{
			Parameter parameter = GetParam(id);
			return SetQuaternion(parameter, value);
		}

		private bool GetQuaternion(Parameter parameter, out Quaternion value)
		{
			if (parameter == null || parameter.type != Parameter.Type.Quaternion)
			{
				value = Quaternion.identity;
				return false;
			}

			value = parameter.quaternionValue;

			return true;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Quaternion型の値を取得する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <param name="value">取得する値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// Get the value of the Quaternion type.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="value">The value you get.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool GetQuaternion(string name, out Quaternion value)
		{
			Parameter parameter = GetParam(name);
			return GetQuaternion(parameter, out value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Quaternion型の値を取得する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <param name="value">取得する値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// Get the value of the Quaternion type.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="value">The value you get.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool GetQuaternion(int id, out Quaternion value)
		{
			Parameter parameter = GetParam(id);
			return GetQuaternion(parameter, out value);
		}

		private bool SetRect(Parameter parameter, Rect value)
		{
			if (parameter != null && parameter.type == Parameter.Type.Rect)
			{
				if (parameter.rectValue != value)
				{
					parameter.rectValue = value;
					parameter.OnChanged();
				}
				return true;
			}

			return false;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Rect型の値を設定する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <param name="value">値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// It wants to set the value of the Rect type.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool SetRect(string name, Rect value)
		{
			Parameter parameter = GetParam(name);
			return SetRect(parameter, value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Rect型の値を設定する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <param name="value">値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// It wants to set the value of the Rect type.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool SetRect(int id, Rect value)
		{
			Parameter parameter = GetParam(id);
			return SetRect(parameter, value);
		}

		private bool GetRect(Parameter parameter, out Rect value)
		{
			if (parameter == null || parameter.type != Parameter.Type.Rect)
			{
				value = new Rect();
				return false;
			}

			value = parameter.rectValue;

			return true;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Rect型の値を取得する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <param name="value">取得する値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// Get the value of the Rect type.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="value">The value you get.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool GetRect(string name, out Rect value)
		{
			Parameter parameter = GetParam(name);
			return GetRect(parameter, out value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Rect型の値を取得する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <param name="value">取得する値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// Get the value of the Rect type.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="value">The value you get.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool GetRect(int id, out Rect value)
		{
			Parameter parameter = GetParam(id);
			return GetRect(parameter, out value);
		}

		private bool SetBounds(Parameter parameter, Bounds value)
		{
			if (parameter != null && parameter.type == Parameter.Type.Bounds)
			{
				if (parameter.boundsValue != value)
				{
					parameter.boundsValue = value;
					parameter.OnChanged();
				}
				return true;
			}

			return false;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Bounds型の値を設定する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <param name="value">値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// It wants to set the value of the Bounds type.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool SetBounds(string name, Bounds value)
		{
			Parameter parameter = GetParam(name);
			return SetBounds(parameter, value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Bounds型の値を設定する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <param name="value">値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// It wants to set the value of the Bounds type.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool SetBounds(int id, Bounds value)
		{
			Parameter parameter = GetParam(id);
			return SetBounds(parameter, value);
		}

		private bool GetBounds(Parameter parameter, out Bounds value)
		{
			if (parameter == null || parameter.type != Parameter.Type.Bounds)
			{
				value = new Bounds();
				return false;
			}

			value = parameter.boundsValue;

			return true;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Bounds型の値を取得する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <param name="value">取得する値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// Get the value of the Bounds type.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="value">The value you get.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool GetBounds(string name, out Bounds value)
		{
			Parameter parameter = GetParam(name);
			return GetBounds(parameter, out value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Bounds型の値を取得する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <param name="value">取得する値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// Get the value of the Bounds type.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="value">The value you get.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool GetBounds(int id, out Bounds value)
		{
			Parameter parameter = GetParam(id);
			return GetBounds(parameter, out value);
		}

		private bool SetTransform(Parameter parameter, Transform value)
		{
			if (parameter != null && parameter.type == Parameter.Type.Transform)
			{
				if (parameter.objectReferenceValue != value)
				{
					parameter.objectReferenceValue = value;
					parameter.OnChanged();
				}
				return true;
			}

			return false;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Transform型の値を設定する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <param name="value">値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// It wants to set the value of the Transform type.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool SetTransform(string name, Transform value)
		{
			Parameter parameter = GetParam(name);
			return SetTransform(parameter, value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Transform型の値を設定する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <param name="value">値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// It wants to set the value of the Transform type.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool SetTransform(int id, Transform value)
		{
			Parameter parameter = GetParam(id);
			return SetTransform(parameter, value);
		}

		private bool GetTransform(Parameter parameter, out Transform value)
		{
			if (parameter == null || parameter.type != Parameter.Type.Transform)
			{
				value = null;
				return false;
			}

			value = parameter.objectReferenceValue as Transform;

			return true;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Transform型の値を取得する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <param name="value">取得する値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// Get the value of the Transform type.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="value">The value you get.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool GetTransform(string name, out Transform value)
		{
			Parameter parameter = GetParam(name);
			return GetTransform(parameter, out value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Transform型の値を取得する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <param name="value">取得する値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// Get the value of the Transform type.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="value">The value you get.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool GetTransform(int id, out Transform value)
		{
			Parameter parameter = GetParam(id);
			return GetTransform(parameter, out value);
		}

		private bool SetRectTransform(Parameter parameter, RectTransform value)
		{
			if (parameter != null && parameter.type == Parameter.Type.RectTransform)
			{
				if (parameter.objectReferenceValue != value)
				{
					parameter.objectReferenceValue = value;
					parameter.OnChanged();
				}
				return true;
			}

			return false;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// RectTransform型の値を設定する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <param name="value">値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// It wants to set the value of the RectTransform type.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool SetRectTransform(string name, RectTransform value)
		{
			Parameter parameter = GetParam(name);
			return SetRectTransform(parameter, value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// RectTransform型の値を設定する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <param name="value">値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// It wants to set the value of the RectTransform type.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool SetRectTransform(int id, RectTransform value)
		{
			Parameter parameter = GetParam(id);
			return SetRectTransform(parameter, value);
		}

		private bool GetRectTransform(Parameter parameter, out RectTransform value)
		{
			if (parameter == null || parameter.type != Parameter.Type.RectTransform)
			{
				value = null;
				return false;
			}

			value = parameter.objectReferenceValue as RectTransform;

			return true;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// RectTransform型の値を取得する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <param name="value">取得する値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// Get the value of the RectTransform type.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="value">The value you get.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool GetRectTransform(string name, out RectTransform value)
		{
			Parameter parameter = GetParam(name);
			return GetRectTransform(parameter, out value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// RectTransform型の値を取得する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <param name="value">取得する値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// Get the value of the RectTransform type.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="value">The value you get.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool GetRectTransform(int id, out RectTransform value)
		{
			Parameter parameter = GetParam(id);
			return GetRectTransform(parameter, out value);
		}

		private bool SetRigidbody(Parameter parameter, Rigidbody value)
		{
			if (parameter != null && parameter.type == Parameter.Type.Rigidbody)
			{
				if (parameter.objectReferenceValue != value)
				{
					parameter.objectReferenceValue = value;
					parameter.OnChanged();
				}
				return true;
			}

			return false;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Rigidbody型の値を設定する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <param name="value">値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// It wants to set the value of the Rigidbody type.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool SetRigidbody(string name, Rigidbody value)
		{
			Parameter parameter = GetParam(name);
			return SetRigidbody(parameter, value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Rigidbody型の値を設定する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <param name="value">値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// It wants to set the value of the Rigidbody type.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool SetRigidbody(int id, Rigidbody value)
		{
			Parameter parameter = GetParam(id);
			return SetRigidbody(parameter, value);
		}

		private bool GetRigidbody(Parameter parameter, out Rigidbody value)
		{
			if (parameter == null || parameter.type != Parameter.Type.Rigidbody)
			{
				value = null;
				return false;
			}

			value = parameter.objectReferenceValue as Rigidbody;

			return true;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Rigidbody型の値を取得する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <param name="value">取得する値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// Get the value of the Rigidbody type.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="value">The value you get.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool GetRigidbody(string name, out Rigidbody value)
		{
			Parameter parameter = GetParam(name);
			return GetRigidbody(parameter, out value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Rigidbody型の値を取得する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <param name="value">取得する値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// Get the value of the Rigidbody type.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="value">The value you get.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool GetRigidbody(int id, out Rigidbody value)
		{
			Parameter parameter = GetParam(id);
			return GetRigidbody(parameter, out value);
		}

		private bool SetRigidbody2D(Parameter parameter, Rigidbody2D value)
		{
			if (parameter != null && parameter.type == Parameter.Type.Rigidbody2D)
			{
				if (parameter.objectReferenceValue != value)
				{
					parameter.objectReferenceValue = value;
					parameter.OnChanged();
				}
				return true;
			}

			return false;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Rigidbody2D型の値を設定する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <param name="value">値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// It wants to set the value of the Rigidbody2D type.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool SetRigidbody2D(string name, Rigidbody2D value)
		{
			Parameter parameter = GetParam(name);
			return SetRigidbody2D(parameter, value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Rigidbody2D型の値を設定する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <param name="value">値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// It wants to set the value of the Rigidbody2D type.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="value">Value.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool SetRigidbody2D(int id, Rigidbody2D value)
		{
			Parameter parameter = GetParam(id);
			return SetRigidbody2D(parameter, value);
		}

		private bool GetRigidbody2D(Parameter parameter, out Rigidbody2D value)
		{
			if (parameter == null || parameter.type != Parameter.Type.Rigidbody2D)
			{
				value = null;
				return false;
			}

			value = parameter.objectReferenceValue as Rigidbody2D;

			return true;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Rigidbody2D型の値を取得する。
		/// </summary>
		/// <param name="name">名前。</param>
		/// <param name="value">取得する値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// Get the value of the Rigidbody2D type.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="value">The value you get.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool GetRigidbody2D(string name, out Rigidbody2D value)
		{
			Parameter parameter = GetParam(name);
			return GetRigidbody2D(parameter, out value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Rigidbody2D型の値を取得する。
		/// </summary>
		/// <param name="id">ID。</param>
		/// <param name="value">取得する値。</param>
		/// <returns>指定した名前のパラメータがあった場合にtrue。</returns>
#else
		/// <summary>
		/// Get the value of the Rigidbody2D type.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="value">The value you get.</param>
		/// <returns>The true when there parameters of the specified name.</returns>
#endif
		public bool GetRigidbody2D(int id, out Rigidbody2D value)
		{
			Parameter parameter = GetParam(id);
			return GetRigidbody2D(parameter, out value);
		}
	}
}
