﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Floatパラメータの参照。
	/// </summary>
#else
	/// <summary>
	/// Reference Float parameters.
	/// </summary>
#endif
	[System.Serializable]
	public class FloatParameterReference : ParameterReference
	{
	}
}