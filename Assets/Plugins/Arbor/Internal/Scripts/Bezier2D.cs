﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 2次元の3次ベジェを扱うクラス
	/// </summary>
#else
	/// <summary>
	/// Class to handle the two-dimensional cubic Bezier
	/// </summary>
#endif
	[System.Serializable]
	public class Bezier2D
	{
		static readonly int _CalculateLengthSplitNum = 16;

		[System.Reflection.Obfuscation( Exclude = true )]
		[SerializeField] private Vector2 _StartPosition;

		[System.Reflection.Obfuscation( Exclude = true )]
		[SerializeField] private Vector2 _StartControl;

		[System.Reflection.Obfuscation( Exclude = true )]
		[SerializeField] private Vector2 _EndPosition;

		[System.Reflection.Obfuscation( Exclude = true )]
		[SerializeField] private Vector2 _EndControl;

		float _Length = 0.0f;
		float[] _SplitLength;
		bool _IsLengthDirty = false;

#if ARBOR_DOC_JA
		/// <summary>
		/// 始点
		/// </summary>
#else
		/// <summary>
		/// Starting point
		/// </summary>
#endif
		public Vector2 startPosition
		{
			get
			{
				return _StartPosition;
			}
			set
			{
				if (_StartPosition != value)
				{
					_StartPosition = value;
					_IsLengthDirty = true;
				}
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 始点のコントロール点
		/// </summary>
#else
		/// <summary>
		/// Control point of the starting point
		/// </summary>
#endif
		public Vector2 startControl
		{
			get
			{
				return _StartControl;
			}
			set
			{
				if (_StartControl != value)
				{
					_StartControl = value;
					_IsLengthDirty = true;
				}
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 終点
		/// </summary>
#else
		/// <summary>
		/// End point
		/// </summary>
#endif
		public Vector2 endPosition
		{
			get
			{
				return _EndPosition;
			}
			set
			{
				if (_EndPosition != value)
				{
					_EndPosition = value;
					_IsLengthDirty = true;
				}
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 終点のコントロール点
		/// </summary>
#else
		/// <summary>
		/// Control point of the end point
		/// </summary>
#endif
		public Vector2 endControl
		{
			get
			{
				return _EndControl;
			}
			set
			{
				if (_EndControl != value)
				{
					_EndControl = value;
					_IsLengthDirty = true;
				}
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 長さ
		/// </summary>
#else
		/// <summary>
		/// Length
		/// </summary>
#endif
		public float length
		{
			get
			{
				CalculateLength();
				return _Length;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 変更されたかどうか
		/// </summary>
#else
		/// <summary>
		/// Whether it has been changed
		/// </summary>
#endif
		public bool isChanged
		{
			get
			{
				return _IsLengthDirty;
			}
		}

		public Bezier2D()
		{
			_StartPosition = Vector2.zero;
			_StartControl = Vector2.zero;
			_EndPosition = Vector2.zero;
			_EndControl = Vector2.zero;
			_IsLengthDirty = true;
		}

		public Bezier2D(Vector2 startPosition, Vector2 startControl, Vector2 endPosition, Vector2 endControl)
		{
			_StartPosition = startPosition;
			_StartControl = startControl;
			_EndPosition = endPosition;
			_EndControl = endControl;
			_IsLengthDirty = true;
		}

		public Bezier2D( Bezier2D bezier )
		{
			_StartPosition = bezier.startPosition;
			_StartControl = bezier.startControl;
			_EndPosition = bezier.endPosition;
			_EndControl = bezier.endControl;
			_IsLengthDirty = true;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// ベジェ曲線上の位置を求める。
		/// </summary>
		/// <param name="t">0から1の値</param>
#else
		/// <summary>
		/// Get a position on the Bezier curve.
		/// </summary>
		/// <param name="t">0-1 of value</param>
#endif
		public Vector2 GetPoint(float t)
		{
			return GetPoint(_StartPosition,_StartControl,_EndPosition,_EndControl,t);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// ベジェ曲線上の位置を求める。
		/// </summary>
		/// <param name="startPosition">始点</param>
		/// <param name="startControl">始点のコントロール点</param>
		/// <param name="endPosition">終点</param>
		/// <param name="endControl">終点のコントロール点</param>
		/// <param name="t">0から1の値</param>
#else
		/// <summary>
		/// Get a position on the Bezier curve.
		/// </summary>
		/// <param name="startPosition">Starting point</param>
		/// <param name="startControl">Control point of the starting point</param>
		/// <param name="endPosition">End point</param>
		/// <param name="endControl">Control point of the end point</param>
		/// <param name="t">0-1 of value</param>
#endif
		public static Vector2 GetPoint(Vector2 startPosition, Vector2 startControl, Vector2 endPosition, Vector2 endControl, float t)
		{
			float t2 = t * t;
			float t3 = t * t * t;
			float t_1 = 1.0f - t;
			float t_12 = t_1 * t_1;
			float t_13 = t_1 * t_1 * t_1;
			return t_13 * startPosition + 3 * t_12 * t * startControl + 3 * t_1 * t2 * endControl + t3 * endPosition;
		}

		void CalculateLength()
		{
			if (_IsLengthDirty || _SplitLength == null || _SplitLength.Length == 0 )
			{
				_Length = 0;
				if (_SplitLength == null || _SplitLength.Length == 0)
				{
					_SplitLength = new float[_CalculateLengthSplitNum + 1];
				}
				_SplitLength[0] = 0.0f;
				Vector2 oldPos = _StartPosition;

				for (int i = 1; i <= _CalculateLengthSplitNum; i++)
				{
					Vector2 pos = GetPoint(i / (float)_CalculateLengthSplitNum);
					_Length += (pos - oldPos).magnitude;
					_SplitLength[i] = _Length;
					oldPos = pos;
				}

				for (int i = 0; i <= _CalculateLengthSplitNum; i++)
				{
					_SplitLength[i] /= _Length;
				}

				_IsLengthDirty = false;

			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// ベジェ曲線上の位置を線形に求める。
		/// </summary>
		/// <param name="t">0から1の値</param>
#else
		/// <summary>
		/// Get a position on the Bezier curve linear.
		/// </summary>
		/// <param name="t">0-1 of value</param>
#endif
		public Vector2 GetLinearPoint(float t)
		{
			CalculateLength();

			int k = 0;
			for (k = 0; k < _CalculateLengthSplitNum; k++)
			{
				if (_SplitLength[k] <= t && t <= _SplitLength[k + 1])
					break;
			}
			if( k < _CalculateLengthSplitNum )
			{
				float x = ( t - _SplitLength[k] ) / ( _SplitLength[k + 1] - _SplitLength[k] );
				x = ( k * ( 1 - x ) + ( 1 + k ) * x ) / (float)_CalculateLengthSplitNum;
				return GetPoint( x );
			}

			return GetPoint( 0 );
		}
	}
}
