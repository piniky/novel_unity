﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 演算ノードを表すクラス
	/// </summary>
#else
	/// <summary>
	/// Class that represents a calculator
	/// </summary>
#endif
	[System.Serializable]
	public sealed class CalculatorNode : Node
	{
		[System.Reflection.Obfuscation(Exclude = true)]
		[SerializeField]
		[FormerlySerializedAs("_Calculator")]
		private Object _Object;

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算ノードIDを取得。
		/// </summary>
#else
		/// <summary>
		/// Gets the calculator node identifier.
		/// </summary>
#endif
		[System.Obsolete("use Node.nodeID")]
		public int calculatorID
		{
			get
			{
				return nodeID;
			}
		}

		public Calculator calculator
		{
			get
			{
				return _Object as Calculator;
			}
		}

		public Object GetObject()
		{
			return _Object;
		}

		public CalculatorNode(ArborFSMInternal stateMachine, int nodeID, System.Type calculatorType) : base(stateMachine, nodeID)
		{
			_Object = Calculator.CreateCalculator(stateMachine, nodeID, calculatorType);
		}

		public Calculator CreateCalculator(System.Type calculatorType)
		{
			_Object = Calculator.CreateCalculator(stateMachine, nodeID, calculatorType);
			return _Object as Calculator;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Editor用。
		/// </summary>
#else
		/// <summary>
		/// For Editor.
		/// </summary>
#endif
		public void Move(ArborFSMInternal stateMachine)
		{
			if (!Application.isEditor || Application.isPlaying)
			{
				throw new System.NotSupportedException();
			}

			_StateMachine = stateMachine;

			Calculator sorceCalculator = _Object as Calculator;
			_Object = null;

			ComponentUtility.MoveCalculator(this, sorceCalculator);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Editor用。
		/// </summary>
#else
		/// <summary>
		/// For Editor.
		/// </summary>
#endif
		public delegate Object InstanceIDToObject(int instanceID);

#if ARBOR_DOC_JA
		/// <summary>
		/// Editor用。
		/// </summary>
#else
		/// <summary>
		/// For Editor.
		/// </summary>
#endif
		public void ForceRebuild(InstanceIDToObject instanceIDToObject)
		{
			if (!Application.isEditor || Application.isPlaying)
			{
				throw new System.NotSupportedException();
			}

			_Object = instanceIDToObject(_Object.GetInstanceID());
		}

		public void SetCalculator(Calculator calculator)
		{
			_Object = calculator;
		}
	}
}
