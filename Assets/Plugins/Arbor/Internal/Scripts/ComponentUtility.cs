﻿/// @cond
using UnityEngine;
using System.Collections;

namespace Arbor
{
	public static class ComponentUtility
	{
		public delegate Component EditorAddComponent(GameObject gameObject, System.Type type);
		public delegate void EditorDestroyObjectImmediate(Object obj);
		public delegate void EditorRecordObject(Object obj, string name);
		public delegate void EditorRecordObjects(Object[] objs, string name);
		public delegate void EditorRegisterCompleteObjectUndo(Object objectToUndo, string name);
		public delegate void EditorSetDirty(Object obj);
		public delegate void EditorMoveBehaviour(State state, StateBehaviour behaviour);
		public delegate void EditorMoveCalculator(CalculatorNode calculatorNode, Calculator calculator);
		public delegate void EditorRefreshStateMachine(ArborFSMInternal stateMachine);
		public delegate void EditorDelaytDestroy(Object obj);

		public static EditorAddComponent editorAddComponent;
		public static EditorDestroyObjectImmediate editorDestroyObjectImmediate;
		public static EditorRecordObject editorRecordObject;
		public static EditorRecordObjects editorRecordObjects;
		public static EditorRegisterCompleteObjectUndo editorRegisterCompleteObjectUndo;
		public static EditorSetDirty editorSetDirty;
		public static EditorMoveBehaviour editorMoveBehaviour;
		public static EditorMoveCalculator editorMoveCalculator;
		public static EditorRefreshStateMachine editorRefreshStateMachine;
		public static EditorDelaytDestroy editorDelayDestroy;

		public static bool enabled = true;

		public static Component AddComponent(GameObject gameObject, System.Type type)
		{
			if (enabled && Application.isEditor && !Application.isPlaying && editorAddComponent != null)
			{
				return editorAddComponent(gameObject, type);
			}
			return gameObject.AddComponent(type);
		}

		public static Type AddComponent<Type>(GameObject gameObject) where Type : Component
		{
			if (enabled && Application.isEditor && !Application.isPlaying && editorAddComponent != null)
			{
				return editorAddComponent(gameObject, typeof(Type)) as Type;
			}
			return gameObject.AddComponent<Type>();
		}

		public static void Destroy(Object obj)
		{
			if (enabled && Application.isEditor && !Application.isPlaying && editorDestroyObjectImmediate != null)
			{
				editorDestroyObjectImmediate(obj);
			}
			else if (Application.isEditor && !Application.isPlaying)
			{
				Object.DestroyImmediate(obj);
			}
			else
			{
				Object.Destroy(obj);
			}
		}

		public static void RecordObject(Object obj, string name)
		{
			if (enabled && Application.isEditor && !Application.isPlaying && editorRecordObject != null)
			{
				editorRecordObject(obj, name);
				return;
			}
		}

		public static void RecordObjects(Object[] objs, string name)
		{
			if (enabled && Application.isEditor && !Application.isPlaying && editorRecordObject != null)
			{
				editorRecordObjects(objs, name);
				return;
			}
		}

		public static void RegisterCompleteObjectUndo(Object objectToUndo, string name)
		{
			if (enabled && Application.isEditor && !Application.isPlaying && editorRegisterCompleteObjectUndo != null)
			{
				editorRegisterCompleteObjectUndo(objectToUndo, name);
				return;
			}
		}

		public static void SetDirty(Object obj)
		{
			if (enabled && Application.isEditor && !Application.isPlaying && editorSetDirty != null)
			{
				editorSetDirty(obj);
				return;
			}
		}

		public static void MoveBehaviour(State state, StateBehaviour behaviour)
		{
			if (enabled && Application.isEditor && !Application.isPlaying && editorMoveBehaviour != null && state != null)
			{
				editorMoveBehaviour(state, behaviour);
				return;
			}

			throw new System.NotSupportedException();
		}

		public static void MoveCalculator(CalculatorNode calculatorNode, Calculator calculator)
		{
			if (enabled && Application.isEditor && !Application.isPlaying && editorMoveBehaviour != null && calculatorNode != null)
			{
				editorMoveCalculator(calculatorNode, calculator);
				return;
			}

			throw new System.NotSupportedException();
		}

		public static void RefreshStateMachine(ArborFSMInternal stateMachine)
		{
			if (enabled && Application.isEditor && editorRefreshStateMachine != null && stateMachine != null)
			{
				editorRefreshStateMachine(stateMachine);
				return;
			}
		}

		public static bool IsValidObject(Object obj)
		{
			return obj != null && (obj is MonoBehaviour || obj is ScriptableObject);
		}

		public static void DelayDestroy(Object obj)
		{
			if (enabled && Application.isEditor && editorDelayDestroy != null)
			{
				editorDelayDestroy(obj);
			}
			else
			{
				Destroy(obj);
			}
		}
	}
}
/// @endcond
