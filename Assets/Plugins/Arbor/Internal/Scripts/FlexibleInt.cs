﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 参照方法が複数ある柔軟なint型を扱うクラス。
	/// </summary>
#else
	/// <summary>
	/// Class to handle a flexible int type reference method there is more than one.
	/// </summary>
#endif

	[System.Serializable]
	public class FlexibleInt
	{
#if ARBOR_DOC_JA
		/// <summary>
		/// 参照タイプ
		/// </summary>
#else
		/// <summary>
		/// Reference type
		/// </summary>
#endif
		public enum Type
		{
#if ARBOR_DOC_JA
			/// <summary>
			/// 定数
			/// </summary>
#else
			/// <summary>
			/// Constant
			/// </summary>
#endif
			Constant,

#if ARBOR_DOC_JA
			/// <summary>
			/// パラメータ
			/// </summary>
#else
			/// <summary>
			/// Parameter
			/// </summary>
#endif
			Parameter,

#if ARBOR_DOC_JA
			/// <summary>
			/// ランダム
			/// </summary>
#else
			/// <summary>
			/// Random
			/// </summary>
#endif
			Random,

#if ARBOR_DOC_JA
			/// <summary>
			/// 演算スロット
			/// </summary>
#else
			/// <summary>
			/// Calculator slot
			/// </summary>
#endif
			Calculator,
		};

		[System.Reflection.Obfuscation( Exclude = true )]
		[SerializeField] private Type _Type = Type.Constant;

		[System.Reflection.Obfuscation( Exclude = true )]
		[SerializeField] private int _Value;

		[System.Reflection.Obfuscation( Exclude = true )]
		[SerializeField] private IntParameterReference _Parameter;

		[System.Reflection.Obfuscation( Exclude = true )]
		[SerializeField] private int _MinRange;

		[System.Reflection.Obfuscation( Exclude = true )]
		[SerializeField] private int _MaxRange;

		[System.Reflection.Obfuscation( Exclude = true )]
		[SerializeField] private InputSlotInt _Slot;

#if ARBOR_DOC_JA
		/// <summary>
		/// Typeを返す
		/// </summary>
#else
		/// <summary>
		/// It returns a type
		/// </summary>
#endif
		public Type type
		{
			get
			{
				return _Type;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Parameterを返す。TypeがParameter以外の場合はnull。
		/// </summary>
#else
		/// <summary>
		/// It return a Paramter. It is null if Type is other than Parameter.
		/// </summary>
#endif
		public Parameter parameter
		{
			get
			{
				if (_Type == Type.Parameter)
				{
					return _Parameter.parameter;
				}
				return null;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 値を返す
		/// </summary>
#else
		/// <summary>
		/// It returns a value
		/// </summary>
#endif
		public int value
		{
			get
			{
				int value = 0;
				switch (_Type)
				{
					case Type.Constant:
						value = _Value;
						break;
					case Type.Parameter:
						if (_Parameter.parameter != null)
						{
							value = _Parameter.parameter.intValue;
						}
						break;
					case Type.Random:
						value = Random.Range(_MinRange, _MaxRange);
						break;
					case Type.Calculator:
						_Slot.GetValue(ref value);
						break;
				}

				return value;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// スロットを返す
		/// </summary>
#else
		/// <summary>
		/// It returns a slot
		/// </summary>
#endif
		public InputSlotInt slot
		{
			get
			{
				return _Slot;
			}
		}

		public FlexibleInt(int value)
		{
			_Type = Type.Constant;
			_Value = value;
		}

		public FlexibleInt(IntParameterReference parameter)
		{
			_Type = Type.Parameter;
			_Parameter = parameter;
		}

		public FlexibleInt(int minRange,int maxRange)
		{
			_Type = Type.Random;
			_MinRange = minRange;
			_MaxRange = maxRange;
		}

		public FlexibleInt(InputSlotInt slot)
		{
			_Type = Type.Calculator;
			_Slot = slot;
		}

		public static explicit operator int (FlexibleInt ai)
		{
			return ai.value;
		}

		public static explicit operator FlexibleInt(int value)
		{
			return new FlexibleInt(value);
		}
	}
}
