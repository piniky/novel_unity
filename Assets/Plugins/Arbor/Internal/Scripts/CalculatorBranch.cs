﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 演算ノード同士を接続する線のクラス。
	/// </summary>
#else
	/// <summary>
	/// Line class that connects the calculator node to each other.
	/// </summary>
#endif
	[System.Serializable]
	public class CalculatorBranch
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// ブランチのID。
		/// </summary>
#else
		/// <summary>
		/// ID of branch.
		/// </summary>
#endif
		public int branchID;

#if ARBOR_DOC_JA
		/// <summary>
		/// 描画するかどうか。エディタ用。
		/// </summary>
#else
		/// <summary>
		/// Whether to draw. For the editor.
		/// </summary>
#endif
		public bool enabled;

#if ARBOR_DOC_JA
		/// <summary>
		/// 入力側のBehaviour
		/// </summary>
#else
		/// <summary>
		/// Input side Behaviour
		/// </summary>
#endif
		public Object inBehaviour;

#if ARBOR_DOC_JA
		/// <summary>
		/// 出力側のBehaviour
		/// </summary>
#else
		/// <summary>
		/// Output side Behaviour
		/// </summary>
#endif
		public Object outBehaviour;

#if ARBOR_DOC_JA
		/// <summary>
		/// 接続する線のベジェ曲線。エディタ用
		/// </summary>
#else
		/// <summary>
		/// Bezier curve of the line to be connected. For editor
		/// </summary>
#endif
		public Bezier2D lineBezier;

		#endregion // Serialize fields

		#region Private fields

		private object _Value;

		private OutputSlot _OutputSlot;
		private System.Reflection.FieldInfo _OutputSlotFieldInfo;

		private InputSlot _InputSlot;
		private System.Reflection.FieldInfo _InputSlotFieldInfo;

		#endregion // Private fields

		#region Properties

#if ARBOR_DOC_JA
		/// <summary>
		/// 値を取得設定する。
		/// Calculatorの出力スロットと接続している場合は必要に応じて値を更新してから取得する。
		/// </summary>
#else
		/// <summary>
		/// get set the value.
		/// When connecting to the output slot of Calculator, update it as necessary and obtain it.
		/// </summary>
#endif
		public object value
		{
			get
			{
				Calculator calculator = outBehaviour as Calculator;
                if (calculator != null )
				{
					calculator.Calculate();
				}
				return _Value;
			}
			set
			{
				isUsed = true;

				if (_Value != value)
				{
					_Value = value;

					updatedTime = Time.unscaledTime;

					Calculator calculator = inBehaviour as Calculator;
					if (calculator != null)
					{
						calculator.SetDirty();
					}
				}
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 現在の値を取得する。
		/// </summary>
#else
		/// <summary>
		/// Get the current value.
		/// </summary>
#endif
		public object currentValue
		{
			get
			{
				return _Value;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 値が使われているかどうかを取得する。
		/// </summary>
#else
		/// <summary>
		/// Gets whether or not a value is used.
		/// </summary>
#endif
		public bool isUsed
		{
			get;
			private set;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// valueを更新した timeScale に依存しない時間。
		/// </summary>
#else
		/// <summary>
		/// Time that does not depend on timeScale when value is updated.
		/// </summary>
#endif
		public float updatedTime
		{
			get;
			private set;
		}

		void FindOutputSlot()
		{
			NodeBehaviour nodeBehaviour = outBehaviour as NodeBehaviour;
			if (nodeBehaviour != null)
			{
				int slotCount = nodeBehaviour.calculatorSlotFieldCount;
				for (int slotIndex = 0; slotIndex < slotCount; slotIndex++)
				{
					CalculatorSlotField slotInfo = nodeBehaviour.GetCalculatorSlotField(slotIndex);
					OutputSlot s = slotInfo.slot as OutputSlot;
					if (s != null && s.branchIDs.Contains(branchID))
					{
						_OutputSlot = s;
						_OutputSlotFieldInfo = slotInfo.fieldInfo;
						return;
					}
				}
			}
		}
		
		void RebuildOutputSlot()
		{
			if ( (_OutputSlot == null || _OutputSlotFieldInfo == null ) && outBehaviour != null )
			{
				_OutputSlot = null;
				_OutputSlotFieldInfo = null;

				NodeBehaviour nodeBehaviour = outBehaviour as NodeBehaviour;
				if (nodeBehaviour != null)
				{
					FindOutputSlot();

					if (_OutputSlot == null || _OutputSlotFieldInfo == null)
					{
						nodeBehaviour.RebuildCalculatorSlotFields();

						FindOutputSlot();
					}
				}
			}
		}

		void FindInputSlot()
		{
			NodeBehaviour nodeBehaviour = inBehaviour as NodeBehaviour;
			if (nodeBehaviour != null)
			{
				int slotCount = nodeBehaviour.calculatorSlotFieldCount;
				for (int slotIndex = 0; slotIndex < slotCount; slotIndex++)
				{
					CalculatorSlotField slotInfo = nodeBehaviour.GetCalculatorSlotField(slotIndex);
					InputSlot s = slotInfo.slot as InputSlot;
					if (s != null && branchID == s.branchID)
					{
						_InputSlot = s;
						_InputSlotFieldInfo = slotInfo.fieldInfo;
						break;
					}
				}
			}
		}

		void RebuildInputSlot()
		{
			if ( (_InputSlot == null || _InputSlotFieldInfo == null ) && inBehaviour != null)
			{
				_InputSlot = null;
				_InputSlotFieldInfo = null;

				NodeBehaviour nodeBehaviour = inBehaviour as NodeBehaviour;
				if (nodeBehaviour != null)
				{
					FindInputSlot();

					if (_InputSlot == null || _InputSlotFieldInfo == null)
					{
						nodeBehaviour.RebuildCalculatorSlotFields();

						FindInputSlot();
					}
				}
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 出力スロットを取得する。
		/// </summary>
#else
		/// <summary>
		/// Get the output slot.
		/// </summary>
#endif
		public OutputSlot outputSlot
		{
			get
			{
				RebuildOutputSlot();

				return _OutputSlot;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 出力スロットのFieldInfoを取得する。
		/// </summary>
#else
		/// <summary>
		/// Get the FieldInfo of the output slot.
		/// </summary>
#endif
		public System.Reflection.FieldInfo outputSlotFieldInfo
		{
			get
			{
				RebuildOutputSlot();

				return _OutputSlotFieldInfo;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 入力スロットを取得する。
		/// </summary>
#else
		/// <summary>
		/// Get the input slot.
		/// </summary>
#endif
		public InputSlot inputSlot
		{
			get
			{
				RebuildInputSlot();

				return _InputSlot;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 入力スロットのFieldInfoを取得する。
		/// </summary>
#else
		/// <summary>
		/// Get the FieldInfo of the input slot.
		/// </summary>
#endif
		public System.Reflection.FieldInfo inputSlotFieldInfo
		{
			get
			{
				RebuildInputSlot();

				return _InputSlotFieldInfo;
			}
		}

		#endregion // Properties

		#region Public methods

#if ARBOR_DOC_JA
		/// <summary>
		/// Behaviourを変更する。
		/// </summary>
#else
		/// <summary>
		/// Change Behavior.
		/// </summary>
#endif
		public void SetBehaviour(Object inBehaviour,Object outBehaviour)
		{
			Refresh();

			this.inBehaviour = inBehaviour;
			this.outBehaviour = outBehaviour;
		}

		public void Refresh()
		{
			_InputSlot = null;
			_InputSlotFieldInfo = null;
			_OutputSlot = null;
			_OutputSlotFieldInfo = null;
		}

#endregion // Public methods
	}
}
