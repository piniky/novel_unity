﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// RectTransformパラメータの参照。
	/// </summary>
#else
	/// <summary>
	/// Reference RectTransform parameters.
	/// </summary>
#endif
	[System.Serializable]
	public class RectTransformParameterReference : ParameterReference
	{
	}
}
