﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Vector3パラメータの参照。
	/// </summary>
#else
	/// <summary>
	/// Reference Vector3 parameters.
	/// </summary>
#endif
	[System.Serializable]
	public class Vector3ParameterReference : ParameterReference
	{
	}
}
