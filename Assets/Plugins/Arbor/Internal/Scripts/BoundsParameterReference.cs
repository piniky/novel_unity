﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Boundsパラメータの参照。
	/// </summary>
#else
	/// <summary>
	/// Reference Bounds parameters.
	/// </summary>
#endif
	[System.Serializable]
	public class BoundsParameterReference : ParameterReference
	{
	}
}
