﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 参照方法が複数ある柔軟なbool型を扱うクラス。
	/// </summary>
#else
	/// <summary>
	/// Class to handle a flexible bool type reference method there is more than one.
	/// </summary>
#endif
	[System.Serializable]
	public class FlexibleBool
	{
#if ARBOR_DOC_JA
		/// <summary>
		/// 参照タイプ
		/// </summary>
#else
		/// <summary>
		/// Reference type
		/// </summary>
#endif
		public enum Type
		{
#if ARBOR_DOC_JA
			/// <summary>
			/// 定数
			/// </summary>
#else
			/// <summary>
			/// Constant
			/// </summary>
#endif
			Constant,

#if ARBOR_DOC_JA
			/// <summary>
			/// パラメータ
			/// </summary>
#else
			/// <summary>
			/// Parameter
			/// </summary>
#endif
			Parameter,

#if ARBOR_DOC_JA
			/// <summary>
			/// ランダム
			/// </summary>
#else
			/// <summary>
			/// Random
			/// </summary>
#endif
			Random,

#if ARBOR_DOC_JA
			/// <summary>
			/// 演算スロット
			/// </summary>
#else
			/// <summary>
			/// Calculator slot
			/// </summary>
#endif
			Calculator,
		};

		[System.Reflection.Obfuscation( Exclude = true )]
		[SerializeField] private Type _Type = Type.Constant;

		[System.Reflection.Obfuscation( Exclude = true )]
		[SerializeField] private bool _Value;

		[System.Reflection.Obfuscation( Exclude = true )]
		[SerializeField] private BoolParameterReference _Parameter;

		[System.Reflection.Obfuscation( Exclude = true )]
		[Range(0.0f,1.0f),SerializeField] private float _Probability;

		[System.Reflection.Obfuscation( Exclude = true )]
		[SerializeField] private InputSlotBool _Slot;

#if ARBOR_DOC_JA
		/// <summary>
		/// Typeを返す
		/// </summary>
#else
		/// <summary>
		/// It returns a type
		/// </summary>
#endif
		public Type type
		{
			get
			{
				return _Type;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Parameterを返す。TypeがParameter以外の場合はnull。
		/// </summary>
#else
		/// <summary>
		/// It return a Paramter. It is null if Type is other than Parameter.
		/// </summary>
#endif
		public Parameter parameter
		{
			get
			{
				if (_Type == Type.Parameter)
				{
					return _Parameter.parameter;
				}
				return null;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 値を返す
		/// </summary>
#else
		/// <summary>
		/// It returns a value
		/// </summary>
#endif
		public bool value
		{
			get
			{
				bool value = false;
				switch (_Type)
				{
					case Type.Constant:
						value = _Value;
						break;
					case Type.Parameter:
						if (_Parameter.parameter != null)
						{
							value= _Parameter.parameter.boolValue;
						}
						break;
					case Type.Random:
						value= Random.Range(0.0f, 1.0f) <= _Probability;
						break;
					case Type.Calculator:
						_Slot.GetValue(ref value);
						break;
				}

				return value;
			}
		}

		public FlexibleBool(bool value)
		{
			_Type = Type.Constant;
			_Value = value;
		}

		public FlexibleBool(BoolParameterReference parameter)
		{
			_Type = Type.Parameter;
			_Parameter = parameter;
		}

		public FlexibleBool(float probability)
		{
			_Type = Type.Random;
			_Probability = probability;
		}

		public FlexibleBool(InputSlotBool slot)
		{
			_Type = Type.Calculator;
			_Slot = slot;
		}

		public static explicit operator bool (FlexibleBool ab)
		{
			return ab.value;
		}

		public static explicit operator FlexibleBool(bool value)
		{
			return new FlexibleBool(value);
		}
	}
}
