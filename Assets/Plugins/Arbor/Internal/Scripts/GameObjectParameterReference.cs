﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// GameObjectパラメータの参照。
	/// </summary>
#else
	/// <summary>
	/// Reference GameObject parameters.
	/// </summary>
#endif
	[System.Serializable]
	public class GameObjectParameterReference : ParameterReference
	{
	}
}
