﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Rigidbodyパラメータの参照。
	/// </summary>
#else
	/// <summary>
	/// Reference Rigidbody parameters.
	/// </summary>
#endif
	[System.Serializable]
	public class RigidbodyParameterReference : ParameterReference
	{
	}
}
