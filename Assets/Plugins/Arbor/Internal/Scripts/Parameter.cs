﻿using UnityEngine;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// ParameterContainerに格納されるParameterのクラス。
	/// </summary>
#else
	/// <summary>
	/// Class of Parameter to be stored in the ParameterContainer.
	/// </summary>
#endif
	[System.Serializable]
	public class Parameter
	{
		public delegate void DelegateOnChanged(Parameter parameter);

#if ARBOR_DOC_JA
		/// <summary>
		/// パラメータの型。
		/// </summary>
#else
		/// <summary>
		/// Parameter type.
		/// </summary>
#endif
		public enum Type
		{
#if ARBOR_DOC_JA
			/// <summary>
			/// Int型。
			/// </summary>
#else
			/// <summary>
			/// Int type.
			/// </summary>
#endif
			Int,

#if ARBOR_DOC_JA
			/// <summary>
			/// Float型。
			/// </summary>
#else
			/// <summary>
			/// Float type.
			/// </summary>
#endif
			Float,

#if ARBOR_DOC_JA
			/// <summary>
			/// Bool型。
			/// </summary>
#else
			/// <summary>
			/// Bool type.
			/// </summary>
#endif
			Bool,

#if ARBOR_DOC_JA
			/// <summary>
			/// GameObject型。
			/// </summary>
#else
			/// <summary>
			/// GameObject type.
			/// </summary>
#endif
			GameObject,

#if ARBOR_DOC_JA
			/// <summary>
			/// String型。
			/// </summary>
#else
			/// <summary>
			/// String type.
			/// </summary>
#endif
			String,

#if ARBOR_DOC_JA
			/// <summary>
			/// Vector2型。
			/// </summary>
#else
			/// <summary>
			/// Vector2 type.
			/// </summary>
#endif
			Vector2 = 1000,

#if ARBOR_DOC_JA
			/// <summary>
			/// Vector3型。
			/// </summary>
#else
			/// <summary>
			/// Vector3 type.
			/// </summary>
#endif
			Vector3,

#if ARBOR_DOC_JA
			/// <summary>
			/// Quaternion型。
			/// </summary>
#else
			/// <summary>
			/// Quaternion type.
			/// </summary>
#endif
			Quaternion,

#if ARBOR_DOC_JA
			/// <summary>
			/// Rect型。
			/// </summary>
#else
			/// <summary>
			/// Rect type.
			/// </summary>
#endif
			Rect,

#if ARBOR_DOC_JA
			/// <summary>
			/// Bounds型。
			/// </summary>
#else
			/// <summary>
			/// Bounds type.
			/// </summary>
#endif
			Bounds,

#if ARBOR_DOC_JA
			/// <summary>
			/// Transform型。
			/// </summary>
#else
			/// <summary>
			/// Transform type.
			/// </summary>
#endif
			Transform = 2000,

#if ARBOR_DOC_JA
			/// <summary>
			/// RectTransform型。
			/// </summary>
#else
			/// <summary>
			/// RectTransform type.
			/// </summary>
#endif
			RectTransform,

#if ARBOR_DOC_JA
			/// <summary>
			/// Rigidbody型。
			/// </summary>
#else
			/// <summary>
			/// Rigidbody type.
			/// </summary>
#endif
			Rigidbody,

#if ARBOR_DOC_JA
			/// <summary>
			/// Rigidbody2D型。
			/// </summary>
#else
			/// <summary>
			/// Rigidbody2D type.
			/// </summary>
#endif
			Rigidbody2D,

#if ARBOR_DOC_JA
			/// <summary>
			/// Component型。
			/// </summary>
#else
			/// <summary>
			/// Component type.
			/// </summary>
#endif
			Component,

#if ARBOR_DOC_JA
			/// <summary>
			/// Long型。
			/// </summary>
#else
			/// <summary>
			/// Long type.
			/// </summary>
#endif
			Long,
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// このパラメータが格納されているコンテナ。
		/// </summary>
#else
		/// <summary>
		/// Container this parameter is stored.
		/// </summary>
#endif
		public ParameterContainerInternal container;

#if ARBOR_DOC_JA
		/// <summary>
		/// ID。
		/// </summary>
#else
		/// <summary>
		/// ID.
		/// </summary>
#endif
		public int id;

#if ARBOR_DOC_JA
		/// <summary>
		/// パラメータの型。
		/// </summary>
#else
		/// <summary>
		/// Type.
		/// </summary>
#endif
		public Type type;

#if ARBOR_DOC_JA
		/// <summary>
		/// パラメータの名前。
		/// </summary>
#else
		/// <summary>
		/// Name.
		/// </summary>
#endif
		public string name;

#if ARBOR_DOC_JA
		/// <summary>
		/// Int型の値。変更した後はOnChanged()を呼び出すこと。
		/// </summary>
#else
		/// <summary>
		/// Int value of type. And invoking the OnChanged () after changing.
		/// </summary>
#endif
		public int intValue;

#if ARBOR_DOC_JA
		/// <summary>
		/// Long型の値。変更した後はOnChanged()を呼び出すこと。
		/// </summary>
#else
		/// <summary>
		/// Long value of type. And invoking the OnChanged () after changing.
		/// </summary>
#endif
		public long longValue;

#if ARBOR_DOC_JA
		/// <summary>
		/// Float型の値。変更した後はOnChanged()を呼び出すこと。
		/// </summary>
#else
		/// <summary>
		/// Float value of type. And invoking the OnChanged () after changing.
		/// </summary>
#endif
		public float floatValue;

#if ARBOR_DOC_JA
		/// <summary>
		/// Bool型の値。変更した後はOnChanged()を呼び出すこと。
		/// </summary>
#else
		/// <summary>
		/// Bool value of type. And invoking the OnChanged () after changing.
		/// </summary>
#endif
		public bool boolValue;

#if ARBOR_DOC_JA
		/// <summary>
		/// String型の値。変更した後はOnChanged()を呼び出すこと。
		/// </summary>
#else
		/// <summary>
		/// String value of type. And invoking the OnChanged () after changing.
		/// </summary>
#endif
		public string stringValue;

#if ARBOR_DOC_JA
		/// <summary>
		/// GameObject型の値。変更した後はOnChanged()を呼び出すこと。
		/// </summary>
#else
		/// <summary>
		/// GameObject value of type. And invoking the OnChanged () after changing.
		/// </summary>
#endif
		public GameObject gameObjectValue;

#if ARBOR_DOC_JA
		/// <summary>
		/// Vector2型の値。変更した後はOnChanged()を呼び出すこと。
		/// </summary>
#else
		/// <summary>
		/// Vector2 value of type. And invoking the OnChanged () after changing.
		/// </summary>
#endif
		public Vector2 vector2Value;

#if ARBOR_DOC_JA
		/// <summary>
		/// Vector3型の値。変更した後はOnChanged()を呼び出すこと。
		/// </summary>
#else
		/// <summary>
		/// Vector3 value of type. And invoking the OnChanged () after changing.
		/// </summary>
#endif
		public Vector3 vector3Value;

#if ARBOR_DOC_JA
		/// <summary>
		/// Quaternion型の値。変更した後はOnChanged()を呼び出すこと。
		/// </summary>
#else
		/// <summary>
		/// Quaternion value of type. And invoking the OnChanged () after changing.
		/// </summary>
#endif
		public Quaternion quaternionValue;

#if ARBOR_DOC_JA
		/// <summary>
		/// Rect型の値。変更した後はOnChanged()を呼び出すこと。
		/// </summary>
#else
		/// <summary>
		/// Rect value of type. And invoking the OnChanged () after changing.
		/// </summary>
#endif
		public Rect rectValue;

#if ARBOR_DOC_JA
		/// <summary>
		/// Bounds型の値。変更した後はOnChanged()を呼び出すこと。
		/// </summary>
#else
		/// <summary>
		/// Bounds value of type. And invoking the OnChanged () after changing.
		/// </summary>
#endif
		public Bounds boundsValue;

#if ARBOR_DOC_JA
		/// <summary>
		/// Object型の値。変更した後はOnChanged()を呼び出すこと。
		/// </summary>
#else
		/// <summary>
		/// Object value of type. And invoking the OnChanged () after changing.
		/// </summary>
#endif
		public Object objectReferenceValue;

		public object value
		{
			get
			{
				switch (type)
				{
					case Type.Int:
						return intValue;
					case Type.Long:
						return longValue;
					case Type.Float:
						return floatValue;
					case Type.Bool:
						return boolValue;
					case Type.GameObject:
						return gameObjectValue;
					case Type.String:
						return stringValue;
					case Type.Vector2:
						return vector2Value;
					case Type.Vector3:
						return vector3Value;
					case Type.Quaternion:
						return quaternionValue;
					case Type.Rect:
						return rectValue;
					case Type.Bounds:
						return boundsValue;
					case Type.Transform:
					case Type.RectTransform:
					case Type.Rigidbody:
					case Type.Rigidbody2D:
					case Type.Component:
						return objectReferenceValue;
				}

				return null;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 値が変更された際に呼び出されるコールバック関数。
		/// </summary>
#else
		/// <summary>
		/// Callback function to be called when the value is changed.
		/// </summary>
#endif
		public event DelegateOnChanged onChanged;

#if ARBOR_DOC_JA
		/// <summary>
		/// 値を変更した際に呼び出す。
		/// </summary>
#else
		/// <summary>
		/// Call when you change the value.
		/// </summary>
#endif
		public void OnChanged()
		{
			if (onChanged != null)
			{
				onChanged(this);
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 値を文字列形式に変換する。
		/// </summary>
		/// <returns>変換した文字列</returns>
#else
		/// <summary>
		/// Convert value to string format.
		/// </summary>
		/// <returns>Converted string</returns>
#endif
		public override string ToString()
		{
			return System.Convert.ToString(value);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 値を文字列形式に変換する。
		/// </summary>
		/// <param name="format">数値書式指定フォーマット(Int,Long,Floatのみ)</param>
		/// <returns>変換した文字列</returns>
		/// <remarks>数値書式指定フォーマットの詳細については、次を参照してください。<a href="https://msdn.microsoft.com/ja-jp/library/dwhawy9k(v=vs.110).aspx">標準の数値書式指定文字列</a>、<a href="https://msdn.microsoft.com/ja-jp/library/0c899ak8(v=vs.110).aspx">カスタム数値書式指定文字列</a></remarks>
#else
		/// <summary>
		/// Convert value to string format.
		/// </summary>
		/// <param name="format">Numeric format string (Int, Long, Float only)</param>
		/// <returns>Converted string</returns>
		/// <remarks>For more information about numeric format specifiers, see <a href="https://msdn.microsoft.com/en-us/library/dwhawy9k(v=vs.110).aspx">Standard Numeric Format Strings</a> and <a href="https://msdn.microsoft.com/en-us/library/0c899ak8(v=vs.110).aspx">Custom Numeric Format Strings</a>.</remarks>
#endif
		public string ToString(string format)
		{
			string s = string.Empty;

			switch (type)
			{
				case Parameter.Type.Int:
					s = intValue.ToString(format);
					break;
				case Parameter.Type.Long:
					s = longValue.ToString(format);
					break;
				case Parameter.Type.Float:
					s = floatValue.ToString(format);
					break;
				case Parameter.Type.String:
					s = stringValue;
					break;
				default:
					s = System.Convert.ToString(value);
					break;
			}

			return s;
		}
	}
}
