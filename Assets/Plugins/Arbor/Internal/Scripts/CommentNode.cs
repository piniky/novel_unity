﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// コメントを表すクラス
	/// </summary>
#else
	/// <summary>
	/// Class that represents a comment
	/// </summary>
#endif
	[System.Serializable]
	public sealed class CommentNode : Node
	{
#if ARBOR_DOC_JA
		/// <summary>
		/// コメントIDを取得。
		/// </summary>
#else
		/// <summary>
		/// Gets the comment identifier.
		/// </summary>
#endif
		[System.Obsolete("use Node.nodeID")]
		public int commentID
		{
			get
			{
				return nodeID;
			}
		}

		public string comment = string.Empty;

		public CommentNode(ArborFSMInternal stateMachine, int nodeID) : base(stateMachine,nodeID)
		{
        }

#if ARBOR_DOC_JA
		/// <summary>
		/// Editor用。
		/// </summary>
#else
		/// <summary>
		/// For Editor.
		/// </summary>
#endif
		public void Move(ArborFSMInternal stateMachine)
		{
			if (!Application.isEditor || Application.isPlaying)
			{
				throw new System.NotSupportedException();
			}

			_StateMachine = stateMachine;
		}
	}
}
