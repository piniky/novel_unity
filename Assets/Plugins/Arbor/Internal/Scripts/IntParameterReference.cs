﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Intパラメータの参照。
	/// </summary>
#else
	/// <summary>
	/// Reference Int parameters.
	/// </summary>
#endif
	[System.Serializable]
	public class IntParameterReference : ParameterReference
	{
	}
}