﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// シリアライズ可能な各フィールドを見つける
	/// </summary>
	/// <typeparam name="T">見つける型</typeparam>
#else
	/// <summary>
	/// Find each serializable field
	/// </summary>
	/// <typeparam name="T">Type to find</typeparam>
#endif
	public class EachField<T>
	{
		public delegate void OnFind(T data);
		public delegate void OnFindEx(T data, System.Reflection.FieldInfo fieldInfo);

		class FindFieldBase
		{
			public virtual void OnFindField(T obj, System.Reflection.FieldInfo fieldInfo)
			{
			}

			void FindField(object obj, System.Reflection.FieldInfo fieldInfo)
			{
				if (obj == null)
				{
					return;
				}

				System.Type type = obj.GetType();

				if (type == typeof(T) || type.IsSubclassOf(typeof(T)))
				{
					OnFindField((T)obj, fieldInfo);
				}
				else if (typeof(IList).IsAssignableFrom(type))
				{
					IList list = (IList)obj;
					if (list != null)
					{
						int itemCount = list.Count;
						for (int itemIndex = 0; itemIndex < itemCount; itemIndex++)
						{
							var item = list[itemIndex];
							FindField(item, fieldInfo);
						}
					}
				}
				else if (AttributeHelper.HasAttribute<System.SerializableAttribute>(type))
				{
					Find(obj, type);
				}
			}

			public void Find(object obj, System.Type type)
			{
				for (System.Type type1 = type; type1 != null; type1 = type1.BaseType)
				{
					System.Reflection.FieldInfo[] fields = type1.GetFields(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Public);

					foreach (System.Reflection.FieldInfo field in fields)
					{
						bool serializeField = false;

						if (field.IsPublic)
						{
							serializeField = true;
						}
						else
						{
							serializeField = AttributeHelper.HasAttribute<SerializeField>(field);
						}

						if (serializeField)
						{
							FindField(field.GetValue(obj), field);
						}
					}
				}
			}
		}

		class FindField : FindFieldBase
		{
			public OnFind onFind;

			public override void OnFindField(T obj, System.Reflection.FieldInfo fieldInfo)
			{
				onFind(obj);
			}
		}

		class FindFieldEx : FindFieldBase
		{
			public OnFindEx onFind;

			public override void OnFindField(T obj, System.Reflection.FieldInfo fieldInfo)
			{
				onFind(obj,fieldInfo);
			}
		}

		static FindField s_Find = new FindField();
		static FindFieldEx s_FindEx = new FindFieldEx();

#if ARBOR_DOC_JA
		/// <summary>
		/// フィールドを見つける。
		/// </summary>
		/// <param name="obj">オブジェクト</param>
		/// <param name="type">オブジェクトの型</param>
		/// <param name="onFind">見つけた場合のコールバック</param>
#else
		/// <summary>
		/// Find the field.
		/// </summary>
		/// <param name="obj">Object</param>
		/// <param name="type">Type of object</param>
		/// <param name="onFind">Callback If you find</param>
#endif
		public static void Find(object obj, System.Type type, OnFind onFind)
		{
			s_Find.onFind = onFind;
			s_Find.Find(obj, type);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// フィールドを見つける。
		/// </summary>
		/// <param name="obj">オブジェクト</param>
		/// <param name="type">オブジェクトの型</param>
		/// <param name="onFind">見つけた場合のコールバック</param>
#else
		/// <summary>
		/// Find the field.
		/// </summary>
		/// <param name="obj">Object</param>
		/// <param name="type">Type of object</param>
		/// <param name="onFind">Callback If you find</param>
#endif
		public static void Find(object obj, System.Type type, OnFindEx onFind)
		{
			s_FindEx.onFind = onFind;
			s_FindEx.Find(obj, type);
		}
	}
}
