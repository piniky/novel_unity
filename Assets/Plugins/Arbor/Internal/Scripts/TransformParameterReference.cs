﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Transformパラメータの参照。
	/// </summary>
#else
	/// <summary>
	/// Reference Transform parameters.
	/// </summary>
#endif
	[System.Serializable]
	public class TransformParameterReference : ParameterReference
	{
	}
}
