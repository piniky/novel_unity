﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Rectパラメータの参照。
	/// </summary>
#else
	/// <summary>
	/// Reference Rect parameters.
	/// </summary>
#endif
	[System.Serializable]
	public class RectParameterReference : ParameterReference
	{
	}
}
