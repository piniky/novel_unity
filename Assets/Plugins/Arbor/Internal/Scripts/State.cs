﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections;
using System.Collections.Generic;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// ステートを表すクラス
	/// </summary>
#else
	/// <summary>
	/// Class that represents the state
	/// </summary>
#endif
	[System.Serializable]
	public sealed class State : Node
	{
		[System.Reflection.Obfuscation(Exclude = true)]
		[SerializeField]
		private bool _Resident = false;

		[System.Reflection.Obfuscation(Exclude = true)]
		[SerializeField]
		private List<Object> _Behaviours = new List<Object>();

		[System.Reflection.Obfuscation(Exclude = true)]
		[SerializeField]
		private bool _BreakPoint;

#if ARBOR_DOC_JA
		/// <summary>
		/// ステートの名前。
		/// </summary>
#else
		/// <summary>
		/// The name of the state.
		/// </summary>
#endif
		public string name = "New State";

		[System.Obsolete("use behaviourCount and GetBehaviourFromIndex()")]
		public StateBehaviour[] behaviours
		{
			get
			{
				ArrayList array = new ArrayList(_Behaviours);
				return (StateBehaviour[])array.ToArray(typeof(StateBehaviour));
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Behaviourの数を取得。
		/// </summary>
#else
		/// <summary>
		/// Get a count of Behavior.
		/// </summary>
#endif
		public int behaviourCount
		{
			get
			{
				return _Behaviours.Count;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// ステートIDを取得。
		/// </summary>
#else
		/// <summary>
		/// Gets the state identifier.
		/// </summary>
#endif
		[System.Obsolete("use Node.nodeID")]
		public int stateID
		{
			get
			{
				return nodeID;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 常駐する<see cref="State"/>かどうかを取得。
		/// </summary>
#else
		/// <summary>
		/// Gets a value indicating whether this <see cref="State"/> is resident.
		/// </summary>
#endif
		public bool resident
		{
			get
			{
				return _Resident;
			}
		}

		public bool breakPoint
		{
			get
			{
				return _BreakPoint;
			}
			set
			{
				_BreakPoint = value;
			}
		}

		public int transitionCount
		{
			get;
			set;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// ステートの生成は<see cref="ArborFSMInternal.CreateState"/>を使用してください。
		/// </summary>
#else
		/// <summary>
		/// Please use the <see cref = "ArborFSMInternal.CreateState" /> state creating.
		/// </summary>
#endif
		public State(ArborFSMInternal stateMachine, int nodeID, bool resident) : base(stateMachine, nodeID)
		{
			_Resident = resident;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// StateBehaviourを追加。
		/// </summary>
		/// <param name="behaviour">追加するStateBehaviour</param>
#else
		/// <summary>
		/// Adds the behaviour.
		/// </summary>
		/// <param name="behaviour">Add StateBehaviour</param>
#endif
		public void AddBehaviour(StateBehaviour behaviour)
		{
			ComponentUtility.RecordObject(stateMachine, "Add Behaviour");

			_Behaviours.Add(behaviour);

			ComponentUtility.SetDirty(stateMachine);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// StateBehaviourを追加。
		/// </summary>
		/// <param name="type">追加するStateBehaviourの型</param>
		/// <returns>追加したStateBehaviour</returns>
#else
		/// <summary>
		/// Adds the behaviour.
		/// </summary>
		/// <param name="type">Type of add StateBehaviour</param>
		/// <returns>Added StateBehaviour</returns>
#endif
		public StateBehaviour AddBehaviour(System.Type type)
		{
			StateBehaviour behaviour = StateBehaviour.CreateStateBehaviour(_StateMachine, nodeID, type);

			ComponentUtility.RecordObject(stateMachine, "Add Behaviour");

			_Behaviours.Add(behaviour);

			ComponentUtility.SetDirty(stateMachine);

			return behaviour;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// StateBehaviourを追加。
		/// </summary>
		/// <typeparam name="T">追加するStateBehaviourの型</typeparam>
		/// <returns>追加したStateBehaviour</returns>
#else
		/// <summary>
		/// Adds the behaviour.
		/// </summary>
		/// <typeparam name="T">Type of add StateBehaviour</typeparam>
		/// <returns>Added StateBehaviour</returns>
#endif
		public T AddBehaviour<T>() where T : StateBehaviour
		{
			T behaviour = StateBehaviour.CreateStateBehaviour<T>(_StateMachine, nodeID);

			ComponentUtility.RecordObject(stateMachine, "Add Behaviour");

			_Behaviours.Add(behaviour);

			ComponentUtility.SetDirty(stateMachine);

			return behaviour;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// StateBehaviourを挿入。
		/// </summary>
		/// <param name="index">挿入先インデックス</param>
		/// <param name="type">追加するStateBehaviourの型</param>
		/// <returns>挿入したStateBehaviour</returns>
#else
		/// <summary>
		/// Insert the behaviour.
		/// </summary>
		/// <param name="index">Insertion destination index</param>
		/// <param name="type">Type of add StateBehaviour</param>
		/// <returns>Inserted StateBehaviour</returns>
#endif
		public StateBehaviour InsertBehaviour(int index, System.Type type)
		{
			StateBehaviour behaviour = StateBehaviour.CreateStateBehaviour(_StateMachine, nodeID, type);

			ComponentUtility.RecordObject(stateMachine, "Insert Behaviour");

			_Behaviours.Insert(index, behaviour);

			ComponentUtility.SetDirty(stateMachine);

			return behaviour;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// StateBehaviourを追加。
		/// </summary>
		/// <typeparam name="T">挿入するStateBehaviourの型</typeparam>
		/// <param name="index">挿入先インデックス</param>
		/// <returns>挿入したStateBehaviour</returns>
#else
		/// <summary>
		/// Adds the behaviour.
		/// </summary>
		/// <typeparam name="T">Type of insert StateBehaviour</typeparam>
		/// /// <param name="index">Insertion destination index</param>
		/// <returns>Inserted StateBehaviour</returns>
#endif
		public T InsertBehaviour<T>(int index, System.Type type) where T : StateBehaviour
		{
			T behaviour = StateBehaviour.CreateStateBehaviour<T>(_StateMachine, nodeID);

			ComponentUtility.RecordObject(stateMachine, "Insert Behaviour");

			_Behaviours.Insert(index, behaviour);

			ComponentUtility.SetDirty(stateMachine);

			return behaviour;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// StateBehaviourをindexから取得。
		/// </summary>
		/// <param name="index">インデックス</param>
		/// <returns>StateBehaviour</returns>
#else
		/// <summary>
		/// Get StateBehavior from index.
		/// </summary>
		/// <param name="index">Index</param>
		/// <returns>StateBehaviour</returns>
#endif
		public StateBehaviour GetBehaviourFromIndex(int index)
		{
			return _Behaviours[index] as StateBehaviour;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// StateBehaviourのObjectをindexから取得。
		/// </summary>
		/// <param name="index">インデックス</param>
		/// <returns>Object</returns>
#else
		/// <summary>
		/// Get Object of StateBehavior from index.
		/// </summary>
		/// <param name="index">Index</param>
		/// <returns>Object</returns>
#endif
		public Object GetBehaviourObjectFromIndex(int index)
		{
			return _Behaviours[index];
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// StateBehaviourを取得。
		/// </summary>
		/// <param name="type">取得したいStateBehaviourの型。</param>
		/// <returns>見つかったStateBehaviour。ない場合はnull。</returns>
#else
		/// <summary>
		/// Gets the behaviour.
		/// </summary>
		/// <param name="type">Type of you want to get StateBehaviour.</param>
		/// <returns>Found StateBehaviour. Or null if it is not.</returns>
#endif
		public StateBehaviour GetBehaviour(System.Type type)
		{
			return _Behaviours.Find(behaviour =>
			{
				System.Type classType = behaviour.GetType();
				return classType == type || classType.IsSubclassOf(type);
			}) as StateBehaviour;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// StateBehaviourを取得。
		/// </summary>
		/// <typeparam name="T">取得したいStateBehaviourの型。</typeparam>
		/// <returns>見つかったStateBehaviour。ない場合はnull。</returns>
#else
		/// <summary>
		/// Gets the behaviour.
		/// </summary>
		/// <typeparam name="T">Type of you want to get StateBehaviour.</typeparam>
		/// <returns>Found StateBehaviour. Or null if it is not.</returns>
#endif
		public T GetBehaviour<T>() where T : StateBehaviour
		{
			System.Type type = typeof(T);

			return _Behaviours.Find(behaviour =>
			{
				System.Type classType = behaviour.GetType();
				return classType == type || classType.IsSubclassOf(type);
			}) as T;
		}

		private System.Array GetBehavioursInternal(System.Type type, bool useSearchTypeAsArrayReturnType)
		{
			ArrayList array = new ArrayList();

			foreach (Object behaviour in _Behaviours)
			{
				System.Type classType = behaviour.GetType();

				if (type.IsAssignableFrom(classType))
				{
					array.Add(behaviour as StateBehaviour);
				}
			}

			if (useSearchTypeAsArrayReturnType)
			{
				return array.ToArray(type);
			}
			else
			{
				return array.ToArray();
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// StateBehaviourを取得。
		/// </summary>
		/// <param name="type">取得したいStateBehaviourの型。</param>
		/// <returns>見つかったStateBehaviourの配列。</returns>
#else
		/// <summary>
		/// Gets the behaviours.
		/// </summary>
		/// <param name="type">Type of you want to get StateBehaviour.</param>
		/// <returns>Array of found StateBehaviour.</returns>
#endif
		public StateBehaviour[] GetBehaviours(System.Type type)
		{
			return (StateBehaviour[])GetBehavioursInternal(type, false);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// StateBehaviourを取得。
		/// </summary>
		/// <typeparam name="T">取得したいStateBehaviourの型。</typeparam>
		/// <returns>見つかったStateBehaviourの配列。</returns>
#else
		/// <summary>
		/// Gets the behaviours.
		/// </summary>
		/// <typeparam name="T">Type of you want to get StateBehaviour.</typeparam>
		/// <returns>Array of found StateBehaviour.</returns>
#endif
		public T[] GetBehaviours<T>() where T : StateBehaviour
		{
			return (T[])GetBehavioursInternal(typeof(T), true);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// StateBehaviourが含まれているかどうか。
		/// </summary>
		/// <param name="behaviour">判定するStateBehaviour。</param>
		/// <returns>含まれているかどうか。</returns>
#else
		/// <summary>
		/// Whether StateBehaviour are included.
		/// </summary>
		/// <param name="behaviour">Judges StateBehaviour.</param>
		/// <returns>Whether included are.</returns>
#endif
		public bool Contains(StateBehaviour behaviour)
		{
			return _Behaviours.Contains(behaviour);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// StateBehaviourを削除する。インスタンスは削除されないため、<see cref="StateBehaviour.Destroy" />を使用すること。
		/// </summary>
		/// <param name="behaviourObj">削除するStateBehaviour。</param>
#else
		/// <summary>
		/// I want to remove the StateBehaviour. For instance is not deleted, that you use the <see cref = "StateBehaviour.Destroy" />.
		/// </summary>
		/// <param name="behaviourObj">StateBehaviour you want to remove.</param>
#endif
		public void RemoveBehaviour(Object behaviourObj)
		{
			int index = _Behaviours.IndexOf(behaviourObj);
			if (index >= 0)
			{
				ComponentUtility.RecordObject(stateMachine, "Remove Behaviour");
				_Behaviours.RemoveAt(index);
				ComponentUtility.SetDirty(stateMachine);
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// StateBehaviourの順番を入れ替える。
		/// </summary>
		/// <param name="fromIndex">入れ替えたいインデックス。</param>
		/// <param name="toIndex">入れ替え先インデックス。</param>
#else
		/// <summary>
		/// Swap the order of StateBehaviour.
		/// </summary>
		/// <param name="fromIndex">The swapping want index.</param>
		/// <param name="toIndex">Exchange destination index.</param>
#endif
		public void SwapBehaviour(int fromIndex, int toIndex)
		{
			Object behaviour = _Behaviours[toIndex];
			_Behaviours[toIndex] = _Behaviours[fromIndex];
			_Behaviours[fromIndex] = behaviour;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// StateBehaviourの順番を移動する。
		/// </summary>
		/// <param name="fromIndex">移動させたいインデックス。</param>
		/// <param name="toIndex">移動先のインデックス。</param>
#else
		/// <summary>
		/// Move the order of StateBehaviour.
		/// </summary>
		/// <param name="fromIndex">The moving want index.</param>
		/// <param name="toIndex">The destination index.</param>
#endif
		public void MoveBehaviour(int fromIndex, int toIndex)
		{
			Object behaviour = _Behaviours[fromIndex];
			_Behaviours.RemoveAt(fromIndex);
			_Behaviours.Insert(toIndex, behaviour);
		}

		public void SetBehaviour(int index, StateBehaviour behaviour)
		{
			_Behaviours[index] = behaviour;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Editor用。
		/// </summary>
#else
		/// <summary>
		/// For Editor.
		/// </summary>
#endif
		public delegate Object InstanceIDToObject(int instanceID);

#if ARBOR_DOC_JA
		/// <summary>
		/// Editor用。
		/// </summary>
#else
		/// <summary>
		/// For Editor.
		/// </summary>
#endif
		public void ForceRebuild(InstanceIDToObject instanceIDToObject)
		{
			if (!Application.isEditor || Application.isPlaying)
			{
				throw new System.NotSupportedException();
			}

			for (int i = 0; i < _Behaviours.Count; i++)
			{
				_Behaviours[i] = instanceIDToObject(_Behaviours[i].GetInstanceID());
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Editor用。
		/// </summary>
#else
		/// <summary>
		/// For Editor.
		/// </summary>
#endif
		public void Move(ArborFSMInternal stateMachine)
		{
			if (!Application.isEditor || Application.isPlaying)
			{
				throw new System.NotSupportedException();
			}

			_StateMachine = stateMachine;

			Object[] sourceBehaviours = _Behaviours.ToArray();
			_Behaviours.Clear();

			foreach (Object obj in sourceBehaviours)
			{
				StateBehaviour sourceBehaviour = obj as StateBehaviour;
				if (sourceBehaviour != null)
				{
					ComponentUtility.MoveBehaviour(this, sourceBehaviour);
				}
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 内部処理用。
		/// </summary>
#else
		/// <summary>
		/// For internal.
		/// </summary>
#endif
		public void DisconnectState(int stateID)
		{
			int behaviourCount = _Behaviours.Count;
			for (int behaviourIndex = 0; behaviourIndex < behaviourCount; behaviourIndex++)
			{
				StateBehaviour behaviour = _Behaviours[behaviourIndex] as StateBehaviour;
				if (behaviour != null)
				{
					System.Type type = behaviour.GetType();

					ComponentUtility.RecordObject(behaviour, "Delete Nodes");

					EachField<StateLink>.Find(behaviour, type, (StateLink stateLink) =>
					{
						if (stateLink.stateID == stateID)
						{
							stateLink.stateID = 0;
						}
					});
				}
			}
		}

		public void DestroyBehaviour(Object behaviourObj)
		{
			stateMachine.DisconnectCalculatorBranch(behaviourObj);
			RemoveBehaviour(behaviourObj);

			ComponentUtility.Destroy(behaviourObj);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 内部処理用。
		/// </summary>
#else
		/// <summary>
		/// For internal.
		/// </summary>
#endif
		public void DestroyBehaviours()
		{
			int behaviourCount = _Behaviours.Count;
			for (int behaviourIndex = behaviourCount-1; behaviourIndex >= 0; behaviourIndex--)
			{
				Object behaviourObj = _Behaviours[behaviourIndex];
				
				DestroyBehaviour(behaviourObj);
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 内部処理用。
		/// </summary>
#else
		/// <summary>
		/// For internal.
		/// </summary>
#endif
		public void DestroySubComponents()
		{
			int behaviourCount = _Behaviours.Count;
			for (int behaviourIndex = 0; behaviourIndex < behaviourCount; behaviourIndex++)
			{
				StateBehaviour behaviour = _Behaviours[behaviourIndex] as StateBehaviour;
				if (behaviour == null)
				{
					continue;
				}

				if (Application.isPlaying)
				{
					Object.Destroy(behaviour);
				}
				else
				{
					Object.DestroyImmediate(behaviour);
				}
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 内部処理用。
		/// </summary>
#else
		/// <summary>
		/// For internal.
		/// </summary>
#endif
		public void SetHideFlags(HideFlags hideFlags)
		{
#if !ARBOR_DEBUG
			int behaviourCount = _Behaviours.Count;
			for (int behaviourIndex = 0; behaviourIndex < behaviourCount; behaviourIndex++)
			{
				Object behaviourObj = _Behaviours[behaviourIndex];

				if (ComponentUtility.IsValidObject(behaviourObj))
				{
					behaviourObj.hideFlags |= hideFlags;
				}
			}
#endif
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 内部処理用。
		/// </summary>
#else
		/// <summary>
		/// For internal.
		/// </summary>
#endif
		public bool IsStateMachine(ArborFSMInternal stateMachine)
		{
			if (_StateMachine != stateMachine)
			{
				return false;
			}

			int behaviourCount = _Behaviours.Count;
			for (int behaviourIndex = 0; behaviourIndex < behaviourCount; behaviourIndex++)
			{
				StateBehaviour behaviour = _Behaviours[behaviourIndex] as StateBehaviour;

				if (behaviour != null && behaviour.stateMachine != stateMachine)
				{
					return false;
				}
			}

			return true;
		}

		private bool _IsStateAwake = false;

#if ARBOR_DOC_JA
		/// <summary>
		/// 内部処理用。
		/// </summary>
#else
		/// <summary>
		/// For internal.
		/// </summary>
#endif
		public void EnableState(bool enable, bool changeState)
		{
			bool callStateAwake = false;
			if (!_IsStateAwake && enable && changeState)
			{
				callStateAwake = true;
				_IsStateAwake = true;
			}

			if (enable && changeState)
			{
				transitionCount++;
				if (Application.isEditor && _BreakPoint)
				{
					Debug.Break();
				}
			}

			int behaviourCount = _Behaviours.Count;
			for (int behaviourIndex = 0; behaviourIndex < behaviourCount; behaviourIndex++)
			{
				StateBehaviour behaviour = _Behaviours[behaviourIndex] as StateBehaviour;
				if (behaviour != null && behaviour.behaviourEnabled)
				{
					if (enable)
					{
						if (!behaviour.enabled)
						{
							behaviour.enabled = true;
							if (changeState)
							{
								State nextState = _StateMachine.nextState;
								if (callStateAwake)
								{
									behaviour.OnStateAwake();
								}
								behaviour.OnStateBegin();
								if (_StateMachine.currentState != null && _StateMachine.currentState.nodeID != nodeID ||
									_StateMachine.nextState != nextState)
								{
									return;
								}
							}
						}
					}
					else
					{
						if (behaviour.enabled)
						{
							if (changeState)
							{
								behaviour.OnStateEnd();
							}
							behaviour.enabled = false;
						}
					}
				}
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 内部処理用。
		/// </summary>
#else
		/// <summary>
		/// For internal.
		/// </summary>
#endif
		public void UpdateBehaviours()
		{
			if (stateMachine.isActiveAndEnabled)
			{
				int behaviourCount = _Behaviours.Count;
				for (int behaviourIndex = 0; behaviourIndex < behaviourCount; behaviourIndex++)
				{
					StateBehaviour behaviour = _Behaviours[behaviourIndex] as StateBehaviour;
					if (behaviour == null)
					{
						continue;
					}

					if (behaviour.enabled && behaviour.behaviourEnabled)
					{
						behaviour.OnStateUpdate();
					}
				}
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 内部処理用。
		/// </summary>
#else
		/// <summary>
		/// For internal.
		/// </summary>
#endif
		public void LateUpdateBehaviours()
		{
			if (stateMachine.isActiveAndEnabled)
			{
				int behaviourCount = _Behaviours.Count;
				for (int behaviourIndex = 0; behaviourIndex < behaviourCount; behaviourIndex++)
				{
					StateBehaviour behaviour = _Behaviours[behaviourIndex] as StateBehaviour;
					if (behaviour == null)
					{
						continue;
					}

					if (behaviour.enabled && behaviour.behaviourEnabled)
					{
						behaviour.OnStateLateUpdate();
					}
				}
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// トリガーメッセージを送信する。<see cref="StateBehaviour.OnStateTrigger"/>が呼び出される。
		/// </summary>
		/// <param name="message"></param>
#else
		/// <summary>
		/// Send a trigger message.<see cref = "StateBehaviour.OnStateTrigger" /> is called.
		/// </summary>
		/// <param name="message"></param>
#endif
		public void SendTrigger(string message)
		{
			if (stateMachine.isActiveAndEnabled)
			{
				int behaviourCount = _Behaviours.Count;
				for (int behaviourIndex = 0; behaviourIndex < behaviourCount; behaviourIndex++)
				{
					StateBehaviour behaviour = _Behaviours[behaviourIndex] as StateBehaviour;
					if (behaviour == null)
					{
						continue;
					}

					if (behaviour.enabled && behaviour.behaviourEnabled)
					{
						behaviour.OnStateTrigger(message);
					}
				}
			}
		}
	}
}
