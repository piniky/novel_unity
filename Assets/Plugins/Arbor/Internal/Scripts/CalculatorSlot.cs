﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 演算ノードを接続するためのスロット。
	/// </summary>
#else
	/// <summary>
	/// Slot for connecting a calculator node.
	/// </summary>
#endif
	[System.Serializable]
	public abstract class CalculatorSlot
	{
#if ARBOR_DOC_JA
		/// <summary>
		/// スロットの種類
		/// </summary>
#else
		/// <summary>
		/// Slot type
		/// </summary>
#endif
		public abstract SlotType slotType
		{
			get;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// スロットが属しているステートマシン
		/// </summary>
#else
		/// <summary>
		/// State machine slot belongs
		/// </summary>
#endif
		public ArborFSMInternal stateMachine;

#if ARBOR_DOC_JA
		/// <summary>
		/// スロットのArborEditor上の位置(Editor Only)
		/// </summary>
#else
		/// <summary>
		/// Position on ArborEditor of slot(Editor Only)
		/// </summary>
#endif
		[System.NonSerialized]
		public Rect position;

#if ARBOR_DOC_JA
		/// <summary>
		/// スロットに格納されるデータの型
		/// </summary>
#else
		/// <summary>
		/// The type of data stored in the slot
		/// </summary>
#endif
		public abstract System.Type dataType
		{
			get;
		}
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// CalculatorSlotのField情報
	/// </summary>
#else
	/// <summary>
	/// Field information of CalculatorSlot
	/// </summary>
#endif
	public class CalculatorSlotField
	{
		/// <summary>
		/// CalculatorSlot
		/// </summary>
		public CalculatorSlot slot
		{
			get;
			private set;
		}

		/// <summary>
		/// FieldInfo
		/// </summary>
		public System.Reflection.FieldInfo fieldInfo
		{
			get;
			private set;
		}

		/// <summary>
		/// CalculatorSlotField constructor
		/// </summary>
		/// <param name="slot">CalculatorSlot</param>
		/// <param name="fieldInfo">FieldInfo</param>
		public CalculatorSlotField(CalculatorSlot slot, System.Reflection.FieldInfo fieldInfo)
		{
			this.slot = slot;
			this.fieldInfo = fieldInfo;
		}
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// 入力スロット
	/// </summary>
#else
	/// <summary>
	/// Input slot
	/// </summary>
#endif
	[System.Serializable]
	public abstract class InputSlot : CalculatorSlot
	{
#if ARBOR_DOC_JA
		/// <summary>
		/// ブランチのID
		/// </summary>
#else
		/// <summary>
		/// Branch ID
		/// </summary>
#endif
		public int branchID;

#if ARBOR_DOC_JA
		/// <summary>
		/// スロットの種類
		/// </summary>
#else
		/// <summary>
		/// Slot type
		/// </summary>
#endif
		public override SlotType slotType
		{
			get
			{
				return SlotType.Input;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// ブランチを取得する。
		/// </summary>
#else
		/// <summary>
		/// Get branch
		/// </summary>
#endif
		public CalculatorBranch branch
		{
			get
			{
				if (stateMachine == null)
				{
					return null;
				}

				return stateMachine.GetCalculatorBranchFromID(branchID);
            }
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 値が使われているかどうかを取得する。
		/// </summary>
#else
		/// <summary>
		/// Gets whether or not a value is used.
		/// </summary>
#endif
		public bool isUsed
		{
			get
			{
				CalculatorBranch b = branch;
				if (b != null)
				{
					return b.isUsed;
				}
				return false;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// valueを更新した timeScale に依存しない時間。
		/// </summary>
#else
		/// <summary>
		/// Time that does not depend on timeScale when value is updated.
		/// </summary>
#endif
		public float updatedTime
		{
			get
			{
				CalculatorBranch b = branch;
				if (b != null)
				{
					return b.updatedTime;
				}
				return 0.0f;
			}
		}
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// 入力スロットのジェネリッククラス
	/// </summary>
	/// <typeparam name="T">データの型</typeparam>
#else
	/// <summary>
	/// Generic class of the input slot
	/// </summary>
	/// <typeparam name="T">Type of data</typeparam>
#endif
	[System.Serializable]
	public abstract class InputSlot<T> : InputSlot
	{
#if ARBOR_DOC_JA
		/// <summary>
		/// スロットに格納されるデータの型
		/// </summary>
#else
		/// <summary>
		/// The type of data stored in the slot
		/// </summary>
#endif
		public override System.Type dataType
		{
			get
			{
				return typeof(T);
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 値を取得する
		/// </summary>
		/// <param name="value">取得する値</param>
		/// <returns>ブランチが接続されているかどうか。</returns>
#else
		/// <summary>
		/// Get the value
		/// </summary>
		/// <param name="value">The value you get</param>
		/// <returns>Whether the branch is connected.</returns>
#endif
		public bool GetValue(ref T value)
		{
			CalculatorBranch b = branch;

			if (b == null)
			{
				return false;
			}

			if (b.value == null)
			{
				return false;
			}

			value = (T)(b.value);
			return true;
		}
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// 出力スロット
	/// </summary>
#else
	/// <summary>
	/// Output slot
	/// </summary>
#endif
	[System.Serializable]
	public abstract class OutputSlot : CalculatorSlot
	{
#if ARBOR_DOC_JA
		/// <summary>
		/// 接続先のブランチのリスト
		/// </summary>
#else
		/// <summary>
		/// List of the destination branch
		/// </summary>
#endif
		public List<int> branchIDs = new List<int>();

#if ARBOR_DOC_JA
		/// <summary>
		/// スロットの種類
		/// </summary>
#else
		/// <summary>
		/// Slot type
		/// </summary>
#endif
		public override SlotType slotType
		{
			get
			{
				return SlotType.Output;
			}
		}
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// 出力スロットのジェネリッククラス
	/// </summary>
	/// <typeparam name="T">データの型</typeparam>
#else
	/// <summary>
	/// Generic class of the output slot
	/// </summary>
	/// <typeparam name="T">Type of data</typeparam>
#endif
	[System.Serializable]
	public abstract class OutputSlot<T> : OutputSlot
	{
#if ARBOR_DOC_JA
		/// <summary>
		/// スロットに格納されるデータの型
		/// </summary>
#else
		/// <summary>
		/// The type of data stored in the slot
		/// </summary>
#endif
		public override System.Type dataType
		{
			get
			{
				return typeof(T);
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 値を設定する
		/// </summary>
		/// <param name="value">設定する値</param>
#else
		/// <summary>
		/// Set the value
		/// </summary>
		/// <param name="value">The value to be set</param>
#endif
		public void SetValue(T value)
		{
			if (stateMachine == null)
			{
				return;
			}

			int idCount = branchIDs.Count;
			for (int idIndex = 0; idIndex < idCount; idIndex++)
			{
				int branchID = branchIDs[idIndex];
				CalculatorBranch branch = stateMachine.GetCalculatorBranchFromID(branchID);
				if (branch != null)
				{
					branch.value = value;
				}
			}
		}
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// int型の入力スロット
	/// </summary>
#else
	/// <summary>
	/// int type of input slot
	/// </summary>
#endif
	[System.Serializable]
	public class InputSlotInt : InputSlot<int>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// int型の出力スロット
	/// </summary>
#else
	/// <summary>
	/// int type of output slot
	/// </summary>
#endif
	[System.Serializable]
	public class OutputSlotInt : OutputSlot<int>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// long型の入力スロット
	/// </summary>
#else
	/// <summary>
	/// long type of input slot
	/// </summary>
#endif
	[System.Serializable]
	public class InputSlotLong : InputSlot<long>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// long型の出力スロット
	/// </summary>
#else
	/// <summary>
	/// long type of output slot
	/// </summary>
#endif
	[System.Serializable]
	public class OutputSlotLong : OutputSlot<long>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// float型の入力スロット
	/// </summary>
#else
	/// <summary>
	/// float type of input slot
	/// </summary>
#endif
	[System.Serializable]
	public class InputSlotFloat : InputSlot<float>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// float型の出力スロット
	/// </summary>
#else
	/// <summary>
	/// float type of output slot
	/// </summary>
#endif
	[System.Serializable]
	public class OutputSlotFloat : OutputSlot<float>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// bool型の入力スロット
	/// </summary>
#else
	/// <summary>
	/// bool type of input slot
	/// </summary>
#endif
	[System.Serializable]
	public class InputSlotBool : InputSlot<bool>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// string型の入力スロット
	/// </summary>
#else
	/// <summary>
	/// string type of input slot
	/// </summary>
#endif
	[System.Serializable]
	public class InputSlotString : InputSlot<string>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// bool型の出力スロット
	/// </summary>
#else
	/// <summary>
	/// bool type of output slot
	/// </summary>
#endif
	[System.Serializable]
	public class OutputSlotBool : OutputSlot<bool>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// string型の出力スロット
	/// </summary>
#else
	/// <summary>
	/// string type of output slot
	/// </summary>
#endif
	[System.Serializable]
	public class OutputSlotString : OutputSlot<string>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// GameObject型の入力スロット
	/// </summary>
#else
	/// <summary>
	/// GameObject type of input slot
	/// </summary>
#endif
	[System.Serializable]
	public class InputSlotGameObject : InputSlot<GameObject>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// GameObject型の出力スロット
	/// </summary>
#else
	/// <summary>
	/// GameObject type of output slot
	/// </summary>
#endif
	[System.Serializable]
	public class OutputSlotGameObject : OutputSlot<GameObject>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// Vector2型の入力スロット
	/// </summary>
#else
	/// <summary>
	/// Vector2 type of input slot
	/// </summary>
#endif
	[System.Serializable]
	public class InputSlotVector2 : InputSlot<Vector2>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// Vector2型の出力スロット
	/// </summary>
#else
	/// <summary>
	/// Vector2 type of output slot
	/// </summary>
#endif
	[System.Serializable]
	public class OutputSlotVector2 : OutputSlot<Vector2>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// Vector3型の入力スロット
	/// </summary>
#else
	/// <summary>
	/// Vector3 type of input slot
	/// </summary>
#endif
	[System.Serializable]
	public class InputSlotVector3 : InputSlot<Vector3>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// Vector3型の出力スロット
	/// </summary>
#else
	/// <summary>
	/// Vector3 type of output slot
	/// </summary>
#endif
	[System.Serializable]
	public class OutputSlotVector3 : OutputSlot<Vector3>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// Quaternion型の入力スロット
	/// </summary>
#else
	/// <summary>
	/// Quaternion type of input slot
	/// </summary>
#endif
	[System.Serializable]
	public class InputSlotQuaternion : InputSlot<Quaternion>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// Quaternion型の出力スロット
	/// </summary>
#else
	/// <summary>
	/// Quaternion type of output slot
	/// </summary>
#endif
	[System.Serializable]
	public class OutputSlotQuaternion : OutputSlot<Quaternion>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// Rect型の入力スロット
	/// </summary>
#else
	/// <summary>
	/// Rect type of input slot
	/// </summary>
#endif
	[System.Serializable]
	public class InputSlotRect : InputSlot<Rect>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// Rect型の出力スロット
	/// </summary>
#else
	/// <summary>
	/// Rect type of output slot
	/// </summary>
#endif
	[System.Serializable]
	public class OutputSlotRect : OutputSlot<Rect>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// Bounds型の入力スロット
	/// </summary>
#else
	/// <summary>
	/// Bounds type of input slot
	/// </summary>
#endif
	[System.Serializable]
	public class InputSlotBounds : InputSlot<Bounds>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// Bounds型の出力スロット
	/// </summary>
#else
	/// <summary>
	/// Bounds type of output slot
	/// </summary>
#endif
	[System.Serializable]
	public class OutputSlotBounds : OutputSlot<Bounds>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// UnityEngine.Object型の入力スロット
	/// </summary>
#else
	/// <summary>
	/// UnityEngine.Object type of input slot
	/// </summary>
#endif
	[System.Serializable]
	public class InputSlotUnityObject : InputSlot<UnityEngine.Object>
	{
#if ARBOR_DOC_JA
		/// <summary>
		/// 値を取得する。
		/// </summary>
		/// <typeparam name="T">取得する値の型</typeparam>
		/// <returns>取得した値。ない場合はnullを返す。</returns>
#else
		/// <summary>
		/// Get value.
		/// </summary>
		/// <typeparam name="T">Type of value to get.</typeparam>
		/// <returns>The value you got. If there is none, it returns null.</returns>
#endif
		public T GetValue<T>() where T : UnityEngine.Object
		{
			UnityEngine.Object obj = null;
			if (GetValue(ref obj))
			{
				return obj as T;
			}
			return null;
		}
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// UnityEngine.Object型の出力スロット
	/// </summary>
#else
	/// <summary>
	/// UnityEngine.Object type of output slot
	/// </summary>
#endif
	[System.Serializable]
	public class OutputSlotUnityObject : OutputSlot<UnityEngine.Object>
	{
		[SerializeField]
		[ClassUnityObject]
		private ClassTypeReference _Type = new ClassTypeReference();

		public override Type dataType
		{
			get
			{
				return _Type.type ?? typeof(UnityEngine.Object);
			}
		}
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// Component型の入力スロット
	/// </summary>
#else
	/// <summary>
	/// Component type of input slot
	/// </summary>
#endif
	[System.Serializable]
	public class InputSlotComponent : InputSlot<Component>
	{
#if ARBOR_DOC_JA
		/// <summary>
		/// 値を取得する。
		/// </summary>
		/// <typeparam name="T">取得する値の型</typeparam>
		/// <returns>取得した値。ない場合はnullを返す。</returns>
#else
		/// <summary>
		/// Get value.
		/// </summary>
		/// <typeparam name="T">Type of value to get.</typeparam>
		/// <returns>The value you got. If there is none, it returns null.</returns>
#endif
		public T GetValue<T>() where T : Component
		{
			Component obj = null;
			if (GetValue(ref obj))
			{
				return obj as T;
			}
			return null;
		}
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// Component型の出力スロット
	/// </summary>
#else
	/// <summary>
	/// Component type of output slot
	/// </summary>
#endif
	[System.Serializable]
	public class OutputSlotComponent : OutputSlot<Component>
	{
		[SerializeField]
		[ClassComponent]
		private ClassTypeReference _Type = new ClassTypeReference();

		public override Type dataType
		{
			get
			{
				return _Type.type ?? typeof(Component);
			}
		}
	}
	
#if ARBOR_DOC_JA
	/// <summary>
	/// Transform型の入力スロット
	/// </summary>
#else
	/// <summary>
	/// Transform type of input slot
	/// </summary>
#endif
	[System.Serializable]
	public class InputSlotTransform : InputSlot<Transform>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// Transform型の出力スロット
	/// </summary>
#else
	/// <summary>
	/// Transform type of output slot
	/// </summary>
#endif
	[System.Serializable]
	public class OutputSlotTransform : OutputSlot<Transform>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// RectTransform型の入力スロット
	/// </summary>
#else
	/// <summary>
	/// RectTransform type of input slot
	/// </summary>
#endif
	[System.Serializable]
	public class InputSlotRectTransform : InputSlot<RectTransform>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// RectTransform型の出力スロット
	/// </summary>
#else
	/// <summary>
	/// RectTransform type of output slot
	/// </summary>
#endif
	[System.Serializable]
	public class OutputSlotRectTransform : OutputSlot<RectTransform>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// Rigidbody型の入力スロット
	/// </summary>
#else
	/// <summary>
	/// Rigidbody type of input slot
	/// </summary>
#endif
	[System.Serializable]
	public class InputSlotRigidbody : InputSlot<Rigidbody>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// Rigidbody型の出力スロット
	/// </summary>
#else
	/// <summary>
	/// Rigidbody type of output slot
	/// </summary>
#endif
	[System.Serializable]
	public class OutputSlotRigidbody : OutputSlot<Rigidbody>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// Rigidbody2D型の入力スロット
	/// </summary>
#else
	/// <summary>
	/// Rigidbody2D type of input slot
	/// </summary>
#endif
	[System.Serializable]
	public class InputSlotRigidbody2D : InputSlot<Rigidbody2D>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// Rigidbody2D型の出力スロット
	/// </summary>
#else
	/// <summary>
	/// Rigidbody2D type of output slot
	/// </summary>
#endif
	[System.Serializable]
	public class OutputSlotRigidbody2D : OutputSlot<Rigidbody2D>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// Collider型の入力スロット
	/// </summary>
#else
	/// <summary>
	/// Collider type of input slot
	/// </summary>
#endif
	[System.Serializable]
	public class InputSlotCollider : InputSlot<Collider>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// Collider型の出力スロット
	/// </summary>
#else
	/// <summary>
	/// Collider type of output slot
	/// </summary>
#endif
	[System.Serializable]
	public class OutputSlotCollider : OutputSlot<Collider>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// Collider2D型の入力スロット
	/// </summary>
#else
	/// <summary>
	/// Collider2D type of input slot
	/// </summary>
#endif
	[System.Serializable]
	public class InputSlotCollider2D : InputSlot<Collider2D>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// Collider2D型の出力スロット
	/// </summary>
#else
	/// <summary>
	/// Collider2D type of output slot
	/// </summary>
#endif
	[System.Serializable]
	public class OutputSlotCollider2D : OutputSlot<Collider2D>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// Collision型の入力スロット
	/// </summary>
#else
	/// <summary>
	/// Collision type of input slot
	/// </summary>
#endif
	[System.Serializable]
	public class InputSlotCollision : InputSlot<Collision>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// Collision型の出力スロット
	/// </summary>
#else
	/// <summary>
	/// Collision type of output slot
	/// </summary>
#endif
	[System.Serializable]
	public class OutputSlotCollision : OutputSlot<Collision>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// Collision2D型の入力スロット
	/// </summary>
#else
	/// <summary>
	/// Collision2D type of input slot
	/// </summary>
#endif
	[System.Serializable]
	public class InputSlotCollision2D : InputSlot<Collision2D>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// Collision2D型の出力スロット
	/// </summary>
#else
	/// <summary>
	/// Collision2D type of output slot
	/// </summary>
#endif
	[System.Serializable]
	public class OutputSlotCollision2D : OutputSlot<Collision2D>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// RaycastHit型の入力スロット
	/// </summary>
#else
	/// <summary>
	/// RaycastHit type of input slot
	/// </summary>
#endif
	[System.Serializable]
	public class InputSlotRaycastHit : InputSlot<RaycastHit>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// RaycastHit型の出力スロット
	/// </summary>
#else
	/// <summary>
	/// RaycastHit type of output slot
	/// </summary>
#endif
	[System.Serializable]
	public class OutputSlotRaycastHit : OutputSlot<RaycastHit>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// RaycastHit2D型の入力スロット
	/// </summary>
#else
	/// <summary>
	/// RaycastHit2D type of input slot
	/// </summary>
#endif
	[System.Serializable]
	public class InputSlotRaycastHit2D : InputSlot<RaycastHit2D>
	{
	}

#if ARBOR_DOC_JA
	/// <summary>
	/// RaycastHit2D型の出力スロット
	/// </summary>
#else
	/// <summary>
	/// RaycastHit2D type of output slot
	/// </summary>
#endif
	[System.Serializable]
	public class OutputSlotRaycastHit2D : OutputSlot<RaycastHit2D>
	{
	}
}
