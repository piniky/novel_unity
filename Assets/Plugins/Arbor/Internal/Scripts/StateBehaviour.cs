﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Stateの挙動を定義するクラス。継承して利用する。
	/// </summary>
#else
	/// <summary>
	/// Class that defines the behavior of the State. Inherited and to use.
	/// </summary>
#endif
	[AddComponentMenu("")]
	public class StateBehaviour : NodeBehaviour
	{
		#region Serialize fields
		
		[System.Reflection.Obfuscation(Exclude = true)]
		[HideInInspector]
		public bool expanded = true;

		[System.Reflection.Obfuscation(Exclude = true)]
		[HideInInspector]
		[SerializeField]
		private bool _BehaviourEnabled = true;

		#endregion // Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// Stateを取得。
		/// </summary>
#else
		/// <summary>
		/// Get the State.
		/// </summary>
#endif
		public State state
		{
			get
			{
				return node as State;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// StateIDを取得。
		/// </summary>
#else
		/// <summary>
		/// Gets the state identifier.
		/// </summary>
#endif
		public int stateID
		{
			get
			{
				return nodeID;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// StateBehaviourの有効状態を取得/設定。
		/// </summary>
		/// <value>
		///   <c>true</c> 有効; その他、 <c>false</c>.
		/// </value>
#else
		/// <summary>
		/// Gets or sets a value indicating whether [behaviour enabled].
		/// </summary>
		/// <value>
		///   <c>true</c> if [behaviour enabled]; otherwise, <c>false</c>.
		/// </value>
#endif
		public bool behaviourEnabled
		{
			get
			{
				return _BehaviourEnabled;
			}
			set
			{
				if (_BehaviourEnabled != value)
				{
					_BehaviourEnabled = value;
					if (stateMachine.currentState == state)
					{
						enabled = _BehaviourEnabled;
					}
				}
			}
		}

		public static StateBehaviour CreateStateBehaviour(ArborFSMInternal stateMachine, int stateID, System.Type type)
		{
			System.Type classType = typeof(StateBehaviour);
			if (type != classType && !type.IsSubclassOf(classType))
			{
				throw new System.ArgumentException("The type `" + type.Name + "' must be convertible to `StateBehaviour' in order to use it as parameter `type'", "type");
			}

			return CreateNodeBehaviour(stateMachine, stateID, type) as StateBehaviour;
		}

		public static Type CreateStateBehaviour<Type>(ArborFSMInternal stateMachine, int stateID) where Type : StateBehaviour
		{
			return CreateNodeBehaviour<Type>(stateMachine, stateID);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Stateに初めて入った際に呼ばれる。
		/// </summary>
#else
		/// <summary>
		/// Called when [state enter first].
		/// </summary>
#endif
		public virtual void OnStateAwake()
		{
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Stateに入った際に呼ばれる。
		/// </summary>
#else
		/// <summary>
		/// Called when [state enter].
		/// </summary>
#endif
		public virtual void OnStateBegin()
		{
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Stateの更新。毎フレーム呼ばれる。
		/// </summary>
#else
		/// <summary>
		/// Update of State. It is called every frame.
		/// </summary>
#endif
		public virtual void OnStateUpdate()
		{
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// State用のLateUpdate。毎フレーム、全てのUpdate後に呼ばれる。
		/// </summary>
#else
		/// <summary>
		/// LateUpdate for State. Every frame, called after all updates.
		/// </summary>
#endif
		public virtual void OnStateLateUpdate()
		{
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Stateから出る際に呼ばれる。
		/// </summary>
#else
		/// <summary>
		/// Called when [state exit].
		/// </summary>
#endif
		public virtual void OnStateEnd()
		{
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// SendTriggerから呼び出される。
		/// </summary>
		/// <param name="message">メッセージ</param>
#else
		/// <summary>
		/// Called from SendTrigger.
		/// </summary>
		/// <param name="message">Message</param>
#endif
		public virtual void OnStateTrigger(string message)
		{
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 状態遷移
		/// </summary>
		/// <param name="nextStateID">遷移先のステートID。</param>
		/// <param name="transitionTiming">遷移するタイミング。</param>
		/// <returns>遷移できたかどうか</returns>
#else
		/// <summary>
		/// State transition
		/// </summary>
		/// <param name="nextStateID">State ID for the transition destination.</param>
		/// <param name="transitionTiming">Transition timing.</param>
		/// <returns>Whether or not the transition</returns>
#endif
		public bool Transition(int nextStateID, TransitionTiming transitionTiming)
		{
			if (!enabled)
			{
				return false;
			}

			if (stateMachine != null)
			{
				return stateMachine.Transition(nextStateID, transitionTiming);
			}

			return false;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 状態遷移
		/// </summary>
		/// <param name="nextStateID">遷移先のステートID。</param>
		/// <param name="immediateTransition">すぐに遷移するかどうか。falseの場合は現在フレームの最後(LateUpdate時)に遷移する。</param>
		/// <returns>遷移できたかどうか</returns>
#else
		/// <summary>
		/// State transition
		/// </summary>
		/// <param name="nextStateID">State ID for the transition destination.</param>
		/// <param name="immediateTransition">Whether or not to transition immediately. If false I will transition to the end of the current frame (when LateUpdate).</param>
		/// <returns>Whether or not the transition</returns>
#endif
		[System.Obsolete("use Transition(int nextStateID, TransitionTiming transitionTiming)")]
		public bool Transition(int nextStateID, bool immediateTransition)
		{
			return Transition(nextStateID, immediateTransition ? TransitionTiming.Immediate : TransitionTiming.LateUpdateOverwrite);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 状態遷移する。実際に遷移するタイミングは現在フレームの最後(LateUpdate時)。
		/// </summary>
		/// <param name="nextStateID">遷移先のステートID。</param>
		/// <returns>遷移できたかどうか</returns>
#else
		/// <summary>
		/// State transition. Timing to actually transition current frame last (when LateUpdate).
		/// </summary>
		/// <param name="nextStateID">State ID for the transition destination.</param>
		/// <returns>Whether or not the transition</returns>
#endif
		public bool Transition(int nextStateID)
		{
			if (!enabled)
			{
				return false;
			}

			if (stateMachine != null)
			{
				return stateMachine.Transition(nextStateID);
			}

			return false;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 状態遷移
		/// </summary>
		/// <param name="nextState">遷移先のステート。</param>
		/// <param name="transitionTiming">遷移するタイミング。</param>
		/// <returns>遷移できたかどうか</returns>
#else
		/// <summary>
		/// State transition
		/// </summary>
		/// <param name="nextState">Destination state.</param>
		/// <param name="transitionTiming">Transition timing.</param>
		/// <returns>Whether or not the transition</returns>
#endif
		public bool Transition(State nextState, TransitionTiming transitionTiming)
		{
			if (!enabled)
			{
				return false;
			}

			if (stateMachine != null)
			{
				return stateMachine.Transition(nextState, transitionTiming);
			}

			return false;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 状態遷移
		/// </summary>
		/// <param name="nextState">遷移先のステート。</param>
		/// <param name="immediateTransition">すぐに遷移するかどうか。falseの場合は現在フレームの最後(LateUpdate時)に遷移する。</param>
		/// <returns>遷移できたかどうか</returns>
#else
		/// <summary>
		/// State transition
		/// </summary>
		/// <param name="nextState">Destination state.</param>
		/// <param name="immediateTransition">Whether or not to transition immediately. If false I will transition to the end of the current frame (when LateUpdate).</param>
		/// <returns>Whether or not the transition</returns>
#endif
		[System.Obsolete("use Transition(State nextState, TransitionTiming transitionTiming)")]
		public bool Transition(State nextState, bool immediateTransition)
		{
			return Transition(nextState, immediateTransition? TransitionTiming.Immediate : TransitionTiming.LateUpdateOverwrite );
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 状態遷移する。実際に遷移するタイミングは現在フレームの最後(LateUpdate時)。
		/// </summary>
		/// <param name="nextState">遷移先のステート。</param>
		/// <returns>遷移できたかどうか</returns>
#else
		/// <summary>
		/// State transition. Timing to actually transition current frame last (when LateUpdate).
		/// </summary>
		/// <param name="nextState">Destination state.</param>
		/// <returns>Whether or not the transition</returns>
#endif
		public bool Transition(State nextState)
		{
			if (!enabled)
			{
				return false;
			}

			if (stateMachine != null)
			{
				return stateMachine.Transition(nextState);
			}

			return false;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 状態遷移
		/// </summary>
		/// <param name="nextStateLink">遷移の接続先。</param>
		/// <param name="transitionTiming">遷移するタイミング。</param>
		/// <returns>遷移できたかどうか</returns>
#else
		/// <summary>
		/// State transition
		/// </summary>
		/// <param name="nextStateLink">The destination of transition.</param>
		/// <param name="transitionTiming">Transition timing.</param>
		/// <returns>Whether or not the transition</returns>
#endif
		public bool Transition(StateLink nextStateLink, TransitionTiming transitionTiming)
		{
			if (!enabled)
			{
				return false;
			}

			if (stateMachine != null)
			{
				return stateMachine.Transition(nextStateLink, transitionTiming);
			}

			return false;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 状態遷移
		/// </summary>
		/// <param name="nextStateLink">遷移の接続先。</param>
		/// <param name="immediateTransition">すぐに遷移するかどうか。falseの場合は現在フレームの最後(LateUpdate時)に遷移する。</param>
		/// <returns>遷移できたかどうか</returns>
#else
		/// <summary>
		/// State transition
		/// </summary>
		/// <param name="nextStateLink">The destination of transition.</param>
		/// <param name="immediateTransition">Whether or not to transition immediately. If false I will transition to the end of the current frame (when LateUpdate).</param>
		/// <returns>Whether or not the transition</returns>
#endif
		[System.Obsolete("use Transition(StateLink nextStateLink, TransitionTiming transitionTiming)")]
		public bool Transition(StateLink nextStateLink, bool immediateTransition)
		{
			return Transition(nextStateLink, immediateTransition? TransitionTiming.Immediate : TransitionTiming.LateUpdateOverwrite );
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 状態遷移する。実際に遷移するタイミングは現在フレームの最後(LateUpdate時)。
		/// </summary>
		/// <param name="nextStateLink">遷移の接続先。</param>
		/// <returns>遷移できたかどうか</returns>
#else
		/// <summary>
		/// State transition. Timing to actually transition current frame last (when LateUpdate).
		/// </summary>
		/// <param name="nextStateLink">The destination of transition.</param>
		/// <returns>Whether or not the transition</returns>
#endif
		public bool Transition(StateLink nextStateLink)
		{
			if (!enabled)
			{
				return false;
			}

			if (stateMachine != null)
			{
				return stateMachine.Transition(nextStateLink);
			}
			return false;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// StateBehaviourを追加。
		/// </summary>
		/// <param name="type">追加するStateBehaviourの型</param>
		/// <returns>追加したStateBehaviour</returns>
#else
		/// <summary>
		/// Adds the behaviour.
		/// </summary>
		/// <param name="type">Type of add StateBehaviour</param>
		/// <returns>Added StateBehaviour</returns>
#endif
		public StateBehaviour AddBehaviour(System.Type type)
		{
			return state.AddBehaviour(type);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// StateBehaviourを追加。
		/// </summary>
		/// <typeparam name="T">追加するStateBehaviourの型</typeparam>
		/// <returns>追加したStateBehaviour</returns>
#else
		/// <summary>
		/// Adds the behaviour.
		/// </summary>
		/// <typeparam name="T">Type of add StateBehaviour</typeparam>
		/// <returns>Added StateBehaviour</returns>
#endif
		public T AddBehaviour<T>() where T : StateBehaviour
		{
			return state.AddBehaviour<T>();
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// StateBehaviourを取得。
		/// </summary>
		/// <param name="type">取得したいStateBehaviourの型。</param>
		/// <returns>見つかったStateBehaviour。ない場合はnull。</returns>
#else
		/// <summary>
		/// Gets the behaviour.
		/// </summary>
		/// <param name="type">Type of you want to get StateBehaviour.</param>
		/// <returns>Found StateBehaviour. Or null if it is not.</returns>
#endif
		public StateBehaviour GetBehaviour(System.Type type)
		{
			return state.GetBehaviour(type);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// StateBehaviourを取得。
		/// </summary>
		/// <typeparam name="T">取得したいStateBehaviourの型。</typeparam>
		/// <returns>見つかったStateBehaviour。ない場合はnull。</returns>
#else
		/// <summary>
		/// Gets the behaviour.
		/// </summary>
		/// <typeparam name="T">Type of you want to get StateBehaviour.</typeparam>
		/// <returns>Found StateBehaviour. Or null if it is not.</returns>
#endif
		public T GetBehaviour<T>() where T : StateBehaviour
		{
			return state.GetBehaviour<T>();
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// StateBehaviourを取得。
		/// </summary>
		/// <param name="type">取得したいStateBehaviourの型。</param>
		/// <returns>見つかったStateBehaviourの配列。</returns>
#else
		/// <summary>
		/// Gets the behaviours.
		/// </summary>
		/// <param name="type">Type of you want to get StateBehaviour.</param>
		/// <returns>Array of found StateBehaviour.</returns>
#endif
		public StateBehaviour[] GetBehaviours(System.Type type)
		{
			return state.GetBehaviours(type);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// StateBehaviourを取得。
		/// </summary>
		/// <typeparam name="T">取得したいStateBehaviourの型。</typeparam>
		/// <returns>見つかったStateBehaviourの配列。</returns>
#else
		/// <summary>
		/// Gets the behaviours.
		/// </summary>
		/// <typeparam name="T">Type of you want to get StateBehaviour.</typeparam>
		/// <returns>Array of found StateBehaviour.</returns>
#endif
		public T[] GetBehaviours<T>() where T : StateBehaviour
		{
			return state.GetBehaviours<T>();
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// インスタンスを削除する。
		/// </summary>
#else
		/// <summary>
		/// Destroys this instance.
		/// </summary>
#endif
		public void Destroy()
		{
			if (state != null)
			{
				state.DestroyBehaviour(this);
			}
		}
	}
}
