﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Stateの挙動を定義するクラス。継承して利用する。
	/// </summary>
#else
	/// <summary>
	/// Class that defines the behavior of the State. Inherited and to use.
	/// </summary>
#endif
	[AddComponentMenu("")]
	public class Calculator : NodeBehaviour
	{

#if ARBOR_DOC_JA
		/// <summary>
		/// Stateを取得。
		/// </summary>
#else
		/// <summary>
		/// Get the State.
		/// </summary>
#endif
		public CalculatorNode calculatorNode
		{
			get
			{
				return node as CalculatorNode;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// CalculatorIDを取得。
		/// </summary>
#else		
		/// <summary>
		/// Gets the Calculator identifier.
		/// </summary>
#endif
		public int calculatorID
		{
			get
			{
				return nodeID;
			}
		}

		bool _IsDirty = true;

		public bool isDirty
		{
			get
			{
				if (OnCheckDirty() || _IsDirty)
				{
					return true;
				}

				int slotCount = calculatorSlotFieldCount;
				for (int slotIndex = 0; slotIndex < slotCount; slotIndex++)
				{
					CalculatorSlotField slotInfo = GetCalculatorSlotField(slotIndex);
					InputSlot s = slotInfo.slot as InputSlot;
					if (s != null)
					{
						CalculatorBranch branch = s.branch;
						if (branch != null)
						{
							Calculator outCalculator = branch.outBehaviour as Calculator;
							if (outCalculator != null && outCalculator.isDirty)
							{
								return true;
							}
						}
					}
				}
				return false;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 変更されているか判定する際に呼ばれる。
		/// </summary>
		/// <returns>変更されている場合はtrue、そうでなければfalseを返す。</returns>
#else
		/// <summary>
		/// It is called when judging whether it has been changed.
		/// </summary>
		/// <returns>Returns true if it has been changed, false otherwise.</returns>
#endif
		public virtual bool OnCheckDirty()
		{
			return false;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// OnCalculateを呼んでほしい場合に呼び出す。
		/// </summary>
#else
		/// <summary>
		/// Call if you want call OnCalculate.
		/// </summary>
#endif
		public void SetDirty()
		{
			_IsDirty = true;
		}

		private List<Parameter> _CachedParameters = null;
		private List<Parameter> cachedParameters
		{
			get
			{
				if (_CachedParameters == null)
				{
					_CachedParameters = new List<Parameter>();
					EachField<ParameterReference>.Find(this, GetType(), (r) => {
						Parameter parameter = r.parameter;
						if (parameter != null)
						{
							_CachedParameters.Add(parameter);
						}
					});
				}
				return _CachedParameters;
			}
		}

		private bool _IsSettedOnChanged;

		void SetOnChanged()
		{
			if (_IsSettedOnChanged)
			{
				ReleaseOnChanged();
				_CachedParameters.Clear();
				_CachedParameters = null;
				_IsSettedOnChanged = false;
			}

			List<Parameter> parameters = cachedParameters;
			for (int i = 0; i < parameters.Count; i++)
			{
				Parameter parameter = parameters[i];
				parameter.onChanged += ParameterOnChanged;
			}
		}

		void ReleaseOnChanged()
		{
			if (_IsSettedOnChanged)
			{
				List<Parameter> parameters = cachedParameters;
				for (int i = 0; i < parameters.Count; i++)
				{
					Parameter parameter = parameters[i];
					parameter.onChanged -= ParameterOnChanged;
				}
				_IsSettedOnChanged = false;
			}
		}

		protected virtual void Awake()
		{
			SetOnChanged();
        }

		protected virtual void OnDestroy()
		{
			ReleaseOnChanged();
        }

		protected override void OnValidate()
		{
			base.OnValidate();

			if (Application.isPlaying)
			{
				if (isActiveAndEnabled && _IsSettedOnChanged)
				{
					SetOnChanged();
				}

				SetDirty();
			}
		}

		void ParameterOnChanged(Parameter parameter)
		{
			SetDirty();
		}

		public static Calculator CreateCalculator( ArborFSMInternal stateMachine,int calculatorID,System.Type type )
		{
			System.Type classType = typeof(Calculator);
			if( type != classType && !type.IsSubclassOf( classType ) )
			{
				throw new System.ArgumentException( "The type `" + type.Name + "' must be convertible to `Calculator' in order to use it as parameter `type'","type" );
			}

			return CreateNodeBehaviour(stateMachine, calculatorID, type) as Calculator;
		}

		public static Type CreateCalculator<Type>( ArborFSMInternal stateMachine,int nodeID) where Type : Calculator
		{
			return CreateNodeBehaviour<Type>(stateMachine, nodeID);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 必要であれば演算する。
		/// </summary>
#else
		/// <summary>
		/// It is calculated, if necessary.
		/// </summary>
#endif
		public void Calculate()
		{
			if (isDirty)
			{
				OnCalculate();

				_IsDirty = false;
            }
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算される際に呼ばれる。
		/// </summary>
#else
		/// <summary>
		/// It called when it is calculated .
		/// </summary>
#endif
		public virtual void OnCalculate()
		{
		}
	}
}
