﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Vector2パラメータの参照。
	/// </summary>
#else
	/// <summary>
	/// Reference Vector2 parameters.
	/// </summary>
#endif
	[System.Serializable]
	public class Vector2ParameterReference : ParameterReference
	{
	}
}
