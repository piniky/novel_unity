﻿using UnityEngine;
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Attributeのヘルパークラス。
	/// </summary>
#else
	/// <summary>
	/// A helper class for Attribute.
	/// </summary>
#endif
	public static class AttributeHelper
	{
		private static readonly Dictionary<MemberInfo, Attribute[]> _Attributes = new Dictionary<MemberInfo, Attribute[]>();

#if ARBOR_DOC_JA
		/// <summary>
		/// Attributeを取得。
		/// </summary>
		/// <param name="member">MemberInfo</param>
		/// <returns>Attributes</returns>
#else
		/// <summary>
		/// Get Attributes.
		/// </summary>
		/// <param name="member">MemberInfo</param>
		/// <returns>Attributes</returns>
#endif
		public static Attribute[] GetAttributes( MemberInfo member )
		{
			Attribute[] attributes = null;
			if( !_Attributes.TryGetValue( member, out attributes ) )
			{
				attributes = Attribute.GetCustomAttributes( member, false );
				_Attributes.Add( member, attributes );
			}

			return attributes;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Attributeを取得 (generic)
		/// </summary>
		/// <typeparam name="T">取得する型</typeparam>
		/// <param name="member">MemberInfo</param>
		/// <returns>Attribute</returns>
#else
		/// <summary>
		/// Get Attribute (generic)
		/// </summary>
		/// <typeparam name="T">Target Type</typeparam>
		/// <param name="member">MemberInfo</param>
		/// <returns>Attribute</returns>
#endif
		public static T GetAttribute<T>( MemberInfo member ) where T : Attribute
		{
			if (member == null)
			{
				return null;
			}

			Attribute[] attributes = GetAttributes( member );
			int attributeCount = attributes.Length;
			for( int index = 0 ; index < attributeCount ; index++ )
			{
				Attribute attribute = attributes[index];

				if( attribute is T )
				{
					return attribute as T;
				}
			}

			return null;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Attributeがあるかどうか。
		/// </summary>
		/// <typeparam name="T">取得する型</typeparam>
		/// <param name="member">MemberInfo</param>
		/// <returns>Attributeがあるかどうか。</returns>
#else
		/// <summary>
		/// Whether has Attribute
		/// </summary>
		/// <typeparam name="T">Target Type</typeparam>
		/// <param name="member">MemberInfo</param>
		/// <returns>Whether has attribute.</returns>
#endif
		public static bool HasAttribute<T>( MemberInfo member ) where T : Attribute
		{
			return ( GetAttribute<T>( member) != null );
		}
	}
}
