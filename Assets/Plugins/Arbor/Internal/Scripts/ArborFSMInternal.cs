﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// <see cref="Arbor.ArborFSM" />の内部クラス。
	/// 実際にGameObjectにアタッチするには<see cref="Arbor.ArborFSM" />を使用する。
	/// </summary>
#else
	/// <summary>
	/// Internal class of <see cref="Arbor.ArborFSM" />.
	/// To actually attach to GameObject is to use the <see cref = "Arbor.ArborFSM" />.
	/// </summary>
#endif
	[AddComponentMenu("")]
	public class ArborFSMInternal : MonoBehaviour, ISerializationCallbackReceiver
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// FSMの名前。<br/>
		/// 一つのGameObjectに複数のFSMがある場合の識別や検索に使用する。
		/// </summary>
#else
		/// <summary>
		/// The FSM name.<br/>
		/// It is used for identification and retrieval when there is more than one FSM in one GameObject.
		/// </summary>
#endif
		public string fsmName;

		[System.Reflection.Obfuscation(Exclude = true)]
		[SerializeField]
		[HideInInspector]
		private int _StartStateID;

		[System.Reflection.Obfuscation(Exclude = true)]
		[SerializeField]
#if !ARBOR_DEBUG
		[HideInInspector]
#endif
		private List<State> _States = new List<State>();

		[System.Reflection.Obfuscation(Exclude = true)]
		[SerializeField]
#if !ARBOR_DEBUG
		[HideInInspector]
#endif
		private List<CommentNode> _Comments = new List<CommentNode>();

		[System.Reflection.Obfuscation(Exclude = true)]
		[SerializeField]
#if !ARBOR_DEBUG
		[HideInInspector]
#endif
		private List<CalculatorNode> _Calculators = new List<CalculatorNode>();

		[System.Reflection.Obfuscation(Exclude = true)]
		[SerializeField]
#if !ARBOR_DEBUG
		[HideInInspector]
#endif
		private List<GroupNode> _Groups = new List<GroupNode>();

		[System.Reflection.Obfuscation(Exclude = true)]
		[SerializeField]
#if !ARBOR_DEBUG
		[HideInInspector]
#endif
		private List<CalculatorBranch> _CalculatorBranchies = new List<CalculatorBranch>();

		[System.Reflection.Obfuscation(Exclude = true)]
		[System.NonSerialized]
		[HideInInspector]
		private State _CurrentState = null;

		[System.Reflection.Obfuscation(Exclude = true)]
		[System.NonSerialized]
		[HideInInspector]
		private State _NextState = null;

		#endregion // Serialize fields

		private bool _IsStarted = false;

		[System.NonSerialized]
		private List<Node> _Nodes = new List<Node>();

#if ARBOR_DOC_JA
		/// <summary>
		/// 開始ステートのIDを取得する。
		/// </summary>
		/// <value>
		/// 開始ステートID。
		/// </value>
#else
		/// <summary>
		/// Gets the start state identifier.
		/// </summary>
		/// <value>
		/// The start state identifier.
		/// </value>
#endif
		public int startStateID
		{
			get
			{
				return _StartStateID;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 現在の<see cref="Arbor.State" />を取得する。
		/// </summary>
		/// <value>
		/// 現在の<see cref="Arbor.State" />。
		/// </value>
#else
		/// <summary>
		/// Gets <see cref="Arbor.State" /> of the current.
		/// </summary>
		/// <value>
		/// <see cref="Arbor.State" /> of the current.
		/// </value>
#endif
		public State currentState
		{
			get
			{
				return _CurrentState;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 遷移先の<see cref="Arbor.State" />を取得する。
		/// </summary>
		/// <value>
		/// 遷移先の<see cref="Arbor.State" />。
		/// </value>
#else
		/// <summary>
		/// Gets <see cref="Arbor.State" /> of the next state.
		/// </summary>
		/// <value>
		/// <see cref="Arbor.State" /> of the next state.
		/// </value>
#endif
		public State nextState
		{
			get
			{
				return _NextState;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Nodeの数を取得。
		/// </summary>
#else
		/// <summary>
		///  Get a count of Node.
		/// </summary>
#endif
		public int nodeCount
		{
			get
			{
				return _Nodes.Count;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Nodeをインデックスから取得
		/// </summary>
		/// <param name="index">インデックス</param>
		/// <returns>Node</returns>
#else
		/// <summary>
		/// Get Node from index.
		/// </summary>
		/// <param name="index">Index</param>
		/// <returns>Node</returns>
#endif
		public Node GetNodeFromIndex(int index)
		{
			return _Nodes[index];
		}

		private Dictionary<int, Node> _DicNodes;

		private Dictionary<int, Node> dicNodes
		{
			get
			{
				if (_DicNodes == null)
				{
					_DicNodes = new Dictionary<int, Node>();

					int nodeCount = _Nodes.Count;
					for (int nodeIndex = 0; nodeIndex < nodeCount; nodeIndex++)
					{
						Node node = _Nodes[nodeIndex];
						_DicNodes.Add(node.nodeID, node);
					}
				}

				return _DicNodes;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Stateの数を取得。
		/// </summary>
#else
		/// <summary>
		///  Get a count of State.
		/// </summary>
#endif
		public int stateCount
		{
			get
			{
				return _States.Count;
			}
		}

		private LinkedList<StateLink> _StateLinkHistory = new LinkedList<StateLink>();

#if ARBOR_DOC_JA
		/// <summary>
		/// 指定したStateLinkによって遷移したヒストリーでのインデックスを取得。
		/// </summary>
		/// <param name="stateLink">取得するStateLink</param>
		/// <returns>ヒストリーのインデックス。-1だと対象外。値が大きいほど古い遷移を指す。</returns>
#else
		/// <summary>
		/// Retrieve the index in the history that transited by the specified StateLink.
		/// </summary>
		/// <param name="stateLink">StateLink to acquire</param>
		/// <returns>Index of history. -1 is not eligible. Larger values indicate older transitions.</returns>
#endif
		public int IndexOfStateLinkHistory(StateLink stateLink)
		{
			int index = 0;
			foreach (StateLink s in _StateLinkHistory)
			{
				if (s == stateLink)
				{
					return index;
				}
				index++;
			}
			return -1;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Stateをインデックスから取得
		/// </summary>
		/// <param name="index">インデックス</param>
		/// <returns>State</returns>
#else
		/// <summary>
		/// Get State from index.
		/// </summary>
		/// <param name="index">Index</param>
		/// <returns>State</returns>
#endif
		public State GetStateFromIndex(int index)
		{
			return _States[index];
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Stateのインデックスを取得
		/// </summary>
		/// <param name="state">State</param>
		/// <returns>インデックス。ない場合は-1を返す。</returns>
#else
		/// <summary>
		/// Get State index.
		/// </summary>
		/// <param name="index">Index</param>
		/// <returns>Index. If not, it returns -1.</returns>
#endif
		public int GetStateIndex(State state)
		{
			return _States.IndexOf(state);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 全ての<see cref="Arbor.State" />を取得する。
		/// </summary>
		/// <value>
		/// <see cref="Arbor.State" />の配列。
		/// </value>
#else
		/// <summary>
		/// Gets all of <see cref="Arbor.State" />.
		/// </summary>
		/// <value>
		/// Array of <see cref="Arbor.State" />.
		/// </value>
#endif
		[System.Obsolete("use stateCount and GetStateFromIndex()")]
		public State[] states
		{
			get
			{
				return _States.ToArray();
			}
		}

		private Dictionary<int, State> _DicStates;

		private Dictionary<int, State> dicStates
		{
			get
			{
				if (_DicStates == null)
				{
					_DicStates = new Dictionary<int, State>();

					int stateCount = _States.Count;
					for (int stateIndex = 0; stateIndex < stateCount; stateIndex++)
					{
						State state = _States[stateIndex];
						_DicStates.Add(state.nodeID, state);
					}
				}

				return _DicStates;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// CommentNodeの数を取得。
		/// </summary>
#else
		/// <summary>
		///  Get a count of CommentNode.
		/// </summary>
#endif
		public int commentCount
		{
			get
			{
				return _Comments.Count;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// CommentNodeをインデックスから取得
		/// </summary>
		/// <param name="index">インデックス</param>
		/// <returns>CommentNode</returns>
#else
		/// <summary>
		/// Get CommentNode from index.
		/// </summary>
		/// <param name="index">Index</param>
		/// <returns>CommentNode</returns>
#endif
		public CommentNode GetCommentFromIndex(int index)
		{
			return _Comments[index];
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// CommentNodeのインデックスを取得
		/// </summary>
		/// <param name="comment">CommentNode</param>
		/// <returns>インデックス。ない場合は-1を返す。</returns>
#else
		/// <summary>
		/// Get CommentNode index.
		/// </summary>
		/// <param name="comment">CommentNode</param>
		/// <returns>Index. If not, it returns -1.</returns>
#endif
		public int GetCommentIndex(CommentNode comment)
		{
			return _Comments.IndexOf(comment);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 全ての<see cref="Arbor.CommentNode" />を取得する。
		/// </summary>
#else
		/// <summary>
		/// Gets all of <see cref = "Arbor.CommentNode" />.
		/// </summary>
#endif
		[System.Obsolete("use commentCount and GetCommentFromIndex()")]
		public CommentNode[] comments
		{
			get
			{
				return _Comments.ToArray();
			}
		}

		private Dictionary<int, CommentNode> _DicComments;

		private Dictionary<int, CommentNode> dicComments
		{
			get
			{
				if (_DicComments == null)
				{
					_DicComments = new Dictionary<int, CommentNode>();

					int commentCount = _Comments.Count;
					for (int commentIndex = 0; commentIndex < commentCount; commentIndex++)
					{
						CommentNode comment = _Comments[commentIndex];
						_DicComments.Add(comment.nodeID, comment);
					}
				}

				return _DicComments;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// CalculatorNodeの数を取得。
		/// </summary>
#else
		/// <summary>
		///  Get a count of CalculatorNode.
		/// </summary>
#endif
		public int calculatorCount
		{
			get
			{
				return _Calculators.Count;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// CalculatorNodeをインデックスから取得
		/// </summary>
		/// <param name="index">インデックス</param>
		/// <returns>CalculatorNode</returns>
#else
		/// <summary>
		/// Get CalculatorNode from index.
		/// </summary>
		/// <param name="index">Index</param>
		/// <returns>CalculatorNode</returns>
#endif
		public CalculatorNode GetCalculatorFromIndex(int index)
		{
			return _Calculators[index];
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// CalculatorNodeのインデックスを取得
		/// </summary>
		/// <param name="calculator">CalculatorNode</param>
		/// <returns>インデックス。ない場合は-1を返す。</returns>
#else
		/// <summary>
		/// Get CalculatorNode index.
		/// </summary>
		/// <param name="calculator">CalculatorNode</param>
		/// <returns>Index. If not, it returns -1.</returns>
#endif
		public int GetCalculatorIndex(CalculatorNode calculator)
		{
			return _Calculators.IndexOf(calculator);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 全ての<see cref="Arbor.CalculatorNode" />を取得する。
		/// </summary>
#else
		/// <summary>
		/// Gets all of <see cref = "Arbor.CalculatorNode" />.
		/// </summary>
#endif
		[System.Obsolete("use calculatorCount and GetCalculatorFromIndex()")]
		public CalculatorNode[] calculators
		{
			get
			{
				return _Calculators.ToArray();
			}
		}

		private Dictionary<int, CalculatorNode> _DicCalculators;

		private Dictionary<int, CalculatorNode> dicCalculators
		{
			get
			{
				if (_DicCalculators == null)
				{
					_DicCalculators = new Dictionary<int, CalculatorNode>();

					int calculatorCount = _Calculators.Count;
					for (int calculatorIndex = 0; calculatorIndex < calculatorCount; calculatorIndex++)
					{
						CalculatorNode calculator = _Calculators[calculatorIndex];
						_DicCalculators.Add(calculator.nodeID, calculator);
					}
				}

				return _DicCalculators;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// GroupNodeの数を取得。
		/// </summary>
#else
		/// <summary>
		///  Get a count of GroupNode.
		/// </summary>
#endif
		public int groupCount
		{
			get
			{
				return _Groups.Count;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// GroupNodeをインデックスから取得
		/// </summary>
		/// <param name="index">インデックス</param>
		/// <returns>GroupNode</returns>
#else
		/// <summary>
		/// Get GroupNode from index.
		/// </summary>
		/// <param name="index">Index</param>
		/// <returns>GroupNode</returns>
#endif
		public GroupNode GetGroupFromIndex(int index)
		{
			return _Groups[index];
		}

		private Dictionary<int, GroupNode> _DicGroups;

		private Dictionary<int, GroupNode> dicGroups
		{
			get
			{
				if (_DicGroups == null)
				{
					_DicGroups = new Dictionary<int, GroupNode>();

					int groupCount = _Groups.Count;
					for (int groupIndex = 0; groupIndex < groupCount; groupIndex++)
					{
						GroupNode group = _Groups[groupIndex];
						_DicGroups.Add(group.nodeID, group);
					}
				}

				return _DicGroups;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// CalculatorBranchの数を取得。
		/// </summary>
#else
		/// <summary>
		///  Get a count of CalculatorBranch.
		/// </summary>
#endif
		public int calculatorBranchCount
		{
			get
			{
				return _CalculatorBranchies.Count;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// CalculatorBranchをインデックスから取得
		/// </summary>
		/// <param name="index">インデックス</param>
		/// <returns>CalculatorBranch</returns>
#else
		/// <summary>
		/// Get CalculatorBranch from index.
		/// </summary>
		/// <param name="index">Index</param>
		/// <returns>CalculatorBranch</returns>
#endif
		public CalculatorBranch GetCalculatorBranchFromIndex(int index)
		{
			return _CalculatorBranchies[index];
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// CalculatorBranchのインデックスを取得
		/// </summary>
		/// <param name="branch">CalculatorBranch</param>
		/// <returns>インデックス。ない場合は-1を返す。</returns>
#else
		/// <summary>
		/// Get CalculatorBranch index.
		/// </summary>
		/// <param name="branch">CalculatorBranch</param>
		/// <returns>Index. If not, it returns -1.</returns>
#endif
		public int GetCalculatorBranchIndex(CalculatorBranch branch)
		{
			return _CalculatorBranchies.IndexOf(branch);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 全ての<see cref="Arbor.CalculatorBranch" />を取得する。
		/// </summary>
#else
		/// <summary>
		/// Gets all of <see cref = "Arbor.CalculatorBranch" />.
		/// </summary>
#endif
		[System.Obsolete("use calculatorBranchCount and GetCalculatorBranchFromIndex()")]
		public CalculatorBranch[] calculatorBranchies
		{
			get
			{
				return _CalculatorBranchies.ToArray();
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// ノードIDを指定して<see cref="Arbor.Node" />を取得する。
		/// </summary>
		/// <param name="nodeID">ノードID</param>
		/// <returns>見つかった<see cref="Arbor.Node" />。見つからなかった場合はnullを返す。</returns>
#else
		/// <summary>
		/// Gets <see cref="Arbor.Node" /> from the node identifier.
		/// </summary>
		/// <param name="nodeID">The node identifier.</param>
		/// <returns>Found <see cref = "Arbor.Node" />. Returns null if not found.</returns>
#endif
		public Node GetNodeFromID(int nodeID)
		{
			Node result = null;
			if (dicNodes.TryGetValue(nodeID, out result))
			{
				if (result.nodeID == nodeID )
				{
					if (_Nodes.Contains(result))
					{
						return result;
					}
					else
					{
						dicNodes.Remove(nodeID);
					}
				}
			}

			int nodeCount = _Nodes.Count;
			for (int nodeIndex = 0; nodeIndex < nodeCount; nodeIndex++)
			{
				Node node = _Nodes[nodeIndex];
				if (node.nodeID == nodeID)
				{
					dicNodes.Add(node.nodeID, node);
					return node;
				}
			}

			return null;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// ステートIDを指定して<see cref="Arbor.State" />を取得する。
		/// </summary>
		/// <param name="stateID">ステートID</param>
		/// <returns>見つかった<see cref="Arbor.State" />。見つからなかった場合はnullを返す。</returns>
#else
		/// <summary>
		/// Gets <see cref="Arbor.State" /> from the state identifier.
		/// </summary>
		/// <param name="stateID">The state identifier.</param>
		/// <returns>Found <see cref = "Arbor.State" />. Returns null if not found.</returns>
#endif
		public State GetStateFromID(int stateID)
		{
			State result = null;
			if (dicStates.TryGetValue(stateID, out result))
			{
				if (result.nodeID == stateID)
				{
					return result;
				}
			}

			int stateCount = _States.Count;
			for (int stateIndex = 0; stateIndex < stateCount; stateIndex++)
			{
				State state = _States[stateIndex];
				if (state.nodeID == stateID)
				{
					dicStates.Add(state.nodeID, state);
					return state;
				}
			}

			return null;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// コメントIDを指定して<see cref="Arbor.CommentNode" />を取得する。
		/// </summary>
		/// <param name="commentID">コメントID</param>
		/// <returns>見つかった<see cref="Arbor.CommentNode" />。見つからなかった場合はnullを返す。</returns>
#else
		/// <summary>
		/// Gets <see cref="Arbor.CommentNode" /> from the comment identifier.
		/// </summary>
		/// <param name="commentID">The comment identifier.</param>
		/// <returns>Found <see cref = "Arbor.CommentNode" />. Returns null if not found.</returns>
#endif
		public CommentNode GetCommentFromID(int commentID)
		{
			CommentNode result = null;
			if (dicComments.TryGetValue(commentID, out result))
			{
				if (result.nodeID == commentID)
				{
					return result;
				}
			}

			int commentCount = _Comments.Count;
			for (int commentIndex = 0; commentIndex < commentCount; commentIndex++)
			{
				CommentNode comment = _Comments[commentIndex];
				if (comment.nodeID == commentID)
				{
					dicComments.Add(comment.nodeID, comment);
					return comment;
				}
			}

			return null;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算ノードIDを指定して<see cref="Arbor.CalculatorNode" />を取得する。
		/// </summary>
		/// <param name="calculatorID">演算ノードID</param>
		/// <returns>見つかった<see cref="Arbor.CalculatorNode" />。見つからなかった場合はnullを返す。</returns>
#else
		/// <summary>
		/// Gets <see cref="Arbor.CalculatorNode" /> from the calculator identifier.
		/// </summary>
		/// <param name="calculatorID">The calculator identifier.</param>
		/// <returns>Found <see cref = "Arbor.CalculatorNode" />. Returns null if not found.</returns>
#endif
		public CalculatorNode GetCalculatorFromID(int calculatorID)
		{
			CalculatorNode result = null;
			if (dicCalculators.TryGetValue(calculatorID, out result))
			{
				if (result.nodeID == calculatorID)
				{
					return result;
				}
			}

			int calculatorCount = _Calculators.Count;
			for (int calculatorIndex = 0; calculatorIndex < calculatorCount; calculatorIndex++)
			{
				CalculatorNode calculator = _Calculators[calculatorIndex];
				if (calculator.nodeID == calculatorID)
				{
					dicCalculators.Add(calculator.nodeID, calculator);
					return calculator;
				}
			}

			return null;
		}

		bool IsUniqueNodeID(int nodeID)
		{
			return nodeID != 0 && GetNodeFromID(nodeID) == null;
		}

		int GetUniqueNodeID()
		{
			int count = _Nodes.Count;

			System.Random random = new System.Random(count);

			while (true)
			{
				int nodeID = random.Next();

				if ( IsUniqueNodeID(nodeID) )
				{
					return nodeID;
				}
			}
		}
		
		int GetUniqueBranchID()
		{
			int count = _CalculatorBranchies.Count;

			System.Random random = new System.Random(count);

			while (true)
			{
				int branchID = random.Next();

				if (branchID != 0 && GetCalculatorBranchFromID(branchID) == null)
				{
					return branchID;
				}
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// グループIDを指定して<see cref="Arbor.GroupNode" />を取得する。
		/// </summary>
		/// <param name="groupID">グループID</param>
		/// <returns>見つかった<see cref="Arbor.GroupNode" />。見つからなかった場合はnullを返す。</returns>
#else
		/// <summary>
		/// Gets <see cref="Arbor.GroupNode" /> from the group identifier.
		/// </summary>
		/// <param name="groupID">The group identifier.</param>
		/// <returns>Found <see cref = "Arbor.GroupNode" />. Returns null if not found.</returns>
#endif
		public GroupNode GetGroupFromID(int groupID)
		{
			GroupNode result = null;
			if (dicGroups.TryGetValue(groupID, out result))
			{
				if (result.nodeID == groupID)
				{
					return result;
				}
			}

			int groupCount = _Groups.Count;
			for (int groupIndex = 0; groupIndex < groupCount; groupIndex++)
			{
				GroupNode group = _Groups[groupIndex];
				if (group.nodeID == groupID)
				{
					dicGroups.Add(group.nodeID, group);
					return group;
				}
			}

			return null;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算ブランチIDを指定して<see cref="Arbor.CalculatorBranch" />を取得する。
		/// </summary>
		/// <param name="branchID">演算ブランチID</param>
		/// <returns>見つかった<see cref="Arbor.CalculatorBranch" />。見つからなかった場合はnullを返す。</returns>
#else
		/// <summary>
		/// Gets <see cref="Arbor.CalculatorBranch" /> from the calculator branch identifier.
		/// </summary>
		/// <param name="branchID">The calculator branch identifier.</param>
		/// <returns>Found <see cref = "Arbor.CalculatorBranch" />. Returns null if not found.</returns>
#endif
		public CalculatorBranch GetCalculatorBranchFromID(int branchID)
		{
			if (branchID == 0)
			{
				return null;
			}

			int branchCount = _CalculatorBranchies.Count;
			for (int branchIndex = 0; branchIndex < branchCount; branchIndex++)
			{
				CalculatorBranch branch = _CalculatorBranchies[branchIndex];
				if (branch.branchID == branchID)
				{
					return branch;
				}
			}

			return null;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// ステートを生成。
		/// </summary>
		/// <param name="nodeID">ノードID</param>
		/// <param name="resident">常駐するかどうかのフラグ。</param>
		/// <returns>生成したステート。ノードIDが重複している場合は生成せずにnullを返す。</returns>
#else
		/// <summary>
		/// Create state.
		/// </summary>
		/// <param name="nodeID">Node ID</param>
		/// <param name="resident">Resident whether flags.</param>
		/// <returns>The created state. If the node ID is not unique, return null without creating it.</returns>
#endif
		public State CreateState(int nodeID, bool resident)
		{
			if (!IsUniqueNodeID(nodeID))
			{
				Debug.LogWarning("CreateState id(" + nodeID + ") is not unique.");
				return null;
			}

			State state = new State(this, nodeID, resident);

			ComponentUtility.RecordObject(this, "Created State");

			_States.Add(state);
			_Nodes.Add(state);

			if (!resident && _StartStateID == 0)
			{
				_StartStateID = state.nodeID;
			}

			ComponentUtility.SetDirty(this);

			return state;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// ステートを生成。
		/// </summary>
		/// <param name="resident">常駐するかどうかのフラグ。</param>
		/// <returns>生成したステート。</returns>
#else
		/// <summary>
		/// Create state.
		/// </summary>
		/// <param name="resident">Resident whether flags.</param>
		/// <returns>The created state.</returns>
#endif
		public State CreateState(bool resident)
		{
			return CreateState(GetUniqueNodeID(), resident);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// ステートを生成。
		/// </summary>
		/// <returns>生成したステート。</returns>
#else
		/// <summary>
		/// Create state.
		/// </summary>
		/// <returns>The created state.</returns>
#endif
		public State CreateState()
		{
			return CreateState(false);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// コメントを生成。
		/// </summary>
		/// <param name="nodeID">ノードID</param>
		/// <returns>生成したコメント。ノードIDが重複している場合は生成せずにnullを返す。</returns>
#else
		/// <summary>
		/// Create comment.
		/// </summary>
		/// <param name="nodeID">Node ID</param>
		/// <returns>The created comment. If the node ID is not unique, return null without creating it.</returns>
#endif
		public CommentNode CreateComment(int nodeID)
		{
			if (!IsUniqueNodeID(nodeID))
			{
				Debug.LogWarning("CreateComment id(" + nodeID + ") is not unique.");
				return null;
			}

			CommentNode comment = new CommentNode(this, nodeID);

			ComponentUtility.RecordObject(this, "Created Comment");

			_Comments.Add(comment);
			_Nodes.Add(comment);

			ComponentUtility.SetDirty(this);

			return comment;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// コメントを生成。
		/// </summary>
		/// <returns>生成したコメント。</returns>
#else
		/// <summary>
		/// Create comment.
		/// </summary>
		/// <returns>The created comment.</returns>
#endif
		public CommentNode CreateComment()
		{
			return CreateComment(GetUniqueNodeID());
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算ノードを生成。
		/// </summary>
		/// <param name="nodeID">ノード ID</param>
		/// <param name="calculatorType">Calculatorの型</param>
		/// <returns>生成した演算ノード。ノードIDが重複している場合は生成せずにnullを返す。</returns>
#else
		/// <summary>
		/// Create calculator.
		/// </summary>
		/// <param name="nodeID">Node ID</param>
		/// <param name="calculatorType">Calculator type</param>
		/// <returns>The created calculator. If the node ID is not unique, return null without creating it.</returns>
#endif
		public CalculatorNode CreateCalculator(int nodeID,System.Type calculatorType)
		{
			if (!IsUniqueNodeID(nodeID))
			{
				Debug.LogWarning("CreateCalculator id(" + nodeID + ") is not unique.");
				return null;
			}

			CalculatorNode calculator = new CalculatorNode(this, nodeID, calculatorType);

			ComponentUtility.RecordObject(this, "Created Calculator");

			_Calculators.Add(calculator);
			_Nodes.Add(calculator);

			ComponentUtility.SetDirty(this);

			return calculator;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算ノードを生成。
		/// </summary>
		/// <param name="calculatorType">Calculatorの型</param>
		/// <returns>生成した演算ノード。</returns>
#else
		/// <summary>
		/// Create calculator.
		/// </summary>
		/// <param name="calculatorType">Calculator type</param>
		/// <returns>The created calculator.</returns>
#endif
		public CalculatorNode CreateCalculator(System.Type calculatorType)
		{
			return CreateCalculator(GetUniqueNodeID(), calculatorType);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// グループを生成。
		/// </summary>
		/// <param name="nodeID">ノード ID</param>
		/// <returns>生成したグループ。ノードIDが重複している場合は生成せずにnullを返す。</returns>
#else
		/// <summary>
		/// Create group.
		/// </summary>
		/// <param name="nodeID">Node ID</param>
		/// <returns>The created group. If the node ID is not unique, return null without creating it.</returns>
#endif
		public GroupNode CreateGroup(int nodeID)
		{
			if (!IsUniqueNodeID(nodeID))
			{
				Debug.LogWarning("CreateGroup id(" + nodeID + ") is not unique.");
				return null;
			}

			GroupNode group = new GroupNode(this, nodeID);

			ComponentUtility.RecordObject(this, "Created Group");

			_Groups.Add(group);
			_Nodes.Add(group);

			ComponentUtility.SetDirty(this);

			return group;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// グループを生成。
		/// </summary>
		/// <returns>生成したグループ。</returns>
#else
		/// <summary>
		/// Create group.
		/// </summary>
		/// <returns>The created group.</returns>
#endif
		public GroupNode CreateGroup()
		{
			return CreateGroup(GetUniqueNodeID());
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算ブランチを生成。
		/// </summary>
		/// <returns>生成した演算ノード。</returns>
#else
		/// <summary>
		/// Create calculator.
		/// </summary>
		/// <returns>The created calculator.</returns>
#endif
		public CalculatorBranch CreateCalculatorBranch()
		{
			CalculatorBranch branch = new CalculatorBranch();
			branch.branchID = GetUniqueBranchID();

			ComponentUtility.RecordObject(this, "Created Branch");

			_CalculatorBranchies.Add(branch);

			ComponentUtility.SetDirty(this);

			return branch;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// ステートを名前で検索。
		/// </summary>
		/// <param name="stateName">検索するステートの名前。</param>
		/// <returns>見つかったステート。ない場合はnullを返す。</returns>
#else
		/// <summary>
		/// Search state by name.
		/// </summary>
		/// <param name="stateName">The name of the search state.</param>
		/// <returns>Found state. Return null if not.</returns>
#endif
		public State FindState(string stateName)
		{
			return _States.Find(state => state.name == stateName);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// ステートを名前で検索。
		/// </summary>
		/// <param name="stateName">検索するステートの名前。</param>
		/// <returns>見つかったステートの配列。</returns>
#else
		/// <summary>
		/// Search state by name.
		/// </summary>
		/// <param name="stateName">The name of the search state.</param>
		/// <returns>Array of found state.</returns>
#endif
		public State[] FindStates(string stateName)
		{
			return _States.FindAll(state => state.name == stateName).ToArray();
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// NodeBehaviourが属しているノードの取得。
		/// </summary>
		/// <param name="behaviour">NodeBehaviour</param>
		/// <returns>NodeBehaviourが属しているノード。ない場合はnullを返す。</returns>
#else
		/// <summary>
		/// Acquisition of nodes NodeBehaviour belongs.
		/// </summary>
		/// <param name="behaviour">NodeBehaviour</param>
		/// <returns>Nodess NodeBehaviour belongs. Return null if not.</returns>
#endif
		public Node FindNodeContainsBehaviour(NodeBehaviour behaviour)
		{
			StateBehaviour stateBehaviour = behaviour as StateBehaviour;
			if (stateBehaviour != null)
			{
				return FindStateContainsBehaviour(stateBehaviour);
			}

			Calculator calculator = behaviour as Calculator;
			if(calculator != null)
			{
				return FindCalculator(calculator);
			}

			return null;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// StateBehaviourが属しているステートの取得。
		/// </summary>
		/// <param name="behaviour">StateBehaviour</param>
		/// <returns>StateBehaviourが属しているステート。ない場合はnullを返す。</returns>
#else
		/// <summary>
		/// Acquisition of states StateBehaviour belongs.
		/// </summary>
		/// <param name="behaviour">StateBehaviour</param>
		/// <returns>States StateBehaviour belongs. Return null if not.</returns>
#endif
		public State FindStateContainsBehaviour(StateBehaviour behaviour)
		{
			return _States.Find(state => state.Contains(behaviour));
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Calculatorが属しているCalculatorNodeの取得。
		/// </summary>
		/// <param name="calculator">Calculator</param>
		/// <returns> Calculatorが属しているCalculatorNode。ない場合はnullを返す。</returns>
#else
		/// <summary>
		/// Acquisition of CalculatorNodes Calculator belongs.
		/// </summary>
		/// <param name="calculator">Calculator</param>
		/// <returns>CalculatorNodes Calculator belongs. Return null if not.</returns>
#endif
		public CalculatorNode FindCalculator(Calculator calculator)
		{
			return _Calculators.Find(calculatorNode => calculatorNode.calculator == calculator);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 内部的に使用するメソッド。特に呼び出す必要はありません。
		/// </summary>
#else
		/// <summary>
		/// Method to be used internally. In particular there is no need to call.
		/// </summary>
#endif
		public void DisconnectCalculatorBranch(Object obj)
		{
			List<CalculatorBranch> branchies = new List<CalculatorBranch>();

			for (int branchIndex = 0; branchIndex < _CalculatorBranchies.Count; branchIndex++)
			{
				CalculatorBranch branch = GetCalculatorBranchFromIndex(branchIndex);
				if (branch.inBehaviour == obj || branch.outBehaviour == obj)
				{
					branchies.Add(branch);
				}
			}

			int branchCount = branchies.Count;
			for (int branchIndex = 0; branchIndex < branchCount; branchIndex++)
			{
				CalculatorBranch branch = branchies[branchIndex];
				DeleteCalculatorBranch(branch);
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// ステートの削除。
		/// </summary>
		/// <param name="state">削除するステート。</param>
		/// <returns>削除した場合にtrue</returns>
#else
		/// <summary>
		/// Delete state.
		/// </summary>
		/// <param name="state">State that you want to delete.</param>
		/// <returns>true if deleted</returns>
#endif
		public bool DeleteState(State state)
		{
			int stateID = state.nodeID;

			ComponentUtility.RegisterCompleteObjectUndo(this, "Delete Nodes");

			ComponentUtility.RecordObject(this, "Delete Nodes");
			_States.Remove(state);
			_Nodes.Remove(state);

			if (_DicStates != null)
			{
				_DicStates.Remove(state.nodeID);
			}

			if (_DicNodes != null)
			{
				_DicNodes.Remove(state.nodeID);
			}

			if (_StartStateID == stateID)
			{
				ComponentUtility.RecordObject(this, "Delete Nodes");
				_StartStateID = 0;
				ComponentUtility.SetDirty(this);
			}

			int stateCount = _States.Count;
			for (int stateIndex = 0; stateIndex < stateCount; stateIndex++)
			{
				State otherState = _States[stateIndex];
				if (otherState != state)
				{
					otherState.DisconnectState(stateID);
				}
			}

			state.DestroyBehaviours();

			ComponentUtility.SetDirty(this);

			return true;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// コメントの削除。
		/// </summary>
		/// <param name="comment">削除するコメント。</param>
#else
		/// <summary>
		/// Delete comment.
		/// </summary>
		/// <param name="comment">Comment that you want to delete.</param>
#endif
		public void DeleteComment(CommentNode comment)
		{
			ComponentUtility.RegisterCompleteObjectUndo(this, "Delete Nodes");

			ComponentUtility.RecordObject(this, "Delete Nodes");
			_Comments.Remove(comment);
			_Nodes.Remove(comment);

			if (_DicComments != null)
			{
				_DicComments.Remove(comment.nodeID);
			}

			if (_DicNodes != null)
			{
				_DicNodes.Remove(comment.nodeID);
			}

			ComponentUtility.SetDirty(this);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算ノードの削除。
		/// </summary>
		/// <param name="calculatorNode">削除する演算ノード。</param>
		/// <returns>削除した場合にtrue</returns>
#else
		/// <summary>
		/// Delete calculator.
		/// </summary>
		/// <param name="calculatorNode">Calculator that you want to delete.</param>
		/// <returns>true if deleted</returns>
#endif
		public bool DeleteCalculator(CalculatorNode calculatorNode)
		{
			Object calculatorObj = calculatorNode.GetObject();
			
			ComponentUtility.RegisterCompleteObjectUndo(this, "Delete Nodes");

			ComponentUtility.RecordObject(this, "Delete Nodes");
			_Calculators.Remove(calculatorNode);
			_Nodes.Remove(calculatorNode);

			if (_DicCalculators != null)
			{
				_DicCalculators.Remove(calculatorNode.nodeID);
			}

			if (_DicNodes != null)
			{
				_DicNodes.Remove(calculatorNode.nodeID);
			}

			DisconnectCalculatorBranch(calculatorObj);

			ComponentUtility.Destroy(calculatorObj);

			ComponentUtility.SetDirty(this);

			return true;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// グループの削除。
		/// </summary>
		/// <param name="group">削除するグループ。</param>
#else
		/// <summary>
		/// Delete group.
		/// </summary>
		/// <param name="group">Group that you want to delete.</param>
#endif
		public void DeleteGroup(GroupNode group)
		{
			ComponentUtility.RegisterCompleteObjectUndo(this, "Delete Nodes");

			ComponentUtility.RecordObject(this, "Delete Nodes");
			_Groups.Remove(group);
			_Nodes.Remove(group);

			if (_DicGroups != null)
			{
				_DicGroups.Remove(group.nodeID);
			}

			if (_DicNodes != null)
			{
				_DicNodes.Remove(group.nodeID);
			}

			ComponentUtility.SetDirty(this);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// ノードの削除。
		/// </summary>
		/// <param name="node">削除するノード。</param>
		/// <returns>削除した場合にtrue</returns>
#else
		/// <summary>
		/// Delete node.
		/// </summary>
		/// <param name="node">Node that you want to delete.</param>
		/// <returns>true if deleted</returns>
#endif
		public bool DeleteNode(Node node)
		{
			State state = node as State;
			if (state != null)
			{
				return DeleteState(state);
			}

			CommentNode commentNode = node as CommentNode;
			if (commentNode != null)
			{
				DeleteComment(commentNode);
				return true;
			}

			CalculatorNode calculatorNode = node as CalculatorNode;
			if (calculatorNode != null)
			{
				return DeleteCalculator(calculatorNode);
			}

			GroupNode groupNode = node as GroupNode;
			if (groupNode != null)
			{
				DeleteGroup(groupNode);
				return true;
			}

			return false;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算ブランチの削除。
		/// </summary>
		/// <param name="branch">削除する演算ブランチ。</param>
#else
		/// <summary>
		/// Delete calculator branch.
		/// </summary>
		/// <param name="branch">Calculator branch that you want to delete.</param>
#endif
		public void DeleteCalculatorBranch(CalculatorBranch branch)
		{
			List<Object> records = new List<Object>();
			records.Add(this);

			Object inBehaviour = branch.inBehaviour;
			if (inBehaviour != null && inBehaviour is MonoBehaviour)
			{
				records.Add(inBehaviour);
			}
			Object outBehaviour = branch.outBehaviour;
			if (outBehaviour != null && outBehaviour is MonoBehaviour)
			{
				records.Add(outBehaviour);
			}
			ComponentUtility.RecordObjects(records.ToArray(), "Delete Branch");

			OutputSlot outputSlot = branch.outputSlot;
			if (outputSlot != null)
			{
				outputSlot.branchIDs.Remove(branch.branchID);
			}

			InputSlot inputSlot = branch.inputSlot;
			if (inputSlot != null)
			{
				inputSlot.branchID = 0;
			}

			_CalculatorBranchies.Remove(branch);

			ComponentUtility.SetDirty(this);
			if (outBehaviour != null)
			{
				ComponentUtility.SetDirty(outBehaviour);
			}
			if (inBehaviour != null)
			{
				ComponentUtility.SetDirty(inBehaviour);
			}
		}
		
#if ARBOR_DOC_JA
		/// <summary>
		/// 内部的に使用するメソッド。特に呼び出す必要はありません。
		/// </summary>
#else
		/// <summary>
		/// Method to be used internally. In particular there is no need to call.
		/// </summary>
#endif
		public void Refresh()
		{
			int stateCount = _States.Count;
			for (int stateIndex = 0; stateIndex < stateCount; stateIndex++)
			{
				State state = _States[stateIndex];

				if (!(Application.isPlaying && isActiveAndEnabled))
				{
					for (int behaviourIndex = 0; behaviourIndex < state.behaviourCount; behaviourIndex++)
					{
						StateBehaviour behaviour = state.GetBehaviourFromIndex(behaviourIndex);
						if (behaviour != null && behaviour.enabled)
						{
							behaviour.enabled = false;
						}
					}
				}

				state.SetHideFlags(HideFlags.HideInHierarchy | HideFlags.HideInInspector);
			}

			int calculatorCount = _Calculators.Count;
			for (int calculatorIndex = 0; calculatorIndex < calculatorCount; calculatorIndex++)
			{
				CalculatorNode calculatorNode = _Calculators[calculatorIndex];
				Object calculatorObj = calculatorNode.GetObject();
				if (ComponentUtility.IsValidObject(calculatorObj))
				{
#if !ARBOR_DEBUG
					calculatorObj.hideFlags |= HideFlags.HideInHierarchy | HideFlags.HideInInspector;
#endif
				}
			}

			List<CalculatorBranch> deleteBranchies = new List<CalculatorBranch>();

			int branchCount = calculatorBranchCount;
			for (int branchIndex = 0; branchIndex < branchCount; branchIndex++)
			{
				CalculatorBranch branch = GetCalculatorBranchFromIndex(branchIndex);
				if (branch.branchID == 0)
				{
					Debug.LogError("branch id 0");
				}
				branch.Refresh();
				if (branch.inBehaviour is MonoBehaviour && branch.inputSlot == null ||
					branch.outBehaviour is MonoBehaviour && branch.outputSlot == null)
				{
					deleteBranchies.Add(branch);
				}
			}

			foreach (CalculatorBranch branch in deleteBranchies)
			{
				DeleteCalculatorBranch(branch);
			}
		}


		void MoveBranch(CalculatorBranch branch)
		{
			Object inBehaviour = branch.inBehaviour;
			Object outBehaviour = branch.outBehaviour;

			int stateCount = _States.Count;
			for (int stateIndex = 0; stateIndex < stateCount; stateIndex++)
			{
				State state = _States[stateIndex];
				int behaviourCount = state.behaviourCount;
				for (int behaviourIndex = 0; behaviourIndex < behaviourCount; behaviourIndex++)
				{
					StateBehaviour behaviour = state.GetBehaviourFromIndex(behaviourIndex);

					int slotCount = behaviour.calculatorSlotFieldCount;
					for (int slotIndex = 0; slotIndex < slotCount; slotIndex++)
					{
						CalculatorSlotField slotInfo = behaviour.GetCalculatorSlotField(slotIndex);
						CalculatorSlot s = slotInfo.slot;
						if (s is InputSlot)
						{
							InputSlot inputSlot = s as InputSlot;
							if (inputSlot.branchID == branch.branchID)
							{
								inBehaviour = behaviour;
							}
						}
						else if (s is OutputSlot)
						{
							OutputSlot outputSlot = s as OutputSlot;
							if (outputSlot.branchIDs.Contains(branch.branchID))
							{
								outBehaviour = behaviour;
							}
						}
					}
				}
			}

			int calculatorCount = _Calculators.Count;
			for (int calculatorIndex = 0; calculatorIndex < calculatorCount; calculatorIndex++)
			{
				CalculatorNode calculatorNode = _Calculators[calculatorIndex];

				Calculator calculator = calculatorNode.calculator;

				int slotCount = calculator.calculatorSlotFieldCount;
				for (int slotIndex = 0; slotIndex < slotCount; slotIndex++)
				{
					CalculatorSlotField slotInfo = calculator.GetCalculatorSlotField(slotIndex);
					CalculatorSlot s = slotInfo.slot;
					if (s is InputSlot)
					{
						InputSlot inputSlot = s as InputSlot;
						if (inputSlot.branchID == branch.branchID)
						{
							inBehaviour = calculator;
						}
					}
					else if (s is OutputSlot)
					{
						OutputSlot outputSlot = s as OutputSlot;
						if (outputSlot.branchIDs.Contains(branch.branchID))
						{
							outBehaviour = calculator;
						}
					}
				}
			}

			branch.SetBehaviour(inBehaviour, outBehaviour);

			branch.inputSlot.stateMachine = branch.outputSlot.stateMachine = this;
		}

		private bool _IsEditor = false;

		bool IsMove()
		{
			if (_IsEditor)
			{
				return false;
			}

			int stateCount = _States.Count;
			for (int stateIndex = 0; stateIndex < stateCount; stateIndex++)
			{
				State state = _States[stateIndex];
				if (!state.IsStateMachine(this))
				{
					return true;
				}
			}

			int calculatorCount = _Calculators.Count;
			for (int calculatorIndex = 0; calculatorIndex < calculatorCount; calculatorIndex++)
			{
				CalculatorNode calculatorNode = _Calculators[calculatorIndex];
				if (calculatorNode.stateMachine != this)
				{
					return true;
				}
				else
				{
					if (calculatorNode.calculator != null && calculatorNode.calculator.stateMachine != this)
					{
						return true;
					}
				}
			}

			int commentCount = _Comments.Count;
			for (int commentIndex = 0; commentIndex < commentCount; commentIndex++)
			{
				CommentNode commentNode = _Comments[commentIndex];
				if (commentNode.stateMachine != this)
				{
					return true;
				}
			}

			int groupCount = _Groups.Count;
			for (int groupIndex = 0; groupIndex < groupCount; groupIndex++)
			{
				GroupNode groupNode = _Groups[groupIndex];
				if (groupNode.stateMachine != this)
				{
					return true;
				}
			}

			return false;
		}

		void DestroyUnusedNodeBehaviour(NodeBehaviour behaviour)
		{
			if (behaviour.stateMachine != this)
			{
				return;
			}

			StateBehaviour stateBehaviour = behaviour as StateBehaviour;
			if (stateBehaviour != null)
			{
				State state = FindStateContainsBehaviour(stateBehaviour);
				if (state == null)
				{
					ComponentUtility.DelayDestroy(stateBehaviour);
				}

				return;
			}

			Calculator calculator = behaviour as Calculator;
			if (calculator != null)
			{
				CalculatorNode calculatorNode = FindCalculator(calculator);
				if (calculatorNode == null)
				{
					ComponentUtility.DelayDestroy(calculator);
				}

				return;
			}
		}

		[System.Reflection.Obfuscation(Exclude = true)]
		void OnValidate()
		{
			if (_DicStates != null)
			{
				_DicStates.Clear();
			}
			if (_DicComments != null)
			{
				_DicComments.Clear();
			}
			if (_DicCalculators != null)
			{
				_DicCalculators.Clear();
			}
			if (_DicGroups != null)
			{
				_DicGroups.Clear();
			}
			if (_DicNodes != null)
			{
				_DicNodes.Clear();
			}

			if (IsMove())
			{
				int stateCount = _States.Count;
				for (int stateIndex = 0; stateIndex < stateCount; stateIndex++)
				{
					State state = _States[stateIndex];
					state.Move(this);
				}

				int commentCount = _Comments.Count;
				for (int commentIndex = 0; commentIndex < commentCount; commentIndex++)
				{
					CommentNode commentNode = _Comments[commentIndex];
					commentNode.Move(this);
				}

				int calculatorCount = _Calculators.Count;
				for (int calculatorIndex = 0; calculatorIndex < calculatorCount; calculatorIndex++)
				{
					CalculatorNode calculatorNode = _Calculators[calculatorIndex];
					calculatorNode.Move(this);
				}

				int groupCount = _Groups.Count;
				for (int groupIndex = 0; groupIndex < groupCount; groupIndex++)
				{
					GroupNode groupNode = _Groups[groupIndex];
					groupNode.Move(this);
				}

				int branchCount = _CalculatorBranchies.Count;
				for (int branchIndex = 0; branchIndex < branchCount; branchIndex++)
				{
					CalculatorBranch branch = _CalculatorBranchies[branchIndex];

					MoveBranch(branch);
				}

				DestroyUnusedNodeBehaviour();
			}

			ComponentUtility.RefreshStateMachine(this);
		}

		void DestroyUnusedNodeBehaviour()
		{
			foreach (NodeBehaviour behaviour in GetComponents<NodeBehaviour>())
			{
				DestroyUnusedNodeBehaviour(behaviour);
			}
		}

		[System.Reflection.Obfuscation(Exclude = true)]
		void Start()
		{
			Refresh();

			int stateCount = _States.Count;
			for (int stateIndex = 0; stateIndex < stateCount; stateIndex++)
			{
				State state = _States[stateIndex];
				if (state.resident)
				{
					state.EnableState(true, true);
				}
			}

			if ( !NextState() )
			{
				State nextState = GetStateFromID(_StartStateID);
				ChangeState(nextState);
			}

			_IsStarted = true;
		}

		[System.Reflection.Obfuscation(Exclude = true)]
		void OnEnable()
		{
			if (!_IsStarted)
			{
				return;
			}

			int stateCount = _States.Count;
			for (int stateIndex = 0; stateIndex < stateCount; stateIndex++)
			{
				State state = _States[stateIndex];
				if (state.resident)
				{
					state.EnableState(true, false);
				}
			}

			if ( !NextState() )
			{
				if (_CurrentState != null)
				{
					_CurrentState.EnableState(true, false);
				}
			}
		}

		[System.Reflection.Obfuscation(Exclude = true)]
		void OnDisable()
		{
			if (!_IsStarted)
			{
				return;
			}

			if (_CurrentState != null)
			{
				_CurrentState.EnableState(false, false);
			}

			int stateCount = _States.Count;
			for (int stateIndex = 0; stateIndex < stateCount; stateIndex++)
			{
				State state = _States[stateIndex];
				if (state.resident)
				{
					state.EnableState(false, false);
				}
			}
		}

		bool NextState()
		{
			if (_NextState != null)
			{
				State nextState = _NextState;
				_NextState = null;

				ChangeState(nextState);

				return true;
			}

			return false;
		}

		void UpdateState()
		{
			int stateCount = _States.Count;
			for (int stateIndex = 0; stateIndex < stateCount; stateIndex++)
			{
				State state = _States[stateIndex];
				if (state.resident)
				{
					state.UpdateBehaviours();
				}
			}

			if (_CurrentState != null)
			{
				_CurrentState.UpdateBehaviours();
			}
		}

		void LateUpdateState()
		{
			int stateCount = _States.Count;
			for (int stateIndex = 0; stateIndex < stateCount; stateIndex++)
			{
				State state = _States[stateIndex];
				if (state.resident)
				{
					state.LateUpdateBehaviours();
				}
			}

			if (_CurrentState != null)
			{
				_CurrentState.LateUpdateBehaviours();
			}
		}

		[System.Reflection.Obfuscation(Exclude = true)]
		void Update()
		{
			UpdateState();
		}

		[System.Reflection.Obfuscation(Exclude = true)]
		void LateUpdate()
		{
			LateUpdateState();

			NextState();

#if ARBOR_TRIAL
			if( !Application.isEditor )
			{
				Trial.TrialGUI.UpdateIfDirty();
			}
#endif
		}

#if ARBOR_TRIAL
		private bool m_LimitLogDisplayed = false;
#endif

		private bool m_ChangingState = false;
		private List<string> m_Triggers = new List<string>();

		void ChangeState(State nextState)
		{
#if ARBOR_TRIAL
			if( Trial.TrialGUI.IsTrialLimitTime() )
			{
				if( !m_LimitLogDisplayed )
				{
					Debug.LogError( "In the trial version, the state can transition within 2 minutes from the start." );
					m_LimitLogDisplayed = true;
				}
				return;
			}
#endif
			m_ChangingState = true;

			if (_CurrentState != null)
			{
				_CurrentState.EnableState(false, true);
			}

			_CurrentState = nextState;

			if (_StateLinkHistory.Count > 0)
			{
				_StateLinkHistory.First.Value.transitionCount++;
			}

			if (_CurrentState != null)
			{
				_CurrentState.EnableState(true, true);
			}

			m_ChangingState = false;

			foreach (string message in m_Triggers)
			{
				SendTrigger(message);
			}
			m_Triggers.Clear();
		}

		private bool TransitionInternal(State nextState, TransitionTiming transitionTiming)
		{
			if (nextState != null && nextState.stateMachine == this && !nextState.resident)
			{
				switch (transitionTiming)
				{
					case TransitionTiming.Immediate:
						if (isActiveAndEnabled)
						{
							_NextState = null;
							ChangeState(nextState);
						}
						else
						{
							_NextState = nextState;
						}
						break;
					case TransitionTiming.LateUpdateDontOverwrite:
						if (_NextState == null)
						{
							_NextState = nextState;
						}
						break;
					case TransitionTiming.LateUpdateOverwrite:
						_NextState = nextState;
						break;
				}
				return true;
			}

			return false;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 状態遷移
		/// </summary>
		/// <param name="nextState">遷移先のステート。</param>
		/// <param name="transitionTiming">遷移するタイミング。</param>
		/// <returns>遷移できたかどうか</returns>
#else
		/// <summary>
		/// State transition
		/// </summary>
		/// <param name="nextState">Destination state.</param>
		/// <param name="transitionTiming">Transition timing.</param>
		/// <returns>Whether or not the transition</returns>
#endif
		public bool Transition(State nextState, TransitionTiming transitionTiming)
		{
			_StateLinkHistory.Clear();

			return TransitionInternal(nextState, transitionTiming);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 状態遷移
		/// </summary>
		/// <param name="nextState">遷移先のステート。</param>
		/// <param name="immediateTransition">すぐに遷移するかどうか。falseの場合は現在フレームの最後(LateUpdate時)に遷移する。</param>
		/// <returns>遷移できたかどうか</returns>
#else
		/// <summary>
		/// State transition
		/// </summary>
		/// <param name="nextState">Destination state.</param>
		/// <param name="immediateTransition">Whether or not to transition immediately. If false I will transition to the end of the current frame (when LateUpdate).</param>
		/// <returns>Whether or not the transition</returns>
#endif
		[System.Obsolete("use Transition(State nextState,TransitionTiming transitionTiming).")]
		public bool Transition(State nextState, bool immediateTransition)
		{
			_StateLinkHistory.Clear();

			return TransitionInternal(nextState, immediateTransition? TransitionTiming.Immediate : TransitionTiming.LateUpdateOverwrite );
		}


#if ARBOR_DOC_JA
		/// <summary>
		/// 状態遷移する。実際に遷移するタイミングは現在フレームの最後(LateUpdate時)。
		/// </summary>
		/// <param name="nextState">遷移先のステート。</param>
		/// <returns>遷移できたかどうか</returns>
#else
		/// <summary>
		/// State transition. Timing to actually transition current frame last (when LateUpdate).
		/// </summary>
		/// <param name="nextState">Destination state.</param>
		/// <returns>Whether or not the transition</returns>
#endif
		public bool Transition(State nextState)
		{
			_StateLinkHistory.Clear();

			return TransitionInternal(nextState, TransitionTiming.LateUpdateOverwrite );
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 状態遷移
		/// </summary>
		/// <param name="nextStateID">遷移先のステートID。</param>
		/// <param name="transitionTiming">遷移するタイミング。</param>
		/// <returns>遷移できたかどうか</returns>
#else
		/// <summary>
		/// State transition
		/// </summary>
		/// <param name="nextStateID">State ID for the transition destination.</param>
		/// <param name="transitionTiming">Transition timing.</param>
		/// <returns>Whether or not the transition</returns>
#endif
		public bool Transition(int nextStateID, TransitionTiming transitionTiming)
		{
			_StateLinkHistory.Clear();

			State nextState = GetStateFromID(nextStateID);
			return TransitionInternal(nextState, transitionTiming);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 状態遷移
		/// </summary>
		/// <param name="nextStateID">遷移先のステートID。</param>
		/// <param name="immediateTransition">すぐに遷移するかどうか。falseの場合は現在フレームの最後(LateUpdate時)に遷移する。</param>
		/// <returns>遷移できたかどうか</returns>
#else
		/// <summary>
		/// State transition
		/// </summary>
		/// <param name="nextStateID">State ID for the transition destination.</param>
		/// <param name="immediateTransition">Whether or not to transition immediately. If false I will transition to the end of the current frame (when LateUpdate).</param>
		/// <returns>Whether or not the transition</returns>
#endif
		[System.Obsolete("use Transition(int nextStateID,TransitionTiming transitionTiming).")]
		public bool Transition(int nextStateID, bool immediateTransition)
		{
			return Transition(nextStateID, immediateTransition? TransitionTiming.Immediate : TransitionTiming.LateUpdateOverwrite );
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 状態遷移する。実際に遷移するタイミングは現在フレームの最後(LateUpdate時)。
		/// </summary>
		/// <param name="nextStateID">遷移先のステートID。</param>
		/// <returns>遷移できたかどうか</returns>
#else
		/// <summary>
		/// State transition. Timing to actually transition current frame last (when LateUpdate).
		/// </summary>
		/// <param name="nextStateID">State ID for the transition destination.</param>
		/// <returns>Whether or not the transition</returns>
#endif
		public bool Transition(int nextStateID)
		{
			return Transition(nextStateID, TransitionTiming.LateUpdateOverwrite);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 状態遷移
		/// </summary>
		/// <param name="nextStateLink">遷移の接続先。</param>
		/// <param name="transitionTiming">遷移するタイミング。</param>
		/// <returns>遷移できたかどうか</returns>
#else
		/// <summary>
		/// State transition
		/// </summary>
		/// <param name="nextStateLink">The destination of transition.</param>
		/// <param name="transitionTiming">Transition timing.</param>
		/// <returns>Whether or not the transition</returns>
#endif
		public bool Transition(StateLink nextStateLink, TransitionTiming transitionTiming)
		{
			if (nextStateLink.stateID != 0)
			{
				if (_StateLinkHistory.Contains(nextStateLink))
				{
					_StateLinkHistory.Remove(nextStateLink);
				}
				_StateLinkHistory.AddFirst(nextStateLink);
				while (_StateLinkHistory.Count > 5)
				{
					_StateLinkHistory.RemoveLast();
				}

				State nextState = GetStateFromID(nextStateLink.stateID);
				return TransitionInternal(nextState, transitionTiming);
			}

			return false;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 状態遷移
		/// </summary>
		/// <param name="nextStateLink">遷移の接続先。</param>
		/// <param name="immediateTransition">すぐに遷移するかどうか。falseの場合は現在フレームの最後(LateUpdate時)に遷移する。</param>
		/// <returns>遷移できたかどうか</returns>
#else
		/// <summary>
		/// State transition
		/// </summary>
		/// <param name="nextStateLink">The destination of transition.</param>
		/// <param name="immediateTransition">Whether or not to transition immediately. If false I will transition to the end of the current frame (when LateUpdate).</param>
		/// <returns>Whether or not the transition</returns>
#endif
		[System.Obsolete("use Transition(StateLink nextStateLink,TransitionTiming transitionTiming).")]
		public bool Transition(StateLink nextStateLink, bool immediateTransition)
		{
			return Transition(nextState, immediateTransition? TransitionTiming.Immediate : TransitionTiming.LateUpdateOverwrite );
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 状態遷移する。実際に遷移するタイミングは現在フレームの最後(LateUpdate時)。
		/// </summary>
		/// <param name="nextStateLink">遷移の接続先。</param>
		/// <returns>遷移できたかどうか</returns>
#else
		/// <summary>
		/// State transition. Timing to actually transition current frame last (when LateUpdate).
		/// </summary>
		/// <param name="nextStateLink">The destination of transition.</param>
		/// <returns>Whether or not the transition</returns>
#endif
		public bool Transition(StateLink nextStateLink)
		{
			return Transition(nextStateLink, nextStateLink.transitionTiming);
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// トリガーの送信
		/// </summary>
		/// <param name="message">送信するメッセージ</param>
#else
		/// <summary>
		/// Sending of trigger
		/// </summary>
		/// <param name="message">Message to be sent</param>
#endif
		public void SendTrigger(string message)
		{
			if (!gameObject.activeInHierarchy || !enabled)
			{
				return;
			}

			if (m_ChangingState)
			{
				m_Triggers.Add(message);
				return;
			}

			int stateCount = _States.Count;
			for (int stateIndex = 0; stateIndex < stateCount; stateIndex++)
			{
				State state = _States[stateIndex];
				if (state.resident)
				{
					state.SendTrigger(message);
				}
			}

			if (_CurrentState != null)
			{
				_CurrentState.SendTrigger(message);
			}
		}

		[System.Reflection.Obfuscation(Exclude = true)]
		void OnDestroy()
		{
			DestroySubComponents();
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 内部的に使用するメソッド。特に呼び出す必要はありません。
		/// </summary>
#else
		/// <summary>
		/// Method to be used internally. In particular there is no need to call.
		/// </summary>
#endif
		public void DestroySubComponents()
		{
			int stateCount = _States.Count;
			for (int stateIndex = 0; stateIndex < stateCount; stateIndex++)
			{
				State state = _States[stateIndex];
				state.DestroySubComponents();
			}
		}

		void ISerializationCallbackReceiver.OnBeforeSerialize()
		{
		}

		void ISerializationCallbackReceiver.OnAfterDeserialize()
		{
			if (_DicNodes != null)
			{
				_DicNodes.Clear();
			}
			_Nodes.Clear();

			for (int i = 0, count = _States.Count; i < count; i++)
			{
				_Nodes.Add(_States[i]);
			}

			for (int i = 0, count = _Calculators.Count; i < count; i++)
			{
				_Nodes.Add(_Calculators[i]);
			}

			for (int i = 0, count = _Comments.Count; i < count; i++)
			{
				_Nodes.Add(_Comments[i]);
			}

			for (int i = 0, count = _Groups.Count; i < count; i++)
			{
				_Nodes.Add(_Groups[i]);
			}
		}
	}
}
