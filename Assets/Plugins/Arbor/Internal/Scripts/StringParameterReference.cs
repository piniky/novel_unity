﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Stringパラメータの参照。
	/// </summary>
#else
	/// <summary>
	/// Reference String parameters.
	/// </summary>
#endif
	[System.Serializable]
	public class StringParameterReference : ParameterReference
	{
	}
}
