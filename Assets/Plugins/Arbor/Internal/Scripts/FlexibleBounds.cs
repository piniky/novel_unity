﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 参照方法が複数ある柔軟なBounds型を扱うクラス。
	/// </summary>
#else
	/// <summary>
	/// Class to handle a flexible Bounds type reference method there is more than one.
	/// </summary>
#endif
	[System.Serializable]
	public class FlexibleBounds
	{
#if ARBOR_DOC_JA
		/// <summary>
		/// 参照タイプ
		/// </summary>
#else
		/// <summary>
		/// Reference type
		/// </summary>
#endif
		public enum Type
		{
#if ARBOR_DOC_JA
			/// <summary>
			/// 定数
			/// </summary>
#else
			/// <summary>
			/// Constant
			/// </summary>
#endif
			Constant,

#if ARBOR_DOC_JA
			/// <summary>
			/// パラメータ
			/// </summary>
#else
			/// <summary>
			/// Parameter
			/// </summary>
#endif
			Parameter,

#if ARBOR_DOC_JA
			/// <summary>
			/// 演算スロット
			/// </summary>
#else
			/// <summary>
			/// Calculator slot
			/// </summary>
#endif
			Calculator,
		};

		[System.Reflection.Obfuscation( Exclude = true )]
		[SerializeField] private Type _Type = Type.Constant;

		[System.Reflection.Obfuscation( Exclude = true )]
		[SerializeField] private Bounds _Value;

		[System.Reflection.Obfuscation( Exclude = true )]
		[SerializeField] private BoundsParameterReference _Parameter;

		[System.Reflection.Obfuscation( Exclude = true )]
		[SerializeField] private InputSlotBounds _Slot;

#if ARBOR_DOC_JA
		/// <summary>
		/// Typeを返す
		/// </summary>
#else
		/// <summary>
		/// It returns a type
		/// </summary>
#endif
		public Type type
		{
			get
			{
				return _Type;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Parameterを返す。TypeがParameter以外の場合はnull。
		/// </summary>
#else
		/// <summary>
		/// It return a Paramter. It is null if Type is other than Parameter.
		/// </summary>
#endif
		public Parameter parameter
		{
			get
			{
				if (_Type == Type.Parameter)
				{
					return _Parameter.parameter;
				}
				return null;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 値を返す
		/// </summary>
#else
		/// <summary>
		/// It returns a value
		/// </summary>
#endif
		public Bounds value
		{
			get
			{
				Bounds value = new Bounds();
				switch (_Type)
				{
					case Type.Constant:
						value = _Value;
						break;
					case Type.Parameter:
						if (_Parameter.parameter != null)
						{
							value = _Parameter.parameter.boundsValue;
						}
						break;
					case Type.Calculator:
						_Slot.GetValue(ref value);
						break;
				}

				return value;
			}
		}

		public FlexibleBounds(Bounds value)
		{
			_Type = Type.Constant;
			_Value = value;
		}

		public FlexibleBounds(BoundsParameterReference parameter)
		{
			_Type = Type.Parameter;
			_Parameter = parameter;
		}

		public FlexibleBounds(InputSlotBounds slot)
		{
			_Type = Type.Calculator;
			_Slot = slot;
		}

		public static explicit operator Bounds(FlexibleBounds af)
		{
			return af.value;
		}

		public static explicit operator FlexibleBounds(Bounds value)
		{
			return new FlexibleBounds(value);
		}
	}
}
