﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 参照方法が複数ある柔軟なbool型を扱うクラス。
	/// </summary>
#else
	/// <summary>
	/// Class to handle a flexible bool type reference method there is more than one.
	/// </summary>
#endif
	[System.Serializable]
	public class FlexibleString
	{
#if ARBOR_DOC_JA
		/// <summary>
		/// 参照タイプ
		/// </summary>
#else
		/// <summary>
		/// Reference type
		/// </summary>
#endif
		public enum Type
		{
#if ARBOR_DOC_JA
			/// <summary>
			/// 定数
			/// </summary>
#else
			/// <summary>
			/// Constant
			/// </summary>
#endif
			Constant,

#if ARBOR_DOC_JA
			/// <summary>
			/// パラメータ
			/// </summary>
#else
			/// <summary>
			/// Parameter
			/// </summary>
#endif
			Parameter,
			
#if ARBOR_DOC_JA
			/// <summary>
			/// 演算スロット
			/// </summary>
#else
			/// <summary>
			/// Calculator slot
			/// </summary>
#endif
			Calculator,
		};

		[System.Reflection.Obfuscation( Exclude = true )]
		[SerializeField] private Type _Type = Type.Constant;

		[System.Reflection.Obfuscation( Exclude = true )]
		[SerializeField]
		private string _Value;

		[System.Reflection.Obfuscation( Exclude = true )]
		[SerializeField] private StringParameterReference _Parameter;

		[System.Reflection.Obfuscation( Exclude = true )]
		[SerializeField] private InputSlotString _Slot;

#if ARBOR_DOC_JA
		/// <summary>
		/// Typeを返す
		/// </summary>
#else
		/// <summary>
		/// It returns a type
		/// </summary>
#endif
		public Type type
		{
			get
			{
				return _Type;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// Parameterを返す。TypeがParameter以外の場合はnull。
		/// </summary>
#else
		/// <summary>
		/// It return a Paramter. It is null if Type is other than Parameter.
		/// </summary>
#endif
		public Parameter parameter
		{
			get
			{
				if (_Type == Type.Parameter)
				{
					return _Parameter.parameter;
				}
				return null;
			}
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 値を返す
		/// </summary>
#else
		/// <summary>
		/// It returns a value
		/// </summary>
#endif
		public string value
		{
			get
			{
				string value = string.Empty;
				switch (_Type)
				{
					case Type.Constant:
						value = _Value;
						break;
					case Type.Parameter:
						if (_Parameter.parameter != null)
						{
							value= _Parameter.parameter.stringValue;
						}
						break;
					case Type.Calculator:
						_Slot.GetValue(ref value);
						break;
				}

				return value;
			}
		}

		public FlexibleString(string value)
		{
			_Type = Type.Constant;
			_Value = value;
		}

		public FlexibleString(StringParameterReference parameter)
		{
			_Type = Type.Parameter;
			_Parameter = parameter;
		}
		
		public FlexibleString(InputSlotString slot)
		{
			_Type = Type.Calculator;
			_Slot = slot;
		}

		public static explicit operator string (FlexibleString ab)
		{
			return ab.value;
		}

		public static explicit operator FlexibleString(string value)
		{
			return new FlexibleString(value);
		}
	}
}
