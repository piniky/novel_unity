﻿-----------------------------------------------------
            Arbor: State Diagram Editor
          Copyright (c) 2014 Cait Sith Ware
          http://caitsithware.com/wordpress/
          support@caitsithware.com
-----------------------------------------------------

Thank you for purchasing Arbor! 

[How to Update] 

1. Please be sure to make a backup of the project before the update.
2. Just in case, we will leave to create a new scene from the menu of the File> New Scene.
3. Already Removed Arbor folder that have been imported.
4. Import Arbor.

[Main flow]

1. Attaching the ArborFSM to GameObject. 
2. Click the Open Editor button in the Inspector of ArborFSM 
3. Create a State in Arbor Editor. 
4. Attaching the Behaviour in State. 
5. Connect the transition to the State from Behaviour.

[Sample scene] 

Here are the sample scene. 
Assets/Arbor/Examples/Scenes/

[Document] 

Click here for detailed document. 
http://arbor.caitsithware.com/en/

(Please select from the sidebar if you want to English)

[Support]

Forum : http://forum-arbor.caitsithware.com/

Mail : support@caitsithware.com

[Update History] 

Ver 2.2.2:

* Arbor Editor
- Add : Added so that nodes and objects using deleted StateBehaviour and Calculator scripts can be deleted.
- Fix : Fixed an exception (ArgumentNullException) occurred in the Arbor Editor window when deleting StateBehaviour and Calculator scripts used by ArborFSM.

* Other
- Change : Raised Unity's minimum action version to 5.4.0f3.

Ver 2.2.1:

* Arbor Editor
- Change : FlexibleString's Constant indication back.
- Fix : Fixed occurrence of an exception (NullReferenceException: SerializedObject of SerializedProperty has been Disposed.) When playing on the Unity editor is open with Arbor Editor open.
- Fix : Fixed that Label of Graph does not change even when switching languages.

* Component
- Add : Long added to the parameters that can be used in ParameterContainer.
- Change : Changed so that parameters can be rearranged by ParameterContainer.
- Change : Display parameter type in ParameterContainer.

* Built in Calculator
- Add : Added LongAddCalculator
- Add : Added LongSubCalculator
- Add : Added LongMulCalculator
- Add : Added LongDivCalculator
- Add : Added LongNegativeCalculator
- Add : Added LongCompareCalculator
- Add : Added LongToFloatCalculator
- Add : Added FloatToLongCalculator

* Script
- Add : Added ConstantMultilineAttribute which makes multiple lines of FlexibleString Constant display.
- Add : Added FlexibleLong, LongParameterReference, InputSlotLong, OutputSlotLong along with the addition of long type parameter.
- Fix : Fixed load was being applied by type enumeration processing at the beginning of play on Unity editor.

Ver 2.2.0:

* Arbor Editor
- Add : Added group node.
- Add : Added comments for each node.
- Add : Added node cutout.
- Add : Added items for cutting, copying, duplicating, deleting nodes in node context menu.
- Add : Transition Timing icon display on StateLink button.
- Add : Copy Component of ArborFSM Inspector supports copying including internal components.
- Change : Editor design change.
- Change : Changed to display state name input field with double click.
- Change : Changed to sort the state list in order of type (start state -> normal state -> resident state).
- Change : Change Immediate Transition of StateLinkSettingWindow to Transition Timing.
- Change : Added type specification to OutputSlotComponent.
- Change : Change the display when Constant of FlexibleString to TextArea.
- Change : Corresponds to scroll at fixed time intervals while scrolling with dragging.
- Change : Adjust the maximum amount of movement during drag scrolling.
- Fix : Fixed that node position shifted when scrolling while dragging a node
- Fix : Correct that vertex number error occurs when CalculatorBranch line is long.
- Fix : Fixed that vertical margin is displayed when StateLinkSettingWindow is displayed at the screen edge.
- Fix : Fixed that lines are hidden when StateLink is reconnected to the same state.
- Fix : Fixed that StateBehaviour appears in the inspector when Apply to the Prefab of ArborFSM object during execution.
- Fix : Fixed that StateBehaviour of the current state is saved in Prefab while it is in the effective state when Apply to the Prefab of ArborFSM object during execution.
- Fix : Corrected the connection when changing Size in Behavior which has CalculatorSlot as an array.
- Fix : Fixed that other nodes are not displayed for a moment when node is deleted.
- Other : Optimize display of dot line of CalculatorBranch.

* Built in Behaviour
- Add : Added AgentStop.
- Add : Added AnimatorCrossFade.
- Add : Added AnimatorSetLayerWeight.
- Add : Added BackToStartState.
- Add : Added ActivateBehaviour.
- Add : Added ActivateRenderer.
- Add : Added ActivateCollider.
- Add : Added ChangeTimingUpdate parameter to UISetSlider, UISetText, UISetToggle.
- Change : Added flag to whether LookAtGameObject uses each coordinate component of Target transform.
- Change : Added flag to determine whether to stop Agent when leaving state to Agent system.
- Change : Added reference URL for format specification to the format document of UITextFromParameter.
- Change : Built-in behavior referring to Component corresponds to FlexibleComponent.
- Change : Move UISetSliderFromParameter and UISetToggleFromParameter to Lagacy by FlexibleComponent correspondence of UISetSlider and UISetToggle.
- Fix : Fixed that exceptions are generated after transition of scenes with behavior being processed when Parameter is changed via GlobalParameterContainer (UISetTextFromParameter, UISetToggleFromParameter, UISetSliderFromParameter, ParameterTransition).
- Fix : Fixed that there was no UISetImage reference.

* Built in Calculator
- Add : Added GameObjectGetComponentCalculator.
- Fix : Fixed an exception occurred after the scene transition in the Calculator processing when Parameter was changed via GlobalParameterContainer.

* Component
- Add : Added various parameters for Animator setting to AgentController.
- Add : Added Waypoint component.
- Add : Added Component to parameters available in ParameterContainer.
- Change : Change Animator specification of AgentController so that it is unified for each parameter.

* Script
- Add : Add parameter reference class for each Animator type.
- Add : Addition of OnStateUpdate callback called during Update in order of StateBehaviour.
- Add : Addition of OnStateLateUpdate callback called during LateUpdate in order of StateBehaviour.
- Add : InputSlotUnityObject, OutputSlotUnityObject added.
- Add : Added FlexibleComponent.
- Add : Added SlotTyeAttribute which can specify type with CalculatorSlot or FlexibleCompoment.
- Add : ComponentParameterReference added.
- Add : Create a NodeBehaviour class that summarizes the intersection of StateBehaviour and Calculator.
- Add : Added interface, INodeBehaviourSerializationCallbackReceiver, to describe the processing at serialization of NodeBehaviour.
- Add : Added type and parameter property to various Flexible classes.
- Change : The ID of each node is unified to Node.nodeID.
- Change : Added ToString method to Parameter class.
- Change : Added updatedTime to CalculatorBranch.
- Change : Addition of isUsed and updatedTime to InputSlot.
- Change : Change transition timing specification in Transition method to TransitionTiming.
- Change : Change CalculatorSlot.position to NonSerialized.
- Change : Add using System.Collections.Generic; and AddComponentMenu ("") to the template.
- Fix : Fixed it because we did not reference the field of base class in EachField.

Ver 2.1.8:

* Arbor Editor
- Fix : Fixed that the drag process will not stop when the node is not displayed by scrolling while dragging StateLink or CalculatorSlot.

Ver 2.1.7:

* Script
- Change : When disabled ArborFSM's Transition method is called, transition is delayed until it becomes enable even if immediateTransition is set to true.

Ver 2.1.6:

* Arbor Editor
- Fix : Fixed a bug that exception occurs with the start of play when you add a StateBehaviour to Prefab instance of ArborFSM.

Ver 2.1.5:

* Arbor Editor
- Fix: Speed up the Arbor Editor window.

* Built in Behaviour
- Fix : Fix not to display RandomTransition on Inspector's Add Component menu.

Ver 2.1.4:

* Arbor Editor
- Fix : Fixed that a warning is displayed at the beginning of playing by copying a node.
- Fix : Fixed a bug that the node will be restored at the start of playing if deleting a node only from Prefabed instance.

* Script
- Add : Added method to obtain index of various nodes in ArborFSMInternal: GetStateIndex(), GetCommentIndex(), GetCalculatorIndex()

Ver 2.1.3:

* Arbor Editor
- Fix : Fixed exception which appears when pressing Apply button of Prefab instance attaching ArborFSM.

Ver 2.1.2:

* Arbor Editor
- Add : Added a function that can transition to arbitrary state on ArborEditor window during play.
- Fix : Fixed that ArborEditor window does not display properly when an exception occurs in StateBehaviour and Calculator Editor.
- Fix : Fixed exception that occurred when StateBehaviour and Calculator script could not be loaded.
- Fix : Fixed an exception "NullReferenceException: SerializedObject of SerializedProperty has been disposed." When ArborEditor is displayed.
- Fix : Fixed that the width of the state list becomes too narrow when the width of the ArborEditor window becomes narrower.

* Built in Behaviour
- Change : Added OnTweenBegin method of callback for initialization at the start of Tween.
- Change : Rename the Event parameter of SendEventGameObject to OnStateBegin.
- Add : Add OnStateAwake event and OnStateEnd event to SendEventGameObject.

* Script
- Add : Added Get / Set method of all types handled by ParameterContainer.
- Add : Added GetParamID method to ParameterContainer.
- Add : Added a method that can also perform Get / Set method of ParameterContainer from ID.
- Fix : When SendTrigger was used during transition processing, it was modified to send Trigger after completion of transition processing.

Ver 2.1.1:

* Arbor Editor
- Change : Move breakpoint and state count display out of frame
- Fix : Fixed that Copy & Paste could not be done with String of ParameterContainer.
- Fix : Fix to delete CalculatorBranch when data slot is deleted from script

Ver 2.1.0:

* Arbor Editor
- Add : Added a toggle to lock so as not to switch even when GameObject is selected.
- Add : Implemented to be able to rearrange by dragging the title bar of StateBehaviour.
- Add : Implemented so that StateBehaviour can be inserted at arbitrary position by drag & drop.
- Add : Implemented so that you can set breakpoints in State.
- Add : Implemented to display the number of times State and StateLink passed during execution.
- Add : Implemented to highlight StateLink that passed immediately during execution.
- Add : Implemented to display the value of CalculaterBranch during execution.
- Add : Opens the help page from the built-in component's help button.
- Add : Opens the help page from the help button of the built-in Calculator.
- Add : Change the line color according to the type of CalculatorBranch.
- Fix : ArborFSM Fixed no longer be able to access the data from the input-output slot when you move to another GameObject.
- Fix : The graph display area of Arbor Editor is fixed.
- Fix : Fixed that Arbor Editor's graphic display blurred when automatically scrolling to the selected state, such as when selecting from the state list.
- Add : Add icon to ArborEditor window

* Built in Behaviour
- Change : Correspond to output GameObject found by FindGameObject, FindWithTagGameObject to the operation node.
- Add : Added RandomTransition.
- Change : TimeType specification added to TimeTransition.
- Fix : Fix caching with reference to Flexible component.
- Fix : Fixed an error when decreasing the size of the array on the editor with Behavior which has StateLink in the array.

* Script
- Change : Change OnStateTrigger to virtual function of StateBehaviour.
- Fix : Fix to prevent errors when null is passed to AgentController.Follow and Escape.

* Other
- Change: Updated reference site.
- Change: Raise Unity's lowest action version to 5.3.0f4.

Ver 2.0.10:

* Arbor Editor
- Change : When selecting GameObject, Arbor Editor also works in conjunction so that display is switched.
- Fix : Fixed an error when creating a new comment.
- Fix : Fixed an error when starting play when copying node.
- Fix : Fixed that you can not paste after copying the node and starting playing once.
- Fix : Fix copy of Calculator node.
- Fix : Fixed processing when copying & pasting or duplicating StateBehaviour or Calculator with CalculatorSlot.

Ver 2.0.9:

* Arbor Editor
- Fix: Fixed an error when opening ArborEditor with Unity 5.6.
- Fix: Speed up the Arbor Editor window.

* Built in Behaviour
- Change : Change Bool of CalculatorTransition to compare two Bool values.

* Scripts
- Change : Add State.behaviourCount and GetBehaviourFromIndex. Deprecated State.behaviours.
- Change : ArborFSMInternal.stateCount and GetStateFromIndex added. Deprecated ArborFSMInternal.states.
- Change : Added ArborFSMInternal.commentCount and GetCommentFromIndex. Deprecated ArborFSMInternal.comments.
- Change : ArborFSMInternal.calculatorCount and GetCalculatorFromIndex added. Deprecated ArborFSMInternal.calculators.
- Change : ArborFSMInternal.calculatorBranchCount and GetCalculatorBranchFromIndex added. Deprecated ArborFSMInternal.calculatorBranchies.

Ver 2.0.8:

* Arbor Editor
- Fix : Fixed an error displayed when copying Node or StateBehaviour in Arbor Editor after Unity 5.3.4 or later.
- Change : Change Script Execution Order so that ArborFSM is executed first.

Ver 2.0.7:

* Arbor Editor
- Add : Added OutputSlotString and InputSlotString
- Add : Added FlexibleString
- Add : Added string to ParameterContainer
- Add : Implement processing with string parameter in CalcParameter
- Add : Implement transitions with string parameters in ParameterTransition
- Add : Implement text setting from string to UISetTextFromParameter.
- Fix : Fixed that slots do not display correctly in Arbor Editor when creating customized class of OutputSlot / InputSlot

Ver 2.0.6:

* Arbor Editor
- Fix : Fixes the Undo / Redo when deleting a node.
- Fix : Fixes the Undo / Redo when deleting a StateBehaviour.

Ver 2.0.5:

* Arbor Editor
- Fix : Fix to repaint the ArborEditor window when you add a calculator node and state behaviour.
- Fix : Fixed the ArborEditor namespace RectUtility.
- Fix : Fix a template for Boo.

* Built in Behaviour
- Change: change the parameters of the Tween series to be able to receive from the calculator node.

Ver 2.0.4:

* Arbor Editor
- Add : Adding to call the OnStateAwake() when you first entered the State.
- Fix : Fix a menu which can be moved to the transition destination when you right-click a transition arrow to display in control + click on the Mac.
- Fix : Fix the error in the Unity5.5.0Beta.

Ver 2.0.3:
* Arbor Editor
- Fix: Fixed a warning that exits at Unity5.4.0Beta
- Fix: Modify the state is not generated in the mouse position at the time of the state of the paste or duplication.

Ver 2.0.2:
* Arbor Editor
- Fix: Unity5.3.0 Fixed When you play start still selected ArborFSM object StateBehaviour from being removed on the later of the Unity editor.

* Built in Behaviour
- Add: corresponding to be able to set the Additive property to Scene / LoadLevel.
- Add: Scene / UnloadLevel add (Unity5.2 or later).
- Fix: Fixed a Application.LoadLevel warning exiting at Unity5.3.0 later Scene / LoadLevel.

Ver 2.0.1:
* Arbor Editor
- Change: reduce the amount of heap memory.
- Fix: Fixed editor management for the object every time you compile had been increasing.

* Built in Behaviour
- Change: Renamed the Audio / PlaySoundAtPoint in Audio / PlaySoundAtTransform.
- Add: Adds the specified AudioMixerGroup and SpatialBlend in Audio / PlaySoundAtTransform.
- Add: add an Audio / PlaySoundAtPoint of new coordinates specified.

Ver 2.0.0:
* Arbor Editor
- Add : Add calculaltor node.
- Add: corresponding to be able to hold the Vector2 in ParameterContainer.
- Add: corresponding to be able to hold the Vector3 in ParameterContainer.
- Add: corresponding to be able to hold a Quaternion in ParameterContainer.
- Add: corresponding to be able to hold the Rect in ParameterContainer.
- Add: corresponding to be able to hold the Bounds in ParameterContainer.
- Add: corresponding to be able to hold the Transform in ParameterContainer.
- Add: corresponding to be able to hold the RectTransform in ParameterContainer.
- Add: corresponding to be able to hold the Rigidbody in ParameterContainer.
- Add: corresponding to be able to hold the Rigidbody2D in ParameterContainer.

* Built in Behaviour
- Add : Transition/Physics/RaycastTransition
- Add : Transition/Physics2D/Raycast2DTransition
- Add : Transition/CalculatorTransition
- Add: Add the output of GameObject generated in InstantiateGameObject
- Add: Add the output of the other Collision hitting the OnCollisionEnterTransition
- Add: Add the output of the other Collision hitting the OnCollisionExitTransition
- Add: Add the output of the other Collision hitting the OnCollisionStayTransition
- Add: Add the output of the other Collider, which hit the OnTriggerEnterTransition
- Add: Add the output of the other Collider, which hit the OnTriggerExitTransition
- Add: Add the output of the other Collider, which hit the OnTriggerStayTransition
- Add: Add the output of the other Collision2D hitting the OnCollisionEnter2DTransition
- Add: Add the output of the other Collision2D hitting the OnCollisionExit2DTransition
- Add: Add the output of the other Collision2D hitting the OnCollisionStayT2Dransition
- Add: Add the output of the other Collider2D hitting the OnTriggerEnter2DTransition
- Add: Add the output of the other Collider2D hitting the OnTriggerExit2DTransition
- Add: Add the output of the other Collider2D hitting the OnTriggerStayT2Dransition
- Change: AgentEscape the corresponding to FlexibleTransform.
- Change: AgentFllow the corresponding to FlexibleTransform.
- Change: PlaySoundAtPoint the corresponding to FlexibleTransform.
- Change: InstantiateGameObject the corresponding to FlexibleTransform.
- Change: LookAtGameObject the corresponding to FlexibleTransform.
- Change: AddForceRigidbody the corresponding to FlexibleRigidbody.
- Change: AddVelocityRigidbody the corresponding to FlexibleRigidbody.
- Change: SetVelocityRigidbody the corresponding to FlexibleRigidbody.
- Change: AddForceRigidbody2D the corresponding to FlexibleRigidbody2D.
- Change: AddVelocityRigidbody2D the corresponding to FlexibleRigidbody2D.
- Change: SetVelocityRigidbody2D the corresponding to FlexibleRigidbody2D.

* Built in Calculator
- Add: Calculator additional Bool
- Add: Calculator additional Bounds
- Add: Calculator additional Collider
- Add: Calculator additional Collider2D
- Add: Calculator additional Collision
- Add: Calculator additional Collision2D
- Add: Calculator additional Component
- Add: Calculator additional Float
- Add: Calculator additional Int
- Add: Calculator additional Mathf
- Add: Calculator additional Quaternion
- Add: Calculator additional RaycastHit
- Add: Calculator additional RaycastHit2D
- Add: Calculator additional Rect
- Add: Calculator additional RectTransform
- Add: Calculator additional Rigidbody
- Add: Calculator additional Rigidbody2D
- Add: Calculator additional Transform
- Add: Calculator additional Vector2
- Add: Calculator additional Vector3

* Scripts
- Add: FlexibleBounds implementation
- Add: FlexibleQuaternion implementation
- Add: FlexibleRect implementation
- Add: FlexibleRectTransform implementation
- Add: FlexibleRigidbody implementation
- Add: FlexibleRigidbody2D implementation
- Add: FlexibleTransform implementation
- Add: FlexibleVector2 implementation
- Add: FlexibleVector3 implementation

Ver 1.7.7p2:
* Arbor Editor
- Fix : Fixed an error that exits at Unity5.2.1 later.

Ver 1.7.7p1:
* Arbor Editor
- Fix : Fix for creation and deletion of the state and the comment could not be Undo.

Ver 1.7.7:
* Arbor Editor
- Add : Corresponding to be able to hold a GameObject in ParameterContainer.
- Change : Change to be able to transition to their own state.
- Change : Change the background of behavior.
- Change : Change the background of ListGUI.
- Change : Change the comment node to resize depending on the contents.
- Fix : Bug fixes around Undo
- Fix : Fixed resident state could be set to the start state.
- Other : Corresponding to save the settings, such as the grid for each major version of Unity instead of every project.

* Built in Behaviour
- Add : Collision/OnCollisionEnterStore
- Add : Collision/OnCollisionExitStore
- Add : Collision/OnControllerColliderHitStore
- Add : Collision/OnTriggerEnterStore
- Add : Collision/OnTriggerExitStore
- Add : Collision2D/OnCollisionEnter2DStore
- Add : Collision2D/OnCollisionExit2DStore
- Add : Collision2D/OnTriggerEnter2DStore
- Add : Collision2D/OnTriggerExit2DStore
- Add : GameObject/FindGameObject
- Add : GameObject/FindWithTagGameObject
- Add : Added to allow relative specified in UITweenPosition.
- Add : Added to allow relative specified in UITweenSize.
- Change : Corresponding value of BroadcastMessageGameObject to use such FlexibleInt.
- Change : Corresponding value of CalcAnimatorParameter to use such FlexibleInt.
- Change : Corresponding value of CalcParameter to use such FlexibleInt.
- Change : Corresponding value of ParameterTransition to use such FlexibleInt.
- Change : Corresponding value of SendMessageGameObject to use such FlexibleInt.
- Change : Corresponding value of SendMessageUpwardsGameObject to use such FlexibleInt.
- Change : The corresponding AgentEscape to ArborGameObject.
- Change : The corresponding AgentFllow to ArborGameObject.
- Change : The corresponding ActivateGameObject to FlexibleGameObject.
- Change : The corresponding DestroyGameObject to FlexibleGameObject.
- Change : The corresponding LookatGameObject to FlexibleGameObject.
- Change : The corresponding BroadcastTrigger to FlexibleGameObject.
- Change : The corresponding SendTriggerGameObject to FlexibleGameObject.
- Change : The corresponding SendTriggerUpwards to FlexibleGameObject.
- Change : Corresponding to be able to store the object that was generated by the InstantiateGameObject the parameter.

* Script
- Add : FlexibleInt implementation
- Add : FlexibleFloat implementation
- Add : FlexibleBool implementation
- Add : FlexibleGameObject implementation
- Add : Corresponding to use the ContextMenu.

* Other
- Change : Parameter related to move to Core folder and the Internal folder.
- Other : Component to the icon set.

Ver 1.7.6:
* Arbor Editor
- Add : The name setting adds to StateLink.
- Add : Add immediate transition flag to StateLink.
- Fix : Fixed search string in the behavior added was not able to save.
- Other : When you open the behavior added, corresponding as focus moves to the search bar.
- Other : In order of at Add Behaviour, adjusted so that the group comes first.

* Component
- Add : GlobalParameterContainer

* Built int Behaviour
- Add : Audio/PlaySound
- Add : Audio/StopSound
- Add : Collision/OnCollisionEnterDestroy
- Add : Collision/OnCollisionExitDestroy
- Add : Collision/OnControllerColliderHitDestroy
- Add : Collision2D/OnCollisionEnter2DDestroy
- Add : Collision2D/OnCollisionExit2DDestroy
- Add : GameObject/BroadcastMessageGameObject
- Add : GameObject/SendMessageUpwardsGameObject
- Add : Physics/AddForceRigidbody
- Add : Physics/AddVelocityRigidbody
- Add : Physics2D/AddForceRigidbody2D
- Add : Physics2D/AddVelocityRigidbody2D
- Add : Renderer/SetSprite
- Add : Transition/Collision/OnCollisionEnterTransition
- Add : Transition/Collision/OnCollisionExitTransition
- Add : Transition/Collision/OnCollisionStayTransition
- Add : Transition/Collision/OnControllerColliderHitTransition
- Add : Transition/Collision2D/OnCollisionEnter2DTransition
- Add : Transition/Collision2D/OnCollisionExit2DTransition
- Add : Transition/Collision2D/OnCollisionStay2DTransition
- Add : Transition/Input/ButtonTransition
- Add : Transition/Input/KeyTransition
- Add : Transition/Input/MouseButtonTransition
- Add : Transition/ExistsGameObjectTransition
- Add : Trigger/BroadcastTrigger
- Add : Trigger/SendTriggerGameObject
- Add : Trigger/SendTriggerUpwards
- Add : Tween/TweenRigidbody2DPosition
- Add : Tween/TweenRigidbody2DRotation
- Add : Tween/TweenTextureOffset
- Add : UI/UISetSlider
- Add : UI/UISetSliderFromParameter
- Add : UI/UISetToggle
- Add : UI/UISetToggleFromParameter
- Add : Add to display a progress bar the current time to TimeTransition.
- Add : It added to allow transition at the time of Tween end.
- Add : Added to allow relative specified in TweenPosition.
- Add : Added to allow relative specified in TweenRotation.
- Add : Added to allow relative specified in TweenScale.
- Add : Added to allow relative specified in TweenRigidbodyPosition.
- Add : Added to allow relative specified in TweenRigidbodyRotation.
- Fix : Fixed OnTriggerExit2DDestroy of was in Collision.
- Fix : Fixed floatValue of had become int of CalcAnimatorParameter.
- Fix : Fixed floatValue of had become int of CalcParameter.
- Fix : Fixed floatValue of had become int of ParameterTransition.
- Other : Renamed SetRigidbodyVelocity to SetVelocityRigidbody.
- Other : Renamed SetRigidbody2DVelocity to SetVelocityRigidbody2D.

* Script
- Add : Corresponding to prevent modification of the immediate transition flag in FixedImmediateTransition attribute.

* Other
- Add : Sample additional GlobalParameterContainer as Example9.
- Fix : Modify because Coin has been added to the Tags.

Ver 1.7.5:
* Arbor Editor
- Fix: Fixed there are times when the grid is not displayed correctly.
- Other: support to be able to resize the width of the state list.

* Built in Behaviour
- Add : Collision/OnTriggerEnterDestroy
- Add : Collision/OnTriggerExitDestroy
- Add : Collision2D/OnTriggerEnter2DDestroy
- Add : Collision2D/OnTriggerExit2DDestroy
- Add : GameObject/LookAtGameObject
- Add : Parameter/SetBoolParameterFromUIToggle
- Add : Parameter/SetFloatParameterFromUISlider
- Add : Physics/SetRigidbodyVelocity
- Add : Physics2D/SetRigidbody2DVelocity
- Add : Transition/EventSystems/OnPointerClickTransition
- Add : Transition/EventSystems/OnPointerDownTransition
- Add : Transition/EventSystems/OnPointerEnterTransition
- Add : Transition/EventSystems/OnPointerExitTransition
- Add : Transition/EventSystems/OnPointerUpTransition
- Add : Tween/TweenCanvasGroupAlpha
- Add : Tween/TweenRigidbodyPosition
- Add : Tween/TweenRigidbodyRotation
- Add : UI/UISetImage
- Add : UI/UISetTextFromParameter
- Add: Add in a way that allows you to specify the initial Transform at the time of generation in InstantiateGameObject.
- Fix: Fixed did not work properly in the case of type Bool in CalcParameter.
- Fix: modified to not bother to specify the person to call in SendEventGameObject.

* Script
- Add: The value property added to the Parameter.
- Add: IntParameterReference added.
- Add: FloatParameterReference added.
- Add: BoolParameterReference added.

* Other
- Add: add from the Hierarchy of the Create button to make the ArborFSM with GameObject.
- Add: add from the Hierarchy of the Create button to make the ParameterContainer with GameObject.
- Add: add from the Hierarchy of the Create button to make the AgentController with GameObject.
- Add: coin pusher game add as Example7.
- Add: sample additional EventSystem as Example8.
- Other: folder organization.

Ver 1.7.4:
- Add: Agent system Behaviour added.
- Add: uGUI system Behaviour added.
- Add: uGUI system Tween added.
- Add: SendEventGameObject added.
- Add: The pass-by-value function added to the SendMessageGameObject.
- Fix: AnimatorParameterReference of reference is to Fixed get an error when that did not refer to a AnimatorController.
- Other: Pull up on the Unity minimum operating version to 4.6.7f1 due to uGUI correspondence.

Ver 1.7.3:
- Add: OnMouse system Transition add
- Fix: move when the scroll position correction to the selected state
- Other: modified to sort the state list by name.
- Other: Arbor change to be able to place the infinite state also to the upper left of the Editor.
- Other: to renew the manual site.

Ver 1.7.2:
- Add: Add a comment node in ArborEditor.
- Add: corresponding to be able to search at the time of behavior added.
- Add: CalcAnimatorParameter added.
- Add: AnimatorStateTransition added.
- Add: add to be able to move to a transition source and a transition destination in the right-click on the transition line.
- Fix: Prefab source to the Fixed not correctly added to the Prefab destination and behavior added.
- Other: Renamed the ForceTransition to GoToTransition.
- Other: change so as not to omit the name of the built-in Behaviour that is displayed in the behavior added.
- Other: change so as not to display the built-in Behaviour to Add Component.

Ver 1.7.1:
- Add: Add the state list.
- Add: Add PropertyDrawer of ParamneterReference.
- Add: Add GUI, the ListGUI for the list that can be deleted elements.
- Fix: Fixed boolValue of had become int of CalcParameter.

Ver 1.7.0;
- Add: parameter container.
- Fix: OnStateBegin () If you have state transitions, fix than it so as not to run the Behaviour under.

Ver 1.6.3f1:
- Fixed an error that exits at Unity5 RC3.
- Unity5 RC3 by the corresponding Renamed OnStateEnter / OnStateExit to OnStateBegin / OnStateEnd.

Ver 1.6.3:
- Add the force flag in Transition. you can do to transition on the spot at the time of the call to be to true.
- Embedded documentation comments to the source code.
- Place the script reference to Assets / Arbor / Docs.
  Please open the index.html Unzip.

Ver 1.6.2:
- FIX: The Fixed a state can not transition in OnStateEnter.

Ver 1.6.1:
- FIX: Error is displayed if you press Grid button in the Mac environment.

Ver 1.6: 
- ADD: Resident state.
- ADD: Multilingual.
- ADD: Correspondence to be named to ArborFSM.
- FIX: Are not reflected in the snap interval when you change the grid size.
- FIX: Deal of the problems StateBehaviour is lost when you copy and paste a component of ArborFSM.
- FIX: Modified to send only to the state currently in effect the SendTrigger.
- FIX: StateBehaviour continues to move If you disable ArborFSM.

Ver 1.5: 
- ADD: Support for multiple selection of the state. 
- ADD: Support for shortcut key. 
- ADD: grid display support. 
- FIX: it placed in a state in which it is spread by default when adding Behaviour. 
- FIX: I react mouse over to the state is shifted while dragging StateLink.

ver 1.4: 
- ADD: Tween-based Behaviour added. 
  - Tween / Color 
  - Tween / Position 
  - Tween / Rotation 
  - Tween / Scale 
- ADD: The HideBehaviour to add attributes that do not appear in the Add Behaviour. 
- ADD: online help of the built-in display Behaviour from the Help button on the Behaviour.

ver 1.3: 
- ADD: Add built-in Behaviour. 
  - Audio / PlaySoundAtPoint 
  - GameObject / SendMessage 
  - Scene / LoadLevel 
  - Transition / Force 
- ADD: Copy and paste across the scene. 
- FIX: memory leak warning is displayed when you save the scene after which you copied the State. 
- FIX: arrow will remain when you scroll the screen to drag the connection of the StateLink.

ver 1.2: 
- ADD: Enabled check box of StateBehaviour. 
- FIX: Errors Occur When you release the maximization of Arbor Editor. 
- FIX: Warning of the new line of code can when you edit a C# script that generated.

ver 1.1: 
- ADD: script generation of Boo and JavaScript. 
- ADD: Copy and paste the State. 
- ADD: Copy and paste the StateBehaviour. 
- FIX: support when the script becomes Missing. 
- FIX: Fixed array of StateLink is not displayed.

ver 1.0.1: 
- FIX: Error in Unity4.5. 
- FIX: Arbor Editor is not repaint when running in the editor. 
- FIX: class name of the Inspector extension of ArborFSM.
