﻿using UnityEngine;
using UnityEngine.Serialization;
#if UNITY_5_5_OR_NEWER
using UnityEngine.AI;
#endif
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// NavMeshAgentをラップしたAI用移動コンポーネント。<br />
	/// 主に組み込みBehaviourのAgentを介して使用する。
	/// </summary>
#else
	/// <summary>
	/// AI for the movement component that wraps the NavMeshAgent.<br />
	/// Used mainly through built-in Behavior's Agent.
	/// </summary>
#endif
	[AddComponentMenu("Arbor/AgentController",40)]
	[BuiltInComponent]
	[HelpURL( ArborReferenceUtility.componentUrl + "agentcontroller.html")]
	public class AgentController : MonoBehaviour , ISerializationCallbackReceiver
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 制御したいNavMeshAgent。
		/// </summary>
#else
		/// <summary>
		/// NavMeshAgent you want to control.
		/// </summary>
#endif
		[SerializeField] private NavMeshAgent _Agent;

#if ARBOR_DOC_JA
		/// <summary>
		/// 制御したいAnimator。
		/// </summary>
#else
		/// <summary>
		/// Animator you want to control.
		/// </summary>
#endif
		[SerializeField] private Animator _Animator;

#if ARBOR_DOC_JA
		/// <summary>
		/// Agentが移動中かどうかをAnimatorへ設定するためのboolパラメータを指定する。
		/// </summary>
#else
		/// <summary>
		/// Specify the bool parameter for setting to the Animator whether or not the Agent is moving.
		/// </summary>
#endif
		[SerializeField] private string _MovingParameter;

#if ARBOR_DOC_JA
		/// <summary>
		/// 移動中と判定する速度の閾値
		/// </summary>
#else
		/// <summary>
		/// Threshold value of the speed of moving
		/// </summary>
#endif
		[SerializeField] private float _MovingSpeedThreshold;

#if ARBOR_DOC_JA
		/// <summary>
		/// 移動速度をAnimatorへ設定するためのfloatパラメータを指定する。
		/// </summary>
#else
		/// <summary>
		/// Specify the float parameter for setting the moving speed to Animator.
		/// </summary>
#endif
		[SerializeField] private string _SpeedParameter;

#if ARBOR_DOC_JA
		/// <summary>
		/// Agentに設定しているspeedで割るかどうか。
		/// </summary>
#else
		/// <summary>
		/// Whether or not to divide by the speed set for Agent.
		/// </summary>
#endif
		[SerializeField] private bool _IsDivAgentSpeed = false;

#if ARBOR_DOC_JA
		/// <summary>
		/// 移動速度のダンプ時間。
		/// </summary>
#else
		/// <summary>
		/// Dump time of moving speed.
		/// </summary>
#endif
		[SerializeField] private float _SpeedDampTime = 0.0f;

#if ARBOR_DOC_JA
		/// <summary>
		/// Agentのローカル空間での移動方向ベクトルのX値をAnimatorへ設定ためのfloatパラメータを指定する。
		/// </summary>
#else
		/// <summary>
		/// Specify the float parameter for setting the X value of the moving direction vector in the Agent's local space to Animator.
		/// </summary>
#endif
		[SerializeField] private string _MovementXParameter;

#if ARBOR_DOC_JA
		/// <summary>
		/// 移動方向ベクトルのX値のダンプ時間。
		/// </summary>
#else
		/// <summary>
		/// Dump time of X value of moving direction vector.
		/// </summary>
#endif
		[SerializeField] private float _MovementXDampTime = 0.0f;

#if ARBOR_DOC_JA
		/// <summary>
		/// Agentのローカル空間での移動方向ベクトルのY値をAnimatorへ設定ためのfloatパラメータを指定する。
		/// </summary>
#else
		/// <summary>
		/// Specify the float parameter for setting the Y value of the moving direction vector in the Agent's local space to Animator.
		/// </summary>
#endif
		[SerializeField] private string _MovementYParameter;

#if ARBOR_DOC_JA
		/// <summary>
		/// 移動方向ベクトルのY値のダンプ時間。
		/// </summary>
#else
		/// <summary>
		/// Dump time of Y value of moving direction vector.
		/// </summary>
#endif
		[SerializeField] private float _MovementYDampTime = 0.0f;
		
#if ARBOR_DOC_JA
		/// <summary>
		/// Agentのローカル空間での移動方向ベクトルのZ値をAnimatorへ設定ためのfloatパラメータを指定する。
		/// </summary>
#else
		/// <summary>
		/// Specify the float parameter for setting the Z value of the moving direction vector in the Agent's local space to Animator.
		/// </summary>
#endif
		[SerializeField] private string _MovementZParameter;

#if ARBOR_DOC_JA
		/// <summary>
		/// 移動方向ベクトルのZ値のダンプ時間。
		/// </summary>
#else
		/// <summary>
		/// Dump time of Z value of moving direction vector.
		/// </summary>
#endif
		[SerializeField] private float _MovementZDampTime = 0.0f;

#if ARBOR_DOC_JA
		/// <summary>
		/// ターン方向をAnimatorへ設定するためのfloatパラメータを指定する。
		/// </summary>
#else
		/// <summary>
		/// Specify the float parameter for setting the turn direction to Animator.
		/// </summary>
#endif
		[SerializeField] private string _TurnParameter;

#if ARBOR_DOC_JA
		/// <summary>
		/// ターン方向のダンプ時間。
		/// </summary>
#else
		/// <summary>
		/// Dump time in the turn direction.
		/// </summary>
#endif
		[SerializeField] private float _TurnDampTime = 0.0f;

		[HideInInspector]
		[SerializeField]
		private int _SerializeVersion = 0;

		[FormerlySerializedAs("_SpeedParameter")]
		[HideInInspector]
		[SerializeField]
		private AnimatorFloatParameterReference _SpeedParameterOld;

		#endregion // Serialize fields

		public NavMeshAgent agent
		{
			get
			{
				return _Agent;
			}
		}

		private Vector3 _StartPosition;

#if ARBOR_DOC_JA
		/// <summary>
		/// 移動完了したかどうか。
		/// </summary>
#else
		/// <summary>
		/// Whether the move is complete or not.
		/// </summary>
#endif
		public bool isDone
		{
			get
			{
				return !_Agent.pathPending && _Agent.remainingDistance <= _Agent.stoppingDistance && !(_Agent.hasPath && isMoving);
			}
		}

		private bool _IsMoving = false;

		private float GetFloat(string name)
		{
			if (_Animator != null && !string.IsNullOrEmpty(name) )
			{
				return _Animator.GetFloat(name);
			}
			return 0.0f;
		}

		private void SetFloat(string name, float value)
		{
			if (_Animator != null && !string.IsNullOrEmpty(name))
			{
				_Animator.SetFloat(name, value);
			}
		}

		private void SetBool(string name, bool value)
		{
			if (_Animator != null && !string.IsNullOrEmpty(name))
			{
				_Animator.SetBool(name, value);
			}
		}

		private void SetFloat(string name, float value, float dampTime, float deltaTime)
		{
			if (_Animator != null && !string.IsNullOrEmpty(name))
			{
				_Animator.SetFloat(name,value,dampTime,deltaTime);
			}
		}

		private bool IsMovingAnimator()
		{
			return Mathf.Abs(GetFloat(_SpeedParameter)) >= 0.01f ||
				Mathf.Abs(GetFloat(_MovementXParameter)) >= 0.01f ||
				Mathf.Abs(GetFloat(_MovementYParameter)) >= 0.01f ||
				Mathf.Abs(GetFloat(_MovementZParameter)) >= 0.01f ||
				Mathf.Abs(GetFloat(_TurnParameter)) >= 0.01f;
		}

		private bool IsMoving()
		{
			return !Mathf.Approximately(_Agent.velocity.magnitude, 0.0f) || IsMovingAnimator();
		}

		public bool isMoving
		{
			get
			{
				return _IsMoving;
			}
		}

		// Use this for initialization
		void Start()
		{
			if (_Agent == null)
			{
				_Agent = GetComponent<NavMeshAgent>();
			}
			
			_StartPosition = transform.position;
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 開始位置から指定半径内をうろつく
		/// </summary>
		/// <param name="speed">移動速度</param>
		/// <param name="radius">開始位置からの半径</param>
#else
		/// <summary>
		/// Prowl the within a specified radius from the start position
		/// </summary>
		/// <param name="speed">Movement speed</param>
		/// <param name="radius">Radius from the starting position</param>
#endif
		public void Patrol(float speed, float radius)
		{
			float angle = Random.Range(-180.0f, 180.0f);

			Vector3 axis = Vector3.up;

			Quaternion rotate = Quaternion.AngleAxis(angle, axis);

			Vector3 dir = rotate * Vector3.forward * Random.Range(0.0f, radius);

			_Agent.speed = speed;
			_Agent.stoppingDistance = 0.0f;
			_Agent.SetDestination(_StartPosition + dir);
			Resume();
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 指定したTransformの位置へ近づく
		/// </summary>
		/// <param name="speed">移動速度</param>
		/// <param name="stoppingDistance">停止距離</param>
		/// <param name="target">目標地点</param>
#else
		/// <summary>
		/// Approach to the position of the specified Transform
		/// </summary>
		/// <param name="speed">Movement speed</param>
		/// <param name="stoppingDistance">Stopping distance</param>
		/// <param name="target">Objective point</param>
#endif
		public void Follow(float speed, float stoppingDistance, Transform target)
		{
			if( target == null )
			{
				return;
			}
			_Agent.speed = speed;
			_Agent.stoppingDistance = stoppingDistance;
			_Agent.SetDestination(target.position);
			Resume();
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 指定したTransformから遠ざかる
		/// </summary>
		/// <param name="speed">移動速度</param>
		/// <param name="distance">遠ざかる距離</param>
		/// <param name="target">対象</param>
#else
		/// <summary>
		/// Away from the specified Transform
		/// </summary>
		/// <param name="speed">Movement speed</param>
		/// <param name="distance">Distance away</param>
		/// <param name="target">Target</param>
#endif
		public void Escape(float speed, float distance, Transform target)
		{
			if( target == null )
			{
				return;
			}

			Vector3 dir = transform.position - target.position;

			if (dir.magnitude >= distance)
			{
				return;
			}

			Vector3 pos = dir.normalized * distance + target.position;

			_Agent.speed = speed;
			_Agent.stoppingDistance = 0.0f;
			if (!_Agent.SetDestination(pos))
			{
				pos = -dir.normalized * distance + transform.position;

				_Agent.SetDestination(pos);
			}
			Resume();
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 移動を再開する。
		/// </summary>
#else
		/// <summary>
		/// Resume movement.
		/// </summary>
#endif
		public void Resume()
		{
#if UNITY_5_6_OR_NEWER
			_Agent.isStopped = false;
#else
			_Agent.Resume();
#endif
		}

#if ARBOR_DOC_JA
		/// <summary>
		/// 停止する。
		/// </summary>
#else
		/// <summary>
		/// Stop.
		/// </summary>
#endif
		public void Stop()
		{
#if UNITY_5_6_OR_NEWER
			_Agent.isStopped = true;
#else
			_Agent.Stop();
#endif
		}
		
		void Update()
		{
			bool currentMoving = IsMoving();
			if (_IsMoving != currentMoving)
			{
				_IsMoving = currentMoving;

				if (!_IsMoving)
				{
					if (_Animator != null)
					{
						SetBool(_MovingParameter, false);
						SetFloat(_SpeedParameter, 0.0f);
						SetFloat(_MovementXParameter, 0.0f);
						SetFloat(_MovementYParameter, 0.0f);
						SetFloat(_MovementZParameter, 0.0f);
						SetFloat(_TurnParameter, 0.0f);
					}
				}
			}

			if (_IsMoving)
			{
				float deltaTime = Time.deltaTime;

				Vector3 velocity = _Agent.velocity;

				float speed = velocity.magnitude;

				bool moving = speed > _MovingSpeedThreshold;

				if (_IsDivAgentSpeed && _Agent.speed != 0.0f)
				{
					speed /= _Agent.speed;
				}
				
				velocity.Normalize();

				velocity = _Agent.transform.InverseTransformDirection(velocity);

				float turn = Mathf.Atan2(velocity.x, velocity.z);

				if (_Animator != null)
				{
					SetBool(_MovingParameter, moving);
					SetFloat(_SpeedParameter, speed, _SpeedDampTime, deltaTime);
					SetFloat(_MovementXParameter, velocity.x, _MovementXDampTime, deltaTime);
					SetFloat(_MovementYParameter, velocity.y, _MovementYDampTime, deltaTime);
					SetFloat(_MovementZParameter, velocity.z, _MovementZDampTime, deltaTime);
					SetFloat(_TurnParameter, turn, _TurnDampTime, deltaTime);
				}
			}
		}

		void SerializeVer1()
		{
			_Animator = _SpeedParameterOld.animator;
			_SpeedParameter = _SpeedParameterOld.name;
		}

		void ISerializationCallbackReceiver.OnBeforeSerialize()
		{
			if (_SerializeVersion == 0)
			{
				SerializeVer1();
				_SerializeVersion = 1;
			}
		}

		void ISerializationCallbackReceiver.OnAfterDeserialize()
		{
			if (_SerializeVersion == 0)
			{
				SerializeVer1();
			}
		}
	}
}
