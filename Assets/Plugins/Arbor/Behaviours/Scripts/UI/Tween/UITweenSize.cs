﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// UIのサイズを徐々に変化させる。
	/// </summary>
#else
	/// <summary>
	/// Gradually change size of UI.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("UI/Tween/UITweenSize")]
	[BuiltInBehaviour]
	public class UITweenSize : TweenBase, INodeBehaviourSerializationCallbackReceiver
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 対象となるRectTransform。<br/>
		/// TypeがConstantの時に指定しない場合、ArborFSMがアタッチされているGameObjectのRectTransformとなる。
		/// </summary>
#else
		/// <summary>
		/// RectTransform of interest.<br/>
		/// If Type is Constant and nothing is specified, ArborFSM is the RectTransform of the attached GameObject.
		/// </summary>
#endif
		[SerializeField] private FlexibleRectTransform _Target;

#if ARBOR_DOC_JA
		/// <summary>
		/// 開始した状態からの相対的な変化かどうか。
		/// </summary>
#else
		/// <summary>
		/// Whether the relative change from the start state.
		/// </summary>
#endif
		[SerializeField] private bool _Relative;

#if ARBOR_DOC_JA
		/// <summary>
		/// 開始サイズ。
		/// </summary>
#else
		/// <summary>
		/// Start size.
		/// </summary>
#endif
		[SerializeField] private FlexibleVector2 _From;

#if ARBOR_DOC_JA
		/// <summary>
		/// 目標サイズ。
		/// </summary>
#else
		/// <summary>
		/// Target size.
		/// </summary>
#endif
		[SerializeField] private FlexibleVector2 _To;

		[SerializeField]
		[HideInInspector]
		private int _SerializeVersion;

		#region old

		[SerializeField, FormerlySerializedAs( "_Target" )]
		[HideInInspector]
		private RectTransform _OldTarget;

		[SerializeField, FormerlySerializedAs( "_From" )]
		[HideInInspector]
		private Vector2 _OldFrom;

		[SerializeField, FormerlySerializedAs( "_To" )]
		[HideInInspector]
		private Vector2 _OldTo;

		#endregion // old

		#endregion // Serialize fields

		void SerializeVer1()
		{
			_Target = (FlexibleRectTransform)_OldTarget;
			_From = (FlexibleVector2)_OldFrom;
			_To = (FlexibleVector2)_OldTo;
		}

		void INodeBehaviourSerializationCallbackReceiver.OnBeforeSerialize()
		{
			if (_SerializeVersion == 0)
			{
				SerializeVer1();
				_SerializeVersion = 1;
			}
		}

		void INodeBehaviourSerializationCallbackReceiver.OnAfterDeserialize()
		{
			if (_SerializeVersion == 0)
			{
				SerializeVer1();
				_SerializeVersion = 1;
			}
		}

		RectTransform _MyTransform;
		RectTransform cachedTarget
		{
			get
			{
				RectTransform transform = _Target.value;
				if( transform == null && _Target.type == FlexibleRectTransform.Type.Constant )
				{
					if( _MyTransform == null )
					{
						_MyTransform = GetComponent<RectTransform>();
					}

					transform = _MyTransform;
				}
				return transform;
			}
		}

		private Vector2 _StartSize;

		protected override void OnTweenBegin()
		{
			RectTransform target = cachedTarget;
			if (_Relative && target != null)
			{
				_StartSize = new Vector2(target.rect.width, target.rect.height);
			}
			else
			{
				_StartSize = Vector2.zero;
            }
		}

		protected override void OnTweenUpdate (float factor)
		{
			RectTransform target = cachedTarget;
			if (target != null )
			{
				Vector2 size = _StartSize + Vector2.Lerp(_From.value, _To.value, factor);

				target.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, size.x);
				target.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, size.y);
            }
		}
	}
}
