﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// UIの色を徐々に変化させる。
	/// </summary>
#else
	/// <summary>
	/// Gradually change color of UI.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("UI/Tween/UITweenColor")]
	[BuiltInBehaviour]
	public class UITweenColor : TweenBase, INodeBehaviourSerializationCallbackReceiver
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 対象となるGraphic。<br/>
		/// 指定しない場合は、ArborFSMと同じGameObjectに割り当てられているGraphic。
		/// </summary>
#else
		/// <summary>
		/// Graphic of interest.<br/>
		/// If not specified, Graphic of GameObject that ArborFSM is assigned a target.
		/// </summary>
#endif
		[SerializeField]
		[SlotType(typeof(Graphic))]
		private FlexibleComponent _Target;

#if ARBOR_DOC_JA
		/// <summary>
		/// 色の変化の指定。
		/// </summary>
#else
		/// <summary>
		/// Specifying the color change.
		/// </summary>
#endif
		[SerializeField] private Gradient _Gradient = new Gradient();

		[SerializeField]
		[HideInInspector]
		private int _SerializeVersion;

		#region old

		[SerializeField]
		[FormerlySerializedAs("_Target")]
		[HideInInspector]
		private Graphic _OldTarget;

		#endregion // old

		#endregion // Serialize fields

		private Graphic _MyGraphic;
		public Graphic cachedTarget
		{
			get
			{
				Graphic graphic = _Target.value as Graphic;
				if (graphic == null && _Target.type == FlexibleComponent.Type.Constant)
				{
					if (_MyGraphic == null)
					{
						_MyGraphic = GetComponent<Graphic>();
					}

					graphic = _MyGraphic;
				}
				return graphic;
			}
		}

		void SerializeVer1()
		{
			_Target = (FlexibleComponent)_OldTarget;
		}

		void INodeBehaviourSerializationCallbackReceiver.OnBeforeSerialize()
		{
			if (_SerializeVersion == 0)
			{
				SerializeVer1();
				_SerializeVersion = 1;
			}
		}

		void INodeBehaviourSerializationCallbackReceiver.OnAfterDeserialize()
		{
			if (_SerializeVersion == 0)
			{
				SerializeVer1();
			}
		}

		protected override void OnTweenUpdate (float factor)
		{
			Graphic target = cachedTarget;
			if (target != null && _Gradient != null )
			{
				target.color = _Gradient.Evaluate( factor );
			}
		}
	}
}
