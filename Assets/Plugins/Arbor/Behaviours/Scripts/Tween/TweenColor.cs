﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Rendererの色を徐々に変化させる。
	/// </summary>
#else
	/// <summary>
	/// Gradually change color of Renderer.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("Tween/TweenColor")]
	[BuiltInBehaviour]
	public class TweenColor : TweenBase, INodeBehaviourSerializationCallbackReceiver
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 対象となるRenderer。<br/>
		/// 指定しない場合は、ArborFSMと同じGameObjectに割り当てられているRenderer。
		/// </summary>
#else
		/// <summary>
		/// Renderer of interest.<br/>
		/// If not specified, Renderer of GameObject that ArborFSM is assigned a target.
		/// </summary>
#endif
		[SerializeField]
		[SlotType(typeof(Renderer))]
		private FlexibleComponent _Target;

#if ARBOR_DOC_JA
		/// <summary>
		/// 色の変化の指定。
		/// </summary>
#else
		/// <summary>
		/// Specifying the color change.
		/// </summary>
#endif
		[SerializeField] private Gradient _Gradient = new Gradient();

		[SerializeField]
		[HideInInspector]
		private int _SerializeVersion;

		#region old

		[SerializeField]
		[FormerlySerializedAs("_Target")]
		[HideInInspector]
		private Renderer _OldTarget;

		#endregion // old

		#endregion // Serialize fields

		private Renderer _MyRenderer;
		public Renderer cachedTarget
		{
			get
			{
				Renderer renderer = _Target.value as Renderer;
				if (renderer == null && _Target.type == FlexibleComponent.Type.Constant)
				{
					if (_MyRenderer == null)
					{
						_MyRenderer = GetComponent<Renderer>();
					}

					renderer = _MyRenderer;
				}
				return renderer;
			}
		}
		
		protected override void OnTweenUpdate (float factor)
		{
			Renderer target = cachedTarget;
			if( target != null && _Gradient != null )
			{
				target.material.color = _Gradient.Evaluate( factor );
			}
		}

		void SerializeVer1()
		{
			_Target = (FlexibleComponent)_OldTarget;
		}

		void INodeBehaviourSerializationCallbackReceiver.OnBeforeSerialize()
		{
			if (_SerializeVersion == 0)
			{
				SerializeVer1();
				_SerializeVersion = 1;
			}
		}

		void INodeBehaviourSerializationCallbackReceiver.OnAfterDeserialize()
		{
			if (_SerializeVersion == 0)
			{
				SerializeVer1();
			}
		}
	}
}
