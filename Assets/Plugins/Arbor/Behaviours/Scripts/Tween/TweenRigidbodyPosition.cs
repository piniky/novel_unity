﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Rigidbodyの位置を徐々に変化させる。
	/// </summary>
#else
	/// <summary>
	/// Gradually change position of Rigidbody.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("Tween/TweenRigidbodyPosition")]
	[BuiltInBehaviour]
	public class TweenRigidbodyPosition : TweenBase, INodeBehaviourSerializationCallbackReceiver
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 対象となるTransform。<br/>
		/// TypeがConstantの時に指定しない場合、ArborFSMがアタッチされているGameObjectのTransformとなる。
		/// </summary>
#else
		/// <summary>
		/// Transform of interest.<br/>
		/// If Type is Constant and nothing is specified, ArborFSM is the Transform of the attached GameObject.
		/// </summary>
#endif
		[SerializeField]
		private FlexibleRigidbody _Target;

#if ARBOR_DOC_JA
		/// <summary>
		/// 開始した状態からの相対的な変化かどうか。
		/// </summary>
#else
		/// <summary>
		/// Whether the relative change from the start state.
		/// </summary>
#endif
		[SerializeField]
		private bool _Relative;

#if ARBOR_DOC_JA
		/// <summary>
		/// 開始位置。
		/// </summary>
#else
		/// <summary>
		/// Starting position.
		/// </summary>
#endif
		[SerializeField]
		private FlexibleVector3 _From;

#if ARBOR_DOC_JA
		/// <summary>
		/// 目標位置。
		/// </summary>
#else
		/// <summary>
		/// Target position.
		/// </summary>
#endif
		[SerializeField]
		private FlexibleVector3 _To;

		[SerializeField]
		[HideInInspector]
		private int _SerializeVersion;

		#region old

		[SerializeField, FormerlySerializedAs( "_Target" )]
		[HideInInspector]
		private Rigidbody _OldTarget;

		[SerializeField, FormerlySerializedAs( "_From" )]
		[HideInInspector]
		private Vector3 _OldFrom;

		[SerializeField, FormerlySerializedAs( "_To" )]
		[HideInInspector]
		private Vector3 _OldTo;

		#endregion // old

		#endregion // Serialize fields

		void SerializeVer1()
		{
			_Target = (FlexibleRigidbody)_OldTarget;
			_From = (FlexibleVector3)_OldFrom;
			_To = (FlexibleVector3)_OldTo;
		}

		void INodeBehaviourSerializationCallbackReceiver.OnBeforeSerialize()
		{
			if (_SerializeVersion == 0)
			{
				SerializeVer1();
				_SerializeVersion = 1;
			}
		}

		void INodeBehaviourSerializationCallbackReceiver.OnAfterDeserialize()
		{
			if (_SerializeVersion == 0)
			{
				SerializeVer1();
				_SerializeVersion = 1;
			}
		}

		Rigidbody _MyRigidbody;
		Rigidbody cachedTarget
		{
			get
			{
				Rigidbody rigidbody = _Target.value;
				if (rigidbody == null && _Target.type == FlexibleRigidbody.Type.Constant )
				{
					if( _MyRigidbody == null )
					{
						_MyRigidbody = GetComponent<Rigidbody>();
					}

					rigidbody = _MyRigidbody;
				}
				return rigidbody;
			}
		}

		protected override bool fixedUpdate
		{
			get
			{
				return true;
			}
		}
		
		Vector3 _StartPosition;

		protected override void OnTweenBegin()
		{
			Rigidbody target = cachedTarget;
			if (_Relative && target != null)
			{
				_StartPosition = target.position;
			}
			else
			{
				_StartPosition = Vector3.zero;
            }
		}

		protected override void OnTweenUpdate (float factor)
		{
			Rigidbody target = cachedTarget;
			if (target != null)
			{
				target.MovePosition(_StartPosition + Vector3.Lerp(_From.value, _To.value, factor) );
			}
		}
	}
}
