﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Rigidbody2Dの位置を徐々に変化させる。
	/// </summary>
#else
	/// <summary>
	/// Gradually change position of Rigidbody2D.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("Tween/TweenRigidbody2DPosition")]
	[BuiltInBehaviour]
	public class TweenRigidbody2DPosition : TweenBase, INodeBehaviourSerializationCallbackReceiver
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 対象となるRigidbody2D。<br/>
		/// TypeがConstantの時に指定しない場合、ArborFSMを割り当ててあるGameObjectのRigidbody2D。
		/// </summary>
#else
		/// <summary>
		/// Rigidbody2D of interest.<br/>
		/// If Type is Constant and nothing is specified, Rigidbody2D of GameObject to which ArborFSM is assigned.
		/// </summary>
#endif
		[SerializeField]
		private FlexibleRigidbody2D _Target;

#if ARBOR_DOC_JA
		/// <summary>
		/// 開始した状態からの相対的な変化かどうか。
		/// </summary>
#else
		/// <summary>
		/// Whether the relative change from the start state.
		/// </summary>
#endif
		[SerializeField]
		private bool _Relative;

#if ARBOR_DOC_JA
		/// <summary>
		/// 開始位置。
		/// </summary>
#else
		/// <summary>
		/// Starting position.
		/// </summary>
#endif
		[SerializeField]
		private FlexibleVector2 _From;

#if ARBOR_DOC_JA
		/// <summary>
		/// 目標位置。
		/// </summary>
#else
		/// <summary>
		/// Target position.
		/// </summary>
#endif
		[SerializeField]
		private FlexibleVector2 _To;

		[SerializeField]
		[HideInInspector]
		private int _SerializeVersion;

		#region old

		[SerializeField, FormerlySerializedAs( "_Target" )]
		[HideInInspector]
		private Rigidbody2D _OldTarget;

		[SerializeField, FormerlySerializedAs( "_From" )]
		[HideInInspector]
		private Vector2 _OldFrom;

		[SerializeField, FormerlySerializedAs( "_To" )]
		[HideInInspector]
		private Vector2 _OldTo;

		#endregion // old

		#endregion // Serialize fields

		void SerializeVer1()
		{
			_Target = (FlexibleRigidbody2D)_OldTarget;
			_From = (FlexibleVector2)_OldFrom;
			_To = (FlexibleVector2)_OldTo;
		}

		void INodeBehaviourSerializationCallbackReceiver.OnBeforeSerialize()
		{
			if (_SerializeVersion == 0)
			{
				SerializeVer1();
				_SerializeVersion = 1;
			}
		}

		void INodeBehaviourSerializationCallbackReceiver.OnAfterDeserialize()
		{
			if (_SerializeVersion == 0)
			{
				SerializeVer1();
				_SerializeVersion = 1;
			}
		}

		Rigidbody2D _MyRigidbody2D;
		Rigidbody2D cachedTarget
		{
			get
			{
				Rigidbody2D rigidbody = _Target.value;
				if (rigidbody == null && _Target.type == FlexibleRigidbody2D.Type.Constant )
				{
					if( _MyRigidbody2D == null )
					{
						_MyRigidbody2D = GetComponent<Rigidbody2D>();
					}

					rigidbody = _MyRigidbody2D;
				}
				return rigidbody;
			}
		}

		protected override bool fixedUpdate
		{
			get
			{
				return true;
			}
		}
		
		Vector2 _StartPosition;

		protected override void OnTweenBegin()
		{
			Rigidbody2D target = cachedTarget;

			if (_Relative && target != null)
			{
				_StartPosition = target.position;
			}
			else
			{
				_StartPosition = Vector2.zero;
			}
		}

		protected override void OnTweenUpdate (float factor)
		{
			Rigidbody2D target = cachedTarget;
			if (target != null)
			{
				target.MovePosition(_StartPosition + Vector2.Lerp(_From.value, _To.value, factor) );
			}
		}
	}
}
