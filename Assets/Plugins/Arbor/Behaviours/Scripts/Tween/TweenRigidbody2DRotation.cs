﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Rigidbody2Dの向きを徐々に変化させる。
	/// </summary>
#else
	/// <summary>
	/// Gradually change rotaion of Rigidbody2D.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("Tween/TweenRigidbody2DRotation")]
	[BuiltInBehaviour]
	public class TweenRigidbody2DRotation : TweenBase, INodeBehaviourSerializationCallbackReceiver
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 対象となるRigidbody2D。<br/>
		/// TypeがConstantの時に指定しない場合、ArborFSMを割り当ててあるGameObjectのRigidbody2D。
		/// </summary>
#else
		/// <summary>
		/// Rigidbody2D of interest.<br/>
		/// If Type is Constant and nothing is specified, Rigidbody2D of GameObject to which ArborFSM is assigned.
		/// </summary>
#endif
		[SerializeField]
		private FlexibleRigidbody2D _Target;

#if ARBOR_DOC_JA
		/// <summary>
		/// 開始した状態からの相対的な変化かどうか。
		/// </summary>
#else
		/// <summary>
		/// Whether the relative change from the start state.
		/// </summary>
#endif
		[SerializeField]
		private bool _Relative;

#if ARBOR_DOC_JA
		/// <summary>
		/// 開始向き。
		/// </summary>
#else
		/// <summary>
		/// Start orientation.
		/// </summary>
#endif
		[SerializeField]
		private FlexibleFloat _From;

#if ARBOR_DOC_JA
		/// <summary>
		/// 目標向き。
		/// </summary>
#else
		/// <summary>
		/// Goal orientation.
		/// </summary>
#endif
		[SerializeField]
		private FlexibleFloat _To;
		
		[SerializeField]
		[HideInInspector]
		private int _SerializeVersion;

		#region old

		[SerializeField, FormerlySerializedAs( "_Target" )]
		[HideInInspector]
		private Rigidbody2D _OldTarget;

		[SerializeField, FormerlySerializedAs( "_From" )]
		[HideInInspector]
		private float _OldFrom;

		[SerializeField, FormerlySerializedAs( "_To" )]
		[HideInInspector]
		private float _OldTo;

		#endregion // old

		#endregion // Serialize fields

		void SerializeVer1()
		{
			_Target = (FlexibleRigidbody2D)_OldTarget;
			_From = (FlexibleFloat)_OldFrom;
			_To = (FlexibleFloat)_OldTo;
		}

		void INodeBehaviourSerializationCallbackReceiver.OnBeforeSerialize()
		{
			if (_SerializeVersion == 0)
			{
				SerializeVer1();
				_SerializeVersion = 1;
			}
		}

		void INodeBehaviourSerializationCallbackReceiver.OnAfterDeserialize()
		{
			if (_SerializeVersion == 0)
			{
				SerializeVer1();
				_SerializeVersion = 1;
			}
		}

		Rigidbody2D _MyRigidbody2D;
		Rigidbody2D cachedTarget
		{
			get
			{
				Rigidbody2D rigidbody = _Target.value;
				if (rigidbody == null && _Target.type == FlexibleRigidbody2D.Type.Constant )
				{
					if( _MyRigidbody2D == null )
					{
						_MyRigidbody2D = GetComponent<Rigidbody2D>();
					}

					rigidbody = _MyRigidbody2D;
				}
				return rigidbody;
			}
		}

		protected override bool fixedUpdate
		{
			get
			{
				return true;
			}
		}
		
		float _StartRotation;

		protected override void OnTweenBegin()
		{
			Rigidbody2D target = cachedTarget;
			if (_Relative && target != null)
			{
				_StartRotation = target.rotation;
			}
			else
			{
				_StartRotation = 0.0f;
            }
		}

		protected override void OnTweenUpdate (float factor)
		{
			Rigidbody2D target = cachedTarget;
			if (target != null)
			{
				target.MoveRotation( _StartRotation + Mathf.Lerp(_From.value, _To.value, factor) );
			}
		}
	}
}
