﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 座標を徐々に変化させる。
	/// </summary>
#else
	/// <summary>
	/// Gradually change position.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("Tween/TweenPosition")]
	[BuiltInBehaviour]
	public class TweenPosition : TweenBase, INodeBehaviourSerializationCallbackReceiver
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 対象となるTransform。<br/>
		/// TypeがConstantの時に指定しない場合、ArborFSMがアタッチされているGameObjectのTransformとなる。
		/// </summary>
#else
		/// <summary>
		/// Transform of interest.<br/>
		/// If Type is Constant and nothing is specified, ArborFSM is the Transform of the attached GameObject.
		/// </summary>
#endif
		[SerializeField]
		private FlexibleTransform _Target;

#if ARBOR_DOC_JA
		/// <summary>
		/// 開始した状態からの相対的な変化かどうか。
		/// </summary>
#else
		/// <summary>
		/// Whether the relative change from the start state.
		/// </summary>
#endif
		[SerializeField]
		private bool _Relative;

#if ARBOR_DOC_JA
		/// <summary>
		/// 開始地点。
		/// </summary>
#else
		/// <summary>
		/// Starting point.
		/// </summary>
#endif
		[SerializeField]
		private FlexibleVector3 _From;

#if ARBOR_DOC_JA
		/// <summary>
		/// 目標地点。
		/// </summary>
#else
		/// <summary>
		/// Target point.
		/// </summary>
#endif
		[SerializeField]
		private FlexibleVector3 _To;

		[SerializeField]
		[HideInInspector]
		private int _SerializeVersion;

		#region old

		[SerializeField, FormerlySerializedAs( "_Target" )]
		[HideInInspector]
		private Transform _OldTarget;

		[SerializeField, FormerlySerializedAs( "_From" )]
		[HideInInspector]
		private Vector3 _OldFrom;

		[SerializeField, FormerlySerializedAs( "_To" )]
		[HideInInspector]
		private Vector3 _OldTo;

		#endregion // old

		#endregion // Serialize fields

		Transform _MyTransform;
		Transform cachedTarget
		{
			get
			{
				Transform transform = _Target.value;
				if (transform == null && _Target.type == FlexibleTransform.Type.Constant )
				{
					if( _MyTransform == null )
					{
						_MyTransform = GetComponent<Transform>();
					}

					transform = _MyTransform;
				}
				return transform;
			}
		}

		void SerializeVer1()
		{
			_Target = (FlexibleTransform)_OldTarget;
			_From = (FlexibleVector3)_OldFrom;
			_To = (FlexibleVector3)_OldTo;
		}

		void INodeBehaviourSerializationCallbackReceiver.OnBeforeSerialize()
		{
			if (_SerializeVersion == 0)
			{
				SerializeVer1();
				_SerializeVersion = 1;
			}
		}

		void INodeBehaviourSerializationCallbackReceiver.OnAfterDeserialize()
		{
			if (_SerializeVersion == 0)
			{
				SerializeVer1();
				_SerializeVersion = 1;
			}
		}
		
		Vector3 _StartPosition;

		protected override void OnTweenBegin()
		{
			Transform target = cachedTarget;

			if (_Relative && target != null)
			{
				_StartPosition = target.localPosition;
			}
			else
			{
				_StartPosition = Vector3.zero;
			}
		}

		protected override void OnTweenUpdate(float factor)
		{
			Transform target = cachedTarget;
			if (target != null)
			{
				target.localPosition = _StartPosition + Vector3.Lerp(_From.value, _To.value, factor);
			}
		}
	}
}
