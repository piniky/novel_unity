﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// TextureのUV座標を徐々に変化させる。
	/// </summary>
#else
	/// <summary>
	/// Gradually change UV cordinates of Texture.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("Tween/TweenTextureOffset")]
	[BuiltInBehaviour]
	public class TweenTextureOffset : TweenBase, INodeBehaviourSerializationCallbackReceiver
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 対象となるRenderer。<br/>
		/// 指定しない場合は、ArborFSMと同じGameObjectに割り当てられているRenderer。
		/// </summary>
#else
		/// <summary>
		/// Renderer of interest.<br/>
		/// If not specified, Renderer of GameObject that ArborFSM is assigned a target.
		/// </summary>
#endif
		[SerializeField]
		[SlotType(typeof(Renderer))]
		private FlexibleComponent _Target;

#if ARBOR_DOC_JA
		/// <summary>
		/// Textureのプロパティ名。
		/// </summary>
#else
		/// <summary>
		/// Property name of Texture.
		/// </summary>
#endif
		[SerializeField] private string _PropertyName = "_MainTex";

#if ARBOR_DOC_JA
		/// <summary>
		/// 開始した状態からの相対的な変化かどうか。
		/// </summary>
#else
		/// <summary>
		/// Whether the relative change from the start state.
		/// </summary>
#endif
		[SerializeField] private bool _Relative;

#if ARBOR_DOC_JA
		/// <summary>
		/// 開始UV座標。
		/// </summary>
#else
		/// <summary>
		/// Start UV coordinates.
		/// </summary>
#endif
		[SerializeField] private FlexibleVector2 _From;

#if ARBOR_DOC_JA
		/// <summary>
		/// 目標UV座標。
		/// </summary>
#else
		/// <summary>
		/// The goal UV coordinates.
		/// </summary>
#endif
		[SerializeField] private FlexibleVector2 _To;

		[SerializeField]
		[HideInInspector]
		private int _SerializeVersion;

		#region old

		[SerializeField]
		[FormerlySerializedAs("_Target")]
		[HideInInspector]
		private Renderer _OldTarget;

		[SerializeField, FormerlySerializedAs( "_From" )]
		[HideInInspector]
		private Vector2 _OldFrom = Vector2.zero;

		[SerializeField, FormerlySerializedAs( "_To" )]
		[HideInInspector]
		private Vector2 _OldTo = Vector2.zero;

		#endregion // old

		#endregion // Serialize fields

		private Renderer _MyRenderer;
		public Renderer cachedTarget
		{
			get
			{
				Renderer renderer = _Target.value as Renderer;
				if (renderer == null && _Target.type == FlexibleComponent.Type.Constant)
				{
					if (_MyRenderer == null)
					{
						_MyRenderer = GetComponent<Renderer>();
					}

					renderer = _MyRenderer;
				}
				return renderer;
			}
		}

		private const int _CurrentSerializeVersion = 2;

		void SerializeVer1()
		{
			_From = (FlexibleVector2)_OldFrom;
			_To = (FlexibleVector2)_OldTo;
		}

		void SerializeVer2()
		{
			_Target = (FlexibleComponent)_OldTarget;
		}

		void Serialize()
		{
			while (_SerializeVersion != _CurrentSerializeVersion)
			{
				switch (_SerializeVersion)
				{
					case 0:
						SerializeVer1();
						_SerializeVersion++;
						break;
					case 1:
						SerializeVer2();
						_SerializeVersion++;
						break;
					default:
						_SerializeVersion = _CurrentSerializeVersion;
						break;
				}
			}
		}

		void INodeBehaviourSerializationCallbackReceiver.OnBeforeSerialize()
		{
			Serialize();
		}

		void INodeBehaviourSerializationCallbackReceiver.OnAfterDeserialize()
		{
			Serialize();
		}

		Vector2 _StartOffset;

		protected override void OnTweenBegin()
		{
			Renderer target = cachedTarget;
			if (_Relative && target != null )
			{
				_StartOffset = target.sharedMaterial.GetTextureOffset(_PropertyName);
			}
			else
			{
				_StartOffset = Vector2.zero;
			}
		}

		protected override void OnTweenUpdate(float factor)
		{
			Renderer target = cachedTarget;
			if (target != null)
			{
				target.sharedMaterial.SetTextureOffset(_PropertyName , _StartOffset + Vector2.Lerp(_From.value, _To.value, factor) );
			}
		}
	}
}
