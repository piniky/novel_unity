﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// スケールを徐々に変化させる。
	/// </summary>
#else
	/// <summary>
	/// Gradually change scale.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("Tween/TweenScale")]
	[BuiltInBehaviour]
	public class TweenScale : TweenBase, INodeBehaviourSerializationCallbackReceiver
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 対象となるTransform。<br/>
		/// TypeがConstantの時に指定しない場合、ArborFSMがアタッチされているGameObjectのTransformとなる。
		/// </summary>
#else
		/// <summary>
		/// Transform of interest.<br/>
		/// If Type is Constant and nothing is specified, ArborFSM is the Transform of the attached GameObject.
		/// </summary>
#endif
		[SerializeField]
		private FlexibleTransform _Target;

#if ARBOR_DOC_JA
		/// <summary>
		/// 開始した状態からの相対的な変化かどうか。
		/// </summary>
#else
		/// <summary>
		/// Whether the relative change from the start state.
		/// </summary>
#endif
		[SerializeField]
		private bool _Relative;

#if ARBOR_DOC_JA
		/// <summary>
		/// 開始スケール。
		/// </summary>
#else
		/// <summary>
		/// Start scale.
		/// </summary>
#endif
		[SerializeField]
		private FlexibleVector3 _From = new FlexibleVector3(Vector3.one);

#if ARBOR_DOC_JA
		/// <summary>
		/// 目標スケール。
		/// </summary>
#else
		/// <summary>
		/// Goal scale.
		/// </summary>
#endif
		[SerializeField]
		private FlexibleVector3 _To = new FlexibleVector3(Vector3.one);

		[SerializeField]
		[HideInInspector]
		private int _SerializeVersion;

		#region old

		[SerializeField, FormerlySerializedAs( "_Target" )]
		[HideInInspector]
		private Transform _OldTarget;

		[SerializeField, FormerlySerializedAs( "_From" )]
		[HideInInspector]
		private Vector3 _OldFrom = Vector3.one;

		[SerializeField, FormerlySerializedAs( "_To" )]
		[HideInInspector]
		private Vector3 _OldTo = Vector3.one;

		#endregion // old

		#endregion // Serialize fields

		void SerializeVer1()
		{
			_Target = (FlexibleTransform)_OldTarget;
			_From = (FlexibleVector3)_OldFrom;
			_To = (FlexibleVector3)_OldTo;
		}

		void INodeBehaviourSerializationCallbackReceiver.OnBeforeSerialize()
		{
			if (_SerializeVersion == 0)
			{
				SerializeVer1();
				_SerializeVersion = 1;
			}
		}

		void INodeBehaviourSerializationCallbackReceiver.OnAfterDeserialize()
		{
			if (_SerializeVersion == 0)
			{
				SerializeVer1();
				_SerializeVersion = 1;
			}
		}

		Transform _MyTransform;
		Transform cachedTarget
		{
			get
			{
				Transform transform = _Target.value;
				if( transform == null && _Target.type == FlexibleTransform.Type.Constant )
				{
					if( _MyTransform == null )
					{
						_MyTransform = GetComponent<Transform>();
					}

					transform = _MyTransform;
				}
				return transform;
			}
		}

		Vector3 _StartScale;

		protected override void OnTweenBegin()
		{
			Transform target = cachedTarget;
			if (_Relative && target != null)
			{
				_StartScale = target.localScale;
			}
			else
			{
				_StartScale = Vector3.zero;
			}
		}

		protected override void OnTweenUpdate(float factor)
		{
			Transform target = cachedTarget;
			if (target != null)
			{
				target.localScale = _StartScale + Vector3.Lerp(_From.value, _To.value, factor);
			}
		}
	}
}
