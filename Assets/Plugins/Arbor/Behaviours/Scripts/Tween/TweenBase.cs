﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Tweenを行う基本クラス
	/// </summary>
#else
	/// <summary>
	/// Base class for Tweening
	/// </summary>
#endif
	[AddComponentMenu("")]
	[HideBehaviour]
	public class TweenBase : StateBehaviour 
	{
		#region enum

		public enum Type
		{
			Once,
			Loop,
			PingPong,
		};

		#endregion // enum

		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 再生タイプ。
		/// <ul>
		/// <li>Once<br/>１回のみ</li>
		/// <li>Loop<br/>繰り返し</li>
		/// <li>PingPong<br/>終端で折り返し</li>
		/// </ul>
		/// </summary>
#else
		/// <summary>
		/// Play type.
		/// <ul>
		/// <li>Once<br/>Only once</li>
		/// <li>Loop<br/>Repeat</li>
		/// <li>PingPong<br/>Turn back at the end</li>
		/// </ul>
		/// </summary>
#endif
		[SerializeField] private Type _Type;

#if ARBOR_DOC_JA
		/// <summary>
		/// 再生時間。
		/// </summary>
#else
		/// <summary>
		/// Playback time.
		/// </summary>
#endif
		[SerializeField] private float _Duration = 1.0f;

#if ARBOR_DOC_JA
		/// <summary>
		/// 時間に対する適用度の変化曲線
		/// </summary>
#else
		/// <summary>
		/// Change curve of applicability with respect to time
		/// </summary>
#endif
		[SerializeField] private AnimationCurve _Curve = AnimationCurve.Linear( 0.0f,0.0f,1.0f,1.0f );

#if ARBOR_DOC_JA
		/// <summary>
		/// Time.timeScaleの影響を受けずリアルタイムに進行するフラグ。
		/// </summary>
#else
		/// <summary>
		/// Flag to progress in real time without the influence of Time.timeScale.
		/// </summary>
#endif
		[SerializeField] private bool _UseRealtime = false;

#if ARBOR_DOC_JA
		/// <summary>
		/// 遷移するまでの繰り返し回数(Loop、PingPongのみ)
		/// </summary>
#else
		/// <summary>
		/// Number of repetitions until the transition (Loop, PingPong only)
		/// </summary>
#endif
		[SerializeField] private int _RepeatUntilTransition = 1;

#if ARBOR_DOC_JA
		/// <summary>
		/// 時間経過後の遷移先。<br/>
		/// (Loop、Pingpongの場合、Repeat Until Transitionで指定した回数だけ繰り返してから遷移する)
		/// </summary>
#else
		/// <summary>
		/// Transition destination after time.<br/>
		/// (In the case of Loop and Pingpong, to transition from repeated as many times as specified in the Repeat Until Transition)
		/// </summary>
#endif
		[SerializeField] private StateLink _NextState;

		#endregion // Serialize fields

		protected virtual bool fixedUpdate
		{
			get
			{
				return false;
			}
		}

		private float _BeginTime = 0.0f;

		private float _FromAdvance = 0.0f;
		private float _ToAdvance = 1.0f;

		private int _RepeatCount = 0;
		public int repeatCount
		{
			get
			{
				return _RepeatCount;
            }
		}
		
		private float GetTime()
		{
			if( _UseRealtime )
			{
				return Time.realtimeSinceStartup;
			}

			if (fixedUpdate)
			{
				return Time.fixedTime;
			}

			return Time.time;
		}

		// Use this for enter state
		public override void OnStateBegin() 
		{
			_BeginTime = GetTime();

			_FromAdvance = 0.0f;
			_ToAdvance = 1.0f;
			_RepeatCount = 0;

			OnTweenBegin();

			OnTweenUpdate(_Curve.Evaluate(0.0f));
		}

		protected virtual void OnTweenBegin(){}
		protected virtual void OnTweenUpdate( float factor ){}

		void TweenUpdate()
		{
			float nowTime = GetTime();

			float t = 0.0f;

			if (_Duration > 0.0f)
			{
				t = (nowTime - _BeginTime) / _Duration;
			}
			else
			{
				t = 1.0f;
			}

			float factor = Mathf.Lerp(_FromAdvance, _ToAdvance, Mathf.Clamp01(t));

			if (t > 1.0f)
			{
				switch (_Type)
				{
					case Type.Once:
						break;
					case Type.Loop:
						_BeginTime = nowTime;
						break;
					case Type.PingPong:
						_BeginTime = nowTime;

						float temp = _FromAdvance;
						_FromAdvance = _ToAdvance;
						_ToAdvance = temp;
						break;
				}

				if (_Type == Type.Once)
				{
					Transition(_NextState);
				}
				else
				{
					_RepeatCount++;
					if (_RepeatCount >= _RepeatUntilTransition)
					{
						Transition(_NextState);
					}
				}
			}

			OnTweenUpdate(_Curve.Evaluate(factor));
		}
		
		// Update is called once per frame
		void Update () 
		{
			if (!fixedUpdate)
			{
				TweenUpdate();
            }
		}

		void FixedUpdate()
		{
			if ( fixedUpdate )
			{
				TweenUpdate();
			}
		}
	}
}
