﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// CanvasGroupのAlphaを徐々に変化させる。
	/// </summary>
#else
	/// <summary>
	/// Gradually change Alpha of Canvas Group.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("Tween/TweenCanvasGroupAlpha")]
	[BuiltInBehaviour]
	public class TweenCanvasGroupAlpha : TweenBase, INodeBehaviourSerializationCallbackReceiver
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 対象となるCanvasGroup。<br/>
		/// 指定しない場合は、ArborFSMが割り当てられているGameObjectのCanvasGroup。
		/// </summary>
#else
		/// <summary>
		/// CanvasGroup of interest.<br/>
		/// If not specified, CanvasGroup of GameObject that ArborFSM is assigned a target.
		/// </summary>
#endif
		[SerializeField]
		[SlotType(typeof(CanvasGroup))]
		private FlexibleComponent _Target;

#if ARBOR_DOC_JA
		/// <summary>
		/// 開始アルファ。
		/// </summary>
#else
		/// <summary>
		/// Start alpha.
		/// </summary>
#endif
		[SerializeField]
		private FlexibleFloat _From;

#if ARBOR_DOC_JA
		/// <summary>
		/// 目標アルファ。
		/// </summary>
#else
		/// <summary>
		/// Target alpha.
		/// </summary>
#endif
		[SerializeField]
		private FlexibleFloat _To;

		[SerializeField]
		[HideInInspector]
		private int _SerializeVersion;

		#region old

		[SerializeField]
		[FormerlySerializedAs("_Target")]
		[HideInInspector]
		private CanvasGroup _OldTarget;

		[SerializeField, FormerlySerializedAs( "_From" )]
		[HideInInspector]
		private float _OldFrom;

		[SerializeField, FormerlySerializedAs( "_To" )]
		[HideInInspector]
		private float _OldTo;

		#endregion // old

		#endregion // Serialize fields

		private CanvasGroup _MyCanvasGroup;
		public CanvasGroup cachedTarget
		{
			get
			{
				CanvasGroup canvasGroup = _Target.value as CanvasGroup;
				if (canvasGroup == null && _Target.type == FlexibleComponent.Type.Constant)
				{
					if (_MyCanvasGroup == null)
					{
						_MyCanvasGroup = GetComponent<CanvasGroup>();
					}

					canvasGroup = _MyCanvasGroup;
				}
				return canvasGroup;
			}
		}

		void SerializeVer1()
		{
			_From = (FlexibleFloat)_OldFrom;
			_To = (FlexibleFloat)_OldTo;
		}

		void SerializeVer2()
		{
			_Target = (FlexibleComponent)_OldTarget;
		}

		private const int _CurrentSerializeVersion = 2;

		void Serialize()
		{
			while (_SerializeVersion != _CurrentSerializeVersion)
			{
				switch (_SerializeVersion)
				{
					case 0:
						SerializeVer1();
						_SerializeVersion++;
						break;
					case 1:
						SerializeVer2();
						_SerializeVersion++;
						break;
					default:
						_SerializeVersion = _CurrentSerializeVersion;
						break;
				}
			}
		}

		void INodeBehaviourSerializationCallbackReceiver.OnBeforeSerialize()
		{
			Serialize();
		}

		void INodeBehaviourSerializationCallbackReceiver.OnAfterDeserialize()
		{
			Serialize();
		}
		
		protected override void OnTweenUpdate(float factor)
		{
			CanvasGroup target = cachedTarget;
			if (target != null)
			{
				target.alpha = Mathf.Lerp(_From.value, _To.value, factor);
			}
		}
	}
}
