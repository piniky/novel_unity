﻿using UnityEngine;
using System.Collections;
using System;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Agentを初期位置の周辺を巡回させる。
	/// </summary>
	/// <remarks>
	/// インターバルはMin IntervalとMax Intervalの範囲からランダムに決定する。<br/>
	/// </remarks>
#else
	/// <summary>
	/// The Agent to patrol the vicinity of the initial position.
	/// </summary>
	/// <remarks>
	/// Intervals are randomly determined from the range of Min Interval and Max Interval.<br/>
	/// </remarks>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("Agent/AgentPatrol")]
	[BuiltInBehaviour]
	public class AgentPatrol : AgentIntervalUpdate
	{
		#region Serialize fields
		
#if ARBOR_DOC_JA
		/// <summary>
		/// 移動半径
		/// </summary>
#else
		/// <summary>
		/// Moving radius
		/// </summary>
#endif
		[SerializeField] private float _Radius;

		#endregion // Serialize fields

		protected override void OnUpdateAgent()
		{
			AgentController agentController = cachedAgentController;
			if (agentController != null)
			{
				agentController.Patrol(_Speed, _Radius);
			}
		}
	}
}
