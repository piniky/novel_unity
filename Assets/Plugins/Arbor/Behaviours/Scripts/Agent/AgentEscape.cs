﻿using System;
using UnityEngine;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// AgentをTargetから逃げるように移動させる。
	/// </summary>
	/// <remarks>
	/// インターバルはMin IntervalとMax Intervalの範囲からランダムに決定する。<br/>
	/// </remarks>
#else
	/// <summary>
	/// Move the Agent to escape from Target.
	/// </summary>
	/// <remarks>
	/// Intervals are randomly determined from the range of Min Interval and Max Interval.<br/>
	/// </remarks>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("Agent/AgentEscape")]
	[BuiltInBehaviour]
	public class AgentEscape : AgentIntervalUpdate
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 離れる距離
		/// </summary>
#else
		/// <summary>
		/// Distance away
		/// </summary>
#endif
		[SerializeField] private float _Distance;

#if ARBOR_DOC_JA
		/// <summary>
		/// 逃げたい対象のTransform
		/// </summary>
#else
		/// <summary>
		/// Transform of object to escape
		/// </summary>
#endif
		[SerializeField] private FlexibleTransform _Target;
		
#if ARBOR_DOC_JA
		/// <summary>
		/// 移動完了した時のステート遷移
		/// </summary>
#else
		/// <summary>
		/// State transition at the time of movement completion
		/// </summary>
#endif
		[SerializeField] private StateLink _Done;

		#endregion

		protected override void OnUpdateAgent()
		{
			AgentController agentController = cachedAgentController;
			if (agentController != null)
			{
				agentController.Escape(_Speed, _Distance, _Target.value);
			}
		}

		protected override void OnDone()
		{
			Transition(_Done);
		}
	}
}
