﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// AnimatorのParameterを演算して変更する。
	/// </summary>
#else
	/// <summary>
	/// Calculate and change the parameter of Animator.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("Animator/CalcAnimatorParameter")]
	[BuiltInBehaviour]
	public class CalcAnimatorParameter : StateBehaviour, INodeBehaviourSerializationCallbackReceiver
	{
#region enum

		public enum Function
		{
			Assign,
			Add,
		}

		#endregion // enum

		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 参照するAnimatorとParameter名。
		/// </summary>
#else
		/// <summary>
		/// Reference Animator and Parameter name.
		/// </summary>
#endif
		[FormerlySerializedAs("reference")]
		[SerializeField]
		private AnimatorParameterReference _Reference;

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算するタイプ(Int、Floatのみ)。<br/>
		/// <ul>
		/// <li>Assign<br/>値を代入する。</li>
		/// <li>Add<br/>値を加算する。<br/>減算したい場合は負値を指定する。</li>
		/// </ul>
		/// </summary>
#else
		/// <summary>
		/// Type to calculate (Int, Float only).<br/>
		/// <ul>
		/// <li>Assign<br/>Substitute values.</li>
		/// <li>Add<br/>Add values.<br/>To subtract it, specify a negative value.</li>
		/// </ul>
		/// </summary>
#endif
		[FormerlySerializedAs("function")]
		[SerializeField]
		private Function _Function;

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算するfloat値
		/// </summary>
#else
		/// <summary>
		/// float value to be computed
		/// </summary>
#endif
		[SerializeField]
		private FlexibleFloat _FloatValue;

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算するint値
		/// </summary>
#else
		/// <summary>
		/// int value to be computed
		/// </summary>
#endif
		[SerializeField]
		private FlexibleInt _IntValue;

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算するbool値。<br/>
		/// bool値の場合はこの値をそのまま代入する。
		/// </summary>
#else
		/// <summary>
		/// bool value to compute.<br/>
		/// In case of bool value, substitute this value as it is.
		/// </summary>
#endif
		[SerializeField]
		private FlexibleBool _BoolValue;

		[SerializeField]
		[HideInInspector]
		private int _SerializeVersion;

#region old

		[FormerlySerializedAs( "floatValue" )]
		[SerializeField]
		[HideInInspector]
		private float _OldFloatValue;

		[FormerlySerializedAs( "intValue" )]
		[SerializeField]
		[HideInInspector]
		private int _OldIntValue;

		[FormerlySerializedAs( "boolValue" )]
		[SerializeField]
		[HideInInspector]
		private bool _OldBoolValue;

#endregion // old

#endregion // Serialize fields

		public float floatValue
		{
			get
			{
				return _FloatValue.value;
			}
		}

		public int intValue
		{
			get
			{
				return _IntValue.value;
			}
		}

		public bool boolValue
		{
			get
			{
				return _BoolValue.value;
			}
		}

		void SerializeVer1()
		{
			_FloatValue = (FlexibleFloat)_OldFloatValue;
			_IntValue = (FlexibleInt)_OldIntValue;
			_BoolValue = (FlexibleBool)_OldBoolValue;
		}
		
		void INodeBehaviourSerializationCallbackReceiver.OnBeforeSerialize()
		{
			if (_SerializeVersion == 0)
			{
				SerializeVer1();
				_SerializeVersion = 1;
			}
		}

		void INodeBehaviourSerializationCallbackReceiver.OnAfterDeserialize()
		{
			if (_SerializeVersion == 0)
			{
				SerializeVer1();
			}
		}

		private int _ParameterID;

		void Awake()
		{
			_ParameterID = Animator.StringToHash(_Reference.name);
        }

		// Use this for enter state
		public override void OnStateBegin()
		{
			if (_Reference.animator == null)
			{
				return;
			}

			switch (_Reference.type)
			{
				case 1:// Float
					{
						float value = _Reference.animator.GetFloat(_ParameterID);
						switch (_Function)
						{
							case Function.Assign:
								value = floatValue;
								break;
							case Function.Add:
								value += floatValue;
								break;
						}
						_Reference.animator.SetFloat(_ParameterID, value);
					}
					break;
				case 3:// Int
					{
						int value = _Reference.animator.GetInteger(_ParameterID);
						switch (_Function)
						{
							case Function.Assign:
								value = intValue;
								break;
							case Function.Add:
								value += intValue;
								break;
						}
						_Reference.animator.SetInteger(_ParameterID, value);
					}
					break;
				case 4:// Bool
					{
						_Reference.animator.SetBool(_ParameterID, boolValue);
					}
					break;
				case 9:// Trigger
					{
						_Reference.animator.SetTrigger(_ParameterID);
					}
					break;
			}
		}
	}
}
