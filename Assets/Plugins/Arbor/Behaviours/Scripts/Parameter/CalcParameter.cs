﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Parameterの値を演算して変更する。
	/// </summary>
#else
	/// <summary>
	/// Change by calculating the value of the Parameter.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("Parameter/CalcParameter")]
	[BuiltInBehaviour]
	public class CalcParameter : StateBehaviour, INodeBehaviourSerializationCallbackReceiver
	{
		#region enum

		public enum Function
		{
			Assign,
			Add,
		}

		#endregion // enum

		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 参照するパラメータ。
		/// </summary>
#else
		/// <summary>
		/// Parameters to be referenced.
		/// </summary>
#endif
		public ParameterReference reference;

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算するタイプ(Int、Float、Stringのみ)。<br/>
		/// <ul>
		/// <li>Assign<br/>値を代入する。</li>
		/// <li>Add<br/>値を加算する。<br/>減算したい場合は負値を指定する。</li>
		/// </ul>
		/// </summary>
#else
		/// <summary>
		/// Type to calculate (Int, Float, String only).<br/>
		/// <ul>
		/// <li>Assign<br/>Substitute values.</li>
		/// <li>Add<br/>Add values.<br/>To subtract it, specify a negative value.</li>
		/// </ul>
		/// </summary>
#endif
		public Function function;

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算するInt値
		/// </summary>
#else
		/// <summary>
		/// Int value to be computed
		/// </summary>
#endif
		[SerializeField]
		private FlexibleInt _IntValue;

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算するLong値
		/// </summary>
#else
		/// <summary>
		/// Long value to be computed
		/// </summary>
#endif
		[SerializeField]
		private FlexibleLong _LongValue;

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算するFloat値
		/// </summary>
#else
		/// <summary>
		/// Float value to be computed
		/// </summary>
#endif
		[SerializeField]
		private FlexibleFloat _FloatValue;

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算するBool値
		/// </summary>
#else
		/// <summary>
		/// Bool value to be computed
		/// </summary>
#endif
		[SerializeField]
		private FlexibleBool _BoolValue;

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算するString値
		/// </summary>
#else
		/// <summary>
		/// String value to be computed
		/// </summary>
#endif
		[SerializeField]
		private FlexibleString _StringValue;

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算するGameObject値
		/// </summary>
#else
		/// <summary>
		/// GameObject value to be computed
		/// </summary>
#endif
		[SerializeField]
		private FlexibleGameObject _GameObjectValue;

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算するVector2値
		/// </summary>
#else
		/// <summary>
		/// Vector2 value to be computed
		/// </summary>
#endif
		[SerializeField]
		private FlexibleVector2 _Vector2Value;

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算するVector3値
		/// </summary>
#else
		/// <summary>
		/// Vector3 value to be computed
		/// </summary>
#endif
		[SerializeField]
		private FlexibleVector3 _Vector3Value;

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算するQuaternion値
		/// </summary>
#else
		/// <summary>
		/// Quaternion value to be computed
		/// </summary>
#endif
		[SerializeField]
		private FlexibleQuaternion _QuaternionValue;

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算するRect値
		/// </summary>
#else
		/// <summary>
		/// Rect value to be computed
		/// </summary>
#endif
		[SerializeField]
		private FlexibleRect _RectValue;

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算するBounds値
		/// </summary>
#else
		/// <summary>
		/// Bounds value to be computed
		/// </summary>
#endif
		[SerializeField]
		private FlexibleBounds _BoundsValue;

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算するTransform値
		/// </summary>
#else
		/// <summary>
		/// Transform value to be computed
		/// </summary>
#endif
		[SerializeField]
		private FlexibleTransform _TransformValue;

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算するRectTransform値
		/// </summary>
#else
		/// <summary>
		/// RectTransform value to be computed
		/// </summary>
#endif
		[SerializeField]
		private FlexibleRectTransform _RectTransformValue;

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算するRigidbody値
		/// </summary>
#else
		/// <summary>
		/// Rigidbody value to be computed
		/// </summary>
#endif
		[SerializeField]
		private FlexibleRigidbody _RigidbodyValue;

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算するRigidbody2D値
		/// </summary>
#else
		/// <summary>
		/// Rigidbody2D value to be computed
		/// </summary>
#endif
		[SerializeField]
		private FlexibleRigidbody2D _Rigidbody2DValue;

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算するComponent値
		/// </summary>
#else
		/// <summary>
		/// Component value to be computed
		/// </summary>
#endif
		[SerializeField]
		private FlexibleComponent _ComponentValue;

		[SerializeField]
		[HideInInspector]
		private int _SerializeVersion;

		#region old

		[FormerlySerializedAs( "intValue" )]
		[SerializeField]
		[HideInInspector]
		private int _OldIntValue;

		[FormerlySerializedAs( "floatValue" )]
		[SerializeField]
		[HideInInspector]
		private float _OldFloatValue;

		[FormerlySerializedAs( "boolValue" )]
		[SerializeField]
		[HideInInspector]
		private bool _OldBoolValue;

		#endregion // old

		#endregion // Serialize fields

		public int intValue
		{
			get
			{
				return _IntValue.value;
			}
		}

		public long longValue
		{
			get
			{
				return _LongValue.value;
			}
		}

		public float floatValue
		{
			get
			{
				return _FloatValue.value;
			}
		}

		public bool boolValue
		{
			get
			{
				return _BoolValue.value;
			}
		}

		public string stringValue
		{
			get
			{
				return _StringValue.value;
			}
		}

		public GameObject gameObjectValue
		{
			get
			{
				return _GameObjectValue.value;
			}
		}

		public Vector2 vector2Value
		{
			get
			{
				return _Vector2Value.value;
			}
		}

		public Vector3 vector3Value
		{
			get
			{
				return _Vector3Value.value;
			}
		}

		public Quaternion quaternionValue
		{
			get
			{
				return _QuaternionValue.value;
			}
		}

		public Rect rectValue
		{
			get
			{
				return _RectValue.value;
			}
		}

		public Bounds boundsValue
		{
			get
			{
				return _BoundsValue.value;
			}
		}

		public Transform transformValue
		{
			get
			{
				return _TransformValue.value;
			}
		}

		public RectTransform rectTransformValue
		{
			get
			{
				return _RectTransformValue.value;
			}
		}

		public Rigidbody rigidbodyValue
		{
			get
			{
				return _RigidbodyValue.value;
			}
		}

		public Rigidbody2D rigidbody2DValue
		{
			get
			{
				return _Rigidbody2DValue.value;
			}
		}

		public Component componentValue
		{
			get
			{
				return _ComponentValue.value;
			}
		}

		void SerializeVer1()
		{
			_IntValue = (FlexibleInt)_OldIntValue;
			_FloatValue = (FlexibleFloat)_OldFloatValue;
			_BoolValue = (FlexibleBool)_OldBoolValue;
		}

		void INodeBehaviourSerializationCallbackReceiver.OnBeforeSerialize()
		{
			if (_SerializeVersion == 0)
			{
				SerializeVer1();
                _SerializeVersion = 1;
			}
		}

		void INodeBehaviourSerializationCallbackReceiver.OnAfterDeserialize()
		{
			if (_SerializeVersion == 0)
			{
				SerializeVer1();
			}
		}

		// Use this for enter state
		public override void OnStateBegin()
		{
			Parameter parameter = reference.parameter;

			if (parameter == null)
			{
				return;
			}

			switch (parameter.type)
			{
				case Parameter.Type.Int:
					{
						int value = parameter.intValue;
                        switch (function)
						{
							case Function.Assign:
								value = intValue;
								break;
							case Function.Add:
								value += intValue;
								break;
						}
						if (parameter.intValue != value)
						{
							parameter.intValue = value;
							parameter.OnChanged();
                        }
					}
					break;
				case Parameter.Type.Long:
					{
						long value = parameter.longValue;
						switch (function)
						{
							case Function.Assign:
								value = longValue;
								break;
							case Function.Add:
								value += longValue;
								break;
						}
						if (parameter.longValue != value)
						{
							parameter.longValue = value;
							parameter.OnChanged();
						}
					}
					break;
				case Parameter.Type.Float:
					{
						float value = parameter.floatValue;
						switch (function)
						{
							case Function.Assign:
								value = floatValue;
								break;
							case Function.Add:
								value += floatValue;
								break;
						}
						if (parameter.floatValue != value)
						{
							parameter.floatValue = value;
							parameter.OnChanged();
						}
					}
					break;
				case Parameter.Type.Bool:
					{
						bool value = boolValue;

						if (parameter.boolValue != value)
						{
							parameter.boolValue = value;
							parameter.OnChanged();
						}
					}
					break;
				case Parameter.Type.String:
					{
						string value = parameter.stringValue;
						switch (function)
						{
							case Function.Assign:
								value = stringValue;
								break;
							case Function.Add:
								value += stringValue;
								break;
						}
						if (parameter.stringValue != value)
						{
							parameter.stringValue = value;
							parameter.OnChanged();
						}
					}
					break;
				case Parameter.Type.GameObject:
					{
						GameObject value = gameObjectValue;

						if (parameter.gameObjectValue != value)
						{
							parameter.gameObjectValue = value;
							parameter.OnChanged();
						}
					}
					break;
				case Parameter.Type.Vector2:
					{
						Vector2 value = vector2Value;

						if (parameter.vector2Value != value)
						{
							parameter.vector2Value = value;
							parameter.OnChanged();
						}
					}
					break;
				case Parameter.Type.Vector3:
					{
						Vector3 value = vector3Value;

						if (parameter.vector3Value != value)
						{
							parameter.vector3Value = value;
							parameter.OnChanged();
						}
					}
					break;
				case Parameter.Type.Quaternion:
					{
						Quaternion value = quaternionValue;

						if (parameter.quaternionValue != value)
						{
							parameter.quaternionValue = value;
							parameter.OnChanged();
						}
					}
					break;
				case Parameter.Type.Rect:
					{
						Rect value = rectValue;

						if (parameter.rectValue != value)
						{
							parameter.rectValue = value;
							parameter.OnChanged();
						}
					}
					break;
				case Parameter.Type.Bounds:
					{
						Bounds value = boundsValue;

						if (parameter.boundsValue != value)
						{
							parameter.boundsValue = value;
							parameter.OnChanged();
						}
					}
					break;
				case Parameter.Type.Transform:
					{
						Transform value = transformValue;

						if (parameter.objectReferenceValue != value)
						{
							parameter.objectReferenceValue = value;
							parameter.OnChanged();
						}
					}
					break;
				case Parameter.Type.RectTransform:
					{
						RectTransform value = rectTransformValue;

						if (parameter.objectReferenceValue != value)
						{
							parameter.objectReferenceValue = value;
							parameter.OnChanged();
						}
					}
					break;
				case Parameter.Type.Rigidbody:
					{
						Rigidbody value = rigidbodyValue;

						if (parameter.objectReferenceValue != value)
						{
							parameter.objectReferenceValue = value;
							parameter.OnChanged();
						}
					}
					break;
				case Parameter.Type.Rigidbody2D:
					{
						Rigidbody2D value = rigidbody2DValue;

						if (parameter.objectReferenceValue != value)
						{
							parameter.objectReferenceValue = value;
							parameter.OnChanged();
						}
					}
					break;
				case Parameter.Type.Component:
					{
						Component value = componentValue;

						if (parameter.objectReferenceValue != value)
						{
							parameter.objectReferenceValue = value;
							parameter.OnChanged();
						}
					}
					break;
			}
		}
	}
}
