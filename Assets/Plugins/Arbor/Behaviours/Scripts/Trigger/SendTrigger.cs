﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// トリガーを送る。
	/// </summary>
#else
	/// <summary>
	/// It will send a trigger.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("Trigger/SendTrigger")]
	[BuiltInBehaviour]
	public class SendTrigger : StateBehaviour
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// トリガーを送る対象。
		/// </summary>
#else
		/// <summary>
		/// Subject to send a trigger.
		/// </summary>
#endif
		[SerializeField] private ArborFSM _Target;

#if ARBOR_DOC_JA
		/// <summary>
		/// 送るトリガー
		/// </summary>
#else
		/// <summary>
		/// Trigger to send
		/// </summary>
#endif
		[SerializeField] private string _Message;

		#endregion // Serialize fields

		public override void OnStateBegin()
		{
			if( _Target != null )
			{
				_Target.SendTrigger( _Message );
			}
		}
	}
}
