﻿using UnityEngine;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Rigidbody2Dに力を加える。
	/// </summary>
#else
	/// <summary>
	/// We will apply a force to Rigidbody2D.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("Physics2D/AddForceRigidbody2D")]
	[BuiltInBehaviour]
	public class AddForceRigidbody2D : StateBehaviour
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 対象となるRigidbody2D。<br/>
		/// TypeがConstantの時に指定しない場合、ArborFSMを割り当ててあるGameObjectのRigidbody2D。
		/// </summary>
#else
		/// <summary>
		/// Rigidbody2D of interest.<br/>
		/// If Type is Constant and nothing is specified, Rigidbody2D of GameObject to which ArborFSM is assigned.
		/// </summary>
#endif
		[SerializeField] private FlexibleRigidbody2D _Target;

#if ARBOR_DOC_JA
		/// <summary>
		/// TargetのTransformから見て上方向を基準とした角度。
		/// </summary>
#else
		/// <summary>
		/// An angle based on the upward direction as seen from the Target's Transform.
		/// </summary>
#endif
		[SerializeField] private float _Angle;

#if ARBOR_DOC_JA
		/// <summary>
		/// 加える力。
		/// </summary>
#else
		/// <summary>
		/// Force applied.
		/// </summary>
#endif
		[SerializeField] private float _Power;

		#endregion // Serialize fields

		private Rigidbody2D _MyRigidbody2D;
		public Rigidbody2D cachedTarget
		{
			get
			{
				Rigidbody2D rb2d = _Target.value;
				if( rb2d == null && _Target.type == FlexibleRigidbody2D.Type.Constant )
				{
					if( _MyRigidbody2D == null )
					{
						_MyRigidbody2D = GetComponent<Rigidbody2D>();
					}

					rb2d = _MyRigidbody2D;
				}
				return rb2d;
			}
		}

		// Use this for enter state
		public override void OnStateBegin()
		{
			Rigidbody2D target = cachedTarget;
			if ( target != null)
			{
				Vector2 direction = Quaternion.Euler(0.0f, 0.0f, _Angle) * target.transform.up;
				target.AddForce( direction.normalized * _Power);
            }
		}
	}
}
