﻿using UnityEngine;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// GameObjectを生成する。
	/// </summary>
#else
	/// <summary>
	/// GameObject the searches in the tag and then stored in the parameter.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("GameObject/InstantiateGameObject")]
	[BuiltInBehaviour]
	public class InstantiateGameObject : StateBehaviour
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 生成するGameObject。
		/// </summary>
#else
		/// <summary>
		/// The Instantiated GameObject.
		/// </summary>
#endif
		[SerializeField] private GameObject _Prefab;

#if ARBOR_DOC_JA
		/// <summary>
		/// 親に指定するTransform。
		/// </summary>
#else
		/// <summary>
		/// Transform that specified in the parent.
		/// </summary>
#endif
		[SerializeField] private FlexibleTransform _Parent;

#if ARBOR_DOC_JA
		/// <summary>
		/// 初期時に指定するTransform。
		/// </summary>
#else
		/// <summary>
		/// Transform that you specify for the initial time.
		/// </summary>
#endif
		[SerializeField] private FlexibleTransform _InitTransform;

#if ARBOR_DOC_JA
		/// <summary>
		/// 格納先のパラメータ
		/// </summary>
#else
		/// <summary>
		/// Storage destination parameters
		/// </summary>
#endif
		[SerializeField] private GameObjectParameterReference _Parameter;

#if ARBOR_DOC_JA
		/// <summary>
		/// 演算ノードへの出力。
		/// </summary>
#else
		/// <summary>
		/// Output to the calculator node.
		/// </summary>
#endif
		[SerializeField] private OutputSlotGameObject _Output;

		#endregion // Serialize fields

		public override void OnStateBegin()
		{
			if( _Prefab != null )
			{
				GameObject obj = null;
                if (_InitTransform.value == null)
				{
					obj = Instantiate(_Prefab) as GameObject;
                }
				else
				{
					obj = Instantiate(_Prefab, _InitTransform.value.position, _InitTransform.value.rotation) as GameObject;
				}

				if (_Parent.value != null)
				{
					obj.transform.SetParent(_Parent.value, _InitTransform.value != null);
				}

				if (_Parameter.parameter != null)
				{
					_Parameter.parameter.gameObjectValue = obj;
				}

				_Output.SetValue(obj);
            }
		}
	}
}
