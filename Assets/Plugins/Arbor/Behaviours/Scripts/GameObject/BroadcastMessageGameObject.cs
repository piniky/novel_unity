﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 指定したGameObjectとその子オブジェクトにメッセージを送る。
	/// </summary>
#else
	/// <summary>
	/// It sends a message to the specified GameObject and its child objects.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("GameObject/BroadcastMessageGameObject")]
	[BuiltInBehaviour]
	public class BroadcastMessageGameObject : StateBehaviour, INodeBehaviourSerializationCallbackReceiver
	{
		#region enum

		public enum Type
		{
			None,
			Int,
			Float,
			Bool,
			String,
		}

		#endregion // enum

		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// メッセージの送り先GameObject。
		/// </summary>
#else
		/// <summary>
		/// Destination GameObject of message.
		/// </summary>
#endif
		[SerializeField]
		private FlexibleGameObject _Target;

#if ARBOR_DOC_JA
		/// <summary>
		/// メッセージのメソッド名。
		/// </summary>
#else
		/// <summary>
		/// Method name of the message.
		/// </summary>
#endif
		[SerializeField]
		private string _MethodName;

#if ARBOR_DOC_JA
		/// <summary>
		/// 送信する値の型。
		/// </summary>
#else
		/// <summary>
		/// The type of the value to be transmitted.
		/// </summary>
#endif
		[SerializeField]
		private Type _Type;

#if ARBOR_DOC_JA
		/// <summary>
		/// Int型の値
		/// </summary>
#else
		/// <summary>
		/// Int type value
		/// </summary>
#endif
		[SerializeField]
		private FlexibleInt _IntValue;

#if ARBOR_DOC_JA
		/// <summary>
		/// Float型の値
		/// </summary>
#else
		/// <summary>
		/// Float type value
		/// </summary>
#endif
		[SerializeField]
		private FlexibleFloat _FloatValue;

#if ARBOR_DOC_JA
		/// <summary>
		/// Bool型の値
		/// </summary>
#else
		/// <summary>
		/// Bool type value
		/// </summary>
#endif
		[SerializeField]
		private FlexibleBool _BoolValue;

#if ARBOR_DOC_JA
		/// <summary>
		/// String型の値
		/// </summary>
#else
		/// <summary>
		/// String type value
		/// </summary>
#endif
		[SerializeField]
		private string _StringValue;

		[SerializeField]
		[HideInInspector]
		private int _SerializeVersion;

		#region old

		[FormerlySerializedAs( "_IntValue" )]
		[SerializeField]
		[HideInInspector]
		private GameObject _OldTarget;

		[FormerlySerializedAs( "_IntValue" )]
		[SerializeField]
		[HideInInspector]
		private int _OldIntValue;

		[FormerlySerializedAs( "_FloatValue" )]
		[SerializeField]
		[HideInInspector]
		private float _OldFloatValue;

		[FormerlySerializedAs( "_BoolValue" )]
		[SerializeField]
		[HideInInspector]
		private bool _OldBoolValue;

		#endregion // old

		#endregion // Serialize fields

		public GameObject target
		{
			get
			{
				return _Target.value;
			}
		}

		public float floatValue
		{
			get
			{
				return _FloatValue.value;
			}
		}

		public int intValue
		{
			get
			{
				return _IntValue.value;
			}
		}

		public bool boolValue
		{
			get
			{
				return _BoolValue.value;
			}
		}

		void SerializeVer1()
		{
			_Target = (FlexibleGameObject)_OldTarget;
			_FloatValue = (FlexibleFloat)_OldFloatValue;
			_IntValue = (FlexibleInt)_OldIntValue;
			_BoolValue = (FlexibleBool)_OldBoolValue;
		}

		void INodeBehaviourSerializationCallbackReceiver.OnBeforeSerialize()
		{
			if (_SerializeVersion == 0)
			{
				SerializeVer1();
				_SerializeVersion = 1;
			}
		}

		void INodeBehaviourSerializationCallbackReceiver.OnAfterDeserialize()
		{
			if (_SerializeVersion == 0)
			{
				SerializeVer1();
			}
		}

		public override void OnStateBegin()
		{
			if(target != null )
			{
				switch (_Type)
				{
					case Type.None:
						target.BroadcastMessage(_MethodName, SendMessageOptions.DontRequireReceiver);
						break;
					case Type.Int:
						target.BroadcastMessage(_MethodName, intValue,SendMessageOptions.DontRequireReceiver);
						break;
					case Type.Float:
						target.BroadcastMessage(_MethodName, floatValue, SendMessageOptions.DontRequireReceiver);
						break;
					case Type.Bool:
						target.BroadcastMessage(_MethodName, boolValue, SendMessageOptions.DontRequireReceiver);
						break;
					case Type.String:
						target.BroadcastMessage(_MethodName, _StringValue, SendMessageOptions.DontRequireReceiver);
						break;
				}
			}
		}
	}
}
