﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections;
using System.Collections.Generic;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Parameterの値を判定して遷移する。
	/// </summary>
	/// <remarks>
	/// 複数のCondisionを設定した場合は、すべての比較結果が真になったときのみ遷移する。
	/// </remarks>
#else
	/// <summary>
	/// It determines the value of Parameter and makes a transition.
	/// </summary>
	/// <remarks>
	/// When more than one Condition is set, it transits only when all comparison results become true.
	/// </remarks>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("Transition/ParameterTransition")]
	[BuiltInBehaviour]
	public class ParameterTransition : StateBehaviour, INodeBehaviourSerializationCallbackReceiver
	{
		#region inner class

		[System.Serializable]
		public class Condision
		{
			#region enum
			public enum Type
			{
				Greater,
				Less,
				Equals,
				NotEquals,
			}

			#endregion // enum

			#region Serialize fields

			[FormerlySerializedAs("reference")]
			[SerializeField]
			private ParameterReference _Reference;

			[FormerlySerializedAs("type")]
			[SerializeField]
			private Type _Type;

			[SerializeField]
			private FlexibleInt _IntValue;

			[SerializeField]
			private FlexibleLong _LongValue;

			[SerializeField]
			private FlexibleFloat _FloatValue;

			[SerializeField]
			private FlexibleBool _BoolValue;

			[SerializeField]
			private FlexibleString _StringValue;

			[SerializeField]
			private FlexibleGameObject _GameObjectValue;

			[SerializeField]
			private FlexibleVector2 _Vector2Value;

			[SerializeField]
			private FlexibleVector3 _Vector3Value;

			[SerializeField]
			private FlexibleQuaternion _QuaternionValue;

			[SerializeField]
			private FlexibleRect _RectValue;

			[SerializeField]
			private FlexibleBounds _BoundsValue;

			[SerializeField]
			private FlexibleTransform _TransformValue;

			[SerializeField]
			private FlexibleRectTransform _RectTransformValue;

			[SerializeField]
			private FlexibleRigidbody _RigidbodyValue;

			[SerializeField]
			private FlexibleRigidbody2D _Rigidbody2DValue;

			[SerializeField]
			private FlexibleComponent _ComponentValue;

			#region old

			[FormerlySerializedAs( "intValue" )]
			[SerializeField]
			[HideInInspector]
			private int _OldIntValue;

			[FormerlySerializedAs( "floatValue" )]
			[SerializeField]
			[HideInInspector]
			private float _OldFloatValue;

			[FormerlySerializedAs( "boolValue" )]
			[SerializeField]
			[HideInInspector]
			private bool _OldBoolValue;

			#endregion // old

			#endregion // Serialize fields

			public ParameterReference reference
			{
				get
				{
					return _Reference;
				}
			}

			public Type type
			{
				get
				{
					return _Type;
				}
			}

			public int intValue
			{
				get
				{
					return (int)_IntValue;
				}
			}

			public long longValue
			{
				get
				{
					return (long)_LongValue;
				}
			}

			public float floatValue
			{
				get
				{
					return (float)_FloatValue;
				}
			}

			public bool boolValue
			{
				get
				{
					return (bool)_BoolValue;
				}
			}

			public string stringValue
			{
				get
				{
					return (string)_StringValue;
				}
			}

			public GameObject gameObjectValue
			{
				get
				{
					return (GameObject)_GameObjectValue;
				}
			}

			public Vector2 vector2Value
			{
				get
				{
					return (Vector2)_Vector2Value;
				}
			}

			public Vector3 vector3Value
			{
				get
				{
					return (Vector3)_Vector3Value;
				}
			}

			public Quaternion quaternionValue
			{
				get
				{
					return (Quaternion)_QuaternionValue;
				}
			}

			public Rect rectValue
			{
				get
				{
					return (Rect)_RectValue;
				}
			}

			public Bounds boundsValue
			{
				get
				{
					return (Bounds)_BoundsValue;
				}
			}

			public Transform transformValue
			{
				get
				{
					return (Transform)_TransformValue;
				}
			}

			public RectTransform rectTransformValue
			{
				get
				{
					return (RectTransform)_RectTransformValue;
				}
			}

			public Rigidbody rigidbodyValue
			{
				get
				{
					return (Rigidbody)_RigidbodyValue;
				}
			}

			public Rigidbody2D rigidbody2DValue
			{
				get
				{
					return (Rigidbody2D)_Rigidbody2DValue;
				}
			}

			public Component componentValue
			{
				get
				{
					return (Component)_ComponentValue;
				}
			}

			public void SerializeVer1()
			{
				_IntValue = (FlexibleInt)_OldIntValue;
				_FloatValue = (FlexibleFloat)_OldFloatValue;
				_BoolValue = (FlexibleBool)_OldBoolValue;
			}
		}

		#endregion // inner class

		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 判定条件を設定する。
		/// <ul><li>＋ボタン<br/>条件を追加。</li>
		/// <li>Reference<br/>パラメータの参照</li>
		/// <li>Type<br/>
		/// <table class="docutils">
		/// <tr><td>Greater</td><td>値よりも大きい</td></tr>
		/// <tr><td>Less</td><td>値よりも小さい</td></tr>
		/// <tr><td>Equals</td><td>値と等しい</td></tr>
		/// <tr><td>NotEquals</td><td>値と等しくない</td></tr>
		/// </table></li>
		/// <li>Value<br/>比較する値</li>
		/// </ul>
		/// </summary>
#else
		/// <summary>
		/// Set the judgment condition.
		/// <ul><li>+ Button<br/>Add condition.</li>
		/// <li>Reference<br/>Parameter reference</li>
		/// <li>Type<br/>
		/// <table class="docutils">
		/// <tr><td>Greater</td><td>Greater than value</td></tr>
		/// <tr><td>Less</td><td>Less than the value</td></tr>
		/// <tr><td>Equals</td><td>Equal to value</td></tr>
		/// <tr><td>NotEquals</td><td>Not equal to value</td></tr>
		/// </table></li>
		/// <li>Value<br/>Value to be compared</li>
		/// </ul>
		/// </summary>
#endif
		[SerializeField] private List<Condision> _Condisions = new List<Condision>();

#if ARBOR_DOC_JA
		/// <summary>
		/// 遷移先ステート。
		/// </summary>
#else
		/// <summary>
		/// Transition destination state.
		/// </summary>
#endif
		[SerializeField] private StateLink _NextState;

		[SerializeField]
		[HideInInspector]
		private int _SerializeVersion;

		#endregion // Serialize fields

		void SerializeVer1()
		{
			int condisionCount = _Condisions.Count;
			for (int condisionIndex = 0; condisionIndex < condisionCount; condisionIndex++)
			{
				Condision condision = _Condisions[condisionIndex];
				condision.SerializeVer1();
			}
		}

		void INodeBehaviourSerializationCallbackReceiver.OnBeforeSerialize()
		{
			if (_SerializeVersion == 0)
			{
				SerializeVer1();
                _SerializeVersion = 1;
			}
		}

		void INodeBehaviourSerializationCallbackReceiver.OnAfterDeserialize()
		{
			if (_SerializeVersion == 0)
			{
				SerializeVer1();
			}
		}

		private List<Parameter> _Parameters = new List<Parameter>();

		void SetOnChanged()
		{
			ReleaseOnChanged();
			
			int condisionCount = _Condisions.Count;
			for (int condisionIndex = 0; condisionIndex < condisionCount; condisionIndex++)
			{
				Condision condision = _Condisions[condisionIndex];
				Parameter parameter = condision.reference.parameter;
				if (parameter != null)
				{
					parameter.onChanged += OnChangedParam;
					_Parameters.Add(parameter);
				}
			}
		}

		void ReleaseOnChanged()
		{
			for (int i = 0; i < _Parameters.Count; i++)
			{
				Parameter parameter = _Parameters[i];
				parameter.onChanged -= OnChangedParam;
			}
			_Parameters.Clear();
		}

		void OnEnable()
		{
			SetOnChanged();
		}

		void OnDisable()
		{
			ReleaseOnChanged();
		}

		protected override void OnValidate()
		{
			base.OnValidate();

			if (Application.isPlaying && isActiveAndEnabled)
			{
				SetOnChanged();
			}
		}

		// Use this for enter state
		public override void OnStateBegin()
		{
			if (CheckCondision())
			{
				Transition(_NextState);
			}
		}
		
		void OnChangedParam(Parameter parameter)
		{
			if ( CheckCondision() )
			{
				Transition(_NextState);
			}
		}

		bool CheckCondision()
		{
			int count = 0;
			int result = 0;

			int condisionCount = _Condisions.Count;
			for (int condisionIndex = 0; condisionIndex < condisionCount; condisionIndex++)
			{
				Condision condision = _Condisions[condisionIndex];
				Parameter parameter = condision.reference.parameter;
				if (parameter == null)
				{
					continue;
				}

				count++;

				switch (parameter.type)
				{
					case Parameter.Type.Int:
						{
							switch (condision.type)
							{
								case Condision.Type.Greater:
									if ( parameter.intValue > condision.intValue )
									{
										result++;
									}
									break;
								case Condision.Type.Less:
									if ( parameter.intValue < condision.intValue )
									{
										result++;
									}
									break;
								case Condision.Type.Equals:
									if ( parameter.intValue == condision.intValue )
									{
										result++;
									}
									break;
								case Condision.Type.NotEquals:
									if ( parameter.intValue != condision.intValue )
									{
										result++;
									}
									break;
							}
						}
						break;
					case Parameter.Type.Long:
						{
							switch (condision.type)
							{
								case Condision.Type.Greater:
									if (parameter.longValue > condision.longValue)
									{
										result++;
									}
									break;
								case Condision.Type.Less:
									if (parameter.longValue < condision.longValue)
									{
										result++;
									}
									break;
								case Condision.Type.Equals:
									if (parameter.longValue == condision.longValue)
									{
										result++;
									}
									break;
								case Condision.Type.NotEquals:
									if (parameter.longValue != condision.longValue)
									{
										result++;
									}
									break;
							}
						}
						break;
					case Parameter.Type.Float:
						{
							switch (condision.type)
							{
								case Condision.Type.Greater:
									if ( parameter.floatValue >= condision.floatValue )
									{
										result++;
									}
									break;
								case Condision.Type.Less:
									if ( parameter.floatValue <= condision.floatValue )
									{
										result++;
									}
									break;
								case Condision.Type.Equals:
									if ( parameter.floatValue == condision.floatValue )
									{
										result++;
									}
									break;
								case Condision.Type.NotEquals:
									if ( parameter.floatValue != condision.floatValue )
									{
										result++;
									}
									break;
							}
						}
						break;
					case Parameter.Type.Bool:
						{
							if ( parameter.boolValue == condision.boolValue )
							{
								result++;
							}
						}
						break;
					case Parameter.Type.String:
						{
							switch (condision.type)
							{
								case Condision.Type.Greater:
									if (parameter.stringValue.CompareTo(condision.stringValue) >= 0 )
									{
										result++;
									}
									break;
								case Condision.Type.Less:
									if (parameter.stringValue.CompareTo(condision.stringValue) <= 0 )
									{
										result++;
									}
									break;
								case Condision.Type.Equals:
									if (parameter.stringValue.Equals(condision.stringValue) )
									{
										result++;
									}
									break;
								case Condision.Type.NotEquals:
									if (!parameter.stringValue.Equals(condision.stringValue))
									{
										result++;
									}
									break;
							}
						}
						break;
					case Parameter.Type.GameObject:
						{
							if (parameter.gameObjectValue == condision.gameObjectValue)
							{
								result++;
							}
						}
						break;
					case Parameter.Type.Vector2:
						{
							if (parameter.vector2Value == condision.vector2Value)
							{
								result++;
							}
						}
						break;
					case Parameter.Type.Vector3:
						{
							if (parameter.vector3Value == condision.vector3Value)
							{
								result++;
							}
						}
						break;
					case Parameter.Type.Quaternion:
						{
							if (parameter.quaternionValue == condision.quaternionValue)
							{
								result++;
							}
						}
						break;
					case Parameter.Type.Rect:
						{
							if (parameter.rectValue == condision.rectValue)
							{
								result++;
							}
						}
						break;
					case Parameter.Type.Bounds:
						{
							if (parameter.boundsValue == condision.boundsValue)
							{
								result++;
							}
						}
						break;
					case Parameter.Type.Transform:
						{
							if (parameter.objectReferenceValue == condision.transformValue)
							{
								result++;
							}
						}
						break;
					case Parameter.Type.RectTransform:
						{
							if (parameter.objectReferenceValue == condision.rectTransformValue)
							{
								result++;
							}
						}
						break;
					case Parameter.Type.Rigidbody:
						{
							if (parameter.objectReferenceValue == condision.rigidbodyValue)
							{
								result++;
							}
						}
						break;
					case Parameter.Type.Rigidbody2D:
						{
							if (parameter.objectReferenceValue == condision.rigidbody2DValue)
							{
								result++;
							}
						}
						break;
					case Parameter.Type.Component:
						{
							if (parameter.objectReferenceValue == condision.componentValue)
							{
								result++;
							}
						}
						break;
				}
			}

			return count > 0 && count == result;
		}
	}
}

