﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Animatorのステートを参照して遷移する。
	/// </summary>
#else
	/// <summary>
	/// Transit by referring to the state of Animator.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("Transition/AnimatorStateTransition")]
	[BuiltInBehaviour]
	public class AnimatorStateTransition : AnimatorBase
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// レイヤー名。
		/// </summary>
#else
		/// <summary>
		/// Layer Name
		/// </summary>
#endif
		[SerializeField]
		[FormerlySerializedAs("layerName")]
		private string _LayerName;

#if ARBOR_DOC_JA
		/// <summary>
		/// ステート名。
		/// </summary>
#else
		/// <summary>
		/// State Name
		/// </summary>
#endif
		[SerializeField]
		[FormerlySerializedAs("stateName")]
		private string _StateName;

#if ARBOR_DOC_JA
		/// <summary>
		/// 遷移先ステート。
		/// </summary>
#else
		/// <summary>
		/// Transition destination state.
		/// </summary>
#endif
		[SerializeField]
		[FormerlySerializedAs("nextState")]
		private StateLink _NextState;

		#endregion // Serialize fields
		
		void CheckTransition()
		{
			Animator animator = cachedAnimator;
			if (animator != null )
			{
				int layerIndex = GetLayerIndex(animator, _LayerName);
				if (layerIndex >= 0)
				{
					AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(layerIndex);
					if (stateInfo.IsName(_LayerName + "." + _StateName))
					{
						Transition(_NextState);
					}
				}
			}
		}

		// Use this for enter state
		public override void OnStateBegin()
		{
			CheckTransition();
        }

		// Update is called once per frame
		public override void OnStateUpdate()
		{
			CheckTransition();
        }
	}
}
