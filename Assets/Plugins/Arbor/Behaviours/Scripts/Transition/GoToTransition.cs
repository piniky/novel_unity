﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 強制的にステートを遷移する。
	/// </summary>
#else
	/// <summary>
	/// It will transition to force the state.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("Transition/GoToTransition")]
	[BuiltInBehaviour]
	public class GoToTransition : StateBehaviour 
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 遷移先ステート。
		/// </summary>
#else
		/// <summary>
		/// Transition destination state.
		/// </summary>
#endif
		[SerializeField] private StateLink _NextState;

		#endregion // Serialize fields

		// Use this for enter state
		public override void OnStateBegin()
		{
			Transition( _NextState );
		}
	}
}
