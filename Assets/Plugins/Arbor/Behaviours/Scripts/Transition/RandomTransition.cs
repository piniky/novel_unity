﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// ランダムに遷移する。
	/// </summary>
#else
	/// <summary>
	/// Transit randomly.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("Transition/RandomTransition")]
	[BuiltInBehaviour]
    public class RandomTransition : StateBehaviour
    {
#region inner class

		[System.Serializable]
        public class LinkWeight
        {
            public StateLink link;
            [Range(0, 100)]
            public float weight;
        }

		#endregion // inner class

		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 遷移先リスト。<br />
		/// <ul>
		/// <li>Weight<br />遷移しやすさ。個別のWeight/全体のWeightによって確率が決まる。</li>
		/// <li>Probability<br />確率(表示のみ)</li>
		/// <li>Link<br />遷移先ステート。</li>
		/// </ul>
		/// </summary>
#else
		/// <summary>
		/// Transition destination list.<br />
		/// <ul>
		/// <li>Weight<br />Ease of transition. Probability depends on individual weight / overall weight.</li>
		/// <li>Probability<br />Probability (display only)</li>
		/// <li>Link<br />Transition destination state.</li>
		/// </ul>
		/// </summary>
#endif
		[SerializeField]
        private List<LinkWeight> _Links = new List<LinkWeight>();

#endregion // Serialize fields

		public float GetTotalWeight()
		{
			if( _Links.Count == 0 )
			{
				return 0;
			}

			float totalWeight = 0.0f;

			int linkCount = _Links.Count;
			for( int linkIndex = 0 ; linkIndex < linkCount ; linkIndex++ )
			{
				LinkWeight link = _Links[linkIndex];
				totalWeight += link.weight;
			}

			return totalWeight;
		}

        StateLink GetRandomLink()
        {
           float totalWeight = GetTotalWeight();

            if (totalWeight == 0.0f)
            {
                return null;
            }

            float r = Random.Range(0, totalWeight);

            totalWeight = 0.0f;

            int index = 0;

			int linkCount = _Links.Count;
			for (int linkIndex = 0; linkIndex < linkCount; linkIndex++)
            {
                LinkWeight link = _Links[linkIndex];

                if (totalWeight <= r && r < totalWeight + link.weight)
                {
                    index = linkIndex;
                    break;
                }

                totalWeight += link.weight;
            }

            return _Links[index].link;
        }

        // Use this for enter state
        public override void OnStateBegin()
        {
            StateLink link = GetRandomLink();
            if (link != null)
            {
                Transition(link);
            }
        }
    }
}
