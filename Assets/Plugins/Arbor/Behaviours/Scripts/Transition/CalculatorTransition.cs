﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 演算結果によって遷移する。
	/// </summary>
	/// <remarks>
	/// 複数のCondisionを設定した場合は、すべての比較結果が真になったときのみ遷移する。
	/// </remarks>
#else
	/// <summary>
	/// It will transition by the calculation result
	/// </summary>
	/// <remarks>
	/// When more than one Condition is set, it transits only when all comparison results become true.
	/// </remarks>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("Transition/CalculatorTransition")]
	[BuiltInBehaviour]
	public class CalculatorTransition : StateBehaviour, INodeBehaviourSerializationCallbackReceiver
	{
		#region inner class

		[System.Serializable]
		public class Condision
		{
			#region enum
			public enum Type
			{
				Int,
				Float,
				Bool,
			}

			public enum CompareType
			{
				Greater,
				Less,
				Equals,
				NotEquals,
			}

			#endregion // enum

			#region Serialize fields

			[SerializeField] private Type _Type;
			[SerializeField] private CompareType _CompareType;
			
			[SerializeField] private FlexibleInt _IntValue1;
			[SerializeField] private FlexibleInt _IntValue2;

			[SerializeField] private FlexibleFloat _FloatValue1;
			[SerializeField] private FlexibleFloat _FloatValue2;

			[FormerlySerializedAs("_BoolValue")]
			[SerializeField] private FlexibleBool _BoolValue1;
			[SerializeField] private FlexibleBool _BoolValue2 = new FlexibleBool(true);

			#endregion // Serialize fields

			public bool Compare()
			{
				switch (_Type)
				{
					case Type.Int:
						switch (_CompareType)
						{
							case CompareType.Greater:
								return (_IntValue1.value > _IntValue2.value);
							case CompareType.Less:
								return (_IntValue1.value < _IntValue2.value);
							case CompareType.Equals:
								return (_IntValue1.value == _IntValue2.value);
							case CompareType.NotEquals:
								return (_IntValue1.value != _IntValue2.value);
						}
						break;
					case Type.Float:
						switch (_CompareType)
						{
							case CompareType.Greater:
								return (_FloatValue1.value > _FloatValue2.value);
							case CompareType.Less:
								return (_FloatValue1.value < _FloatValue2.value);
							case CompareType.Equals:
								return (_FloatValue1.value == _FloatValue2.value);
							case CompareType.NotEquals:
								return (_FloatValue1.value != _FloatValue2.value);
						}
						break;
					case Type.Bool:
						return _BoolValue1.value == _BoolValue2.value;

				}
				return false;
			}

			public Condision(Type type)
			{
				_Type = type;
			}

			public void SerializeVer1()
			{
				_BoolValue2 = new FlexibleBool( true );
			}
		}

		#endregion // inner class

		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 判定条件を設定する。
		/// <ul><li>＋ボタン<br/>条件を追加。</li>
		/// <li>Compare Type<br/>
		/// <table class="docutils">
		/// <tr><td>Greater</td><td>値よりも大きい</td></tr>
		/// <tr><td>Less</td><td>値よりも小さい</td></tr>
		/// <tr><td>Equals</td><td>値と等しい</td></tr>
		/// <tr><td>NotEquals</td><td>値と等しくない</td></tr>
		/// </table></li>
		/// <li>Value1,Value2<br/>比較する値</li>
		/// </ul>
		/// </summary>
#else
		/// <summary>
		/// Set the judgment condition.
		/// <ul><li>+ Button<br/>Add condition.</li>
		/// <li>Compare Type<br/>
		/// <table class="docutils">
		/// <tr><td>Greater</td><td>Greater than value</td></tr>
		/// <tr><td>Less</td><td>Less than the value</td></tr>
		/// <tr><td>Equals</td><td>Equal to value</td></tr>
		/// <tr><td>NotEquals</td><td>Not equal to value</td></tr>
		/// </table></li>
		/// <li>Value1,Value2<br/>Value to be compared</li>
		/// </ul>
		/// </summary>
#endif
		[SerializeField] private List<Condision> _Condisions = new List<Condision>();

#if ARBOR_DOC_JA
		/// <summary>
		/// 遷移先ステート。
		/// </summary>
#else
		/// <summary>
		/// Transition destination state.
		/// </summary>
#endif
		[SerializeField] private StateLink _NextState;

		[SerializeField]
		[HideInInspector]
		private int _SerializeVersion;

		#endregion // Serialize fields

		// Use this for enter state
		public override void OnStateBegin()
		{
			if ( CheckCondision() )
			{
				Transition(_NextState);
			}
		}

		public void AddCondition(Condision.Type type)
		{
			Condision condision = new Condision(type);
			_Condisions.Add(condision);
        }

		bool CheckCondision()
		{
			int count = 0;
			int result = 0;

			int condisionCount = _Condisions.Count;
			for (int condisionIndex = 0; condisionIndex < condisionCount; condisionIndex++)
			{
				Condision condision = _Condisions[condisionIndex];

				count++;

				if (condision.Compare())
				{
					result++;
				}
			}

			return count > 0 && count == result;
		}

		void INodeBehaviourSerializationCallbackReceiver.OnBeforeSerialize()
		{
		}

		void SerializeVer1()
		{
			int condisionCount = _Condisions.Count;
			for( int condisionIndex = 0 ; condisionIndex < condisionCount ; condisionIndex++ )
			{
				Condision condision = _Condisions[condisionIndex];
				condision.SerializeVer1();
			}
		}

		void INodeBehaviourSerializationCallbackReceiver.OnAfterDeserialize()
		{
			if ( _SerializeVersion == 0 )
			{
				SerializeVer1();
				_SerializeVersion = 1;
			}
		}
	}
}

