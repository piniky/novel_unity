﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// OnPointerUpが呼ばれたときにステートを遷移する。
	/// </summary>
#else
	/// <summary>
	/// It will transition the state when the OnPointerUp is called.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("Transition/EventSystems/OnPointerUpTransition")]
	[BuiltInBehaviour]
	public class OnPointerUpTransition : StateBehaviour , IPointerUpHandler
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// ボタンをチェックするかどうか。
		/// </summary>
#else
		/// <summary>
		/// Whether to check the button.
		/// </summary>
#endif
		[SerializeField] private bool _CheckButton;

#if ARBOR_DOC_JA
		/// <summary>
		/// チェックするボタン。
		/// </summary>
#else
		/// <summary>
		/// Button to check.
		/// </summary>
#endif
		[SerializeField] private PointerEventData.InputButton _Button;

#if ARBOR_DOC_JA
		/// <summary>
		/// 遷移先ステート。
		/// </summary>
#else
		/// <summary>
		/// Transition destination state.
		/// </summary>
#endif
		[SerializeField] private StateLink _NextState;

		#endregion // Serialize fields

		public void OnPointerUp(PointerEventData data)
		{
			if (!_CheckButton || data.button == _Button)
			{
				Transition(_NextState);
			}
		}
	}
}
