﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 時間経過後にステートを遷移する。
	/// </summary>
#else
	/// <summary>
	/// It will transition the state after the lapse of time.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("Transition/TimeTransition")]
	[BuiltInBehaviour]
	public class TimeTransition : StateBehaviour
	{
		#region enum

		public enum TimeType
		{
			Normal,
			Unscaled,
			Realtime,
		}

		#endregion // enum
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 時間の種類。<br/>
		/// <ul>
		/// <li>Normal<br/>timeScaleを考慮した現在フレームの時間を使用。</li>
		/// <li>Unscaled<br/>timeScaleを考慮しない現在フレームの時間を使用。</li>
		/// <li>Realtime<br/>リアルタイムを使用。</li>
		/// </ul>
		/// </summary>
#else
		/// <summary>
		/// Type of time.<br/>
		/// <ul>
		/// <li>Normal<br/>Use time of current frame considering timeScale.</li>
		/// <li>Unscaled<br/>Use time of current frame without considering timeScale.</li>
		/// <li>Realtime<br/>Use realtime.</li>
		/// </ul>
		/// </summary>
#endif
		[SerializeField]
		public TimeType _TimeType = TimeType.Normal;

#if ARBOR_DOC_JA
		/// <summary>
		/// 遷移するまでの秒数。
		/// </summary>
#else
		/// <summary>
		/// The number of seconds until the transition.
		/// </summary>
#endif
		[SerializeField] public float _Seconds;

#if ARBOR_DOC_JA
		/// <summary>
		/// 遷移先ステート。
		/// </summary>
#else
		/// <summary>
		/// Transition destination state.
		/// </summary>
#endif
		[SerializeField] private StateLink _NextState;

		#endregion // Serialize fields

		public float currentTime
		{
			get
			{
				switch( _TimeType )
				{
					case TimeType.Normal:
						return Time.time;
					case TimeType.Unscaled:
						return Time.unscaledTime;
					case TimeType.Realtime:
						return Time.realtimeSinceStartup;
				}

				return Time.time;
			}
		}

		float _BeginTime = 0.0f;
		public float elapsedTime
		{
			get
			{
				return currentTime - _BeginTime;
			}
		}

		public override void OnStateBegin()
		{
			_BeginTime = currentTime;
		}

		public override void OnStateUpdate()
		{
			if (elapsedTime >= _Seconds)
			{
				Transition(_NextState);
			}
		}
	}
}
