﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// ステートトリガーが送られてきたときにステートを遷移します。
	/// </summary>
#else
	/// <summary>
	/// It will transition the state when the trigger has been sent.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("Transition/TriggerTransition")]
	[BuiltInBehaviour]
	public class TriggerTransition : StateBehaviour
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 遷移するか判定するメッセージ。
		/// </summary>
#else
		/// <summary>
		/// Message to decide whether to transition.
		/// </summary>
#endif
		[SerializeField] private string _Message;

#if ARBOR_DOC_JA
		/// <summary>
		/// 遷移先ステート。
		/// </summary>
#else
		/// <summary>
		/// Transition destination state.
		/// </summary>
#endif
		[SerializeField] private StateLink _NextState;

		#endregion // Serialize fields

		public override void OnStateTrigger( string message )
		{
			if( !enabled )
			{
				return;
			}

			if( _Message == message )
			{
				Transition( _NextState );
			}
		}
	}
}
