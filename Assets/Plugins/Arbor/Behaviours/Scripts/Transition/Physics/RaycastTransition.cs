﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// レイキャストによって遷移する。
	/// </summary>
#else
	/// <summary>
	/// It will transition by Ray cast.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("Transition/Physics/RaycastTransition")]
	[BuiltInBehaviour]
	public class RaycastTransition : StateBehaviour
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// レイの始点
		/// </summary>
#else
		/// <summary>
		/// The starting point of the Ray
		/// </summary>
#endif
		[SerializeField] private FlexibleVector3 _Origin;

#if ARBOR_DOC_JA
		/// <summary>
		/// レイの方向
		/// </summary>
#else
		/// <summary>
		/// Direction of Ray
		/// </summary>
#endif
		[SerializeField] private FlexibleVector3 _Direction;

#if ARBOR_DOC_JA
		/// <summary>
		/// レイの距離
		/// </summary>
#else
		/// <summary>
		/// Distance of Ray
		/// </summary>
#endif
		[SerializeField] private FlexibleFloat _Distance = new FlexibleFloat(Mathf.Infinity);

#if ARBOR_DOC_JA
		/// <summary>
		/// 判定対象のレイヤー
		/// </summary>
#else
		/// <summary>
		/// Determination target layer
		/// </summary>
#endif
		[SerializeField] private LayerMask _LayerMask = Physics.DefaultRaycastLayers;

#if ARBOR_DOC_JA
		/// <summary>
		/// Update時に判定するかどうか。
		/// </summary>
#else
		/// <summary>
		/// Whether or not to make an update decision.
		/// </summary>
#endif
		[SerializeField] private bool _CheckUpdate;

#if ARBOR_DOC_JA
		/// <summary>
		/// レイキャストによるヒット情報を出力。
		/// </summary>
#else
		/// <summary>
		/// Output hit information by raycast.
		/// </summary>
#endif
		[SerializeField] private OutputSlotRaycastHit _RaycastHit;

#if ARBOR_DOC_JA
		/// <summary>
		/// 遷移先ステート。
		/// </summary>
#else
		/// <summary>
		/// Transition destination state.
		/// </summary>
#endif
		[SerializeField] private StateLink _NextState;

		#endregion // Serialize fields

		void CheckRaycast()
		{
			RaycastHit hit;
			if (Physics.Raycast(_Origin.value, _Direction.value, out hit, _Distance.value, _LayerMask.value))
			{
				_RaycastHit.SetValue(hit);
				Transition(_NextState);
			}
		}

		public override void OnStateBegin()
		{
			CheckRaycast();
		}

		// Update is called once per frame
		public override void OnStateUpdate()
		{
			if (_CheckUpdate)
			{
				CheckRaycast();
			}
		}
	}
}
