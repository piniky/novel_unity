﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// GameObjectが存在しているかどうかで遷移する。
	/// </summary>
#else
	/// <summary>
	/// GameObject is I will transition on whether exists.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("Transition/ExistsGameObjectTransition")]
	[BuiltInBehaviour]
	public class ExistsGameObjectTransition : StateBehaviour
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 存在しているかチェックする対象。
		/// </summary>
#else
		/// <summary>
		/// Subject to check that you are there.
		/// </summary>
#endif
		[SerializeField] private GameObject[] _Targets;

#if ARBOR_DOC_JA
		/// <summary>
		/// Update時にチェックするかどうか。
		/// </summary>
#else
		/// <summary>
		/// Whether Update at check.
		/// </summary>
#endif
		[SerializeField] private bool _CheckUpdate;

#if ARBOR_DOC_JA
		/// <summary>
		/// すべて存在した場合の遷移先。
		/// </summary>
#else
		/// <summary>
		/// Transition destination when all are present.
		/// </summary>
#endif
		[SerializeField] private StateLink _AllExistsState;

#if ARBOR_DOC_JA
		/// <summary>
		/// すべて存在しなかった場合の遷移先。
		/// </summary>
#else
		/// <summary>
		/// Transition destination when all it did not exist.
		/// </summary>
#endif
		[SerializeField] private StateLink _AllNothingState;

		#endregion // Serialize fields

		void CheckTransition()
		{
			if (_Targets.Length > 0)
			{
				int existsCount = 0;
				int nothingCount = 0;

				foreach (GameObject target in _Targets)
				{
					if (target != null)
					{
						existsCount++;
					}
					else
					{
						nothingCount++;
					}
                }
				if (existsCount == _Targets.Length)
				{
					Transition(_AllExistsState);
				}
				else if (nothingCount == _Targets.Length)
				{
					Transition(_AllNothingState);
				}
			}
		}

		// Use this for enter state
		public override void OnStateBegin()
		{
			CheckTransition();
        }

		// Update is called once per frame
		public override void OnStateUpdate()
		{
			if (_CheckUpdate)
			{
				CheckTransition();
            }
		}
	}
}
