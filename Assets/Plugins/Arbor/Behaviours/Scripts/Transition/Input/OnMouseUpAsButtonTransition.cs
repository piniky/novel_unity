﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// OnMouseUpAdButtonが呼ばれたときにステートを遷移する。
	/// </summary>
#else
	/// <summary>
	/// It will transition the state when the OnMouseUpAdButton is called.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("Transition/Input/OnMouseUpAsButtonTransition")]
	[BuiltInBehaviour]
	public class OnMouseUpAsButtonTransition : StateBehaviour
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 遷移先ステート。
		/// </summary>
#else
		/// <summary>
		/// Transition destination state.
		/// </summary>
#endif
		[SerializeField] private StateLink _NextState;

		#endregion // Serialize fields

		void OnMouseUpAdButton()
		{
			Transition(_NextState);
		}
	}
}
