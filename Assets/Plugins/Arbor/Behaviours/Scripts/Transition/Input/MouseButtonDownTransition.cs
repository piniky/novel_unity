﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// マウスボタンが押されたときにステートを遷移する。
	/// </summary>
#else
	/// <summary>
	/// It will transition the state when the mouse button is pressed.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("Transition/Input/MouseButtonDownTransition")]
	[BuiltInBehaviour]
	public class MouseButtonDownTransition : StateBehaviour
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// マウスボタンの指定。
		/// </summary>
#else
		/// <summary>
		/// Specified mouse button.
		/// </summary>
#endif
		[SerializeField] private int _Button = 0;

#if ARBOR_DOC_JA
		/// <summary>
		/// 遷移先ステート。
		/// </summary>
#else
		/// <summary>
		/// Transition destination state.
		/// </summary>
#endif
		[SerializeField] private StateLink _NextState;

		#endregion // Serialize fields

		// Update is called once per frame
		void Update () 
		{
			if( Input.GetMouseButtonDown( _Button ) )
			{
				Transition( _NextState );
			}
		}
	}
}
