﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// OnMouseExitが呼ばれたときにステートを遷移する。
	/// </summary>
#else
	/// <summary>
	/// It will transition the state when the OnMouseExit is called.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("Transition/Input/OnMouseExitTransition")]
	[BuiltInBehaviour]
	public class OnMouseExitTransition : StateBehaviour
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 遷移先ステート。
		/// </summary>
#else
		/// <summary>
		/// Transition destination state.
		/// </summary>
#endif
		[SerializeField] private StateLink _NextState;

		#endregion // Serialize fields

		void OnMouseExit()
		{
			Transition(_NextState);
		}
	}
}
