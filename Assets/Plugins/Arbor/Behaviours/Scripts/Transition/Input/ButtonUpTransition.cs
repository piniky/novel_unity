﻿using UnityEngine;
using System.Collections;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// ボタンが離されたときにステートを遷移する。
	/// </summary>
#else
	/// <summary>
	/// It will transition the state when the button is released.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("Transition/Input/ButtonUpTransition")]
	[BuiltInBehaviour]
	public class ButtonUpTransition : StateBehaviour
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// ボタンの名前。
		/// </summary>
#else
		/// <summary>
		/// The name of the button.
		/// </summary>
#endif
		[SerializeField] private string _ButtonName = "Fire1";

#if ARBOR_DOC_JA
		/// <summary>
		/// 遷移先ステート。
		/// </summary>
#else
		/// <summary>
		/// Transition destination state.
		/// </summary>
#endif
		[SerializeField] private StateLink _NextState;

		#endregion // Serialize fields

		// Update is called once per frame
		void Update () 
		{
			if( Input.GetButtonUp( _ButtonName ) )
			{
				Transition( _NextState );
			}	
		}
	}
}
