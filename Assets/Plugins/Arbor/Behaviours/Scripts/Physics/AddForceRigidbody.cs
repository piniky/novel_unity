﻿using UnityEngine;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// Rigidbodyに力を加える。
	/// </summary>
#else
	/// <summary>
	/// We will apply a force to Rigidbody.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("Physics/AddForceRigidbody")]
	[BuiltInBehaviour]
	public class AddForceRigidbody : StateBehaviour
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 対象となるRigidbody。<br/>
		/// TypeがConstantの時に指定しない場合、ArborFSMを割り当ててあるGameObjectのRigidbody。
		/// </summary>
#else
		/// <summary>
		/// Rigidbody of interest.<br/>
		/// If Type is Constant and nothing is specified, Rigidbody of GameObject to which ArborFSM is assigned.
		/// </summary>
#endif
		[SerializeField] private FlexibleRigidbody _Target;

#if ARBOR_DOC_JA
		/// <summary>
		/// TargetのTransformから見て前方向を基準とした各軸の角度。
		/// </summary>
#else
		/// <summary>
		/// The angle of each axis with reference to the front direction as seen from the Target's Transform.
		/// </summary>
#endif
		[SerializeField] private Vector3 _Angle;

#if ARBOR_DOC_JA
		/// <summary>
		/// 加える力。
		/// </summary>
#else
		/// <summary>
		/// Force applied.
		/// </summary>
#endif
		[SerializeField] private float _Power;

		#endregion // Serialize fields

		private Rigidbody _MyRigidbody;
		public Rigidbody cachedTarget
		{
			get
			{
				Rigidbody rb = _Target.value;
				if( rb == null && _Target.type == FlexibleRigidbody.Type.Constant )
				{
					if( _MyRigidbody == null )
					{
						_MyRigidbody = GetComponent<Rigidbody>();
					}

					rb = _MyRigidbody;
				}
				return rb;
			}
		}

		// Use this for enter state
		public override void OnStateBegin()
		{
			Rigidbody target = cachedTarget;
			if ( target != null)
			{
				Vector3 direction = Quaternion.Euler(_Angle) * target.transform.forward;
				target.AddForce( direction.normalized * _Power);
            }
		}
	}
}
