﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Arbor
{
#if ARBOR_DOC_JA
	/// <summary>
	/// 指定したシーンを読み込む。
	/// </summary>
#else
	/// <summary>
	/// Load the specified scene.
	/// </summary>
#endif
	[AddComponentMenu("")]
	[AddBehaviourMenu("Scene/LoadLevel")]
	[BuiltInBehaviour]
	public class LoadLevel : StateBehaviour
	{
		#region Serialize fields

#if ARBOR_DOC_JA
		/// <summary>
		/// 読み込むシーンの名前。
		/// </summary>
#else
		/// <summary>
		/// The name of the load scene.
		/// </summary>
#endif
		[SerializeField] private string _LevelName;

#if ARBOR_DOC_JA
		/// <summary>
		/// 現在のシーンに追加するかどうか。
		/// </summary>
#else
		/// <summary>
		/// Whether to add to the current scene.
		/// </summary>
#endif
		[SerializeField] private bool _Additive;

		#endregion // Serialize fields

		// Use this for enter state
		public override void OnStateBegin() 
		{
			LoadSceneMode mode = _Additive ? LoadSceneMode.Additive : LoadSceneMode.Single;
			SceneManager.LoadScene( _LevelName, mode);
		}
	}
}
