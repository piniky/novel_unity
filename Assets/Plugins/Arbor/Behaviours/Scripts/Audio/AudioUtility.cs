﻿using UnityEngine;
using UnityEngine.Audio;

namespace Arbor
{
	public static class AudioUtility
	{
		public static void PlayClipAtPoint(AudioClip clip, Vector3 position, float volume, AudioMixerGroup outputAudioMixerGroup, float spatialBlend)
		{
			if (clip == null)
			{
				Debug.LogError("Clip cannot be null.");
				return;
			}

			GameObject gameObject = new GameObject("One shot audio");
			gameObject.transform.position = position;
			AudioSource audioSource = (AudioSource)gameObject.AddComponent(typeof(AudioSource));
			audioSource.clip = clip;
			audioSource.volume = volume;
			audioSource.outputAudioMixerGroup = outputAudioMixerGroup;
			audioSource.spatialBlend = spatialBlend;
			audioSource.Play();
			Object.Destroy(gameObject, clip.length * Time.timeScale);
		}
	}

}
