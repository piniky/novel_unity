﻿using UnityEngine;
using UnityEditor;
using System.Collections;

using Arbor;
namespace ArborEditor
{
	[CustomEditor(typeof(UISetToggle))]
	public class UISetToggleInspector : Editor
	{
		public override void OnInspectorGUI()
		{
			serializedObject.Update();

			EditorGUILayout.PropertyField(serializedObject.FindProperty("_Toggle"));

			SerializedProperty valueProperty = serializedObject.FindProperty("_Value");
			EditorGUILayout.PropertyField(valueProperty);

			SerializedProperty typeProperty = valueProperty.FindPropertyRelative("_Type");
			FlexibleBool.Type type = EnumUtility.GetValueFromIndex<FlexibleBool.Type>(typeProperty.enumValueIndex);
			if (type == FlexibleBool.Type.Parameter)
			{
				EditorGUILayout.PropertyField(serializedObject.FindProperty("_ChangeTimingUpdate"));
			}
			
			serializedObject.ApplyModifiedProperties();
		}
	}
}