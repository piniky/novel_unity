﻿using UnityEngine;
using UnityEditor;
using System.Collections;

using Arbor;
namespace ArborEditor
{
	[CustomEditor(typeof(UISetText))]
	public class UISetTextInspector : Editor
	{
		public override void OnInspectorGUI()
		{
			serializedObject.Update();

			EditorGUILayout.PropertyField(serializedObject.FindProperty("_Text"));

			SerializedProperty stringProperty = serializedObject.FindProperty("_String");
			EditorGUILayout.PropertyField(stringProperty);

			SerializedProperty typeProperty = stringProperty.FindPropertyRelative("_Type");
			FlexibleString.Type type = EnumUtility.GetValueFromIndex<FlexibleString.Type>(typeProperty.enumValueIndex);
			if (type == FlexibleString.Type.Parameter)
			{
				EditorGUILayout.PropertyField(serializedObject.FindProperty("_ChangeTimingUpdate"));
			}

			serializedObject.ApplyModifiedProperties();
		}
	}
}