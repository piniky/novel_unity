﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;
using System.Collections;
using System.Collections.Generic;

using Arbor;
namespace ArborEditor
{
	[CustomEditor(typeof(Arbor.AnimatorStateTransition))]
	public class AnimatorStateTransitionInspector : Editor
	{
		private string[] _LayerNames;
		private string[] _StateNames;
		public override void OnInspectorGUI()
		{
			serializedObject.Update();

			SerializedProperty animatorProperty = serializedObject.FindProperty("_Animator");
			SerializedProperty layerNameProperty = serializedObject.FindProperty("_LayerName");
			SerializedProperty stateNameProperty = serializedObject.FindProperty("_StateName");

			EditorGUILayout.PropertyField(animatorProperty);

			if (animatorProperty.FindPropertyRelative("_Type").enumValueIndex == EnumUtility.GetIndexFromValue(FlexibleComponent.Type.Constant))
			{
				Animator animator = animatorProperty.FindPropertyRelative("_Value").objectReferenceValue as Animator;

				EditorGUITools.AnimatorStateField(animator, layerNameProperty, stateNameProperty);
			}
			else
			{
				EditorGUILayout.PropertyField(layerNameProperty);
				EditorGUILayout.PropertyField(stateNameProperty);
			}

			serializedObject.ApplyModifiedProperties();
		}
	}
}
