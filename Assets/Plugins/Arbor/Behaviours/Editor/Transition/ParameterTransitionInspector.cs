﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

using Arbor;

namespace ArborEditor
{
	[CustomEditor(typeof(ParameterTransition))]
	public class ParameterTransitionInspector : Editor
	{
		ListGUI _CondisionsGUI;

		private void OnEnable()
		{
			_CondisionsGUI = new ListGUI( serializedObject.FindProperty( "_Condisions" ) );

			_CondisionsGUI.drawChild = OnDrawChild;
			_CondisionsGUI.remove = OnRemove;
		}

		private void CondisionGUI(ParameterContainer container, SerializedProperty referenceProperty, SerializedProperty condisionProperty)
		{
			SerializedProperty idProperty = referenceProperty.FindPropertyRelative("id");

			Parameter parameter = container.GetParam(idProperty.intValue);
			
			if (parameter != null)
			{
				SerializedProperty typeProperty = condisionProperty.FindPropertyRelative("_Type");

				switch (parameter.type)
				{
					case Parameter.Type.Int:
						{
							EditorGUILayout.PropertyField(typeProperty);

							EditorGUILayout.PropertyField(condisionProperty.FindPropertyRelative("_IntValue"), EditorGUITools.GetTextContent( "Int Value" ) );
						}
						break;
					case Parameter.Type.Long:
						{
							EditorGUILayout.PropertyField(typeProperty);

							EditorGUILayout.PropertyField(condisionProperty.FindPropertyRelative("_LongValue"), EditorGUITools.GetTextContent("Long Value"));
						}
						break;
					case Parameter.Type.Float:
						{
							EditorGUILayout.PropertyField(typeProperty);

							EditorGUILayout.PropertyField(condisionProperty.FindPropertyRelative("_FloatValue"), EditorGUITools.GetTextContent( "Float Value" ) );
						}
						break;
					case Parameter.Type.Bool:
						{
							EditorGUILayout.PropertyField(condisionProperty.FindPropertyRelative("_BoolValue"), EditorGUITools.GetTextContent( "Bool Value" ) );
						}
						break;
					case Parameter.Type.String:
						{
							EditorGUILayout.PropertyField(typeProperty);

							EditorGUILayout.PropertyField(condisionProperty.FindPropertyRelative("_StringValue"), EditorGUITools.GetTextContent( "String Value" ) );
						}
						break;
					case Parameter.Type.GameObject:
						{
							EditorGUILayout.PropertyField(condisionProperty.FindPropertyRelative("_GameObjectValue"), EditorGUITools.GetTextContent( "GameObject Value" ) );
						}
						break;
					case Parameter.Type.Vector2:
						{
							EditorGUILayout.PropertyField(condisionProperty.FindPropertyRelative("_Vector2Value"), EditorGUITools.GetTextContent( "Vector2 Value" ) );
						}
						break;
					case Parameter.Type.Vector3:
						{
							EditorGUILayout.PropertyField(condisionProperty.FindPropertyRelative("_Vector3Value"), EditorGUITools.GetTextContent( "Vector3 Value" ) );
						}
						break;
					case Parameter.Type.Quaternion:
						{
							EditorGUILayout.PropertyField(condisionProperty.FindPropertyRelative("_QuaternionValue"), EditorGUITools.GetTextContent( "Quaternion Value" ) );
						}
						break;
					case Parameter.Type.Rect:
						{
							EditorGUILayout.PropertyField(condisionProperty.FindPropertyRelative("_RectValue"), EditorGUITools.GetTextContent( "Rect Value" ) );
						}
						break;
					case Parameter.Type.Bounds:
						{
							EditorGUILayout.PropertyField(condisionProperty.FindPropertyRelative("_BoundsValue"), EditorGUITools.GetTextContent( "Bounds Value" ) );
						}
						break;
					case Parameter.Type.Transform:
						{
							EditorGUILayout.PropertyField(condisionProperty.FindPropertyRelative("_TransformValue"), EditorGUITools.GetTextContent( "Transform Value" ) );
						}
						break;
                    case Parameter.Type.RectTransform:
						{
							EditorGUILayout.PropertyField(condisionProperty.FindPropertyRelative("_RectTransformValue"), EditorGUITools.GetTextContent( "RectTransform Value" ) );
						}
						break;
					case Parameter.Type.Rigidbody:
						{
							EditorGUILayout.PropertyField(condisionProperty.FindPropertyRelative("_RigidbodyValue"), EditorGUITools.GetTextContent( "Rigidbody Value" ) );
						}
						break;
					case Parameter.Type.Rigidbody2D:
						{
							EditorGUILayout.PropertyField(condisionProperty.FindPropertyRelative("_Rigidbody2DValue"), EditorGUITools.GetTextContent( "Rigidbody2D Value" ) );
						}
						break;
					case Parameter.Type.Component:
						{
							EditorGUILayout.PropertyField(condisionProperty.FindPropertyRelative("_ComponentValue"), EditorGUITools.GetTextContent("Component Value"));
						}
						break;
				}
			}
		}

		ParameterContainer GetParameterContainer(SerializedProperty referenceProperty)
		{
			SerializedProperty containerProperty = referenceProperty.FindPropertyRelative("container");

			ParameterContainerBase containerBase = containerProperty.objectReferenceValue as ParameterContainerBase;
			ParameterContainer container = null;
			if (containerBase != null)
			{
				container = containerBase.defaultContainer as ParameterContainer;
			}

			return container;
		}

		Parameter GetParameter(SerializedProperty referenceProperty)
		{
			ParameterContainer container = GetParameterContainer(referenceProperty);

			if (container != null)
			{
				SerializedProperty idProperty = referenceProperty.FindPropertyRelative("id");

				return container.GetParam(idProperty.intValue);
			}

			return null;
		}

		void OnDrawChild(SerializedProperty property)
		{
			SerializedProperty referenceProperty = property.FindPropertyRelative("_Reference");

			Parameter oldParameter = GetParameter(referenceProperty);

			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(referenceProperty);
			if (EditorGUI.EndChangeCheck() && oldParameter != null)
			{
				SerializedProperty valueProperty = null;
				switch (oldParameter.type)
				{
					case Parameter.Type.Int:
						{
							valueProperty = property.FindPropertyRelative("_IntValue");
						}
						break;
					case Parameter.Type.Long:
						{
							valueProperty = property.FindPropertyRelative("_LongValue");
						}
						break;
					case Parameter.Type.Float:
						{
							valueProperty = property.FindPropertyRelative("_FloatValue");
						}
						break;
					case Parameter.Type.Bool:
						{
							valueProperty = property.FindPropertyRelative("_BoolValue");
						}
						break;
					case Parameter.Type.String:
						{
							valueProperty = property.FindPropertyRelative("_StringValue");
						}
						break;
					case Parameter.Type.GameObject:
						{
							valueProperty = property.FindPropertyRelative("_GameObjectValue");
						}
						break;
					case Parameter.Type.Vector2:
						{
							valueProperty = property.FindPropertyRelative("_Vector2Value");
						}
						break;
					case Parameter.Type.Vector3:
						{
							valueProperty = property.FindPropertyRelative("_Vector3Value");
						}
						break;
					case Parameter.Type.Quaternion:
						{
							valueProperty = property.FindPropertyRelative("_QuaternionValue");
						}
						break;
					case Parameter.Type.Rect:
						{
							valueProperty = property.FindPropertyRelative("_RectValue");
						}
						break;
					case Parameter.Type.Bounds:
						{
							valueProperty = property.FindPropertyRelative("_BoundsValue");
						}
						break;
					case Parameter.Type.Transform:
						{
							valueProperty = property.FindPropertyRelative("_TransformValue");
						}
						break;
					case Parameter.Type.RectTransform:
						{
							valueProperty = property.FindPropertyRelative("_RectTransformValue");
						}
						break;
					case Parameter.Type.Rigidbody:
						{
							valueProperty = property.FindPropertyRelative("_RigidbodyValue");
						}
						break;
					case Parameter.Type.Rigidbody2D:
						{
							valueProperty = property.FindPropertyRelative("_Rigidbody2DValue");
						}
						break;
					case Parameter.Type.Component:
						{
							valueProperty = property.FindPropertyRelative("_ComponentValue");
						}
						break;
				}

				if (valueProperty != null)
				{
					object valueObj = EditorGUITools.GetPropertyObject<object>(valueProperty);

					EachField<InputSlot>.Find(valueObj, valueObj.GetType(), (s) => {
						if (s.stateMachine != null && s.branch != null)
						{
							s.stateMachine.DeleteCalculatorBranch(s.branch);
						}
					});
				}
			}

			ParameterContainer container = GetParameterContainer(referenceProperty);
			
			if (container != null)
			{
				CondisionGUI(container, referenceProperty, property);
			}
		}

		void OnRemove(SerializedProperty property)
		{
			SerializedProperty referenceProperty = property.FindPropertyRelative("_Reference");

			Parameter parameter = GetParameter(referenceProperty);

			if (parameter != null)
			{
				SerializedProperty valueProperty = null;
				switch (parameter.type)
				{
					case Parameter.Type.Int:
						{
							valueProperty = property.FindPropertyRelative("_IntValue");
						}
						break;
					case Parameter.Type.Long:
						{
							valueProperty = property.FindPropertyRelative("_LongValue");
						}
						break;
					case Parameter.Type.Float:
						{
							valueProperty = property.FindPropertyRelative("_FloatValue");
						}
						break;
					case Parameter.Type.Bool:
						{
							valueProperty = property.FindPropertyRelative("_BoolValue");
						}
						break;
					case Parameter.Type.String:
						{
							valueProperty = property.FindPropertyRelative("_StringValue");
						}
						break;
					case Parameter.Type.GameObject:
						{
							valueProperty = property.FindPropertyRelative("_GameObjectValue");
						}
						break;
					case Parameter.Type.Vector2:
						{
							valueProperty = property.FindPropertyRelative("_Vector2Value");
						}
						break;
					case Parameter.Type.Vector3:
						{
							valueProperty = property.FindPropertyRelative("_Vector3Value");
						}
						break;
					case Parameter.Type.Quaternion:
						{
							valueProperty = property.FindPropertyRelative("_QuaternionValue");
						}
						break;
					case Parameter.Type.Rect:
						{
							valueProperty = property.FindPropertyRelative("_RectValue");
						}
						break;
					case Parameter.Type.Bounds:
						{
							valueProperty = property.FindPropertyRelative("_BoundsValue");
						}
						break;
					case Parameter.Type.Transform:
						{
							valueProperty = property.FindPropertyRelative("_TransformValue");
						}
						break;
					case Parameter.Type.RectTransform:
						{
							valueProperty = property.FindPropertyRelative("_RectTransformValue");
						}
						break;
					case Parameter.Type.Rigidbody:
						{
							valueProperty = property.FindPropertyRelative("_RigidbodyValue");
						}
						break;
					case Parameter.Type.Rigidbody2D:
						{
							valueProperty = property.FindPropertyRelative("_Rigidbody2DValue");
						}
						break;
					case Parameter.Type.Component:
						{
							valueProperty = property.FindPropertyRelative("_ComponentValue");
						}
						break;
				}

				if (valueProperty != null)
				{
					object valueObj = EditorGUITools.GetPropertyObject<object>(valueProperty);

					EachField<InputSlot>.Find(valueObj, valueObj.GetType(), (s) => {
						if (s.stateMachine != null && s.branch != null)
						{
							s.stateMachine.DeleteCalculatorBranch(s.branch);
						}
					});
				}
			}
		}

		public override void OnInspectorGUI()
		{
			serializedObject.Update();

			_CondisionsGUI.OnGUI();

			serializedObject.ApplyModifiedProperties();
		}
	}
}
