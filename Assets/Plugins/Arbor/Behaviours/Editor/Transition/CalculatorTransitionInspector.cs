﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

using Arbor;

namespace ArborEditor
{
	[CustomEditor(typeof(CalculatorTransition))]
	public class CalculatorTransitionInspector : Editor
	{
		CalculatorTransition _CalculatorTransition;

		ListGUI _CondisiondGUI;

		void OnEnable()
		{
			_CalculatorTransition = target as CalculatorTransition;

			_CondisiondGUI = new ListGUI( serializedObject.FindProperty( "_Condisions" ) );

			_CondisiondGUI.addButton = OnAddButton;
			_CondisiondGUI.drawChild = OnDrawChild;
			_CondisiondGUI.remove = OnRemove;
		}

		void OnDrawChild(SerializedProperty property)
		{
			SerializedProperty typeProperty = property.FindPropertyRelative("_Type");

			CalculatorTransition.Condision.Type type = EnumUtility.GetValueFromIndex<CalculatorTransition.Condision.Type>(typeProperty.enumValueIndex);

			switch (type)
			{
				case CalculatorTransition.Condision.Type.Int:
					{
						EditorGUILayout.PropertyField(property.FindPropertyRelative("_CompareType"));
						EditorGUILayout.PropertyField(property.FindPropertyRelative("_IntValue1"));
						EditorGUILayout.PropertyField(property.FindPropertyRelative("_IntValue2"));
					}
					break;
				case CalculatorTransition.Condision.Type.Float:
					{
						EditorGUILayout.PropertyField(property.FindPropertyRelative("_CompareType"));
						EditorGUILayout.PropertyField(property.FindPropertyRelative("_FloatValue1"));
						EditorGUILayout.PropertyField(property.FindPropertyRelative("_FloatValue2"));
					}
					break;
				case CalculatorTransition.Condision.Type.Bool:
					{
						EditorGUILayout.PropertyField(property.FindPropertyRelative("_BoolValue1"));
						EditorGUILayout.PropertyField(property.FindPropertyRelative("_BoolValue2"));
					}
					break;
			}
		}

		void OnRemove(SerializedProperty property)
		{
			SerializedProperty typeProperty = property.FindPropertyRelative("_Type");

			CalculatorTransition.Condision.Type type = EnumUtility.GetValueFromIndex<CalculatorTransition.Condision.Type>(typeProperty.enumValueIndex);

			List<SerializedProperty> valueProperties = new List<SerializedProperty>();

			switch (type)
			{
				case CalculatorTransition.Condision.Type.Int:
					{
						valueProperties.Add(property.FindPropertyRelative("_IntValue1"));
						valueProperties.Add(property.FindPropertyRelative("_IntValue2"));
					}
					break;
				case CalculatorTransition.Condision.Type.Float:
					{
						valueProperties.Add(property.FindPropertyRelative("_FloatValue1"));
						valueProperties.Add(property.FindPropertyRelative("_FloatValue2"));
					}
					break;
				case CalculatorTransition.Condision.Type.Bool:
					{
						valueProperties.Add(property.FindPropertyRelative("_BoolValue1"));
						valueProperties.Add(property.FindPropertyRelative("_BoolValue2"));
					}
					break;
			}

			int propertyCount = valueProperties.Count;
			for (int propertyIndex = 0; propertyIndex < propertyCount; propertyIndex++)
			{
				SerializedProperty valueProperty = valueProperties[propertyIndex];
				object valueObj = EditorGUITools.GetPropertyObject<object>(valueProperty);

				EachField<InputSlot>.Find(valueObj, valueObj.GetType(), (s) => {
					if (s.stateMachine != null && s.branch != null)
					{
						s.stateMachine.DeleteCalculatorBranch(s.branch);
					}
				});
			}
		}

		void AddCondisionMenu(object value)
		{
			Undo.RecordObject(_CalculatorTransition, "Condision Added");
			CalculatorTransition.Condision.Type type = (CalculatorTransition.Condision.Type)value;
			_CalculatorTransition.AddCondition(type);
		}

		void OnAddButton()
		{
			GenericMenu genericMenu = new GenericMenu();
			foreach (object userData in System.Enum.GetValues(typeof(CalculatorTransition.Condision.Type)))
			{
				genericMenu.AddItem( EditorGUITools.GetTextContent( userData.ToString() ), false, AddCondisionMenu, userData);
			}

			genericMenu.ShowAsContext();
		}

		public override void OnInspectorGUI()
		{
			serializedObject.Update();

			_CondisiondGUI.OnGUI();

			serializedObject.ApplyModifiedProperties();
		}
	}
}
