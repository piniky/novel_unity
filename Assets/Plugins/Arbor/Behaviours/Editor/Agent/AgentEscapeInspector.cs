﻿using UnityEngine;
using UnityEditor;
using System.Collections;

using Arbor;
namespace ArborEditor
{
	[CustomEditor(typeof(AgentEscape))]
	public class AgentEscapeInspector : AgentIntervalUpdateInspector
	{
		public override void OnInspectorGUI()
		{
			serializedObject.Update();

			DrawBase();

			EditorGUILayout.Space();

			EditorGUILayout.PropertyField(serializedObject.FindProperty("_Distance"));
			EditorGUILayout.PropertyField(serializedObject.FindProperty("_Target"));
			
			serializedObject.ApplyModifiedProperties();
		}
	}
}