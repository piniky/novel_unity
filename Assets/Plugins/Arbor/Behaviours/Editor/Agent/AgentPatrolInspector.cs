﻿using UnityEngine;
using UnityEditor;
using System.Collections;

using Arbor;
namespace ArborEditor
{
	[CustomEditor(typeof(AgentPatrol))]
	public class AgentPatrolInspector : AgentIntervalUpdateInspector
	{
		public override void OnInspectorGUI()
		{
			serializedObject.Update();

			DrawBase();

			EditorGUILayout.Space();

			EditorGUILayout.PropertyField(serializedObject.FindProperty("_Radius"));
			
			serializedObject.ApplyModifiedProperties();
		}
	}
}