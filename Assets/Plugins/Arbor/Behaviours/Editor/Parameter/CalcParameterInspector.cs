﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

using Arbor;

namespace ArborEditor
{
	[CustomEditor(typeof(CalcParameter))]
	public class CalcParameterInspector : Editor
	{
		ParameterContainer GetParameterContainer(SerializedProperty referenceProperty)
		{
			SerializedProperty containerProperty = referenceProperty.FindPropertyRelative("container");

			ParameterContainerBase containerBase = containerProperty.objectReferenceValue as ParameterContainerBase;
			ParameterContainer container = null;
			if (containerBase != null)
			{
				container = containerBase.defaultContainer as ParameterContainer;
			}

			return container;
		}

		Parameter GetParameter(SerializedProperty referenceProperty)
		{
			ParameterContainer container = GetParameterContainer(referenceProperty);

			if (container != null)
			{
				SerializedProperty idProperty = referenceProperty.FindPropertyRelative("id");

				return container.GetParam(idProperty.intValue);
			}

			return null;
		}

		public override void OnInspectorGUI()
		{
			serializedObject.Update();

			SerializedProperty referenceProperty = serializedObject.FindProperty("reference");

			Parameter oldParameter = GetParameter(referenceProperty);

			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(referenceProperty);
			if (EditorGUI.EndChangeCheck() && oldParameter != null)
			{
				SerializedProperty valueProperty = null;
                switch (oldParameter.type)
				{
					case Parameter.Type.Int:
						{
							valueProperty = serializedObject.FindProperty("_IntValue");
                        }
						break;
					case Parameter.Type.Float:
						{
							valueProperty = serializedObject.FindProperty("_FloatValue");
						}
						break;
					case Parameter.Type.Bool:
						{
							valueProperty = serializedObject.FindProperty("_BoolValue");
						}
						break;
					case Parameter.Type.String:
						{
							valueProperty = serializedObject.FindProperty("_StringValue");
						}
						break;
					case Parameter.Type.GameObject:
						{
							valueProperty = serializedObject.FindProperty("_GameObjectValue");
						}
						break;
					case Parameter.Type.Vector2:
						{
							valueProperty = serializedObject.FindProperty("_Vector2Value");
						}
						break;
					case Parameter.Type.Vector3:
						{
							valueProperty = serializedObject.FindProperty("_Vector3Value");
						}
						break;
					case Parameter.Type.Quaternion:
						{
							valueProperty = serializedObject.FindProperty("_QuaternionValue");
						}
						break;
					case Parameter.Type.Rect:
						{
							valueProperty = serializedObject.FindProperty("_RectValue");
						}
						break;
					case Parameter.Type.Bounds:
						{
							valueProperty = serializedObject.FindProperty("_BoundsValue");
						}
						break;
					case Parameter.Type.Transform:
						{
							valueProperty = serializedObject.FindProperty("_TransformValue");
						}
						break;
					case Parameter.Type.RectTransform:
						{
							valueProperty = serializedObject.FindProperty("_RectTransformValue");
						}
						break;
					case Parameter.Type.Rigidbody:
						{
							valueProperty = serializedObject.FindProperty("_RigidbodyValue");
						}
						break;
					case Parameter.Type.Rigidbody2D:
						{
							valueProperty = serializedObject.FindProperty("_Rigidbody2DValue");
						}
						break;
					case Parameter.Type.Component:
						{
							valueProperty = serializedObject.FindProperty("_ComponentValue");
						}
						break;
					case Parameter.Type.Long:
						{
							valueProperty = serializedObject.FindProperty("_LongValue");
						}
						break;
				}

				if (valueProperty != null)
				{
					object valueObj = EditorGUITools.GetPropertyObject<object>(valueProperty);

					EachField<InputSlot>.Find(valueObj, valueObj.GetType(), (s) => {
						if (s.stateMachine != null && s.branch != null )
						{
							s.stateMachine.DeleteCalculatorBranch(s.branch);
                        }
					});
                }
			}

			ParameterContainer container = GetParameterContainer(referenceProperty);
			
			if (container != null)
			{
				SerializedProperty idProperty = referenceProperty.FindPropertyRelative("id");

				Parameter parameter = container.GetParam(idProperty.intValue);

				if (parameter != null)
				{
					SerializedProperty functionProperty = serializedObject.FindProperty("function");

					switch (parameter.type)
					{
						case Parameter.Type.Int:
							{
								EditorGUILayout.PropertyField(functionProperty);

								EditorGUILayout.PropertyField(serializedObject.FindProperty("_IntValue"));
							}
							break;
						case Parameter.Type.Long:
							{
								EditorGUILayout.PropertyField(functionProperty);

								EditorGUILayout.PropertyField(serializedObject.FindProperty("_LongValue"));
							}
							break;
						case Parameter.Type.Float:
							{
								EditorGUILayout.PropertyField(functionProperty);

								EditorGUILayout.PropertyField(serializedObject.FindProperty("_FloatValue"));
							}
							break;
						case Parameter.Type.Bool:
							{
								EditorGUILayout.PropertyField(serializedObject.FindProperty("_BoolValue"));
							}
							break;
						case Parameter.Type.String:
							{
								EditorGUILayout.PropertyField(functionProperty);

								EditorGUILayout.PropertyField(serializedObject.FindProperty("_StringValue"));
							}
							break;
						case Parameter.Type.GameObject:
							{
								EditorGUILayout.PropertyField(serializedObject.FindProperty("_GameObjectValue"));
							}
							break;
						case Parameter.Type.Vector2:
							{
								EditorGUILayout.PropertyField(serializedObject.FindProperty("_Vector2Value"));
							}
							break;
						case Parameter.Type.Vector3:
							{
								EditorGUILayout.PropertyField(serializedObject.FindProperty("_Vector3Value"));
							}
							break;
						case Parameter.Type.Quaternion:
							{
								EditorGUILayout.PropertyField(serializedObject.FindProperty("_QuaternionValue"));
							}
							break;
						case Parameter.Type.Rect:
							{
								EditorGUILayout.PropertyField(serializedObject.FindProperty("_RectValue"));
							}
							break;
						case Parameter.Type.Bounds:
							{
								EditorGUILayout.PropertyField(serializedObject.FindProperty("_BoundsValue"));
							}
							break;
						case Parameter.Type.Transform:
							{
								EditorGUILayout.PropertyField(serializedObject.FindProperty("_TransformValue"));
							}
							break;
						case Parameter.Type.RectTransform:
							{
								EditorGUILayout.PropertyField(serializedObject.FindProperty("_RectTransformValue"));
							}
							break;
						case Parameter.Type.Rigidbody:
							{
								EditorGUILayout.PropertyField(serializedObject.FindProperty("_RigidbodyValue"));
							}
							break;
						case Parameter.Type.Rigidbody2D:
							{
								EditorGUILayout.PropertyField(serializedObject.FindProperty("_Rigidbody2DValue"));
							}
							break;
						case Parameter.Type.Component:
							{
								EditorGUILayout.PropertyField(serializedObject.FindProperty("_ComponentValue"));
							}
							break;
					}
				}
			}

			serializedObject.ApplyModifiedProperties();
		}
	}
}

