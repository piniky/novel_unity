﻿using System;
using UnityEngine;
using UnityEngine.UI;

[SerializeField]
public class DateInGame
{
	int _month = 0, _week = 0;

	public DateInGame(int month = 4, int week = 1)
	{
		_month = month;
		_week = week;
	}

	public void nextWeek()
	{
		_week++;
		if (_week == 5)
		{
			_month++; _week = 1;
		}
		if (_month > 12)
		{
			_month = 1;
		}
	}
	public int getCurrentMonth() { return _month; }
	public int getCurrentWeek() { return _week; }
	public int getLeftWeek()
	{
		int mon = _month;
		if (_month < 4) mon += 12;

		return 64 - mon * 4 - _week + 1;

	}
}
