﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeManager : MonoBehaviour
{
	Fade fade;

	void Start()
	{
		fade = GameObject.Find("FadeCanvas").GetComponent<Fade>();
	}

	public void FsmFadeOut(float fadetime)
	{
		fade.FadeOut(fadetime);
	}
	public void FsmFadeIn(float fadetime)
	{
		gameObject.GetComponent<FadeImage>().RandomizeTexture();
		fade.FadeIn(fadetime);
	}
	void FsmFadeInImmediate()
	{
		if (fade == null) fade = GameObject.Find("FadeCanvas").GetComponent<Fade>();
		fade.FadeIn(0.01f);
	}
}
