﻿using UnityEngine;
using System.Collections;

namespace Arbor
{

	[AddComponentMenu("")]
	[AddBehaviourMenu("自作/フェードアウト遷移")]
	[BuiltInBehaviour]
	public class FadeOutTransition : StateBehaviour
	{

		public enum SpeedType
		{
			Fast,
			Slow,
			Normal
		}

		#region Serialize fields


#if ARBOR_DOC_JA
		/// <summary>
		/// 遷移するまでの秒数。
		/// </summary>
#else
		/// <summary>
		/// The number of seconds until the transition.
		/// </summary>
#endif
		float _Seconds;
		[SerializeField] public SpeedType _SpeedType;

#if ARBOR_DOC_JA
		/// <summary>
		/// 遷移先ステート。
		/// </summary>
#else
		/// <summary>
		/// Transition destination state.
		/// </summary>
#endif
		[SerializeField] private StateLink _NextState;

		#endregion // Serialize fields

		public float currentTime
		{
			get
			{
				return Time.time;
			}
		}

		float _BeginTime = 0.0f;
		public float elapsedTime
		{
			get
			{
				return currentTime - _BeginTime;
			}
		}

		public override void OnStateBegin()
		{
			_BeginTime = currentTime;
			switch (_SpeedType)
			{
				case SpeedType.Normal:
					_Seconds = 1f;
					break;
				case SpeedType.Fast:
					_Seconds = 0.7f;
					break;
				case SpeedType.Slow:
					_Seconds = 1.5f;
					break;
				default:
					break;
			}

			GameObject fadeCanvas = GameObject.Find("FadeCanvas");
			fadeCanvas.GetComponent<FadeManager>().FsmFadeOut(_Seconds);

		}

		public override void OnStateUpdate()
		{
			if (elapsedTime >= _Seconds)
			{
				GameObject text = GameObject.Find("Canvas/MessagePanel/Text");
				if (text != null) text.GetComponent<TextController>().StartStreaming();
				Transition(_NextState);
			}
		}
	}
}
