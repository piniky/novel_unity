﻿using UnityEngine;
using System.Collections;
using Arbor;
using UnityEngine.UI;

public class CheckEvent : StateBehaviour {
	public StateLink YES, NO;
	public Text textPanel;
	
	// Use this for initialization
	void Start () {
	}

	// Use this for enter state
	public override void OnStateBegin() {
		CubismManager.SetPosition(CubismManager.Position.Center);

		int eventNo = CheckForceEvent();
		if (eventNo != -1)
		{
			setEvent(eventNo);
		}
		else
		{
			eventNo = checkRandomEvent();
			if (eventNo != -1){
				setEvent(eventNo);
			}
			else
			{
				Transition(NO);
			}
		}
	
	}

	int CheckForceEvent()
	{

		return -1;
	}
	int checkRandomEvent()
	{
		return -1;
	}

	void setEvent(int eventNo)
	{
		textPanel.GetComponent<TextController>().setLines(DataTable.EventScripts[eventNo].text);
		GameData.State = "event";
		GameData.Save();
		Transition(YES);
	}
}
