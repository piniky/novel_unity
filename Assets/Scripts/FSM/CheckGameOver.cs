using UnityEngine;
using System.Collections;
using Arbor;
using UnityEngine.UI;

public class CheckGameOver : StateBehaviour {
	public StateLink YES, NO;

	public override void OnStateBegin() {

		DateInGame date = GameData.dateInGame;
		if (date.getCurrentMonth() == 3 && date.getCurrentWeek() == 4)
		{
			Transition(YES);
		}
		else
		{
			Transition(NO);
		}
	
	}
}
