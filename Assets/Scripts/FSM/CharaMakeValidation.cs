using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Arbor;

public class CharaMakeValidation : StateBehaviour
{
	public StateLink nextState_OK, nextState_NG;
	public GameObject Input_Lastname, Input_Firstname;
	public GameObject BirthPlace;
	public GameObject BirthMonth;
	public GameObject BirthDay;

	// Use this for enter state
	public override void OnStateBegin()
	{
		string firstname = Input_Firstname.GetComponent<Text>().text;
		string lastname = Input_Lastname.GetComponent<Text>().text;
		string birthPlace = BirthPlace.GetComponent<Text>().text;
		string birthMonth = BirthMonth.GetComponent<Text>().text;
		string birthDay = BirthDay.GetComponent<Text>().text;

		// 空チェック
		if (firstname.Length == 0 || lastname.Length == 0)
		{
			Transition(nextState_NG);
			return;
		}

		// 長さ制限
		if (firstname.Length > 8 || lastname.Length > 8)
		{
			Transition(nextState_NG);
			return;
		}

		int month = 0;
		if (!int.TryParse(birthMonth, out month))
		{
			Transition(nextState_NG);
			return;
		}

		if (month > 12 || month < 1)
		{
			Transition(nextState_NG);
			return;
		}
		int day = 0;
		if (!int.TryParse(birthDay, out day))
		{
			Transition(nextState_NG);
			return;
		}

		List<int> lastDays = new List<int> { 0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		if (day < 1 || day > lastDays[month])
		{
			Transition(nextState_NG);
			return;
		}
		Transition(nextState_OK);

	}

}
