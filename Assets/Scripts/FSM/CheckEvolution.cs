﻿using UnityEngine;
using System.Collections;
using Arbor;
using UnityEngine.UI;

public class CheckEvolution : StateBehaviour {
	public StateLink YES, NO;
	// Use this for initialization
	void Start () {
	}

	// Use this for enter state
	public override void OnStateBegin() {
		Personality candidate = Personalities.getEvolutionCandidate();
		if (candidate != null)
		{
			gameObject.GetComponent<ParameterContainer>().SetBool("canEvolute", true);
			gameObject.GetComponent<ParameterContainer>().SetString("BeforeEvol", Personalities.currentPersonality.Name);
			gameObject.GetComponent<ParameterContainer>().SetString("AfterEvol", candidate.Name);
			GameData.State = "evolution";
			GameData.Save();
			Transition(YES);
		}
		else
		{
			gameObject.GetComponent<ParameterContainer>().SetBool("canEvolute", false);
			Transition(NO);
		}
	
	}
}
