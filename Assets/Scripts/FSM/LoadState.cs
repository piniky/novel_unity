﻿using UnityEngine;
using System.Collections;
using Arbor;

public class LoadState : StateBehaviour {
	

	// Use this for enter state
	public override void OnStateBegin()
	{
		ArborFSM fsm = ArborFSM.FindFSM("Daily");
		string nextState  = "";
		switch (GameData.State)
		{
			case "default":
				nextState = "NewDay";
				break;
			case "lesson":
				nextState = "ForcedEventCheck";
				break;
			case "work":
				nextState = "ForcedEventCheck";
				break;
			case "play":
				nextState = "ForcedEventCheck";
				break;
			case "event":
				nextState = "EvolutionCheck";
				break;
			case "evolution":
				nextState = "EvolutionCheck";
				break;
			default:
				break;
		}
		State state = fsm.FindState(nextState);
		Transition(state);

	}
}
