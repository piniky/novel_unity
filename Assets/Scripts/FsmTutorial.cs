﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;

public class FsmTutorial : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	void FsmCharaMake()
	{
		string firstname = GameObject.Find("Canvas/CharaMake/CharaMakePanel/Input_Firstname/Text").GetComponent<Text>().text;
		string lastname = GameObject.Find("Canvas/CharaMake/CharaMakePanel/Input_Lastname/Text").GetComponent<Text>().text;
		System.IO.File.Delete("save1");

		// ランダムな能力値を生成
		int[] param = new int[Const.NUM_STATUS_PARAMETER];
		System.Random r = new System.Random();
		for (int i = 0; i < Const.NUM_STATUS_PARAMETER; i++)
		{
			param[i] = r.Next(10, 20);
		}
		SaveData.SetList("param", new List<int>(param));
		SaveData.SetInt("money", GameData.Money);
		SaveData.SetInt("month", 4);
		SaveData.SetInt("week", 1);
		SaveData.SetInt("hp", 100);
		SaveData.SetString("state", "default");
		SaveData.SetList("specialabilities", new List<int>());
		SaveData.SetInt("charactertype", Character.ModelType);
		SaveData.SetInt("personality", new int[] { 0, 10, 15, 20, 24, 27 }[Character.ModelType]);
		SaveData.SetString("birthplace", GameObject.Find("Canvas/CharaMake/CharaMakePanel/Input_Place/Text").GetComponent<Text>().text);
		SaveData.SetInt("birthmonth", int.Parse(GameObject.Find("Canvas/CharaMake/CharaMakePanel/Input_BirthdayMonth/Text").GetComponent<Text>().text));
		SaveData.SetInt("birthday", int.Parse(GameObject.Find("Canvas/CharaMake/CharaMakePanel/Input_BirthdayDay/Text").GetComponent<Text>().text));
		SaveData.SetString("firstname", firstname);
		SaveData.SetString("lastname", lastname);
		SaveData.Save();
	}

	public void FsmSetIntroduction()
	{
		
		TextAsset txtFile = Resources.Load("IntroductionText") as TextAsset;
		string str = txtFile.text;
		string[] lines = str.Split('\n');
		
		GameObject.Find("Canvas/Prologue/MessagePanel/Text").GetComponent<TextController>().setLines(lines);
	}

	public void FsmSetIntroduction2()
	{

		TextAsset txtFile = Resources.Load("IntroductionText2") as TextAsset;
		string str = txtFile.text;
		string[] lines = str.Split('\n');

		GameObject.Find("Canvas/Prologue/MessagePanel/Text").GetComponent<TextController>().setLines(lines);
	}

	void FsmRandomizeName()
	{
		// 名前
		System.Random random = new System.Random();
		int index = random.Next(DataTable.RandomFirstnames.Count);
		GameObject.Find("Canvas/CharaMake/CharaMakePanel/Input_Firstname").GetComponent<InputField>().text = DataTable.RandomFirstnames[index];
		index = random.Next(DataTable.RandomLastnames.Count);
		GameObject.Find("Canvas/CharaMake/CharaMakePanel/Input_Lastname").GetComponent<InputField>().text = DataTable.RandomLastnames[index];

		// 誕生日
		int month = GameData.random.Next(12);
		GameObject.Find("Canvas/CharaMake/CharaMakePanel/Input_BirthdayMonth").GetComponent<InputField>().text = (month + 1).ToString();
		int day = GameData.random.Next(new int[] { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 }[month] + 1);
		GameObject.Find("Canvas/CharaMake/CharaMakePanel/Input_BirthdayDay").GetComponent<InputField>().text = (day + 1).ToString();

		// 出身地
		int prefecture = GameData.random.Next(47);
		GameObject.Find("Canvas/CharaMake/CharaMakePanel/Input_Place").GetComponent<InputField>().text = Const.PREFECTURES[prefecture].ToString();
	}

	void FsmInitModel()
	{
		CubismManager.Init(Const.CHARACTER_NAMES[0]);
		CubismManager.SetPosition(CubismManager.Position.Left);
	}
}
