﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalitySelect : MonoBehaviour {
	float CursorRotation = 0f;

	public GameObject Cursor;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		CursorRotation += 1.5f;
		transform.rotation = Quaternion.Euler(CursorRotation, 0, 0);
	}

	public void OnClickPersonalityButton(int index)
	{
		Cursor.GetComponent<RectTransform>().anchorMax = new Vector2(0.5f, 0.87f - 0.16f * index);
		Cursor.GetComponent<RectTransform>().anchorMin = new Vector2(0.5f, 0.87f - 0.16f * index);
		CubismManager.Init(Const.CHARACTER_NAMES[index]);
		Character.ModelType = index;
	}
}
