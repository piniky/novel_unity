﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickEffect : MonoBehaviour {

	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(0))
		{
			var pos = Input.mousePosition - new Vector3(400, 300, 10);
			GetComponent<RectTransform>().localPosition = pos;
			transform.Find("ClickEffect").GetComponent<ParticleSystem>().Play();
		}
	}
}
