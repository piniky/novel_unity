﻿using System.Collections.Generic;
using UnityEngine.Assertions;

public enum AbilityParams { Vo, Va, Da, In, St, Me }

public class Ability
{
	int[] rawdata;

	public Ability()
	{
		rawdata = new int[6];
	}
	public Ability(int[] initialParam)
	{
		rawdata = new int[6];
		Assert.IsTrue(initialParam.Length != 6, "length is not 6");
		for (int i = 0; i < 6; i++)
		{
			rawdata[i] = initialParam[i];
		}

	}

	int this[int i]
	{
		set { this.rawdata[i] = value; }
		get { return this.rawdata[i]; }
	}

	public void addParam(int index, int value) {
		rawdata[index] += value;
	}

	public int getParamMod(int index)
	{
		return rawdata[index] % 100;
	}
	public int getParamLv(int index)
	{
		return rawdata[index] / 100;
	}
	
	public static Ability operator +(Ability a, int[] b)
	{
		Ability ab = new Ability();

		for (int i = 0; i < Const.NUM_STATUS_PARAMETER; i++)
		{
			ab[i] = a[i] + b[i];
		}
		return ab;
	}
	public static Ability operator -(Ability a, int[] b)
	{
		Ability ab = new Ability();

		for (int i = 0; i < Const.NUM_STATUS_PARAMETER; i++)
		{
			ab[i] = a[i] - b[i];
		}
		return ab;
	}
}