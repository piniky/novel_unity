﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class VacationScripts {
	public Dictionary<int, List<EventScript>> Scripts;

	public VacationScripts()
	{
		Scripts = new Dictionary<int, List<EventScript>>();
		TextAsset csvFile = Resources.Load("Vacation") as TextAsset;
		StringReader reader = new StringReader(csvFile.text); reader.ReadLine();//1行読み飛ばす

		while (reader.Peek() > -1){
			// 1エピソード
			int personality = int.Parse(reader.ReadLine().Split(',')[0]);
			List<string> lines = new List<string>();
			string line = reader.ReadLine();
			while (line != null && line.Length > 1)
			{
				lines.Add(line.Replace(",", "//"));
				line = reader.ReadLine();
			}
			lines.Add("[endLessonResult]");

			EventScript es = new EventScript();
			es.text = lines.ToArray();

			if (!Scripts.ContainsKey(personality))
			{
				Scripts[personality] = new List<EventScript>();
			}
			Scripts[personality].Add(es);
		}
	}
}
