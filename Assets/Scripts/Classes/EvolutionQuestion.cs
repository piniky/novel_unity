﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvolutionQuestion {
	public List<string> QuestionLines;
	public List<string> StayChoices;
	public List<List<string>> MoveChoices;

	public List<string> ToLines()
	{
		List<int> selectedChoices = new List<int>();
		List<string> lines = new List<string>();

		// 質問
		foreach (var q in QuestionLines)
		{
			lines.Add("トレーナー//" + q);
		}

		List<int> indexes = new List<int>();
		while (indexes.Count < 3)
		{
			int newindex = GameData.random.Next(6);
			if (!indexes.Contains(newindex)) indexes.Add(newindex);
		}

		string line = "[choices:";
		foreach (var index in indexes)
		{
			int a = GameData.random.Next(MoveChoices[index].Count);
			selectedChoices.Add(a);
			line += MoveChoices[index][a];
			line += "/" + index.ToString() + "/";
		}
		line = line.Remove(line.Length - 1); // 余分なカンマを削除
		line += "]";
		lines.Add(line);
		

		for (int i = 0; i < 3; i++)
		{
			lines.Add("[label:" + indexes[i].ToString() + "]");
			lines.Add("[firstname]//" + MoveChoices[indexes[i]][selectedChoices[i]]);
			lines.Add("[pparam:" + indexes[i].ToString() + "/1]");
			lines.Add("[jump:endevolchoice]");
		}
		lines.AddRange(new List<string>{ "[label:endevolchoice]", "トレーナー//わかりました。", "[endEvolEvent]" });

		return lines;
	}
}
