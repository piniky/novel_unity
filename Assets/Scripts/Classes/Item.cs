﻿using System.Collections;
using System.Collections.Generic;

public struct Item {

	public string Name;
	public string Description;
	public string EffectTarget;
	public int EffectValue;
	
}

public struct OwnItem
{
	public int ID;
	public int Count;
}