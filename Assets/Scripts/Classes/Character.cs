﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Character
{
	public static string Firstname { get; set; }
	public static string Lastname { get; set; }
	public static string Fullname { get; set; }
	public static Personality Personality { get; set; }
	public static List<SpecialAbility> AcquiredAbilities;
	static int[] param;

	static int HP;
	public static int ModelType = 0;

	// ゲームには直接関係ないキャラ情報
	public static string birthplace;
	public static int birthmonth;
	public static int birthday;

	public static void Init(string new_lastname, string new_firstname)
	{
		Firstname = new_firstname;
		Lastname = new_lastname;
		Fullname = new_lastname + new_firstname;
		Personality = Personalities.get("クール");
		AcquiredAbilities = new List<SpecialAbility>();
		HP = 100;

		birthplace = "ウサミン星";
		birthmonth = 4;
		birthday = 4;
		
	}

	public static bool ValidateInput(string newname, int personality)
	{
		//TODO: 名前のバリデーション(禁止ワード、文字数)

		return true;
	}
	public static void AddParam(int[] delta)
	{
		for (int i = 0; i < Const.NUM_STATUS_PARAMETER; i++)
		{
			param[i] += delta[i];
			if (param[i] >= Const.MAX_PARAMETER_VALUE)
			{
				param[i] = Const.MAX_PARAMETER_VALUE;
			}
		}
		
	}

	public static int[] GetParam()
	{
		return param;
	}

	public static void SetParam(int[] p)
	{
		if (param == null) param = new int[] { 0, 0, 0, 0, 0, 0 };
		Array.Copy(p, param, p.Length);
	}

	public static int TryIncreaseHP(int delta)
	{
		int actualDelta = 0;
		if (HP + delta <= 0)
		{
			actualDelta = 1 - HP;
			HP = 1;
		}
		else if (HP + delta > 100)
		{
			actualDelta = 100 - HP;
			HP = 100;
		}
		else
		{
			HP += delta;
			actualDelta = delta;
		}
		return actualDelta;
	}
	public static int TryDecreaseHP(int delta)
	{
		return TryIncreaseHP(-delta);
	}
	public static int GetHP()
	{
		return HP;
	}

	public static void SetHP(int hp)
	{
		HP = hp;
	}

	public static int CastBuff(SpecialAbility.Trigger trigger, SpecialAbility.Target target, int rawValue)
	{
		int multiplier = 0;
		foreach (var ability in AcquiredAbilities)
		{
			if (ability.EffectTrigger == trigger && ability.EffectTarget == target)
			{
				multiplier += ability.EffectValue;
			}
		}
		return rawValue + Mathf.FloorToInt(rawValue * multiplier / 100f);
	}

	public static List<SpecialAbility> GetNewAbilities(SpecialAbility.Trigger trigger)
	{
		List<SpecialAbility> list = new List<SpecialAbility>();

		foreach (var ability in DataTable.SpecialAbilities)
		{
			if (ability.AcquireTrigger != trigger) continue;

			if ((int)ability.AcquireConditionTarget < 7 && param[(int)ability.AcquireConditionTarget - 1] >= ability.AcquireConditionValue
				|| (int)ability.AcquireConditionTarget == 8 && true)
			{
				if (AcquiredAbilities.IndexOf(ability) == -1)
					list.Add(ability);
			}
			
		}

		AcquiredAbilities.AddRange(list);
		return list;
	}

	public static List<SpecialAbility> GetNewAbilities(int id)
	{
		List<SpecialAbility> list = new List<SpecialAbility>();

		var sp = DataTable.SpecialAbilities[id];
		list.Add(sp);
		AcquiredAbilities.AddRange(list);
		return list;
	}

	public static void SetAcquiredAbilities(List<int> list)
	{
		AcquiredAbilities = new List<SpecialAbility>();
		foreach (var a in list)
		{
			AcquiredAbilities.Add(DataTable.SpecialAbilities[a]);
		}
	}
}
