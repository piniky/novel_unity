﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Linq;

public class Personality
{
	public int Id;
	public string Name { get; private set; }
	string desc;
	Skill skill;
	public List<int> Neighbors;

	public Personality(int id, string name, string desc, int skill, List<int> list)
	{
		this.Id = id;
		this.Name = name;
		this.desc = desc;
		this.skill = new Skill();
		Neighbors = new List<int>(list);
	}


}

public static class Personalities
{
	static List<Personality> list;
	static List<bool> isOpened;
	static int[] parameters;
	public static Personality currentPersonality { get; private set; }

	public static void init()
	{
		parameters = new int[6];

		//Load Personalities.txt
		list = new List<Personality>();
		isOpened = new List<bool>();
		TextAsset csvFile = Resources.Load("Personalities") as TextAsset;
		StringReader reader = new StringReader(csvFile.text); reader.ReadLine();//1行読み飛ばす

		while (reader.Peek() > -1)
		{
			string line = reader.ReadLine();
			string[] splitted = line.Split(',');
			List<int> l = splitted.Skip(3).Take(splitted.Length - 3).Select(s => int.Parse(s)).ToList();
			list.Add(new Personality(list.Count, splitted[0], splitted[1], int.Parse(splitted[2]), l));
			isOpened.Add(false);
		}
		currentPersonality = list[0];

		Neighbors.Init();
	}

	public static Personality get(string name)
	{
		foreach (Personality p in list)
		{
			if (p.Name == name)
			{
				return p;
			}
		}
		return null;
	}

	public static Personality get(int index)
	{
		return list[index];
	}

	public static void AddParam(int[] p)
	{
		if (parameters == null) parameters = new int[6];
		for (int i = 0; i < 5; i++)
		{
			parameters[i] += p[i];
		}
	}
	
	public static void AddParam(int index, int value)
	{
		if (parameters == null) parameters = new int[6];
		parameters[index] += value;
	}

	public static int[] GetParam()
	{
		if (parameters == null) parameters = new int[6];
		return parameters;
	}

	public static Personality getEvolutionCandidate(){
		int current = currentPersonality.Id;
		foreach (var neighbor in Neighbors.List)
		{
			if (neighbor.src == current)
			{
				bool canEvolute = true;
				for (int i = 0; i < 6; i++)
				{
					if (neighbor.param[i] > parameters[i])
					{
						canEvolute = false;
						break;
					}
				}
				if (canEvolute) return list[neighbor.dest];
			}
		}
		return null;
	}

	public static void Evolute(){
		currentPersonality = getEvolutionCandidate();
		
		// リセット
		parameters = new int[6];
	}

}

public static class Neighbors
{
	public static List<Neighbor> List;

	public static void Init()
	{
		List = new List<Neighbor>();
		TextAsset csvFile = Resources.Load<TextAsset>("Neighbors");
		StringReader reader = new StringReader(csvFile.text); reader.ReadLine();//1行読み飛ばす

		while (reader.Peek() > -1)
		{
			string line = reader.ReadLine();
			List.Add(new Neighbor(line));
		}
	}
}
public class Neighbor
{
	public int src;
	public int dest;
	public int[] param;
	public Neighbor(string line)
	{
		string[] splitted = line.Split(',');
		src = int.Parse(splitted[0]);
		dest = int.Parse(splitted[1]);
		param = new int[]{ int.Parse(splitted[2]), int.Parse(splitted[3]), int.Parse(splitted[4]), int.Parse(splitted[5]) , int.Parse(splitted[6]) , int.Parse(splitted[7]) };
	}
}