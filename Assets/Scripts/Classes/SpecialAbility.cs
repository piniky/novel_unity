﻿using System.Collections;
using System.Collections.Generic;

public struct SpecialAbility {
	public int ID;
	public string Name;
	public string Description;
	public Trigger EffectTrigger;
	public Target EffectTarget;
	public int EffectValue;
	public Trigger AcquireTrigger;
	public Target AcquireConditionTarget;
	public int AcquireConditionValue;

	public enum Trigger
	{
		All,
		Lesson,
		Work,
		Play
	}
	public enum Target
	{
		All,
		Da,
		Vo,
		St,
		Va,
		Me,
		In,
		Hp
	}
}