﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class DataTable {

	public static EventScript[] EventScripts;
	public static VacationScripts VacationScripts;
	public static Hitokotos Hitokotos;
	public static List<Item> ItemTable;
	public static List<SpecialAbility> SpecialAbilities;
	public static List<EvolutionQuestion> EvolutionQuestions;
	public static List<string> RandomFirstnames;
	public static List<string> RandomLastnames;

	public static void Init()
	{
		//EventScriptInit();
		ItemTableInit();
		SpecialAbilitiesInit();
		Hitokotos = new Hitokotos();
		VacationScripts = new VacationScripts();
		EvolutionQuestionInit();
		RandomNameInit();
	}


	static void EventScriptInit()
	{
		using (StreamReader sr = new StreamReader("Assets/Json/Events.json"))
		{
			string json = sr.ReadToEnd();
			EventScripts es = JsonUtility.FromJson<EventScripts>(json);
			EventScripts = es.scripts;

		}
	}
	
	static void ItemTableInit()
	{
		ItemTable = new List<Item>();
		TextAsset csvFile = Resources.Load("ItemTable") as TextAsset;
		StringReader reader = new StringReader(csvFile.text); reader.ReadLine();//1行読み飛ばす

		while (reader.Peek() > -1)
		{
			string line = reader.ReadLine();
			string[] splitted = line.Split(',');
			Item item = new Item();
			item.Name = splitted[0];
			item.Description = splitted[1];
			item.EffectTarget = splitted[2];
			item.EffectValue = int.Parse(splitted[3]);
			ItemTable.Add(item);
		}
	}
	
	static void SpecialAbilitiesInit()
	{
		SpecialAbilities = new List<SpecialAbility>();
		TextAsset csvFile = Resources.Load("SpecialAbilities") as TextAsset;
		StringReader reader = new StringReader(csvFile.text); reader.ReadLine();//1行読み飛ばす

		while (reader.Peek() > -1)
		{
			string line = reader.ReadLine();
			string[] splitted = line.Split(',');
			SpecialAbility item = new SpecialAbility();
			item.ID = SpecialAbilities.Count;
			item.Name = splitted[0];
			item.Description = splitted[1];
			item.EffectTrigger = (SpecialAbility.Trigger)int.Parse(splitted[2]);
			item.EffectTarget = (SpecialAbility.Target)int.Parse(splitted[3]);
			item.EffectValue = int.Parse(splitted[4]);
			item.AcquireTrigger = (SpecialAbility.Trigger)int.Parse(splitted[5]);
			item.AcquireConditionTarget = (SpecialAbility.Target)int.Parse(splitted[6]);
			item.AcquireConditionValue = int.Parse(splitted[7]);
			SpecialAbilities.Add(item);
		}
	}

	static void EvolutionQuestionInit()
	{
		EvolutionQuestions = new List<EvolutionQuestion>();
		TextAsset csvFile = Resources.Load("EvolutionQuestions") as TextAsset;
		StringReader reader = new StringReader(csvFile.text);

		while (reader.Peek() > -1)
		{
			reader.ReadLine(); // 先頭の1行は読み飛ばす

			// トレーナーの質問
			EvolutionQuestion question = new EvolutionQuestion();
			question.QuestionLines = new List<string>();
			string line = RemoveColon(reader.ReadLine());
			while (line != "")
			{
				question.QuestionLines.Add(line);
				line = RemoveColon(reader.ReadLine());
			}

			// 現状維持の質問
			line = RemoveColon(reader.ReadLine());
			question.StayChoices = new List<string>(line.Split(','));

			// 変化する質問
			question.MoveChoices = new List<List<string>>();
			for (int i = 0; i < 6; i++)
			{
				line = RemoveColon(reader.ReadLine());
				question.MoveChoices.Add(new List<string>(line.Split(',')));
			}

			EvolutionQuestions.Add(question);
		}
	}

	static string RemoveColon(string line)
	{
		string line2 = line;
		for (int i = line.Length - 1; i >= 0; i--)
		{
			if (line2[i] == ',') line2 = line2.Remove(i, 1);
			else break;
		}

		return line2;
	}

	static void RandomNameInit()
	{
		TextAsset csvFile = Resources.Load("RandomNames") as TextAsset;
		StringReader reader = new StringReader(csvFile.text);

		RandomLastnames = new List<string>(reader.ReadLine().Split(','));
		RandomFirstnames = new List<string>(reader.ReadLine().Split(','));
		
	}
}
