public static class Const
{
	public const bool TRIAL_MODE = true;
	public const bool DEBUG_MODE = true;

	public static readonly float[] CONFIG_TEXTSPEED = { 0.4f, 0.25f, 0.15f, 0.1f, 0.07f, 0.05f, 0.04f, 0.03f, 0.02f, 0.01f, 0.001f };

	public const int CONFIG_DEFAULT_BGMVOLUME = 60;
	public const int CONFIG_DEFAULT_SEVOLUME = 60;
	public const int CONFIG_DEFAULT_TEXTSPEED = 6;

	public const int NUM_STATUS_PARAMETER = 6;
	public const int MAX_PARAMETER_VALUE = 255;

	public const int MONEY_DEFAULT = 25000;
	public const int MONEY_LESSON_NORMAL = 4000;
	public const int MONEY_LESSON_CHEAP = 0;
	public const int MONEY_LESSON_EXPENSIVE = 10000;
	public const int MONEY_WORK = 12000;
	public const int MONEY_PLAY = 5000;

	public const int HP_LESSON = 20;
	public const int HP_WORK = 25;
	public const int HP_RECOVERY = 70;
	public const int HP_NATURAL_RECOVERY = 7;

	public static readonly string[] ABILITY_NAME = { "ダンス", "ボーカル", "スタミナ", "バラエティ", "メンタル", "インテリジェンス" };
	public static readonly string[] LESSON_NAME_NORMAL = {"ダンスレッスン", "ボイトレ", "ランニング", "配信", "ヨガ", "座学" };
	public static readonly string[] LESSON_NAME_CHEAP = { "振り付け確認", "ボイトレ", "ジョギング", "座談会", "瞑想", "読書" };
	public static readonly string[] LESSON_NAME_EXPENSIVE = { "コーチ特訓：踊", "コーチ特訓：歌", "タイムトライアル", "グループ配信", "メンタルトレーニング", "検定試験" };
	public static readonly int[] LESSON_MAIN_GROWTH = { 0, 1, 2, 3, 4, 5 };
	public static readonly int[] LESSON_SUB_GROWTH = { 2, 5, 1, 4, 0, 3 };
	public const int LESSON_NORMAL_MAIN_MAX = 18;
	public const int LESSON_NORMAL_MAIN_MIN = 12;
	public const int LESSON_NORMAL_SUB_MAX = 9;
	public const int LESSON_NORMAL_SUB_MIN = 6;
	public const int LESSON_EXPENSIVE_MAIN_MAX = 36;
	public const int LESSON_EXPENSIVE_MAIN_MIN = 28;
	public const int LESSON_EXPENSIVE_SUB_MAX = 18;
	public const int LESSON_EXPENSIVE_SUB_MIN = 12;
	public const int LESSON_CHEAP_MAIN_MAX = 7;
	public const int LESSON_CHEAP_MAIN_MIN = 3;
	public const int LESSON_CHEAP_SUB_MAX = 3;
	public const int LESSON_CHEAP_SUB_MIN = 1;

	public static readonly string[] CHARACTER_NAMES = { "shizuku", "arisa", "kaede", "ayaka2", "miko", "yuki" };
}