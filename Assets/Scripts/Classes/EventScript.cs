﻿using System;
using System.IO;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public class EventScripts
{
	public EventScript[] scripts;
}

[Serializable]
public class EventScript {
	public string[] text;
}

public class Hitokotos
{
	Dictionary<HitokotoSituation, List<Hitokoto>> hitokotos;
	public Hitokotos()
	{
		hitokotos = new Dictionary<HitokotoSituation, List<Hitokoto>>();

		OpenFile(HitokotoSituation.Home);
		OpenFile(HitokotoSituation.Lesson);
		OpenFile(HitokotoSituation.Work);
		OpenFile(HitokotoSituation.Play);
	}
	public string GetHitokoto (HitokotoSituation situation)
	{
		List<Hitokoto> list = new List<Hitokoto>();
		foreach (var hitokoto in hitokotos[situation])
		{
			if (hitokoto.personalityID == Character.Personality.Id)
			{
				// 現在の性格で絞る
				list.Add(hitokoto);
			}
		}

		int maxSize = list.Count;
		int rnd = GameData.random.Next(maxSize);
		return list[rnd].text;

	}
	void OpenFile(HitokotoSituation situation)
	{
		hitokotos[situation] = new List<Hitokoto>();
		var csvFile = Resources.Load<TextAsset>("Hitokoto_" + situation.ToString());
		var reader = new StringReader(csvFile.text); reader.ReadLine();//1行読み飛ばす
		while (reader.Peek() > -1)
		{
			string line = reader.ReadLine();
			string[] splitted = line.Split(',');
			Hitokoto h = new Hitokoto();
			h.personalityID = int.Parse(splitted[1]);
			h.text = splitted[2];
			hitokotos[situation].Add(h);
		}
	}
}


public class Hitokoto
{
	public int personalityID;
	public string text;
}

public enum HitokotoSituation
{
	Home,
	Lesson,
	Work,
	Play
}