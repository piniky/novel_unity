using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class FsmGraduates : MonoBehaviour {
	List<Dictionary<string, object>> Graduates;
	public GameObject Cursor;
	public GameObject Page2;
	float CursorRotation = 0f;

	int CurrentPage;
	int TotalPage;
	int CurrentTab;
	int CurrentIndex;
	// Use this for initialization
	public void FsmInit() {
		Graduates = new List<Dictionary<string, object>>();
		string[] paths = Directory.GetFiles(Directory.GetCurrentDirectory() + "/archive", "*", SearchOption.TopDirectoryOnly);
		foreach (string path in paths)
		{
			string filename = Path.GetFileNameWithoutExtension(path);
			Dictionary<string, object> dict = SaveData.GetArchive(filename);
			if (null != dict)
			{
				dict["fullname"] = (string)dict["lastname"] + "　" + (string)dict["firstname"];
				Graduates.Add(dict);
			}
		}
		TotalPage = (Graduates.Count - 1) / 10 + 1;
		GameObject.Find("Canvas/Pagination/Total").GetComponent<Text>().text = TotalPage.ToString();

		CurrentTab = 0;
		CurrentPage = 1;
		RefreshList(CurrentPage);

	}
	
	// Update is called once per frame
	void Update () {
		CursorRotation += 1.5f;
		Cursor.transform.rotation = Quaternion.Euler(CursorRotation, 0, 0);
	}

	public void OnGraduateClick(int index)
	{
		if ((CurrentPage - 1) * 10 + index >= Graduates.Count) return;

		CurrentIndex = index;

		var dict = Graduates[(CurrentPage - 1) * 10 + index];
		GameObject.Find("Canvas/Detail/Name").GetComponent<Text>().text = (string)dict["fullname"];
		GameObject.Find("Canvas/Detail/Birthday").GetComponent<Text>().text = string.Format("{0}月{1}日", ((int)dict["birthmonth"]).ToString(), ((int)dict["birthday"]).ToString());
		GameObject.Find("Canvas/Detail/Birthplace").GetComponent<Text>().text = (string)dict["birthplace"];
		GameObject.Find("Canvas/Detail/Personality").GetComponent<Text>().text = Personalities.get((int)dict["personality"]).name;

		// ステータス
		int i = 0;
		var children = GameObject.Find("Canvas/Detail/Status").transform;
		var status = (List<int>)Graduates[(CurrentPage - 1) * 10 + index]["param"];
		foreach (Transform child in children.transform)
		{
			child.GetComponent<Text>().text = status[i].ToString();
			i++;
		}

		// 特殊能力
		OnTabClick(0);

		// カーソル移動
		Cursor.GetComponent<RectTransform>().anchorMax = new Vector2(0.1f, 0.73f - 0.053f * index);
		Cursor.GetComponent<RectTransform>().anchorMin = new Vector2(0.1f, 0.73f - 0.053f * index);
	}

	public void OnNextPageClick()
	{
		if (CurrentPage == TotalPage) return;
		CurrentPage++;
		GameObject.Find("Canvas/Pagination/Current").GetComponent<Text>().text = CurrentPage.ToString();
		RefreshList(CurrentPage);
	}

	public void OnPrevPageClick()
	{
		if (CurrentPage < 2) return;
		CurrentPage--;
		GameObject.Find("Canvas/Pagination/Current").GetComponent<Text>().text = CurrentPage.ToString();
		RefreshList(CurrentPage);
	}

	public void OnTabClick(int index)
	{
		CurrentTab = index;

		int i = index * 12;
		var children = GameObject.Find("Canvas/Detail/SpecialAbilities").transform;
		var abilities = (List<int>)Graduates[(CurrentPage - 1) * 10 + CurrentIndex]["specialabilities"];
		foreach (Transform child in children.transform)
		{
			if (i >= abilities.Count)
			{
				child.GetComponent<Text>().text = "";
			}
			else
			{
				child.GetComponent<Text>().text = DataTable.SpecialAbilities[abilities[i]].Name;
			}
			i++;
		}

		if (index == 1)
		{
			Page2.SetActive(true);
			GameObject.Find("Canvas/Detail/SpecialAbilities").GetComponent<RectTransform>().anchoredPosition = new Vector2(2, 0);
		}
		else
		{
			Page2.SetActive(false);
			GameObject.Find("Canvas/Detail/SpecialAbilities").GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
		}
	}

	void RefreshList(int page)
	{
		// 一覧の名前
		int i = (page - 1) * 10;
		var children = GameObject.Find("Canvas/Graduates").transform;
		foreach (Transform child in children.transform)
		{
			if (i >= Graduates.Count)
			{
				child.GetComponent<Text>().text = "";
			}
			else
			{
				child.GetComponent<Text>().text = (string)Graduates[i]["fullname"];
			}
			i++;
		}

		// 一覧の卒業日
		i = (page - 1) * 10;
		children = GameObject.Find("Canvas/GraduationDates").transform;
		foreach (Transform child in children.transform)
		{
			if (i >= Graduates.Count)
			{
				child.GetComponent<Text>().text = "";
			}
			else
			{
				child.GetComponent<Text>().text = (string)Graduates[i]["savedate"];
			}
			i++;
		}
		CurrentIndex = 0;
		OnGraduateClick(CurrentIndex);
	}
}
