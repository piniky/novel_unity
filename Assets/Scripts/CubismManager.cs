﻿using System;
using System.IO;
using UnityEngine;
using Live2D.Cubism.Rendering;
using Live2D.Cubism.Framework.Json;
using Live2D.Cubism.Framework.Raycasting;

public static class CubismManager {
	
	static Animator animator;
	static GameObject gameObject;

	public static void Init(string name)
	{
		// 変更前と変更後のキャラクターが同じ場合、何もしない
		if (gameObject != null && Const.CHARACTER_NAMES[Character.ModelType] == name) return;

		// 既にシーン上にmodelがあった場合、一旦消す
		if (GameObject.Find("Cubism") != null) UnityEngine.Object.DestroyImmediate(GameObject.Find("Cubism"), true);

		gameObject = Resources.Load<GameObject>(string.Format("Cubism3/{0}/{0}", name));
		gameObject = UnityEngine.Object.Instantiate(gameObject);
		gameObject.transform.localScale = new Vector3(1.7f, 1.7f, 1.7f);
		gameObject.transform.position = new Vector3(-1f, -0.6f, 0);
		gameObject.name = "Cubism";

		// Animator Controllerを動的にattachする
		RuntimeAnimatorController rac = Resources.Load<RuntimeAnimatorController>("Cubism3/Motion/Animator");
		animator = gameObject.GetComponent<Animator>();
		animator.runtimeAnimatorController = rac;

		// クリック判定のコンポーネント
		gameObject.AddComponent<CubismCharacterTouch>();
		gameObject.AddComponent<CubismRaycaster>();
		var drawables = gameObject.transform.Find("Drawables");
		foreach (Transform d in drawables.transform)
		{
			d.gameObject.AddComponent<CubismRaycastable>();			
		}
	}

	public static void SetPosition(Position pos)
	{
		float x = 0f;
		switch (pos)
		{
			case Position.Center:
				x = 0f;
				break;
			case Position.Left:
				x = -1f;
				break;
			case Position.Right:
				x = 1f;
				break;
		}
		gameObject.transform.position = new Vector3(x, -0.6f, 0);

	}

	public enum Position
	{
		Left,
		Center,
		Right
	}

	public static void PlayMotion(int index)
	{
		animator.SetInteger("MotionNum", index);
		animator.SetTrigger("Trigger");
	}

}
