﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class AbilityLabel : MonoBehaviour, IPointerEnterHandler {
	string description = "説明です";

	public void OnPointerEnter(PointerEventData eventData)
	{
		GameObject.Find("Canvas/StatusExpand/AbilityDesc").GetComponent<Text>().text = description;
	}

	public void SetDescription(SpecialAbility sp)
	{
		description = sp.Description;
	}
	
}
