using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Arbor;

public class FsmDaily : MonoBehaviour
{
	Fade fade;

	int workCount = 0;
	int lessonCount = 0;

	void Start()
	{
		fade = GameObject.Find("FadeCanvas").GetComponent<Fade>();


	 	if (Screen.width * 9 == Screen.height * 16) Screen.SetResolution(800, 600, true);
		AudioManager.Awake();
		BgManager.Awake();
		ConfigManager.LoadConfigFromFile();
		AudioManager.ChangeBGM(0);
	}

	void FsmDoLesson(int lessonIndex)
	{
		int lessonGrade = lessonIndex / 6;
		int lessonType = lessonIndex % 6;

		Character.TryDecreaseHP(Const.HP_LESSON);
		int mainVal = 0;
		int subVal = 0;
		switch (lessonGrade)
		{
			case 0:
				GameData.Money -= Const.MONEY_LESSON_NORMAL;
				mainVal = GameData.random.Next(Const.LESSON_NORMAL_MAIN_MIN, Const.LESSON_NORMAL_MAIN_MAX + 1);
				subVal = GameData.random.Next(Const.LESSON_NORMAL_SUB_MIN, Const.LESSON_NORMAL_SUB_MAX + 1);

				break;
			case 1:
				GameData.Money -= Const.MONEY_LESSON_CHEAP;
				mainVal = GameData.random.Next(Const.LESSON_CHEAP_MAIN_MIN, Const.LESSON_CHEAP_MAIN_MAX + 1);
				subVal = GameData.random.Next(Const.LESSON_CHEAP_SUB_MIN, Const.LESSON_CHEAP_SUB_MAX + 1);

				break;
			case 2:
				GameData.Money -= Const.MONEY_LESSON_EXPENSIVE;
				mainVal = GameData.random.Next(Const.LESSON_EXPENSIVE_MAIN_MIN, Const.LESSON_EXPENSIVE_MAIN_MAX + 1);
				subVal = GameData.random.Next(Const.LESSON_EXPENSIVE_SUB_MIN, Const.LESSON_EXPENSIVE_SUB_MAX + 1);

				break;
		}

		// バフ
		mainVal = Character.CastBuff(SpecialAbility.Trigger.Lesson, (SpecialAbility.Target)(Const.LESSON_MAIN_GROWTH[lessonType] + 1), mainVal);
		subVal = Character.CastBuff(SpecialAbility.Trigger.Lesson, (SpecialAbility.Target)(Const.LESSON_SUB_GROWTH[lessonType] + 1), subVal);
		
		// パラメータを加算
		int[] value = { 0, 0, 0, 0, 0, 0 };
		value[Const.LESSON_MAIN_GROWTH[lessonType]] = mainVal;
		value[Const.LESSON_SUB_GROWTH[lessonType]] = subVal;
		Character.AddParam(value);

		string s;
		s = "";

		s += Const.ABILITY_NAME[Const.LESSON_MAIN_GROWTH[lessonType]] + "が";
		s += mainVal.ToString() + "上がった\n";
		s += Const.ABILITY_NAME[Const.LESSON_SUB_GROWTH[lessonType]] + "が";
		s += subVal.ToString() + "上がった\n";

		// 特殊能力を獲得
		List<SpecialAbility> gotAbilities = Character.GetNewAbilities(SpecialAbility.Trigger.Lesson);
		foreach (var ablity in gotAbilities)
		{
			s += "特殊能力「" + ablity.Name + "」を習得した\n";
		}

		workCount = 0;
		lessonCount++;
		if (lessonCount == 4)
		{
			Character.GetNewAbilities(3);
			s += "特殊能力「ストイック」を習得した\n";
		}

		string hitokoto = DataTable.Hitokotos.GetHitokoto(HitokotoSituation.Lesson);
		string[] lines = new string[3];
		lines[0] = "[firstname]//" + hitokoto;
		lines[1] = s;
		lines[2] = "[endLessonResult]";

		GameObject.Find("Canvas/MessagePanel/Text").GetComponent<TextController>().setLines(lines);

		GameData.State = "lesson";
		GameData.Save();
		FsmRefreshStatus();

		CubismManager.SetPosition(CubismManager.Position.Center);
	}

	void FsmWork()
	{
		string resultMessage = "";
		GameData.State = "work";

		// 体力
		int decreasedHP = Character.TryDecreaseHP(Const.HP_WORK);
		resultMessage += "体力が" + -decreasedHP + "減った\n";

		// お金
		GameData.Money += Const.MONEY_WORK;
		resultMessage += "所持金が" + Const.MONEY_WORK + "円増えた\n";

		// 特殊能力を獲得
		List<SpecialAbility> gotAbilities = Character.GetNewAbilities(SpecialAbility.Trigger.Work);
		foreach (var ability in gotAbilities)
		{
			resultMessage += "特殊能力「" + ability.Name + "」を習得した\n";
		}

		workCount++;
		lessonCount = 0;
		if (workCount == 3)
		{
			Character.GetNewAbilities(2);
			resultMessage += "特殊能力「バイトリーダー」を習得した\n";
		}

		string hitokoto = DataTable.Hitokotos.GetHitokoto(HitokotoSituation.Work);		
		string[] lines = {"[firstname]//" + hitokoto , resultMessage, "[endLessonResult]" };
		GameObject.Find("Canvas/MessagePanel/Text").GetComponent<TextController>().setLines(lines);

		GameData.Save();
		FsmRefreshStatus();
		BgManager.ChangeBackground("karaoke");
		BgManager.ChangePlace("カラオケ");
		CubismManager.SetPosition(CubismManager.Position.Center);
	}

	void FsmPlay()
	{
		GameData.State = "play";

		//// 体力
		int increasedHP = Character.TryIncreaseHP(Const.HP_RECOVERY);
		//if (increasedHP > 0) resultMessage += "体力が" + increasedHP + "増えた\n";

		int personality = 1;
		int index = GameData.random.Next(DataTable.VacationScripts.Scripts[personality].Count);
		//string[] lines = DataTable.vacationScripts.Scripts[personality][index].text;
		string[] lines = { "たまには息抜きも必要だよね～。", "体力が50回復した。", "[endLessonResult]" };
		GameObject.Find("Canvas/MessagePanel/Text").GetComponent<TextController>().setLines(lines);

		workCount = 0;
		lessonCount = 0;

		GameData.Save();
		FsmRefreshStatus();
		CubismManager.SetPosition(CubismManager.Position.Center);
	}

	void FsmLessonDetailOpen(int lessonType)
	{
		
		GameObject button = GameObject.Find("Canvas/LessonList/LessonDetailPanel/Button0/Text");
		button.GetComponent<Text>().text = Const.LESSON_NAME_CHEAP[lessonType];

		GameObject.Find("Canvas/LessonList/LessonDetailPanel/Button1").GetComponent<Button>().interactable =
			GameData.Money >= Const.MONEY_LESSON_NORMAL;
		GameObject.Find("Canvas/LessonList/LessonDetailPanel/Button1/Text").GetComponent<Text>().text = Const.LESSON_NAME_NORMAL[lessonType];

		GameObject.Find("Canvas/LessonList/LessonDetailPanel/Button2").GetComponent<Button>().interactable =
			GameData.Money >= Const.MONEY_LESSON_EXPENSIVE;
		GameObject.Find("Canvas/LessonList/LessonDetailPanel/Button2/Text").GetComponent<Text>().text = Const.LESSON_NAME_EXPENSIVE[lessonType];

		// 位置調整
		int x = 0; int y = 0;
		switch (lessonType)
		{
			case 0:
			case 2:
			case 4:
				x = -85;
				break;
			case 1:
			case 3:
			case 5:
				x = 66;
				break;
		}
		switch (lessonType)
		{
			case 0:
			case 1:
				y = 91;
				break;
			case 2:
			case 3:
				y = 47;
				break;
			case 4:
			case 5:
				y = 0;
				break;
		}
		GameObject.Find("Canvas/LessonList/LessonDetailPanel").transform.localPosition = new Vector3(x, y, 0f);
	}

	void FsmDailyInit()
	{
		
		// タイトルを経由せず直接このシーンから始めた場合
		if (GameData.dateInGame == null)
		{
			Debug.Log("ロードしなかったので最初から始めます");
			Personalities.init();
			DataTable.Init();

			GameData.Init();
			Character.Init("前川", "みく");

		}
		GameData.Load();
		if (GameData.State != "default") Debug.Log(GameData.State);
		GameData.ownItems.Add(new OwnItem { ID = 4,  Count = 2 });
		GameData.ownItems.Add(new OwnItem { ID = 5, Count = 2 });
		GameData.ownItems.Add(new OwnItem { ID = 10, Count = 2 });

		// Live2D生成
		CubismManager.Init(Const.CHARACTER_NAMES[Character.ModelType]);
		CubismManager.SetPosition(CubismManager.Position.Left);
	}
	void FsmNewDayStart()
	{
		GameData.State = "default";
		GameData.Save();

		FsmRefreshStatus();
		BgManager.ChangeBackground("office");
		BgManager.ChangePlace("養成所");
		CubismManager.SetPosition(CubismManager.Position.Left);
		string hitokoto = DataTable.Hitokotos.GetHitokoto(HitokotoSituation.Home);
		GameObject.Find("Canvas/MessagePanel/Text").GetComponent<TextController>().setText("[firstname]//" + hitokoto);
	}

	void ItemHandler(int ownItemID)
	{
		OwnItem ownItem = GameData.ownItems[ownItemID];

		Item item = DataTable.ItemTable[ownItem.ID];
		ownItem.Count -= 1;
		
		if (ownItem.Count == 0)
		{
			GameData.ownItems.RemoveAt(ownItemID);
		}
		else
		{
			GameData.ownItems[ownItemID] = ownItem;
		}
		FsmRefreshItemList();

		string message;
		if (item.Name == "ウマ券")
		{
			message = "ウマ券を使った。気になるレースの結果は…………………";
			if (GameData.random.Next(100) < 30)
			{
				message += "大当たり！所持金が20000円増えた。";
				GameData.Money += 20000;
			}
			else
			{
				message += "ハズレだった。次こそ当たるさ。";
			}
		}
		else if (item.Name == "宝くじ")
		{
			message = "宝くじを使った。気になる当選結果は…………………";
			if (GameData.random.Next(100) < 10)
			{
				message += "1等大当たり！所持金が100000円増えた。";
				GameData.Money += 100000;
			}
			else if (GameData.random.Next(90) < 30)
			{
				message += "2等当たり！所持金が20000円増えた。";
				GameData.Money += 20000;
			}
			else if (GameData.random.Next(60) < 30)
			{
				message += "3等だった！所持金が10000円増えた。";
				GameData.Money += 10000;
			}
			else
			{
				message += "ハズレだった。次こそ当たるさ。";
			}
		}
		else
		{
			// 効果
			message = item.Name + "を使った。";
			switch (item.EffectTarget)
			{
				case "hp":
					int delta = Character.TryIncreaseHP(item.EffectValue);
					message += "体力が" + delta.ToString() + "回復した！";
					break;
				case "vo":
					message += "ボーカルが" + item.EffectValue.ToString() + "増えた！";
					Character.AddParam(new int[] { 0, item.EffectValue, 0, 0, 0, 0 });
					break;
				case "da":
					message += "ダンスが" + item.EffectValue.ToString() + "増えた！";
					Character.AddParam(new int[] { item.EffectValue, 0, 0, 0, 0, 0 });
					break;
				case "va":
					message += "バラエティが" + item.EffectValue.ToString() + "増えた！";
					Character.AddParam(new int[] { 0, 0, 0, item.EffectValue, 0, 0 });
					break;
				case "st":
					message += "スタミナが" + item.EffectValue.ToString() + "増えた！";
					Character.AddParam(new int[] { 0, 0, item.EffectValue, 0, 0, 0 });
					break;
				case "me":
					message += "メンタルが" + item.EffectValue.ToString() + "増えた！";
					Character.AddParam(new int[] { 0, 1, 0, 0, item.EffectValue, 0 });
					break;
				case "in":
					message += "インテリジェンスが" + item.EffectValue.ToString() + "増えた！";
					Character.AddParam(new int[] { 0, 1, 0, 0, 0, item.EffectValue });
					break;
				default:
					Debug.LogAssertion("アイテムの効果対象が間違っています");
					break;
			}

		}

		GameObject.Find("Canvas/MessagePanel/Text").GetComponent<TextController>().setText(message);
		GameObject.Find("Canvas/MessagePanel/Text").GetComponent<TextController>().StartStreaming();
		FsmRefreshStatus();
		GameData.Save();
	}

	void FsmRefreshItemList()
	{
		if (GameData.ownItems.Count == 0)
			GameObject.Find("Canvas/ItemList/EmptyText").GetComponent<Text>().text = "アイテムがありません";
		else
			GameObject.Find("Canvas/ItemList/EmptyText").GetComponent<Text>().text = "";

		//アイテムリスト内のアイテムボタンをすべて削除
		foreach (Transform child in GameObject.Find("Canvas/ItemList/ScrollView/Viewport/Content").transform)
		{
			Destroy(child.gameObject);
		}
		// アイテムをリストアップ
		int ownItemCount = 0;
		foreach (var ownItem in GameData.ownItems)
		{
			Item item = DataTable.ItemTable[ownItem.ID];
			GameObject itemPrefab = (GameObject)Resources.Load("Prefab/ItemButton");
			GameObject button = Instantiate(itemPrefab);
			button.transform.SetParent(GameObject.Find("Canvas/ItemList/ScrollView/Viewport/Content").transform, false);
			button.GetComponentInChildren<Text>().text = item.Name + "  × " + ownItem.Count;
			int count = ownItemCount;
			button.GetComponent<Button>().onClick.AddListener(() => ItemHandler(count));
			button.GetComponent<MouseEnterOnItem>().SetDescription("//" + item.Description);
		}


	}

	void FsmRefreshSpAbilityList()
	{
		// 名前
		GameObject.Find("Canvas/StatusExpand/Name").GetComponent<Text>().text = Character.Fullname;
		GameObject.Find("Canvas/StatusExpand/Birthday").GetComponent<Text>().text = String.Format("{0}月{1}日", Character.birthmonth, Character.birthday);
		GameObject.Find("Canvas/StatusExpand/Birthplace").GetComponent<Text>().text = Character.birthplace;
		GameObject.Find("Canvas/StatusExpand/Personality").GetComponent<Text>().text = Character.Personality.Name;

		GameObject parent = GameObject.Find("Canvas/StatusExpand/SpecialAbility");
		foreach (Transform n in parent.transform)
		{
			GameObject.Destroy(n.gameObject);
		}

		// 特殊能力
		int y = 0;
		GameObject label;
		foreach (var ability in Character.AcquiredAbilities)
		{
			label = Instantiate((GameObject)Resources.Load("Prefab/AbilityLabel"));
			label.transform.SetParent(GameObject.Find("Canvas/StatusExpand/SpecialAbility").transform, false);
			label.GetComponentInChildren<Text>().text = ability.Name;
			label.transform.localPosition = new Vector3(label.transform.localPosition.x, label.transform.localPosition.y - y, 0f);
			y += 35;
			label.GetComponent<AbilityLabel>().SetDescription(ability);
		}

		// パラメーター
		parent = GameObject.Find("Canvas/StatusExpand/AbilityBars");
		var param = Character.GetParam();
		int i = 0;
		foreach (Transform bar in parent.transform)
		{
			bar.GetComponent<RectTransform>().sizeDelta = new Vector2(param[i] * 0.7f, 25f);
			i++;
		}
		parent = GameObject.Find("Canvas/StatusExpand/AbilityNums");
		i = 0;
		foreach (Transform num in parent.transform)
		{
			num.GetComponent<Text>().text = param[i].ToString();
			if (param[i] < 30)
			{
				num.GetComponent<RectTransform>().localPosition = new Vector3(param[i] * 0.7f + 2, -28 - 40f * i, 0);
			}
			else
			{
				num.GetComponent<RectTransform>().localPosition = new Vector3(5f, -28 - 40f * i, 0);
			}
			i++;
		}
	}

	void FsmRefreshStatus()
	{

		GameObject.Find("Canvas/Above/HPBar").GetComponent<Slider>().value = Character.GetHP();
		GameObject.Find("Canvas/Above/Date").GetComponent<Text>().text = String.Format("{0}     {1}", GameData.dateInGame.getCurrentMonth(), GameData.dateInGame.getCurrentWeek());
		GameObject.Find("Canvas/Above/LeftWeeks").GetComponent<Text>().text = GameData.dateInGame.getLeftWeek().ToString();
		GameObject.Find("Canvas/Above/PersonalityHexagon/PersonalityName").GetComponent<Text>().text = Character.Personality.Name;
		for (int i = 0; i < Const.NUM_STATUS_PARAMETER; i++)
		{
			int[] param = Character.GetParam();
			GameObject.Find("Canvas/Above/AbilityBackground/Status" + i.ToString()).GetComponent<Text>().text = param[i].ToString();
		}

		bool can_lesson = true;
		bool can_work = true;
		// 体力による行動制限
		int currentHP = Character.GetHP();
		can_lesson &= (currentHP >= Const.HP_LESSON);
		can_work &= (currentHP >= Const.HP_WORK);
		if (GameObject.Find("Canvas/RightMenu") != null)
		{
			GameObject.Find("Canvas/RightMenu/LessonButton").GetComponent<Button>().interactable = can_lesson;
			GameObject.Find("Canvas/RightMenu/WorkButton").GetComponent<Button>().interactable = can_work;
		}

	}
	public void FsmLessonDescUpdate(int lessonNo)
	{
		string desc = "";
		if (lessonNo == 5)
			desc += "インテリ 大UP  ";
		else
			desc += Const.ABILITY_NAME[Const.LESSON_MAIN_GROWTH[lessonNo]] + " 大UP  ";

		if (lessonNo == 1)
			desc += "インテリ 小UP";
		else
			desc += Const.ABILITY_NAME[Const.LESSON_SUB_GROWTH[lessonNo]] + " 小UP";

		GameObject.Find("Canvas/LessonList/DescText").GetComponent<Text>().text = desc;
	}
	void FsmDoEvolution(bool isEvolute)
	{
		string s;
		if (isEvolute)
		{
			Personality newPersonality = Personalities.getEvolutionCandidate();
			s = "おめでとう！" + Personalities.currentPersonality.Name + "から" + newPersonality.Name + "にチェンジしました。";
			Personalities.Evolute();
		}
		else
		{
			s = "チェンジしませんでした。";
		}
		GameObject.Find("Canvas/MessagePanel/Text").GetComponent<TextController>().setText(s);
	}

	void FsmNaturalRecovery()
	{
		Character.TryIncreaseHP(Const.HP_NATURAL_RECOVERY);
	}

	void FsmNextWeek()
	{
		GameData.dateInGame.nextWeek();
	}

	public void FsmCloseConfig()
	{
		GameObject.Find("Canvas/ConfigPanel/BGMSlider").GetComponent<Slider>().onValueChanged.RemoveListener(AudioManager.ChangeBGMVolume);
		GameObject.Find("Canvas/ConfigPanel/SESlider").GetComponent<Slider>().onValueChanged.RemoveListener(AudioManager.ChangeSEVolume);

		int bgmVolume = (int)GameObject.Find("Canvas/ConfigPanel/BGMSlider").GetComponent<Slider>().value;
		int seVolume = (int)GameObject.Find("Canvas/ConfigPanel/SESlider").GetComponent<Slider>().value;
		int textSpeed = (int)GameObject.Find("Canvas/ConfigPanel/TextSpeedSlider").GetComponent<Slider>().value;

		ConfigManager.SaveConfigToFile(bgmVolume, seVolume, textSpeed);
	}
	public void FsmOpenConfig()
	{
		GameObject.Find("Canvas/ConfigPanel/BGMSlider").GetComponent<Slider>().onValueChanged.AddListener(AudioManager.ChangeBGMVolume);
		GameObject.Find("Canvas/ConfigPanel/SESlider").GetComponent<Slider>().onValueChanged.AddListener(AudioManager.ChangeSEVolume);

		ConfigValues cv = ConfigManager.CurrentConfig;
		GameObject.Find("Canvas/ConfigPanel/BGMSlider").GetComponent<Slider>().value = cv.BGMVolume;
		GameObject.Find("Canvas/ConfigPanel/SESlider").GetComponent<Slider>().value = cv.SEVolume;
		GameObject.Find("Canvas/ConfigPanel/TextSpeedSlider").GetComponent<Slider>().value = cv.TextSpeed;
		
	}

	void FsmSetEvolutionQuestion()
	{
		int count = DataTable.EvolutionQuestions.Count;
		int index = GameData.random.Next(count);
		EvolutionQuestion eq = DataTable.EvolutionQuestions[index];
		GameObject.Find("Canvas/MessagePanel/Text").GetComponent<TextController>().setLines(eq.ToLines());
	}
}