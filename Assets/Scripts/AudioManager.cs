﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AudioManager {
	public static List<AudioClip> audioClipBGM = new List<AudioClip>();
	public static List<AudioClip> audioClipSE = new List<AudioClip>();
	static AudioSource[] audioSources;

	public static void Awake()
	{
		audioSources = GameObject.Find("Audio").GetComponents<AudioSource>();

		// すべてのファイルを読み込む
		audioClipSE.AddRange(Resources.LoadAll<AudioClip>("Audio/SE"));
		audioClipBGM.AddRange(Resources.LoadAll<AudioClip>("Audio/BGM"));

		// 音量をコンフィグからとってくる

	}

	public static void ChangeBGM(int clipIndex)
	{
		audioSources[0].Stop();
		audioSources[0].clip = audioClipBGM[clipIndex];
		audioSources[0].Play();
	}
	public static void PlaySE(int clipIndex)
	{
		audioSources[1].Stop();
		audioSources[1].clip = audioClipSE[clipIndex];
		audioSources[1].Play();
	}

	public static void ChangeBGMVolume(float volume)
	{
		audioSources[0].volume = volume * 0.7f / 100.0f;
	}

	public static void ChangeSEVolume(float volume)
	{
		audioSources[1].volume = volume * 0.7f / 100.0f;
	}
}
