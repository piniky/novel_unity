﻿using System;
using System.Text;
using System.IO;
using UnityEngine;

public static class ConfigManager {
	static public ConfigValues CurrentConfig = new ConfigValues { BGMVolume = 60, SEVolume = 60, TextSpeed = 6};

	static public void LoadConfigFromFile()
	{
		if (!System.IO.File.Exists("config.ini"))
		{
			// config.iniがなければ新規作成
			SaveConfigToFile(Const.CONFIG_DEFAULT_BGMVOLUME, Const.CONFIG_DEFAULT_SEVOLUME, Const.CONFIG_DEFAULT_TEXTSPEED);
		}
		using (StreamReader reader = new StreamReader("config.ini"))
		{
			string JsonString = reader.ReadToEnd();
			CurrentConfig = JsonUtility.FromJson<ConfigValues>(JsonString);

			// バリデーション
			CurrentConfig.BGMVolume = Math.Min(100, Math.Max(0, CurrentConfig.BGMVolume));
			CurrentConfig.SEVolume = Math.Min(100, Math.Max(0, CurrentConfig.SEVolume));
			CurrentConfig.TextSpeed = Math.Min(10, Math.Max(0, CurrentConfig.TextSpeed));

			// 読み込んだ値を反映
			AudioManager.ChangeBGMVolume(CurrentConfig.BGMVolume);
			AudioManager.ChangeSEVolume(CurrentConfig.SEVolume);
		}
	}
	
	static public void SaveConfigToFile(int bgmVolume, int seVolume, int textSpeed)
	{
		using (StreamWriter writer = new StreamWriter("config.ini", false, Encoding.GetEncoding("utf-8")))
		{
			CurrentConfig = new ConfigValues { BGMVolume = bgmVolume, SEVolume = seVolume, TextSpeed = textSpeed };
			string dictJsonString = JsonUtility.ToJson(CurrentConfig);
			//var encryptedString = Crypt.Encrypt(dictJsonString);
			var encryptedString = dictJsonString;
			writer.WriteLine(encryptedString);
		}
	}
}

[Serializable]
public class ConfigValues
{
	public int BGMVolume;
	public int SEVolume;
	public int TextSpeed;
}