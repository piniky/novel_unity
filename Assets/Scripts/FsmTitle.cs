﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FsmTitle : MonoBehaviour {

	// Use this for initialization
	void Start () {
		AudioManager.Awake();
		BgManager.Awake();
		ConfigManager.LoadConfigFromFile();
	}
	

	void FsmFetchDataTable()
	{
		Personalities.init();
		DataTable.Init();
		GameData.Init();
	}

	void FsmLoad()
	{
		//GameData.Load();
	}
	
	void FsmEnableContinueButton()
	{
		var button = GameObject.Find("Canvas/Button_LoadGame").GetComponent<Button>();
		if (System.IO.File.Exists("save1") && !SaveData.ContainsKey("archived") && SaveData.ContainsKey("state"))
		{
			button.interactable = true;
		}
		else
		{
			button.interactable = false;
		}
	}

	void FsmOpenBrowser()
	{
		Application.OpenURL("http://forsweetwater.org");
	}
}
