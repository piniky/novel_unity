﻿using UnityEngine;
using Live2D.Cubism.Framework.Raycasting;
using UnityEngine.SceneManagement;
using Arbor;

public class CubismCharacterTouch : MonoBehaviour {

	ArborFSM fsm;

	private void Start()
	{
		fsm = ArborFSM.FindFSM("Daily");
	}
	private void Update()
	{
		if (SceneManager.GetActiveScene().name != "default" || fsm.currentState.name != "Root")
		{
			return;
		}

		// Return early in case of no user interaction.
		if (!Input.GetMouseButtonDown(0))
		{
			return;
		}

		var raycaster = GetComponent<CubismRaycaster>();
		// Get up to 4 results of collision detection.
		var results = new CubismRaycastHit[4];

		// Cast ray from pointer position.
		var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		var hitCount = raycaster.Raycast(ray, results);

		if (hitCount > 0)
		{
			CubismManager.PlayMotion(3);
		}
	}
}
