﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FsmEnding : MonoBehaviour {

	void SetEndingText()
	{
		string[] lines = new string[] {
			"トレーナー//「そろそろここのシステムには慣れてきましたか？一応説明しておきますね。」",
			"トレーナー//「自らがやりたい、ここを伸ばしたいと思える項目を選んで、練習をしてください。」",
			"トレーナー//「練習は３種類から選べますので、ご自分の財布と相談しながらレッスンをやっていきましょう。」",
			"トレーナー//「お金が足りなくなればバイトで稼ぎましょう。自主練習もできますが普通より効率は落ちます。」",
			"トレーナー//「また、息抜きも大切です。そういう時は遊ぶを選択しましょう。」",
			"トレーナー//「お金は消費することとなりますが、新しい発見があるかもしれません。」",
			"トレーナー//「アイテムは色々な効果があり、それぞれ、体力の回復や、持ち金の増加などが期待できます。」",
			"トレーナー//「特に体力が０になった場合、レッスンができなくなるので気をつけてください。」",
			"トレーナー//「進め方は以上になります。もし分からないことがあれば、公式サイトのヘルプを参照してください。」",
			"トレーナー//「５月からは、研修生として定期的にステージに 立ってもらいます。ここからが本番です。」",
			"トレーナー//「理想のアイドルになれるよう、１年間頑張っていきましょう。」",
			"[endEnding]" };

		GameObject.Find("Canvas/MessagePanel/Text").GetComponent<TextController>().setLines(lines);
	}

	void ArchiveCurrentData()
	{
		SaveData.SetInt("archived", 1);
		SaveData.Save();

		System.DateTime dt = System.DateTime.Now;
		var filename = dt.ToString("yyyyMMddHHmmss");
		System.IO.File.Move("save1", "archive/" + filename);
		System.IO.File.Delete("save1");
	}
}
