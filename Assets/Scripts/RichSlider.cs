﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RichSlider : MonoBehaviour
{
	public float targetValue { get; set; }
	// Use this for initialization
	void Start()
	{
		targetValue = gameObject.GetComponent<Slider>().value;
	}

	// Update is called once per frame
	void Update()
	{
		Slider slider = gameObject.GetComponent<Slider>();
		float value = slider.value;
		if (targetValue > value)
		{
			slider.value += Mathf.Min(targetValue - value, 0.3f);
			if (slider.value == 100f && targetValue >= 100f)
			{
				targetValue %= 100;
				slider.value = 0f;
			}
		}
		else if (targetValue < value)
		{
			slider.value += Mathf.Max(targetValue - value, -0.3f);

		}
	}

}
