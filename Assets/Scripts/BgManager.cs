﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class BgManager {
	public static GameObject BgPanel;
	public static GameObject PlacePanel;
	public static List<Sprite> Sprites;
	
	public static void Awake()
	{
		BgPanel = GameObject.Find("CanvasBack/Image");
		PlacePanel = GameObject.Find("Canvas/PlaceLabel/Text");
		// すべてのファイルを読み込む
		Sprites = new List<Sprite>(Resources.LoadAll<Sprite>("Background"));
	}

	public static void ChangeBackground(string name)
	{
		foreach (var sprite in Sprites)
		{
			if (sprite.name == name)
			{
				BgPanel.GetComponent<Image>().sprite = sprite;
				return;
			}
		}
	}

	public static void ChangePlace(string place)
	{
		PlacePanel.GetComponent<Text>().text = place;
	}
}
