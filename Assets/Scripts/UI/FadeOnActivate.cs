﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class FadeOnActivate : MonoBehaviour {
	int frame = 4;

	public void Open() {
		GetComponent<CanvasGroup>().alpha = 0f;
		gameObject.SetActive(true);

		Observable.IntervalFrame(1)
			.Take(frame)
			.Subscribe(l =>
			{
				GetComponent<CanvasGroup>().alpha = (l + 1f) / frame;
			});
	}
	
	public void Close()
	{
		GetComponent<CanvasGroup>().alpha = 1f;

		Observable.IntervalFrame(1)
			.Take(frame)
			.Subscribe(l =>
			{
				GetComponent<CanvasGroup>().alpha = (frame - l - 1f) / frame;
			},
			() => gameObject.SetActive(false));
	}
}
