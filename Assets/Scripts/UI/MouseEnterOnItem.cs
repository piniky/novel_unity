﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseEnterOnItem : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public GameObject TextController;
	string Description;

	void Start()
	{
		TextController = GameObject.Find("Canvas/MessagePanel/Text");
		Debug.Assert(TextController != null);
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		if (Description != null)
		{
			if (Input.GetAxis("Mouse X") != 0f || Input.GetAxis("Mouse Y") != 0f)
			{
				//TextController.GetComponent<TextController>().setLines();
				TextController.GetComponent<TextController>().setText(Description);
				TextController.GetComponent<TextController>().StartStreaming();
			}
		}
	}
	public void OnPointerExit(PointerEventData eventData)
	{
		TextController.GetComponent<TextController>().setText("// ");
		TextController.GetComponent<TextController>().StartStreaming();
	}
	public void SetDescription(string desc)
	{
		Description = desc;
	}
}
