﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MouseOverSE : MonoBehaviour, IPointerEnterHandler {
	public void OnPointerEnter(PointerEventData eventData)
	{
		if (gameObject.GetComponent<Button>() != null && gameObject.GetComponent<Button>().interactable)
		{
			AudioManager.PlaySE(2);
		}
	}
}
