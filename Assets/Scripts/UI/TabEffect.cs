﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TabEffect : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	Vector3 initialPos;
	void Start()
	{
		initialPos = gameObject.GetComponent<Transform>().transform.position;

	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		iTween.ScaleTo(this.gameObject, iTween.Hash(
			"x", 1.05f,
			"y", 1.05f,
			"time", 0.3f,
			"easetype",
			iTween.EaseType.easeOutExpo
		));
	}
	public void OnPointerExit(PointerEventData eventData)
	{
		iTween.ScaleTo(this.gameObject, iTween.Hash(
			"x", 1f,
			"y", 1f,
			"time", 0.3f,
			"easetype",
			iTween.EaseType.easeOutExpo
		));
	}

}
