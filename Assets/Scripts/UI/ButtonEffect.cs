﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonEffect : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

	public void OnPointerEnter(PointerEventData eventData)
	{
		iTween.ScaleTo(this.gameObject, iTween.Hash(
			"x", 1.1f, "y", 1.1f, "time", 0.3f,
			"easetype",
			iTween.EaseType.easeOutExpo
			));
	}
	public void OnPointerExit(PointerEventData eventData)
	{
		iTween.ScaleTo(this.gameObject, iTween.Hash(
			"x", 1.0f, "y", 1.0f, "time", 0.3f,
			"easetype",
			iTween.EaseType.easeOutExpo
			));
	}
}
