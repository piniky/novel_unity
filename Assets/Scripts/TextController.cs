﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextController : MonoBehaviour
{
	Text textComponent;
	public Text nameComponent;
	string text;
	float textSpeed = 0.05f;
	float deltaTime;
	string[] dispString;
	string[] dispName;
	int dispLineIndex;
	bool IsStreaming;
	int skipCount = 0;

	public GameObject ArborObject;
	public GameObject ChoicesPanel;
	public GameObject[] Choices;

	// Use this for initialization
	void OnEnable()
	{
		textComponent = gameObject.GetComponent<Text>();
		text = "";
		deltaTime = Time.time;
		ChangeTextSpeed(ConfigManager.CurrentConfig.TextSpeed);
	}

	// Update is called once per frame
	void Update()
	{
		if (dispString == null) return;
		if (dispString.Length <= dispLineIndex) return;
		if (!IsStreaming) return;

		if (Input.GetButtonDown("Fire1"))
		{
			onMouseDown();
		}
		else if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
		{
			skipCount = (skipCount + 1) % 10;
			if (skipCount == 1) onMouseDown();
		}
		if (nameComponent != null) nameComponent.text = dispName[dispLineIndex];
		deltaTime += Time.deltaTime;
		if (deltaTime > textSpeed)
		{
			if (textComponent.text.Length < dispString[dispLineIndex].Length)
			{
				int diff = Mathf.Min(Mathf.FloorToInt(deltaTime / textSpeed), dispString[dispLineIndex].Length - textComponent.text.Length);
				deltaTime = deltaTime % textSpeed;
				textComponent.text += dispString[dispLineIndex].Substring(textComponent.text.Length, diff);
				if (textComponent.text.Length == dispString[dispLineIndex].Length)
				{
					text = "";
				}
			}
		}
	}

	public void setText(string s)
	{
		string[] lines = new string[2];
		lines[0] = s;
		lines[1] = "[end]";
		setLines(lines);
		textComponent.text = "";
		nameComponent.text = "";
		deltaTime = 0f;
		IsStreaming = false;
	}

	public void setLines(string[] lines2)
	{
		string[] lines = new string[lines2.Length];
		System.Array.Copy(lines2, lines, lines2.Length);
		dispName = new string[lines.Length];

		//タグ置換
		for (int i = 0; i < lines.Length; i++)
		{
			string line = lines[i];
			line = line.Replace("[name]", Character.Fullname);
			line = line.Replace("[firstname]", Character.Firstname);
			line = line.Replace("[lastname]", Character.Lastname);
			// 名前部分分離
			if (line.IndexOf("//") != -1)
			{
				dispName[i] = line.Substring(0, line.IndexOf("//"));
				line = line.Substring(line.IndexOf("//") + 2);
			}
			else
			{
				dispName[i] = "";
			}
			lines[i] = line;

			if (line[0] == '[')
			{
				int colonPos = line.IndexOf(':');
				if (colonPos != -1)
				{
					//変数を含むタグ
					string tagname = line.Substring(1, colonPos - 1);
					string[] param_string = line.Substring(colonPos + 1, line.Length - colonPos - 2).Split('/');
					int[] param_int = new int[6];
					switch (tagname)
					{
						case "ability":
							string result = "";
							for (int j = 0; j < Const.NUM_STATUS_PARAMETER; j++)
							{
								Debug.Log(param_string[j]);
								param_int[j] = int.Parse(param_string[j]);
								if (param_int[j] != 0)
								{
									result += Const.ABILITY_NAME[j] + "が" + param_int[j].ToString() + (param_int[j] > 0 ? "上がった！\n" : "下がった\n");
								}
							}
							Character.AddParam(param_int);
							lines[i] = result;
							break;
						default:
							break;
					}
				}
			}
		}

		dispString = lines;
		textComponent.text = "";
		nameComponent.text = "";
		dispLineIndex = 0;
		deltaTime = 0f;
		IsStreaming = false;
	}
	
	public void setLines(List<string> lines)
	{
		setLines(lines.ToArray());
	}


	public void onMouseDown()
	{

		if (textComponent.text.Length == dispString[dispLineIndex].Length)
		{
			if (dispLineIndex == dispString.Length - 1) return;
			if (ChoicesPanel != null && ChoicesPanel.activeSelf)
			{
				return;
			}

			Arbor.ArborFSM a = ArborObject.GetComponent<Arbor.ArborFSM>();

			var nextline = dispString[dispLineIndex + 1];
			if (nextline.StartsWith("[end]")) return;

			//次の行がコマンドの場合
			if (nextline[0] == '[')
			{
				while (nextline[0] == '[')
				{
					int colonPos = nextline.IndexOf(':');
					// 変数を含むタグ
					if (colonPos != -1)
					{
						string tagname = nextline.Substring(1, colonPos - 1);
						string[] parameters = nextline.Substring(colonPos + 1, nextline.Length - colonPos - 2).Split('/');

						if (tagname == "choices")
						{
							ChoicesPanel.SetActive(true);
							SetChoices(parameters);
							break;
						}

						switch (tagname)
						{
							case "motion":
								CubismManager.PlayMotion(int.Parse(parameters[0]));
								break;
							case "label":
								break;
							case "jump":
								JumpByLabelName(parameters[0]);
								break;
							case "pparam": // 性格の隠れ値
								int[] param_int = new int[6];
								if (parameters.Length == 6)
								{
									for (int j = 0; j < Const.NUM_STATUS_PARAMETER; j++)
									{
										param_int[j] = int.Parse(parameters[j]);
									}
									Personalities.AddParam(param_int);
								}
								else
								{
									Personalities.AddParam(int.Parse(parameters[0]), int.Parse(parameters[1]));
								}
								break;
							case "place":
								BgManager.ChangePlace(parameters[0]);
								break;
							case "bg":
								BgManager.ChangeBackground(parameters[0]);
								break;
							default:
								break;
						}
						if (tagname != "jump") NextLine();
						nextline = dispString[dispLineIndex];
					}
					else
					{
						switch (nextline)
						{
							case "[endLessonResult]":
								a.SendTrigger("endLessonResult");
								break;
							case "[endEvent]":
								a.SendTrigger("endEvent");
								break;
							case "[end]":
								break;
							case "[endIntroduction]":
								a.SendTrigger("endIntroduction");
								break;
							case "[endEvolEvent]":
								a.SendTrigger("endEvolEvent");
								break;
							case "[endEnding]":
								a.SendTrigger("endEnding");
								break;
							default:
								break;
						}
						dispString[dispLineIndex + 1] = "[end]";

						break;
					}
				}
			}
			else
			{
				//次の行がコマンドでない場合
				NextLine();

			}


		}
		else
		{
			textComponent.text = dispString[dispLineIndex];
		}
	}

	void SetChoices(string[] parameters)
	{
		if (parameters.Length % 2 != 0) Debug.Log("[choices]の引数が正しくありません");

		for (int i = 0; i < parameters.Length / 2; i++)
		{
			Choices[i].SetActive(true);
			Choices[i].transform.Find("Text").GetComponent<Text>().text = parameters[i * 2];

			var label = parameters[i * 2 + 1];
			Choices[i].GetComponent<Button>().onClick.RemoveAllListeners();
			Choices[i].GetComponent<Button>().onClick.AddListener(() =>
			{
				ChoicesPanel.SetActive(false);
				JumpByLabelName(label);

			});
		}

		// 使わないボタンを非アクティブ化
		for (int i = parameters.Length / 2; i < 4; i++)
 	   {
			Choices[i].SetActive(false);
		}

	}
	// 指定されたラベルが存在する次の行にジャンプする
	void JumpByLabelName(string labelName)
	{
		for (int i = dispLineIndex; i < dispString.Length; i++)
		{
			if (dispString[i].Length > 7 && dispString[i].Substring(0, 7) == "[label:" && dispString[i].Substring(7) == labelName + "]")
			{
				// ジャンプする
				dispLineIndex = i + 1;
				textComponent.text = "";
				deltaTime = 0f;
			}
		}

	}

	public void ChangeTextSpeed(float speed)
	{
		int index = Mathf.FloorToInt(speed);
		textSpeed = Const.CONFIG_TEXTSPEED[index];
	}
	public void StartStreaming()
	{
		IsStreaming = true;
	}

	void NextLine(int skipCount = 1)
	{
		textComponent.text = "";
		dispLineIndex++;
		deltaTime = 0f;
	}
}
