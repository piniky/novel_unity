﻿using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class sliderMove : MonoBehaviour, IPointerEnterHandler
{

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void OnPointerEnter(PointerEventData eventData)
	{
		iTween.ScaleTo(this.gameObject, iTween.Hash(
			"x", 2f, "y", 2f, "time", 1f,
			"easetype",
			iTween.EaseType.easeOutExpo
			));
		iTween.ValueTo(this.gameObject, iTween.Hash(
			"from", 0f, "to", 1f, "time", 1f,
			"easetype",	iTween.EaseType.easeOutExpo,
			"onupdate",	"setValue"
			));
	}
	public void onClick()
    {
		iTween.ValueTo(this.gameObject, iTween.Hash(
			"from", 0f, "to", 1f, "time", 1f,
			"easetype",
			iTween.EaseType.easeOutExpo,
			"onupdate", 
			"setValue"
			));
    }
	void setValue(float v)
	{
		GameObject.Find("Canvas/Slider").GetComponent<UnityEngine.UI.Slider>().value = v;
	}
}
