﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;
using System.IO;
using UniRx;

public static class GameData
{
	public static DateInGame dateInGame;
	//static int _money = Const.MONEY_DEFAULT;
	static ReactiveProperty<int> _money = new ReactiveProperty<int>(Const.MONEY_DEFAULT);
	public static string State;
	public static List<OwnItem> ownItems;
	public static System.Random random;


	public static void Init()
	{
		dateInGame = new DateInGame(4, 1);
		State = "default";
		Money = Const.MONEY_DEFAULT;
		random = new System.Random();

		_money.SubscribeToText(GameObject.Find("Canvas/Above/Money").GetComponent<Text>());

		ownItems = new List<OwnItem>();
	}

	public static int Money
	{
		get
		{
			return _money.Value;
		}
		set
		{
			_money.Value = value;
		}
	}
	
	public static void Save()
	{
		SaveData.SetInt("money", Money);
		SaveData.SetInt("month", dateInGame.getCurrentMonth());
		SaveData.SetInt("week", dateInGame.getCurrentWeek());
		//SaveData.SetClass<DateInGame>("dateingame", dateInGame);
		DateTime dt = DateTime.Now;
		SaveData.SetString("savedate", dt.ToString());
		SaveData.SetString("state", State);
		SaveData.SetList<int>("param", new List<int>(Character.GetParam()));
		SaveData.SetList<int>("pparam", new List<int>(Personalities.GetParam()));
		SaveData.SetInt("personality", Character.Personality.Id);
		SaveData.SetInt("charactertype", Character.ModelType);

		List<int> spList = new List<int>();
		foreach (var sp in Character.AcquiredAbilities)
		{
			spList.Add(sp.ID);
		}
		SaveData.SetList("specialabilities", spList);
		SaveData.SetInt("hp", Character.GetHP());
		SaveData.Save();
		Debug.Log("セーブしました");
	}
	public static void Load()
	{
		Debug.Log("ロードしました");
		Money = SaveData.GetInt("money");
		int month = SaveData.GetInt("month", 4);
		int week = SaveData.GetInt("week", 1);
		dateInGame = new DateInGame(month, week);
		//dateInGame = SaveData.GetClass<DateInGame>("dateingame", null);
		string firstname = SaveData.GetString("firstname");
		string lastname = SaveData.GetString("lastname");
		int[] param = SaveData.GetList<int>("param", new List<int> { 0, 0, 0, 0, 0, 0 }).ToArray();
		Character.SetParam(param);
		Personalities.AddParam(SaveData.GetList<int>("pparam", new List<int> { 0, 0, 0, 0, 0, 0 }).ToArray());
		Character.Personality = Personalities.get(SaveData.GetInt("personality", 0));
		Character.SetAcquiredAbilities(SaveData.GetList("specialabilities", new List<int> { }));
		Character.SetHP(SaveData.GetInt("hp", 100));
		Character.ModelType = SaveData.GetInt("charactertype", 0);

		Character.birthplace = SaveData.GetString("birthplace", "");
		Character.birthmonth = SaveData.GetInt("birthmonth", 1);
		Character.birthday= SaveData.GetInt("birthday", 1);

		GameData.State = SaveData.GetString("state");
	}
}
